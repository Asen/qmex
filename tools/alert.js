term=true;   // multiple opening of window
kr=0;       // for timer
imap_pp=0;  // for other timer


function pos(object,ll,tt)             // function of positioning of "fall out window" objects
{
    //object.style.position="absolute";
	object.style.left=ll+"px";
	object.style.top=tt+"px";

	}

function hide_wnd(window_w, upd)             // hide of "fall out window" objects
{
	if(!window_w) window_w=document.getElementById("wnd_0021");
	if(!upd) upd = false;
	if(imap_pp!=0) window.clearInterval(imap_pp);
	cc=0;
	imap_pp=window.setInterval(function(){
		if(cc>=15) 
		{
		term=true;
		document.body.removeChild(window_w)
		window.clearInterval(imap_pp);
		}	
		cc++;	
		},40);
		$("#wnd_0021").fadeOut("slow");
		if(upd==true) document.location.reload();
	}
	
function show_wnd(text,wnd_height,show_button,hide_time,bcolor,upd)   // show of "fall out window" objects
{
	if(!text) text='Уведомление';
	if(!wnd_height) wnd_height=100;
	if(show_button===undefined) show_button=true;
	if(!hide_time) hide_time=7;
	if(!bcolor || bcolor==0) bcolor="#069";
	if(!upd) upd = false;
	if(term==true)
	{
    if(kr!=0)
	window.clearTimeout(kr);
	term=false;
	op_code=document.getElementById("wnd_0021");
	if(op_code) document.body.removeChild(op_code);
	caption = document.createElement("div");
	caption.style.padding="5px";
	caption.style.fontSize="12px";
	caption.style.color="white";
	caption.style.backgroundColor="#069";
	caption.innerHTML="Уведомление";
	back = document.createElement("div");
	back.style.position="fixed";
	back.style.background="rgba(55,55,55,0.5)";
	back.style.borderRadius="5px";
	back.style.padding="10px";
	back.style.left=String(window.innerWidth/2-200)+"px";
	back.style.top=String((window.innerHeight/2)-(wnd_height/2))+"px";
	back.id="wnd_0021";
	
	div=document.createElement("div");
	paragraph=document.createElement("p");
	paragraph.innerHTML=text;
	paragraph.style.color="#000";
	paragraph.style.padding="15px";
	paragraph.style.fontFamily="Arial, Helvetica, sans-serif";
	paragraph.style.fontSize="0.8em";
	//paragraph.style.fontWeight="bold";
	//div.style.position="fixed";
	div.style.textAlign="left";
	div.align="center";
	div.style.width="400px";
	div.style.height="0px";
	div.style.left=String(window.innerWidth/2-200)+"px";
	div.style.top=String((window.innerHeight/2)-(wnd_height/2))+"px";	
	div.style.opacity=0.0;
	div.style.backgroundColor="white";
	div.style.border="solid "+bcolor+" 5px";
	div.style.borderRadius="5px";
	document.body.appendChild(back);
	back.appendChild(div);
	div.appendChild(caption);
	div.appendChild(paragraph);
	if(show_button==true)
	{
	btn=document.createElement("div");
	btn.innerHTML="OK";
	btn.className="qButton";
	btn.style.width="100px";
	btn.style.textAlign="center";
	btn.onclick=function(){ hide_wnd();}
	//pos(btn,280,14);
	div.appendChild(btn);
	}
	//div.style.position="fixed";        //For Mozilla, NetScape, Opera (not IE !)
	imap=window.setInterval(function(){
		if(div.clientHeight>=wnd_height) 
		{
		term=true;
		window.clearInterval(imap);
		}
		div.style.height=String(div.clientHeight+17)+"px";
		pos(btn,300,parseInt(div.clientHeight)-35);
		pos(paragraph,10,parseInt(div.clientTop)+caption.clientHeight)},40);
		$(div).fadeTo('slow',0.9);
	kr=window.setTimeout(function(){op_code=document.getElementById("wnd_0021");if(op_code) hide_wnd(op_code,upd);},hide_time*1000);
	}
}