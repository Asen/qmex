QUERY = {};

QUERY.splitGetQuery = 
function(queryStr)
{
	var query = {};
	if(queryStr.indexOf("?")<0) return query;
	var query_bits = (queryStr.split("?")[1]+"&").split("&");
	for( var key in query_bits )
	if(query_bits[key].trim()!='') { var item = query_bits[key].split("="); query[ item[0] ] = item[1]; }
	
	return query;
	}
	
QUERY.completeQueryFromHash = 
function(hash, is_remove_empty)
{	
	var query = ""
	for(var key in hash)
	if((is_remove_empty && (hash[key]+" ").trim().length!=0) || !is_remove_empty) 
	//query+=encodeURIComponent(key)+"="+encodeURIComponent(hash[key]+"&"
	query+=key+"="+hash[key]+"&"
	
	return query;
	}
	

function getHashQuery() {
	var uri = window.location.toString();
	uri = (uri.indexOf("#") == -1) ? '' : uri.substring(uri.indexOf('#') + 1);

	return uri;
}

function getQuery(keys,vals)
{
	query = "";
	for(i=0;i<keys.length;i++)
	{
		query+=encodeURIComponent(keys[i].toString());
		query+="=";
		query+=encodeURIComponent(vals[i].toString());
		query+="&";
		}
		return query;
	}
	

function getQueryString(bits)
{
	var qstring = "";
	for(key in bits)
	qstring+=encodeURIComponent(key)+"="+encodeURIComponent(bits[key])+"&";
	
	return qstring;
	}
	

function SendDynamicSync(_data)
{
	$.ajax({		
		url: _data['url'],
		type: _data['type'],
		dataType: _data['dataType'],
		data: getQueryString(_data['data']),
		async:true,
		success: _data['func']
		});
	}
	
function SendDynamicMsg(params,values,res_ret,url_send,type_req,dataType_t)
{
	
	result = "";
	
	if(!url_send || url_send==null) url_send = 'handlers/ask.php';
	if(!type_req || type_req==null) type_req = 'POST';
	if(!dataType_t || dataType_t==null) dataType_t='html';
	
	request = getQuery(params,values);
	
	$.ajax({
		
		url:url_send,
		type:type_req,
		dataType:dataType_t,
		data:request,
		async:false,
		global:false,
		success:function(message)
		{
			result = message;
			if(res_ret && res_ret!=null)
			   {
			   $("#"+res_ret.toString()).empty();
			   $("#"+res_ret.toString()).append(message);
			   }
		}
		
		});
		
		return result;
		
	}


function getPostfix(digit)
{
	new_w = '';
	msg_s = '';
	if(digit==1) new_w = 'ое';else new_w = 'ых';
	switch(digit%10)
	{
		case 1: msg_s='ие';break;
		case 2:case 3:case 4: msg_s='ия';break;
		case 5:case 6:case 7:
		case 8:case 9:case 10: msg_s='ий';break;
		
		}
		if(digit>10 && digit<14) msg_s="ий";
		
		return [new_w,msg_s];
	}
	
	
	
COMMON = {};
COMMON.CreateFastScroll = 
function()
{
	var SCROLL_DIRECTION = false;
	$(document.body).append('<div id="scroll-to-begin">Вниз</div>');
	$("#scroll-to-begin").css({'position':'fixed', 
	                           'top':'0px', 
							   'left':($("#qcontent").position().left+$("#qcontent").width()+2)+'px',
							   'padding-top':'25%',
							   'color':'white',
							   'background-color':'#000',
							   'font-size':'small',
							   'font-family':'Arial, Helvetica, sans-serif',
							   'opacity':0.2, 
							   'cursor':'pointer',
							   'text-align':'center',
							   'width':'10%'
							   });
	
	//$("#scroll-to-begin").fadeOut(0);	
	$("#scroll-to-begin").height(screen.availHeight)
	$("#scroll-to-begin").mouseover(function(){
		$(this).stop();
		$(this).css({"text-decoration":"underline"});
		$(this).animate({'opacity':0.6},500);
		});
	$("#scroll-to-begin").mouseout(function(){
		$(this).stop();
		$(this).css({"text-decoration":"none"});
		$(this).animate({'opacity':0.2},500);
		});
	$("#scroll-to-begin").click(function(){
		
		var THIS = this;
		$("*").stop();
		$(THIS).hide(0);
		
		function scrollDown(){
			var scroll_max = (document.body.scrollHeight - document.body.clientHeight);
			var scroll_one = scroll_max / 100;
			var scroll_curr = scroll_one;
			var IID = window.setInterval(function(){
				window.scrollTo(0,scroll_curr); 
				if(scroll_curr>=scroll_max){window.clearInterval(IID); $(THIS).show(0); return;}
				scroll_curr+=scroll_one;
				},1);
		}
		
		function scrollUp()
		{
			var scroll = window.pageYOffset;
			var scroll_one = scroll/100;
			var scroll_curr = scroll-scroll_one;
			var IID = window.setInterval(function(){
				window.scrollTo(0,scroll_curr); 
				if(scroll_curr<=0){window.clearInterval(IID); $(THIS).show(0); return;}
				scroll_curr-=scroll_one;
				},1);
			}
			
		if(SCROLL_DIRECTION) scrollUp(); else scrollDown();
			
		});
	$(window).scroll(function(){
		if(window.pageYOffset>=10) { $("#scroll-to-begin").text("Наверх"); SCROLL_DIRECTION = true; } else
		                            { $("#scroll-to-begin").text("Вниз");  SCROLL_DIRECTION = false; }
		});
	}
	
COMMON.IFrameTest = 
function(selector)
{
	function adjustFrame(e)
	{
		$(e).addClass('full-width');
		if(e.contentWindow && e.contentWindow.document && e.contentWindow.document.body)
		{
			if(e.contentWindow.document.body.innerHTML.trim()=='')
				$(e.contentWindow.document.body).html($(e).attr("srcdoc"));
			$(e).height( $(e.contentWindow.document).height() /*+50*/ );
			}
		}
	
	$(selector).load(function(){ adjustFrame(this) });		
	$(selector).each(function(i,e){ adjustFrame(e) });
}

COMMON.AddPagedNav = 
function(position){
	
	var pageNum = -1;
	var pagePred = -1;
	var pageNext = -1;
	var query = [];
	var href = "?";
	var url = location.href;
	var queries = QUERY.splitGetQuery(url);
	pageNum = ('page' in queries) ? queries['page'] : 1;
	pagePred = (pageNum-1 > 0) ? parseInt(pageNum)-1 : 1;
	pageNext = parseInt(pageNum)+1;
	
	var startHref = location.href.split('?')[0];
	
	queries['page'] = pageNext;
	var hrefNext = startHref+'?'+QUERY.completeQueryFromHash(queries);
	queries['page'] = pagePred;
	var hrefPred = startHref+'?'+QUERY.completeQueryFromHash(queries);
	
	$(position).html("<div style='padding:5px; border-top:1px solid #069'>"+
					 "<a href='"+hrefPred+"'>"+
					 "<button class='qButton qHint' tag='Назад' style='padding:5px'>←</button>"+
					 "</a>"+
					 "<input style='text-align:center;width:50px; background-color:#FFF; border:#999 1px solid; padding:5px;"+
					 "font-weight:bold;' value='"+pageNum+"' id='search-line' maxlength='7'>"+
					 "<a href='"+hrefNext+"'>"+
					 "<button class='qButton qHint' tag='Дальше' style='padding:5px'>→</button>"+
					 "</a><br>"+
					 "<a onclick='return false;' id='search-button' class='href'>перейти на страницу</a></div>");
	
	$("#search-button").on("click",function(){ 
		if( $("#search-line").val().trim() == '' ) $("#search-line").val('1');
		queries['page'] = parseInt($("#search-line").val());
		if(queries['page']<1) queries['page']=1;
		location.href=startHref+'?'+QUERY.completeQueryFromHash(queries); 
	});
	$("#search-line").on("keypress",function(key){ 
		if( $("#search-line").val().trim() == '' ) $("#search-line").val('1');
		queries['page'] = parseInt($(this).val());
		if(queries['page']<1) queries['page']=1;
		if(key.keyCode==13) location.href=startHref+'?'+QUERY.completeQueryFromHash(queries); 
	});
	
	}