GEO = {}

GEO.interface = "http://maps.googleapis.com/maps/api/geocode/json?"; 

GEO.wasNotFoundLocation = function(){
	WINDOW.ShowMessage("Не удалось определить ваше местоположение, т.к отсутствует подключение к интернету, или ваш браузер устарел.", 
						WINDOW.C.ERROR);
	}
	
GEO.DefCountryCity = function(loc){
	
	var city = '';
	var country = '';
	
	if(!('language' in loc)) loc['language']='ru';
	var loc = GEO.interface+"latlng="+loc['latitude']+','+loc['longitude']+"&sensor=true&language="+loc['language'];
	loc = SendDynamicMsg(['mylocation'],[loc],null,"handlers/ask.php","GET",'json');
	
	if('results' in loc)
	if('address_components' in loc['results'][0]){
		
		loc = loc['results'][0]['address_components'];
		for(var k in loc) {
			if('types' in loc[k]) {
				for(var k2 in loc[k]['types']) {
					if(loc[k]['types'][k2]=='locality') { city = loc[k]['short_name']; break; }
					if(city=='') if(loc[k]['types'][k2]=='administrative_area_level_1') { city = loc[k]['short_name']; break; }
				}
				for(var k2 in loc[k]['types']) if(loc[k]['types'][k2]=='country') {  country = loc[k]['long_name']; break; }
			}
			if(city!='' && country!='') break;
			}
		
		}
		
	return {'city':city, 'country':country};
	
	}
	
	
	
GEO.DefCoords = function(loc){
	
	var longitude = 0.0;
	var latitude = 0.0;
	
	if(!('language' in loc)) loc['language']='ru';
	var coords = GEO.interface+"address="+loc['country']+','+loc['city']+"&sensor=true&language="+loc['language'];
	coords = SendDynamicMsg(['mylocation'],[coords],null,"handlers/ask.php","GET",'json');
	
	if(coords.status!="ZERO_RESULTS" && 
	   ('results' in coords) && 
	   ('geometry' in coords.results[0]) && 
	   ('location' in coords.results[0].geometry) )
	{
		coords = coords.results[0].geometry.location;
		longitude = coords.lng;
		latitude = coords.lat;
		}
		
	return {'latitude':latitude, 'longitude':longitude};
	
	}