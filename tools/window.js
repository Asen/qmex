function pos(object,ll,tt)  
{
    object.style.position="absolute";
	object.style.left=ll+"px";
	object.style.top=tt+"px";

	}
	
function win_hide()
{
	$("#back_window").fadeTo('slow',"0.0");
	window.setTimeout(function(){$("#back_window").remove()},500);
	}
	
function CreateWindow(caption,text,h)
{
	var width = 400;
	var height = (h && h>0) ? h:200;
	var left = window.innerWidth/2-width/2;
	var top  =(window.innerHeight/2)-(height/2); 
	
	$("#back_window").remove();
	
	Window = $("<div id='back_window' class='UIBackWindow' style='left:"+(left)+"px; top:"+(top)+"px; width:"+width+"px'>"+
	  		   "<div id='main_window' class='UIWindow'>"+
			   "<div id='main_window_caption' class='UIWindowCaption'>"+caption+
			   "<img class='UIWindowClose' src='/qmex_img/ex.png'></div>"+
			   "<div id='main_window_text' class='UIWindowText'>"+text+
			   "<div id='main_window_user_text'></div>"+
			   "<div id='sub_text_capt' class='UIWindowService'></div>"+
			   "</div></div>");
	
	$(".UIWindowClose").live('click',win_hide)
			   
	return Window;
	
	}
	
function ShowWindow(h)
{
	$('#back_window').animate({"height":String(h)+"px"},300);
	$('#main_window').animate({"height":String(h)+"px"},300);
	}
	
WINDOW = {};

WINDOW.C = {};
WINDOW.C.ERROR   = 0;
WINDOW.C.SUCCESS = 1;
WINDOW.C.GRAY = 2;
WINDOW.C.STANDARD = 3;

WINDOW.Confirm = 
function(message,func)
{
	var h = Math.max(120, Math.ceil(message.length/60)*20 + 80);
	var schedule = function(){ func(); win_hide(); $(document.body).unbind('keypress'); };
	var Window = CreateWindow('Подтверждение',message,h);
	$(document.body).append(Window);
	$('#main_window').append("<div style='text-align:right; padding-right:5px;'><input type='button' class='qButton' id='MB_YES' value='Да'> <input type='button' class='qButton_silver' value='Нет' onclick='win_hide();'></div>");
	document.getElementById("MB_YES").onclick = schedule;
	$(document.body).bind('keypress',function(e){ if(e.keyCode==13) schedule(); });
	ShowWindow(h);
	
	}
	
WINDOW.ShowMessage = function(text,constant,lifetime){ 

    var color = "";
    switch(constant)
	{
		case WINDOW.C.ERROR:     color="#C66"; break;
		case WINDOW.C.SUCCESS:   color="#096"; break;
		case WINDOW.C.GRAY:      color="#CCC"; break;
		case WINDOW.C.STANDARD:  color="#069"; break;
		}
		
	show_wnd(text,
			Math.max((text.length/50.0)*25 + 65, 90),
			true,
			(!lifetime ? 12 : lifetime),
			color,
			false) ;
	}
	
	
WINDOW.Prompt = function(caption, text, func, params){
	
	params = params && params instanceof Object ? params : {};
	var h = ('height' in params) ? parseInt(params.height) : 150;
	
	$("#back_window").remove();
	var Window = CreateWindow(caption,text,h);
	$(document.body).append(Window);
	$('#main_window_user_text').append("<input type='text' class='UIWindowInput' autofocus id='main_window_input' value='"+
									   (('value' in params) ? params.value : '')+"' maxlength='200'>");
	$('#main_window').append("<div class='UIWindowButtonDiv'>"+
		"<button class='qButton' id='main_window_prompt_ready'>Готово</button>"+
		"<button class='qButton_silver' id='main_window_prompt_cancel'>Отмена</button>"+
		"</div>");
	ShowWindow(h);
	
	$("#main_window_prompt_ready").on('click', function(){ func($("#main_window_input").val()); win_hide(); });
	$("#main_window_prompt_cancel").on('click', win_hide);
	
	}