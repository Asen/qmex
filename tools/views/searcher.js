function startSearch()
{
	$('#qSearch').keyup();
	}

function sender(obj)
{
	var name = $(obj).attr('name');
	var v = ($(obj).attr('value')=='') ? '':$(obj).val() || $(obj).attr('value');
	if(name=='q'){
		location.href='/search#q='+encodeURIComponent(v);
		definer();
		return;
		}
	
	if($(obj).attr('name')=='geo') v = obj.checked ? 'yes':'';
	var uri = getHashQuery();
	answer = SendDynamicMsg([name], [v], null, 'handlers/user_search/interlayer.php?'+uri, 'GET');
	location.href = '/search'+answer;  // It is not redirect! It`s updating current query string.
	////////////////////////////////
	pred_path = window.location;  // This is last changed URL. It`s refresh here.
	////////////////////////////////	
	find_bits_sender();
	definer();
	}
	
	
function definer(){
	
	var query_bits = location.href.toString().split("#")[1].split('&');
	for(i=0;i<query_bits.length;i++)
	{
		var bit = query_bits[i].split('=');
		var key = decodeURIComponent(bit[0]), val = decodeURIComponent(bit[1]);
		$('input[type="text"][name="'+key.toString()+'"]').val(val);

		if(key=='q') $("#search-text").html("<b>"+val+"</b>");
		if(key=='keyinterest') {
			var deal = $("#interest-"+val).text();
			$("#interest-default").text( deal+"(*)" );
			$("#search-text").append(" интересующиеся темой '"+deal+"'");			
		}
	}	
	$("#search-text").append(" : ");
		
}
	
	
$(function(){
		
	if(String(window.location).indexOf('/search')==-1) 
	$("#qSearch").one('mousedown',function(){location.href='/search';});	
	
	$('#qSearch').on('keyup',function(e){ 
	
		if(window.location.toString().indexOf("/search")==-1) { sender(this); return; }			
		if(e.keyCode==38 || e.keyCode==40) return;			
		var printed = $('#qSearch').val();
		
		$.ajax({
		type:'GET',
		url:'handlers/user_search/search.php',
		data:'get-proposals-list='+printed,
		dataType:"json",
		success: function(proposals)
		{
			DATALIST.Create( $('#qSearch').first(), proposals, {position:'fixed'} );
			}
		});
			
	});	
	});