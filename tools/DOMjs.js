function DOMjs_rotate_object(angle)
{
	css = ' .DOMjs-rotation{ transition: all ease 500ms; -o-transition: all ease 500ms; -webkit-transition: all ease 500ms; -moz-transition: all ease 500ms; transform: rotate('+angle+'deg); -o-transform: rotate('+angle+'deg); -webkit-transform: rotate('+angle+'deg); -moz-transform: rotate('+angle+'deg); } ';
	
	$("#DOMjs-style-rotation").remove();
	style = document.createElement("style");
	style.setAttribute("id","DOMjs-style-rotation");
	style.innerHTML=css;
	$(document.body).append(style);	
	}
	
function Rotate(angle,obj)
{
	DOMjs_rotate_object(angle); 
	$(obj).addClass("DOMjs-rotation");
	}