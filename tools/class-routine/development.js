$(function(){
	/*$(".developments-comein").on('mouseover', function(){ animFade(this,500,'+') })
	$(".developments-comein").on('mouseout', function(){ animFade(this,500,'-') })
	$(".developments-comein").on('click', function(){ location.href='/developments?view=show&id='+$(this).attr('goto') })
	*/
	$(".dev-cover").on('load', function(){
		
		var id = $(this).attr('tag');
		var caption = $("#dev-caption-"+id).first();
		$(caption).css({'margin-top': ( -$(caption).innerHeight() )+"px"});
		$(".dev-cover").on('mouseover', function(){ showEnter(this) })
		
	});						
});		
		  
function showEnter(o)
{
	var id = $(o).attr('tag');
	var left = $(o).position().left;
	var top = $(o).position().top;
	var width = $(o).width();
	var height = $(o).height();
	var padding = height/3;
	height -= padding;
	
	$("#development-door").remove();				
	$("body").append( 
	"<a id='development-door' href='developments?view=show&id="+id+"'>"+
	"<div id='enter-box' style='position:absolute; left:"+left+"px; top:"+top+"px; cursor:pointer;" +
	"width:"+width+"px; height:"+height+"px; background-color:black; opacity:0.0;color:white; text-align:center;"+
	"padding-top:"+padding+"px'>"+
	"<div class='ui_development_go_inside_icon'></div>"+
	"<div style='color:#06F'>интересно</div>"+
	"</div></a>" );
	$("#enter-box").animate({opacity:0.7},200);
	
	}

function animFade(o, duration, vector)
{
	$(o).stop(0,0,0);
	$(o).fadeTo(duration, vector=='+' ? 0.75:0.5);
	}