SWOP = {};
SWOP.cause_swop_connection = -1;
SWOP.Main = function(){
	
$(function(){
			
	$(".ui_swop_remove").live('click', function(){ RemoveSwop(this); });
	$(".swop-who-can-help").live('click', function(){ ShowSwopHelpers(this); });
	$(".swop-who-is-needed").live('click', function(){ ShowSwopClients(this); });
	$(".swop-connect-with-user").live('click', function(){ WriteConnection(this); });
	$(".ui_swop_link_conf").live('click', function(){ LinkTalk(this); })
	$(".ui_swop_unlink_conf").live('click', function(){ UnLinkTalk(this); })

});	
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////

function UnLinkTalk(o)
{
	var doUnLink = function(swop_id, icon){
		var res = SendDynamicMsg(["unlink-conf-swop"],
			[parseInt(swop_id)],null,"handlers/swop.php","POST",'text');
		switch(res)
		{
			case "+":
				WINDOW.ShowMessage("Обсуждение было отвязано.", WINDOW.C.SUCCESS);
				$(icon).hide(200);
				break;
			case "-":
				WINDOW.ShowMessage("Произошла неизвестная ошибка.", WINDOW.C.ERROR);
				break;
			}
		}
	
	var sid = $(o).attr('sid');
	WINDOW.Confirm("Отвязать ранее привязанную конференцию?", 
					function(){ doUnLink(sid, o) });	
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////

function LinkTalk(o)
{
	var CreateLink = function(cid, swop_id, icon)
	{
		var res = SendDynamicMsg(["link-conf-swop","swop-id"],
			[parseInt(cid),swop_id],null,"handlers/swop.php","POST",'text');
		switch(res){
			case '!': 
				WINDOW.ShowMessage("Конференции с таким номером не существует, либо она вам не принадлежит.", WINDOW.C.ERROR);
				break;
			case '+':
				WINDOW.ShowMessage("Конференция привязана!", WINDOW.C.SUCCESS);
				$(icon).hide(200);
				break;
			case '~': 
				WINDOW.ShowMessage("Эта конференция уже привязана к чему-то другому.", WINDOW.C.ERROR);
				break;
			case '-':
				WINDOW.ShowMessage("Привязка не удалась. Пожалуйста, повторите попытку.", WINDOW.C.ERROR);
				break;
			}	
		}
	
	var sid = $(o).attr('sid');
	WINDOW.Prompt("Номер обсуждения","Привязав обсуждение, вы получите возможность общаться со всеми интересующимися сразу,"+
				  "а не по отдельности.<br><b>Введите № связанного обсуждения:</b>", function(e){ CreateLink(e, sid, o) },
				  {'value':'', 'height':180})
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
function WriteConnection(o)
{
	var sid = $(o).attr('sid');
	location.href = '/dialogs?message&cause='+SWOP.cause_swop_connection+'&id='+sid;
	} 
	
function ShowSwopHelpers(o)
{
	var sid = $(o).attr('sid');
	location.href = 'swop?helpers='+sid;
	}
	
function ShowSwopClients(o)
{
	var sid = $(o).attr('sid');
	location.href = 'swop?clients='+sid;
	}
	
function RemoveSwop(o)
{
	WINDOW.Confirm('Удалить?', function(){
		var sid = $(o).attr('sid');
		var swopper = SendDynamicMsg(["remove-swop"],[sid],null,"handlers/swop.php","POST",'text');
		if(swopper=='+') $("#swop-"+sid).slideUp(300); 
		else 
		WINDOW.ShowMessage('Произошла неизвестная ошибка.', WINDOW.C.ERROR);
		});
	}
	
}