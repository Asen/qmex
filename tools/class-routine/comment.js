$(".ui-comment-base").slideUp(0);
		
		var baseOff = function(){ 
			var id = $(this).attr("tag");
			$("#comment-base-"+id).stop();
			$("#base-text-"+id).text("Показать дискуссию");
			$("#comment-base-"+id).slideUp(1000); 
			Rotate(0,$("#cbase-arrow-"+id)[0]);
		};
		var baseOn = function(){ 
			var id = $(this).attr("tag");
			$("#comment-base-"+id).stop();
			$("#base-text-"+id).text("Свернуть");
			$("#comment-base-"+id).slideDown(1000); 
			Rotate(-90,$("#cbase-arrow-"+id)[0]);
		}
		
		$(".ui-comment-base-roll").toggle(baseOn,baseOff);
		$(".base-roll-down").live('click',function(){ $("#roll-"+$(this).attr("tag")).click(); })
		$(".qm-comment-delete").live("click",function(){deleteComment(this)});
		$(".qm-comment-edit").live("click",function(){editComment(this)});
		$(".qm-comment-answer").live("click",
				function(){
				id=$(this).attr("num");
				if($("#qm-edit-block-"+id).length>0) {$("#qm-edit-block-"+id+" .qm-edit-cancel").click(); return;}
				obj=$("#comment-"+id).children(".qm-comment-text")[0]; 
				$(obj).append("<div id=\"qm-edit-block-"+id+"\"><textarea class=\"UI-edit-box\" id=\"edit-c-"+id+"\"></textarea><br><button class=\"qButton qm-answer-ready\"  n=\""+id+"\">Готово!</button> | <button class=\"qButton_silver qm-edit-cancel\" n=\""+id+"\">Отмена</button></div>");
				$('#edit-c-'+id).focus();
				});
		$(".qm-edit-cancel").live("click",
				function()
				{ 
				id=$(this).attr("n");
				obj=$("#comment-"+id).children(".qm-comment-text")[0]; 
				$(obj).children(".qm-text").show(0);
				$("#qm-edit-block-"+id).remove(); 
				});
		$(".qm-edit-ready").live("click",
				function()
				{
				id=$(this).attr("n");
				obj=$("#comment-"+id).children(".qm-comment-text")[0]; 
				txt=$("#edit-c-"+id).val();
				var res = SendDynamicMsg(["edit-comment","text","id"],[1,txt,id],null,"handlers/uiHandler.php","POST","html");
				if(res.indexOf("success-editing")>0){
					$(obj).children(".qm-text").show(0);
					$(obj).children(".qm-text").html(res);
					$("#qm-edit-block-"+id).remove(); 
					} 
					else show_wnd(res,100,true,3.5,"#C40000",false);
				});
		$(".qm-answer-ready").live("click",
				function()
				{
				id=$(this).attr("n");
				obj=$("#comment-"+id).children(".qm-comment-text")[0]; 
				txt=$("#edit-c-"+id).val();
				var res = SendDynamicMsg(["answer-comment","text","id"],[1,txt,id],null,"handlers/uiHandler.php","POST","html");
				if(res=="+") 
				{
					$("#qm-edit-block-"+id).remove(); 
					comment = SendDynamicMsg(["last_comment","attachment"],[1,id],null,"handlers/uiHandler.php","GET","html");
					$("#comment-"+id).after(comment);
					margin = (parseInt($("#comment-"+id).attr("level"))+1)*10;
					$(".comment-abstraction").css({"margin-left":margin+"px"});
					$(".comment-abstraction").animate({opacity:1},1000);				
				}
				else show_wnd(res,100,true,3.5,"#C40000",false);
				
				});
			
				
		function editComment(o)
		{
			id=$(o).attr("num");
			if($("#qm-edit-block-"+id).length>0) {$("#qm-edit-block-"+id+" .qm-edit-cancel").click(); return;}
			obj=$("#comment-"+id).children(".qm-comment-text")[0];
			$(obj).children(".qm-text").hide(0);
			text=$(obj).children('.qm-text').html().split("<br>").join("\n");
			$(obj).prepend("<div id=\"qm-edit-block-"+id+"\"><textarea class=\"UI-edit-box\" id=\"edit-c-"+id+"\">"+text+"</textarea><br><button class=\"qButton qm-edit-ready\"  n=\""+id+"\">Готово!</button> | <button class=\"qButton_silver qm-edit-cancel\" n=\""+id+"\">Отмена</button></div>")
			} 
		
		function deleteComment(o)
		{
			WINDOW.Confirm("Удалить комментарий безвозвратно?", function(){			
				id = $(o).attr("num");
				attach = $("#comment-"+id+" attach").attr("v");
				type = $("#comment-"+id+" attach").attr("type");
				a = SendDynamicMsg(["delete-comment","type","id","attach"],[1,type,id,attach],null,"handlers/uiHandler.php","POST","html");
				if(a=="+") {
					$("#comment-"+id).slideUp(500);
					$("#qm-level-"+id).slideUp(500);
					$("#answer-roll-box-"+id).slideUp(500);
					}
			});
			}