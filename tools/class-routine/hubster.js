HUBSTER = {};
HUBSTER.comm_cause_hub_connection = -1;
HUBSTER.Main = function(){

if( document.getElementsByClassName("script-note").length>1 ) $(this).remove();

function Ready()
{
	 $(".note-comments").slideUp(0);
	 $(".hide_show_note").toggle(function(){actNote(this,1)}, function(){actNote(this,0)});
	 $(".private_note").toggle(function(){actNote(this,5)}, function(){actNote(this,6)});
	 $(".private_note_o").toggle(function(){actNote(this,6)}, function(){actNote(this,5)});
	 $(".edit_note").toggle(function(){actNote(this,3)},function(){actNote(this,4)});  
	 $('.comment-word-line').toggle(function(){showComments(this)},function(){hideComments(this)});
	}

$(document).ready(function()
{
	$(".ui_single_note_content").children("img").each(function(ind,el){
		if($(el).width()>200) $(el).width("200px"); 
		});
	
	 $(".qNoteRait").live("mouseover",function(){showNotify(this,
	 "<img src='qmex_img/l_view.png' width='10px' height='10px' tag='+1'> "+$(this).attr('l')+" | <img src='qmex_img/disl_view.png' width='10px' height='10px' tag='+1'> "+$(this).attr('dl'))});
	 $(".qNoteRait").live("mouseout",function(){hideNotify();});
	 
	 $(".rem_note").live('click',function(){actNote(this,2)});
	 $(".ui_note_tool").live('mouseover',function(){showNotify(this,$(this).attr("tag"));});
	 $(".ui_note_tool").live('mouseout',function(){hideNotify();});
	 $('.ui_note_reg_ready').live('click',function(){changeNote(this)});
	 $('.note-like-item').live('click',function(){VoiteNote(this,1)});
	 $('.note-dislike-item').live('click',function(){VoiteNote(this,0)});
	 $(".comment-send-word").live('click',function(){sendComment(this)});
	 $(".make_intent").live('click',function(){ makeIntent(this); });
	 $(".intent_curr").live('click',function(){ currIntent(this); });
	 $(".add_to_favorites").live('click',function(){ addToFavorites(this) })
	 $(".rem_from_favorites").live('click',function(){ remFromFavorites(this) })
	 $(".connect_with_author").live('click',function(){ connectWithAuthor(this) })
	 $(".connect-idea").live('click',function(){location.href='/?ext='+$(this).attr("num")+"#hub";})
	Ready();
	COMMON.IFrameTest(".public-frame");
	
});

function connectWithAuthor(o)
{
	var id = parseInt($(o).attr("num"));
	location.href = '/dialogs?message&cause='+HUBSTER.comm_cause_hub_connection+'&id='+id;
	
	}

function remFromFavorites(o)
{
	var id = parseInt($(o).attr("num"));
	var res = SendDynamicMsg(['rem_note_from_favorites'],[id],null,'handlers/uiHandler.php','POST');
	if(res=='+'){
		$(o).hide(1000);
		WINDOW.ShowMessage("Хаб удален из интересных материалов", WINDOW.C.SUCCESS);
		window.setTimeout(function(){ location.href=location.href }, 1000);
		}
	}

function addToFavorites(o)
{
	var id = parseInt($(o).attr("num"));
	var res = SendDynamicMsg(['add_note_to_favorites'],[id],null,'handlers/uiHandler.php','POST');
	if(res=='+'){
		$(o).hide(1000);
		WINDOW.ShowMessage("Хаб добавлен в интересные материалы!", WINDOW.C.SUCCESS);
		}
	}

function makeIntent(o)
{
	var id = parseInt($(o).attr("num"));
	var res = SendDynamicMsg(['make_note_intent'],[id],null,'handlers/uiHandler.php','POST');
	if(res=='+') {
		$(o).attr("src","qmex_img/UI/hubster/idea-curr.png");
		$(o).removeClass("make_intent");
		$(o).addClass("intent_curr");
		show_wnd("Основная цель установлена!",100,false,2,"#09F",true); 
		}
		 else show_wnd("Произошла неизвестная ошибка.");
	}
	
function currIntent(o)
{
	var res = SendDynamicMsg(['curr_note_intent'],[0],null,'handlers/uiHandler.php','POST');
	if(res=='+') {
		$(o).attr("src","qmex_img/UI/hubster/no-idea-curr.png");
		$(o).removeClass("intent_curr");
		$(o).addClass("make_intent");
		show_wnd("Основная цель удалена.",100,false,2,"#FAA",true); 
		}
		else show_wnd("Произошла неизвестная ошибка.");
	}

function sendComment(o)
{
	id = $(o).attr('tag');
	text = $("#note-new-comment-"+id).val();
	var a = SendDynamicMsg(['post_comment','type','text','attach'],[1,'ab_note',text,id],null,'handlers/uiHandler.php','POST','html');
	if(a=="")
	{
		$("#note-new-comment-"+id).val("");
		comment = SendDynamicMsg(['last_comment','attachment'],[1,id],null,'handlers/uiHandler.php','GET','html');
		$("#comments-box-"+id).prepend(comment);
		$(".comment-abstraction:first").animate({opacity:1},1000);
		}
		else show_wnd(a,100,true,3.5,"#C40000",false);
	}

function showComments(o)
{
	id = $(o).attr("tag");
	$("#cb-"+id).slideDown(300);
	}
	
function hideComments(o)
{
	id = $(o).attr("tag");
	$("#cb-"+id).slideUp(300);
	}


function VoiteNote(o,v)
{
	var sender = $("#data_note_"+$(o).attr("num")).attr("sender");
	var id = parseInt($(o).attr("num"));
	var v = parseInt(v);
	var answ=SendDynamicMsg(['mvoite','user','voite','type','essence'],[1,sender,v,'note',id]);
	if(String(answ).indexOf('voite-error')==-1) 
	{
		$("#note-rait-sign-"+id).hide(400);
		window.setTimeout(function(){
		v = (v==1) ? 1:-1;
		color = (answ>0) ? "#009900":"#d70000";
		if(answ==0) color='gray';
		$("#note-rait-sign-"+id).css({"color":color});
		$("#note-rait-sign-"+id).html(String(answ));
		$("#note-rait-sign-"+id).slideDown(400);
		},400);
	} else $("#note-rait-sign-"+id).append(String(answ));
	}


function changeNote(obj)
{
	var id = $(obj).attr("tag");
	var tb_content = GetEditor("ta-"+id).getContent();//$(GetEditor("ta-"+id)).html();
	var tb_caption = $("#tq-"+id).val();
	var tb_substance = $("#tq-substance"+id).val();
	
	var result = SendDynamicMsg(['note_reg_ready','tb_content','tb_caption','tb_substance'],
	[id,tb_content,tb_caption,tb_substance],null,"handlers/uiHandler.php");
	
	if(result=='info_error') WINDOW.ShowMessage("Вы указали не всё или указали слишком кратко.", WINDOW.C.ERROR); 
	else
	document.location.reload();
	//$("#editor_note_"+id).click();
	}

function actNote(note,t)
{
	var note_id = $(note).attr("num");
	var id = 'note_'+note_id;
	if(parseInt(note_id)>-1)
	{
		switch(t)
		{
		case 2: WINDOW.Confirm('Вы уверены, что желаете удалить этот хаб безвозвратно?',
				function(){
					SendDynamicMsg(['remove_note'],[note_id],null,"handlers/uiHandler.php");
		        	//$(note).parent(".ui_note_tools").fadeOut(500);
		       	 	$("#public-note-"+note_id).slideUp(600);	
					//$("#"+id+" img").parent("div").slideUp(600);
					window.setTimeout(function(){ $("#public-note-"+note_id).remove();  },650);				
				});
				break;
		case 3: //$("#data_"+id).fadeOut(0);
		        //$("#data_"+id).fadeIn(500);
		        $(note).css({border:"#00F 1px solid"});
				$(note).attr("tag","Отмена");
				
				/*$("#data_"+id+" .ui_single_note_content .quote").wrap("<quote></quote>");
				quote_inner = $("#data_"+id+" .ui_single_note_content .quote").html();
				$("#data_"+id+" .ui_single_note_content quote").html(quote_inner);
				$("#data_"+id+" .ui_single_note_content .quote").remove();*/
				
		        var lazynote = SendDynamicMsg(['get_note_content'],[note_id],null,'handlers/uiHandler.php','GET');
				//$("#data_"+id+" .ui_single_note_content iframe").attr("srcdoc");
				var caption = $("#data_"+id+" .ui_single_note_caption .ui_note_meaning").text().trim();
				var substance = $("#substance_"+note_id).text().trim();
		        $("#data_"+id+" .ui_single_note_content").hide();
				$("#data_"+id+" .ui_textbox").remove();
				
				$("#data_"+id).append("<table cellspacing='10px' id='edit-box-"+id+"'>"+
									  "<tr>"+
									  "<td colspan='2'>"+
									  "<div class='ui_textbox' id='ui_textbox-"+id+"'>"+
									  "<div style='text-align:center'><textarea class='UI-edit-box' rows='5' id='ta-"+note_id+"'>"+
									  lazynote+"</textarea></div>"+
									  "</td>"+
									  "</tr>"+
									  "<tr>"+
									  "<td>"+
									  "<div style='font-weight:bold; padding:5px; border-left:1px solid black'>"+
									  "Название : <input type='text' style='width:70%; padding:3px;' value='"+caption+
									  "' id='tq-"+note_id+"' maxlength='50'></div>"+
									  "<div style=' font-weight:bold; padding:5px; border-left:1px solid black'>"+
									  "Основная суть : <input type='text' maxlength='150' style='width:70%; padding:3px;' value='"+
									  substance+"' id='tq-substance"+note_id+"'></div></div>"+
									  "</td>"+
									  "<td>"+
									  "<div style='text-align:right; padding:3px'>"+
									  "<input type='button' class='qButton ui_note_reg_ready' value='Применить' tag='"+note_id+
									  "'></div>"+
									  "</td>"+
									  "</tr></table>");
									  
				InitEditor("#ta-"+note_id,"300px");	
				$(GetEditor("ta-"+note_id)).html(lazynote);			
				break;
		case 4: $("#data_"+id).fadeOut(0);
		        $("#data_"+id).fadeIn(500);
		        $(note).css({border:"#FFF 1px solid"});
				$(note).attr("tag","Редактировать");
				$("#data_"+id+" .ui_single_note_caption .ui_note_meaning").html($("#tq-"+note_id).val());
		        $("#edit-box-"+id).remove();
				$("#data_"+id+" .ui_single_note_content").show();
		        break;
		case 5: WINDOW.Confirm("Этот хаб будет скрыт от всех, кроме вас и вашего окружения. Вы уверены, что желаете его скрыть?",
				function(){
					$(note).attr("tag","Сделать доступным для всех");
		       		$(note).attr("src","/qmex_img/UI/hubster/common.png");
		       		SendDynamicMsg(['visible_note'],[note_id],null,"handlers/uiHandler.php");
				});
				break;
		case 6: $(note).attr("tag","Скрыть хаб от посторонних");
				$(note).attr("src","/qmex_img/UI/hubster/private.png");
				SendDynamicMsg(['invisible_note'],[note_id],null,"handlers/uiHandler.php");
				break;
		}
		}
}


}