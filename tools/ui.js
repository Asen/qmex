UI = {}

UI.completeSettings = function (user)
{
	if(!user || user==null) user='';

	var data = SendDynamicMsg(["ui_settings"],[encodeURI(user).toString()],null,'handlers/uiHandler.php','GET','json');
	var background = data['bg']['b'].toString();
	var background_style = (parseInt(data['bg_style'])==2) ? '100% 100%':'';	
	var opacity = parseFloat(data['opacity']);
	var background_type = parseInt(data['bg']['type']);
	if(background_type==1) $("html, body").css({'background-image':'url('+background+')','background-size':background_style.toString()});
	//if(background_type==0) $("html, body").css({'background-color':''+background});
 
	$("#qmex_content_block").css({"background":"rgba(225,240,255,"+opacity.toString()+")"});
	}
	
	
	//completeSettings();