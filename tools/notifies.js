function showNotify(obj,text)
{
	if(!text || text==='') 
	if($(obj).attr("tag")) text = $(obj).attr("tag"); else text="Редактировать";
	var top = $(obj).offset().top+$(obj).height()+20;
	var left = $(obj).offset().left;
	if(left<0) left += $(obj).width()-20;
	$(document.body).append("<div id='NotifyBox' style='top:"+top+"px; left:"+left+"px'><div id='NotifyArrow'></div><div id='NotifyText'>"+text+"</div></div>");
	$("#NotifyBox").prepend('');
	window.setTimeout(function(){
		
		$("#NotifyBox").slideDown(250);
		$("#NotifyArrow").addClass("NotifyClass");
		
		},10);
	}
	
	
function hideNotify()
{
	$("#NotifyBox").remove();
	}
	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MENU = {}

MENU.showSlideMenu = function(o)
{
	var items = $(o).attr("items").split("|");
	var func = $(o).attr("func").split("|");
	var id = $(o).attr("tag");
	if(items.length>0)
	{
		var menu = $("<div class='slideMenu' id='sm-"+id+"'></div>");
		var top = $(o).offset().top+$(o).height()+10;
		var left = $(o).offset().left;
		$(menu).css({'left':left+'px','top':top+'px','min-width':$(o).width()+'px'})
		for(var key in items)
		$(menu).append('<div class="slideMenuItem">'+items[key]+'</div>')
		$(menu).children('.slideMenuItem').each(function(index, element) {
            $(element).on('click', function(){ MENU.hideSlideMenu(o); eval(func[index]+'()')   } );
        });
		$(document.body).append(menu)
		$(menu).slideUp(0);
		$(menu).slideDown(300);
		}
	}
	
MENU.hideSlideMenu = function(o)
{
	var id = $(o).attr("tag");
	$("#sm-"+id).slideUp(300);
	window.setTimeout(function(){ $("#sm-"+id).remove() },350);
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function getAbsolutePosition(el) {
	var r = {
		x : el.offsetLeft,
		y : el.offsetTop
	};
	if(el.offsetParent) {
		var tmp = getAbsolutePosition(el.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
}

function binds(o)
{
	$(o).bind("mouseover", function() {this.style.backgroundColor = "#2886D4"});
	$(o).bind("mouseout", function() {this.style.backgroundColor = "transparent"});
	}

function rContextMenu(o) {
	$(o).css({"background-color" : "inherit"});
	$("#QMenu").slideUp(500);
	binds(o);
}

function ContextMenu(o) {
	
	var _sz_resize = function(){ 
				var position = getAbsolutePosition(o);
				$("#QMenu").css({'left':(position.x)+'px','top':(position.y + 40)+'px'}); 
			};
	
	if($("#QMenu").length>0){
		$(o).unbind("mouseover");
		$(o).unbind("mouseout"); 
		_sz_resize();
		$("#QMenu").slideDown(500); 
		return; 
		}
	
	var cmItems = [["Настройки","/settings","m_settings",'qmex_img/UI/settings.png'],
	               ["Изменить информацию о себе","/editprofile","m_edit",'qmex_img/UI/write.png'],
	               ["Диалоги","/dialogs","m_messages",'qmex_img/UI/msg.png'],
				   ["Последние события","/events","m_events",'qmex_img/UI/checker.png'],
				   ["qCoins","/qcoins","m_qcoins",'qmex_img/UI/qcoins.png'],
	               ["Окружение","/peoples","m_peoples",'qmex_img/UI/profile/people.png'],
				   ["Интересные материалы","/necessaries","m_favorites",'qmex_img/UI/favorite.png'],
				   ["qMex-Time","/qmex-time","m_time",'qmex_img/UI/qtime.png']
	              ];
	          
	h = cmItems.length*parseInt($(".QmenuItem:first").height());    
	
	var menu = "";
	for(i=0;i<cmItems.length;i++)
	{
		var img = cmItems[i].length>=4 ? '<img src="'+cmItems[i][3]+'" height="15px"/>' : '' ;
		menu+="<a href='"+cmItems[i][1]+"' class='QMenuItemHref'><div class='QmenuItem' id='"+cmItems[i][2]+"'>"+img+
		" <span class='mitext'>"+cmItems[i][0]+"</span> <span class='count'></span></div></a>";
	}
	
	wid_th = $("#userdata").width()-2;
	$(document.body).append("<div style='position:absolute; z-index:1; width:" + wid_th + 
	"px; height:"+h+"px; background-color:#2886D4;' id='QMenu'>"+menu+"</div>");
	
	$("#QMenu").css({
		"border-bottom" : "#FFF solid 1px",
		"border-left" : "#FFF solid 1px",
		"border-right" : "#FFF solid 1px",
		"position" : "fixed",
		"-moz-box-shadow": "#000 5px 5px 5px 1px",
		"-webkit-box-shadow": "#000 5px 5px 5px 1px",
		"box-shadow": "#000 5px 5px 5px 1px"
	});
	
	
	$("#QMenu").slideUp(0);
	rContextMenu(o);
	$(window).resize(_sz_resize);
}


DATALIST = {};
DATALIST.Create = function(input, values, params){
	
	if(values.length==0) { DATALIST.Remove(); return; }
	
	var top  = $(input).position().top + $(input).innerHeight() + 1;
	var left = $(input).position().left;
	var width = $(input).width();
	var active = -1;
	
	/////////////////////////// PARAMS ///////////////////////////
	var position = (params && ('position' in params)) ? params['position'] : 'fixed';
	//////////////////////////////////////////////////////////////
	
	$("#DLdatalist").remove();
	$(document.body).append("<div class='UIdatalist' id='DLdatalist' style='position:"+position+";"+ 
							"top:"+top+"px; left:"+left+"px; min-width:"+width+"px'></div>");
	for(var k in values) $("#DLdatalist").append('<div class="UIdatalist-item">'+values[k]+'</div>');
	$("#DLdatalist").slideUp(0);
	$("#DLdatalist").slideDown(300);
	
	values = $(".UIdatalist-item").length;
	
	$(".UIdatalist-item").on('click', function(){ 
		var words = $(input).val().split(' ');
		words[words.length-1] = $(this).text();
		$(input).val(words.join(' ')); 
		$(input).trigger('change'); 
		DATALIST.Remove(); 
		});
	$(input).unbind('keydown');
	$(input).bind('keydown', function(e){ 
		var code = e.keyCode; 
		if(code==13 && active>-1 && active<values){
			$(".UIdatalist-item").eq(active).trigger('click'); 
			return;
			}
		if(code==38){
			if(active>=0)   active--;
			if(active==-1) active=values-1;
			} 
		if(code==40){
			if(active<values) active++; 
			if(active>=values) active=0;
			}
		$(".UIdatalist-item").removeClass('UIdatalist-item-active');
		$(".UIdatalist-item").eq(active).addClass('UIdatalist-item-active');
		})
	
	}
	
DATALIST.Remove = function(){
	$("#DLdatalist").slideUp(300);
	window.setTimeout(function(){ $("#DLdatalist").remove() }, 300);
	}