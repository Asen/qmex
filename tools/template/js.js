
(function(){

IS_AUTHORIZED = false;

$(jsMain);


function jsMain()
{
	IS_AUTHORIZED = $("#authorized-user-sign").length>0;
	styles();
	events();

	if(IS_AUTHORIZED)
	{
		ui_messages_get();
		window.setInterval(function(){ ui_messages_get() },2500);
	}
	}


function styles()
{
	$("#qmex_site").css({"min-height":$(window).height()+"px"});
	$("#qcontent").css({"min-height":$(window).height()+"px"});
	if(IS_AUTHORIZED){
		$("#qmex-intro").css({'display':'block', 'top' : ($(window).height()-$("#qmex-intro").innerHeight())+"px"})
		$("#qmex-intro").width( $("#qmex_head").width()+"px" );
	}
		
	$("*[t]").addClass("typical_text");
	$("img").on('error',function(e){ 
		$(this).attr("src","qmex_img/picture_error.png");
		$(this).addClass("imgError");
		$(this).load(function(){
			if($(element).width()>200 && $(element).height()>200)  $(element).css({"width":"100px","height":"100px"});
		});
	});

	
	window.setTimeout(function(){
		$(".imgError").each(function(index, element) {
			if($(element).width()>500 || $(element).height()>500)  $(element).css({"width":"250px","height":"250px"});
		});
	},5000);
	
	}


function events()
{
	$.ajaxSetup({timeout:5000});
	$(".qEdit,.qHint").live('mouseover',function(){showNotify(this);});
	$(".qEdit,.qHint").live('mouseout',function(){hideNotify();});
	$(".uiUserMessageBox").live("keydown",function(){Increasing(this);});
	
	//$(".ui_upper_part_menu_item").on('mouseover', function(){ $(this).stop(); $(this).animate({'padding-left':'20px'},150) })
	//$(".ui_upper_part_menu_item").on('mouseout', function(){  $(this).stop(); $(this).animate({'padding-left':'10px'},150) })	
	var aniType = 'pulse animated';
	$(".ui_upper_part_menu_item").on('mouseover', function(){ $(this).addClass(aniType) });
	$(".ui_upper_part_menu_item").on('mouseout', function(){ $(this).removeClass(aniType) });
	
	$(window).on('scroll', function(){ $("#qmex_head").height('2000px') })
	$(window).on('resize', styles)
	
	$(".choise-selector").live('click',function(){ 
					if( parseInt($(this).attr("tag"))<99 ) $(this).attr("tag", parseInt(Math.random() * 100000+100))
					var isShown = parseInt($(this).attr('isShown') )
					if(isShown==1) MENU.hideSlideMenu(this); else MENU.showSlideMenu(this);
					$(this).attr('isShown', isShown==1 ? '0':'1' )
					})
	
	$("#qmex_head_hider").on('mouseover', function(){ $(this).stop(true,true,true); $(this).animate({'opacity':'1'},100) })
	$("#qmex_head_hider").on('mouseout', function(){ $(this).stop(true,true,true); $(this).animate({'opacity':'0.5'},100) })
	$("#qmex_head_hider").toggle(hideHead, showHead);
	
	}	
////////////////////	

function hideHead()
{
	var TM = 'fast';
	$("*").stop(false,false,false);
	$("#qmex_head").animate({left:(-$("#qmex_head").width()+20)+"px"}, TM);
	$("#qmex_head_menu").animate({width:'0%'}, TM);
	$("#qmex_content_base").animate({width:'100%'}, TM);
	window.setTimeout(function(){
		$("#qmex_head_hider").text(">>");
		$("#qmex_head_hider").css('text-align','right');
		$("#qmex_head_hider").attr('tag','Развернуть');
	}, TM);
	
	
	
	}
	
function showHead()
{
	var TM = 'fast';
	$("*").stop(false,false,false);
	$("#qmex_head").animate({left:"0px"}, TM);
	$("#qmex_head_menu").animate({width:'20%'}, TM);
	$("#qmex_content_base").animate({width:'80%'}, TM);
	window.setTimeout(function(){
		$("#qmex_head_hider").text("<<");
		$("#qmex_head_hider").css('text-align','center');
		$("#qmex_head_hider").attr('tag','Скрыть');
	}, TM);
	}

function Increasing(uiMsgBox)
{
	rows = $(uiMsgBox).val().length/Math.floor(parseInt($(uiMsgBox).attr('cols'),10)) + 1;	
	if(rows<3) rows=3; if(rows>15) rows=15;
	uiMsgBox.setAttribute("rows",rows);
	}

///////////////////////////////////////* Dynamic Queryes */////////////////////////////////////////////

function isForbiddenUrlForNotify()
{
	var url = location.href.toString();
	return url.indexOf('dialogs?') > 0 || url.indexOf('/dialogs') > 0;
	}

last_time_ui_msg = 0;
forcapt = true;
capt = window.document.title;
ac=null;

function ui_messages_get()
{
	$.ajax({
		   type:'GET',
		   data:"ui_start_info",
		   url:"handlers/uiHandler.php",
		   dataType:"json",
		   async:true,
		   error: function(e, msg){ },
		   success:function(info){
			   
			   if(parseFloat(info['coins'])>0) $("#m_qcoins > .count").text("["+info['coins']+"]");
			   if(parseInt(info['events'])>0) $("#m_events > .count").text("[+"+info['events']+"]");
			   if(info['messages']>0){
				   
				  $("#m_messages > .count").text('[+'+info['messages']+']');			  
				  if(info['messages']>last_time_ui_msg && !isForbiddenUrlForNotify())
				  {
					  if(ac!=null) window.clearInterval(ac);
					  var pfsx = getPostfix(info['messages']);
					  
					  $("#ui_messages_notification").html('У вас <b>'+info['messages']+'</b> нов'+pfsx[0]+' сообщен'+pfsx[1]+' в диалогах!'+
					  '<br> <b>Перейти к диалогам &gt;&gt;</b> </a>');
					  
					  $("#ui_messages_notification").animate({
						  top:'90%',
						  opacity:'1.0'
						  }, 1000);
					  ac = window.setInterval(function(){
						  window.document.title = 
						  (forcapt==true) ? '**У вас '+info['messages']+' нов'+pfsx[0]+' сообщен'+pfsx[1]+'!**':capt;
						  forcapt = !forcapt;
						  },1500);
						  
					  window.setTimeout(function(){
					  $("#ui_messages_notification").animate({
							 top:'100%',
							 opacity:'0.0'
							 }, 1000);			   
							},5000);
						}
							 
					last_time_ui_msg = info['messages'];
					}
				}
		   });
		   
	}


///////////////////////////////*  There is sided functional below *///////////////////////////////			   
	
tm = 0;		   
scale=1;
rotate=0;

$(document).bind('keydown',
function(event){ 
event = event || window.event; 
tm++;

switch(event.which)
{
	case 118: scale+=0.1;break;    // F7                      
	case 119: scale-=0.1;break;    // F8
	case 120: rotate = (rotate>=360) ? 0:rotate+30; // F9
	          break;
	}
	deg = (tm%2==1) ? 360:-360;
	if(event.which==117){   // F6 
	$("div").css({"-webkit-transition":"all 6000ms ease-in-out","-webkit-transform":"rotate("+deg+"deg)"});
	$("div").css({"-o-transition":"all 6000ms ease-in-out","-webkit-transform":"rotate("+deg+"deg)"});
	$("div").css({"-moz-transition":"all 6000ms ease-in-out","-webkit-transform":"rotate("+deg+"deg)"});
	$("div").css({"transition":"all 6000ms ease-in-out","-webkit-transform":"rotate("+deg+"deg)"});	
	}
});

})();