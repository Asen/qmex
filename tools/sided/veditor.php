<script type="text/javascript" src="tools/sided/tiny/tinymce.min.js"></script>
<script type="text/javascript">

function InitEditor(id, h)
{
	
h = (h!=0 && h!=null) ? h:0;
	
tinymce.init({
    selector: id,
	language:"ru",
	width:"100%",
	height:'100px',
	theme:'modern',
	height:h.toString(),
	
	plugins: [
		"advlist autolink lists link image charmap print preview anchor",
		"searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste textcolor hr emoticons textcolor"
	],
	toolbar: "undo redo | styleselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media emoticons",
	menubar: "view insert tools table ",
    target_list: [
        {title: 'Открывать в новой странице', value: '_blank'}
    ],
	
 });
}

function GetEditor(id)
{
	return tinyMCE.get(id);
	}
	
</script>