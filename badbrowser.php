﻿<script>
location.href='/';
</script>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="views/main/styles/ui.css">
<link rel="shortcut icon" href="qmex_img/favicon.png">
<title>qMex | Ваш браузер устарел!</title>
</head>
<body>
<div class="browser_error_page">
<div class='Box UI-rounded-box'>
<table cellpadding="0" cellspacing="0" border=0>
<tr>
<td><img src="qmex_img/qmex.png" style="padding:10px" height="50px"></td>
<td>
<div style="font-weight:bold;">В вашем браузере не работает JavaScript!</div>
Пожалуйста, включите JavaScript или обновите свой браузер, чтобы он поддерживал <b><u>JavaScript</u></b>.<br>Без JavaScript система qMex не сможет работать корректно. 
</td>
</tr>
</table>
</div>
<br><br>
<div class="badbrowser UI-rounded-box">
<div class="perform" style="border-bottom:1px #000 solid;">
Для работы с системой qMex необходима поддержка Javascript и Cookies.<br>
Чтобы использовать все возможности сайта, используйте один из следующих браузеров:
</div>
<div class="perform" style="border-bottom:1px #000 solid;">
<table style="margin:0 auto; text-align:center; width:90%">
<tr>
<td class="browser_image">
<a href="http://www.google.ru/chrome" target="_blank"><img src="qmex_img/service/browsers/google.png">
<br>Google Chrome</a>
</td>
<td class="browser_image">
<a href="http://www.mozilla.org/ru/firefox" target="_blank"><img src="qmex_img/service/browsers/mozilla.png"><br>
Mozilla FireFox</a>
</td>
<td class="browser_image">
<a href="http://www.opera.com/" target="_blank"><img src="qmex_img/service/browsers/opera.png"><br>
Opera</a>
</td>
<td class="browser_image">
<a href="http://www.apple.com/ru/safari/" target="_blank"><img src="qmex_img/service/browsers/safari.png"><br>
Safari</a>
</td>
</tr>
</table>
</div>
<div class="perform">
Прежде, чем вы пожелаете установить один из предложенных браузеров, убедитесь, что в вашем браузере включена поддержка JavaScript.
</div>
</div>
</div>
</body>
</html>

<style>

.Box{
	background-color:#09C;
	border:#00F solid 1px;
	padding:10px;
	text-align:left;
	color:#FFF;
	}

.browser_error_page
{
	width:50%;
	text-align:center;
	margin:0 auto;
	padding-top:10%;
	}
.badbrowser
{
	margin:0 auto;
	text-align:center;
	font-family:
	border:#FFF 1px solid;
	font-size:12px;
	color:#000;
	border:#000 1px solid;
	background-color:#DAF0FA
	}
	
.perform{
	padding:10px;
	}
	
.browser_image img{
	width:80px;
	height:80px;
	padding:5px;
	border-width:0px;
	}
	
	
.browser_image{
	color:#FFF;
	}
</style>