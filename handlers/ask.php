<?php

require_once("service/handlers_common.php");
$db = new db();

if(isset($_GET))
{
	if(isset($_GET["recovering_keyword"]))    
		RecoveringKeyword($_GET["recovering_keyword"]);
	if(isset($_GET["recovering_password"]))   
		RecoveringPassword($_GET["recovering_password"], $_GET["recovering_repassword"]);
	
	if(isset($_GET['qmex-time']))  qMexTime();
	if(isset($_GET['mylocation'])) DefLocation($_GET['mylocation']);
	}
	
if(isset($_POST))
{ 	
	if(isset($_POST["UiChanger"])) UiChanger($_POST["UiChanger"], $_POST["value"]);  
	if(isset($_POST["mvoite"]))    Voite($_POST);
	}

function qMexTime()
{
	date_default_timezone_set(SysInfo::qMexTime);
	$data = array();
	$data['day'] = date('d');
	$data['month'] = date('m');
	$data['month-name'] = Enum::$MONTHES[(int)$data['month']-1];
	$data['year'] = date('Y');
	$data['hour'] = date('H');
	$data['minute'] = date('i');
	$data['second'] = date('s');
	$data['date'] = date("d.m.Y H:i:s");
	die( json_encode($data) );
	}
	
function DefLocation($location_url)
{
	die( file_get_contents($location_url) );
	}

function RecoveringKeyword($keyword)
{	
	$keyword = trim(mb_strtolower($keyword));
	$rr = $_SESSION["names"]["rKey"];
	if($keyword!=$_SESSION["names"]["rKey"]) 
		die(json_encode(array("result"=>"-", "msg"=>"Ключевое слово введено неверно!")));
	die(json_encode(array("result"=>"+", "msg"=>"")));
}
	
function RecoveringPassword($password, $repassword)
{
	$password   = trim(mb_strtolower($password));
	$repassword = trim(mb_strtolower($repassword));
	$res = Check::Password($password);
	if($res['result']=='-') die(json_encode($res));
	$res = Check::PasswordEquals($password, $repassword);
	die(json_encode($res));
	}
	

function UiChanger($type, $value)
{
	global $db;
	$value= mysql_real_escape_string(mb_substr(trim($value),0,200));
	$uid = $_SESSION["id"];
	$return = array('code'=>'-', 'msg'=>'Произошла ошибка!');
	switch($type)
	{
		case 'graphic':
			if(!strpos($value,'://')) { $return['msg']="Формат URL-адреса неверен!"; $return['code']='-'; break; }
			if($db->query("UPDATE qmex_users SET photo='$value' WHERE ID=$uid"))
			{
				$_SESSION["avatar"]=$value;
				$return['msg']='Изображение успешно обновлено!'; 
				$return['code']='+';
			}
			break;
		case 'status':
			if($db->query("UPDATE qmex_users SET current_business='$value' WHERE ID=$uid"))
			{
				$_SESSION["current_business"] = (trim($value)!='') ? $value : "Несколько слов о делах...";
				$return['msg']='Информация обновлена!'; 
				$return['code']='+';
			}
			break;
		case 'caption':
			if($db->query("UPDATE qmex_users SET caption='$value' WHERE ID=$uid"))
			{
				$_SESSION["caption"] = $value;
				$return['msg']='Информация установлена!'; 
				$return['code']='+';
			}
			break;
		}		
		die( json_encode($return) );
	}
		
		
	function Voite($voiter)
	{		
		if(!isset($_SESSION['id']))
		die("<input type='hidden' value='voite-error' />
			 <script>show_wnd('Чтобы давать оценки, вам необходимо авторизоваться в системе qMex.',100,true,2.0,0,false);
			 </script>");
			
		if( !Security::isCurrentAccountChecked() )
		die("<input type='hidden' value='voite-error' />
			 <script>show_wnd('Чтобы давать оценки, вам необходимо подтвердить свою действительность.',100,true,2.0,0,false);
			 </script>");
			
		global $db;
		$this_user = $_SESSION["id"];
		$user = (int)$_POST["user"];
		$voite = (int)$_POST["voite"];
		$type = mb_strtolower(trim($_POST["type"]));
		$essence = isset($_POST["essence"])?$_POST["essence"]:0;
		if($voite>=1) $voite=1; else $voite=-1;
		if($type!='page' && $type!='note' && $type!='conf_opinion') die('');  // locker!
					
		$response = "";
					
		if($user==$_SESSION['id']) die("");
		
		$db->query("SELECT COUNT(*) FROM qmex_voites WHERE (Login=$user AND Who=$this_user AND Type='$type' AND Essence=$essence)");
		
		if($db->one(0)>0)
		die("<input type='hidden' value='voite-error' />
			 <script>show_wnd('Вы уже голосовали. Нельзя голосовать повторно.',100,true,2.0,0,false);
			 </script>");		

		$res = $db->query("INSERT INTO qmex_voites(Login,Who,Type,Essence,Voite) VALUES($user,$this_user,'$type',$essence,$voite)");
		if(!$res) return;
		
		switch($type)
		{
			case 'page': 
				Event::CreateEvent(Event::TYPE_PAGEVOITE,$user,0,$voite);
				$voite = ($voite==1) ? RATING::INTERESTING_PAGE : -RATING::INTERESTING_PAGE;
				Human::addRating($user, $voite, $db);
				$response = ($voite>0) ? "<div id='SucMsg' >Вы выразили свое почтение!</div>":
										  "<div id='ErrMsg'>Вам выразили свое неодобрение.</div>";
										  break;
			case 'note':
				Event::CreateEvent(Event::TYPE_HUBVOITE,$user,$essence,$voite);
				$voite = ($voite==1) ? RATING::NOTE_VOITE : -RATING::NOTE_VOITE;
				$db->query("SELECT SUM(Voite) FROM qmex_voites WHERE (Type='note' AND Essence=$essence)");
				$response = $db->one(0);
				$essence = $db->query("SELECT Theme FROM qmex_user_notes WHERE ID=$essence");
				$essence = $db->one(0);
				if($voite>0) 
					$res = $db->query("INSERT INTO qmex_voites(Login,Who,Type,Essence,Voite) VALUES($user,-1,'interest',$essence,1) ");
				Human::addRating($user, $voite, $db);
				break;
			
			case 'conf_opinion':
				$voite *= RATING::CONF_OPINION;
				Human::addRating($user, $voite, $db);
				break;
			}
		
		die($response);
		
	}	
?>