<?php

include_once("../../qmex_php_functions.php");
mb_internal_encoding("UTF-8");	
session_start();

function __autoload($class)
{
	$class = mb_strtolower($class);
	include_once("../../classes/$class.php");
	}

function is_empty($value)
{
	return trim($value)!='';
	}

if(!empty($_GET))
{
	if(isset($_GET['next']))			    Search();
	if(isset($_GET['get-proposals-list']))  CreateProposalsList($_GET['get-proposals-list']);
	}

function Search()
{
	// The request completing
	$q = str_replace('#','',trim(mb_strtolower($_GET['data'])));
	
	// This block prepares a correct
	$query_bits = explode('&',$q);
	$query = array();
	foreach($query_bits as $val)
	{
		if(trim($val)!='')
		{
		$bit = explode('=',$val);
		if(isset($bit[1])) $query[$bit[0]]=mysql_escape_string(trim(mb_strtolower(urldecode($bit[1]) )));
		}
	}
	
    ////// Initialization section here ////////
	$db = new db();
	$all_content = "";
	$limit = (int)$_GET["limit"];
	if(!$limit || $limit<10) $limit=10;
	$p_string=$p_login=$p_login=$p_name=$p_sename=$p_country=$p_city=$p_ints=$p_biz=$p_keywords="";
	$key_interest = 0;
	$p_sex=0;
	$p_f_age=0;
	$p_s_age=10000;
	////////////////////////////////////////////
	
	$search_type = 0;
	$user_n = 0;
	$friends = array();	
    if(isset($query['t'])){
		if($query['t']=='ipeople') { 
			$search_type=0xF; 
			$user_n=(int)$query['u']; 
			$db->query("SELECT a.ID2 FROM qmex_user_bookmarks AS a INNER JOIN qmex_user_bookmarks AS b 
						ON a.ID1=$user_n AND a.ID2=b.ID1 AND b.ID2=$user_n"); 
			if($db->rowCount()==0) $search_type=0xA;
			while($fellow = $db->one(0)) 
				array_push($friends,$fellow);
			array_unique($friends);
			$friends = implode(',',$friends);
			}
		}
		
	if(isset($query['q']))  $p_string = $query['q']; 
	                        $p_string_buffer =  array_unique(array_filter(explode(' ',str_replace(',',' ',$p_string)),'is_empty'));												
	if(isset($query['qid'])) $p_login = $query['qid'];
	if(isset($query['name']))   $p_name = $query['name'];
	if(isset($query['sename'])) $p_sename = $query['sename'];
	if(isset($query['f_age'])) $p_f_age = (int)$query['f_age'];
	if(isset($query['to_age'])) $p_s_age = (int)$query['to_age'];
	if(isset($query['sex'])) $p_sex = (int)$query['sex'];
	if(isset($query['country'])) $p_country = $query['country'];
	if(isset($query['city'])) $p_city = $query['city'];
	if(isset($query['biz'])) $p_biz = $query['biz'];
	if(isset($query['keywords'])) $p_keywords = mb_strtolower(trim($query['keywords']));
	if(isset($query['keyinterest'])) $key_interest = (int)$query['keyinterest'];
	if(isset($query['geo'])) {
		$geoInfo = Human::getGeoInfo(@$_SESSION['id']) ;
		$p_country = $geoInfo['country'];
		$p_city    = $geoInfo['city'];
		}
	
	
	if($p_f_age<1) $p_f_age=0;
	if($p_s_age<1) $p_s_age=10000;
	$curr_date = date("Y");          
	
	// SQL QUERY HERE
	$add = "";
	if($p_sex!=0) $add.="AND Sex=$p_sex";
	if($search_type==0xF) $add.=" AND ID IN (".$friends.")";
	      
	$JOIN = "";
	
	if($search_type==0xF)  $JOIN .= " INNER JOIN qmex_user_bookmarks t3 ON (t3.ID1=$user_n AND t3.ID2=t1.ID)"; 
		
	if($key_interest!=0 || $p_keywords!=''){
		$sq = array();
		if($key_interest!=0) array_push($sq,"t2.key_interest=$key_interest");
		if($p_keywords!='')  array_push($sq,"t2.interest_keywords LIKE '%$p_keywords%'");
		$sq = implode(" AND ",$sq);
		
		$JOIN .= "INNER JOIN qmex_user_interests t2 ON 
				  (( $sq ) AND t1.ID=t2.Login )"; 
		}
	  
	if(sizeof($p_string_buffer)>0){
		foreach($p_string_buffer as $val)
		{
			$gsub = "'%".$val."%'";
			$add .= " AND CONCAT(DescrWords,Login,Name,Sename,Profession) LIKE $gsub ";
		}
	}
	/* ----------------------- */	
		  
	$S = 1.5;  
	$geoCoords = isset($geoInfo) ? "( ((latitude BETWEEN ".($geoInfo['latitude']-$S)." AND ".($geoInfo['latitude']+$S).") AND 
									   (longitude BETWEEN ".($geoInfo['longitude']-$S)." AND ".($geoInfo['longitude']+$S).") ) OR 
									  (Country LIKE '%".$p_country."%' AND City LIKE '%".$p_city."%') ) " :
								   '1';
																	 
$db->query("
SELECT t1.Login,t1.Name,t1.Sename,t1.Sex,t1.Country,t1.City,t1.photo,t1.current_business,t1.Target,
t1.Profession,t1.About,t1.caption,t1.rating,t1.birth_year,t1.ID, t1.DescrWords, 
(SELECT row_number FROM(SELECT ID, @row := @row+1 AS row_number FROM qmex_users JOIN(SELECT @row:=0) r 
					  ORDER BY rating DESC ) AS c WHERE ID=t1.ID) 
					  AS RatingPosition
FROM ( 
SELECT Login,Name,Sename,Sex,Country,City,photo,current_business,Target,Profession,About,caption,rating,birth_year,ID,DescrWords 
FROM qmex_users 
WHERE Name Like '%$p_name%' AND Sename Like '%$p_sename%' AND ".$geoCoords." AND (caption LIKE '%$p_biz%' OR Profession LIKE '%$p_biz%') AND Login Like '%$p_login%' AND $curr_date-ABS(birth_year) BETWEEN $p_f_age AND $p_s_age ".$add." 
) t1 ".$JOIN." GROUP BY(Login) ORDER BY RatingPosition LIMIT ".($limit-10).", 10");
	
	//echo mysql_error();
	
	/*
	$db->query("SELECT t1.Login,t1.Name,t1.Sename,t1.Sex,t1.Country,t1.City,t1.photo,t1.current_business,t1.Target,t1.Profession,t1.About,t1.caption,t1.rating,t1.birth_year,t1.ID FROM qmex_users t1 JOIN qmex_user_interests t2 ON t1.Name Like '%$p_name%' AND t1.Sename Like '%$p_sename%' AND t1.Country Like '%$p_country%' AND t1.City Like '%$p_city%' AND t1.current_business Like '%$p_biz%' AND t1.Login Like '%$p_login%' AND $curr_date-ABS(t1.birth_year) BETWEEN $p_f_age AND $p_s_age ".$add." ORDER BY t1.rating DESC");*/

	//$db_lay = new db();	

	if($db->rowCount()>0)
	while($data = $db->one())
	{
		$id = $data[14];
		$login = get_not_escaped($data[0]);
		$all_ints="";
		$photo = $data[6];
		if(!mb_strstr($photo,'://')) $photo = "qmex_img/qmex_human.png";
		$login = get_not_escaped($data[0]);
		$login_m = make_normal($login,0,25);
		$current_business = htmlspecialchars($data[7]);
		$name = make_normal($data[1]);
		$sename = make_normal($data[2]);
		$city = make_normal($data[5]);
		$country = make_normal($data[4]);
		$target = (int)$data[8];
		$prof = $data[9];
		$about = get_not_escaped($data[10]);
		$caption = make_normal(get_not_escaped($data[11]));
		$rating = $data[12];
		$age = date("Y")-$data[13];
		$user_position = Human::getUserRatingPosition($id);
		
		$descr_words = explode(' ',$data[15]);

		$interests = new Interests();
		$interests->get_interests($id);
		$submit = array('login'=>$login,
						'login_m'=>$login_m,
						'target_id'=>$target,
						'photo'=>$photo,
						'current_business'=>$current_business,
						'name'=>$name,
						'sename'=>$sename,
						'country'=>$country,
						'city'=>$city,
						'caption'=>$caption,
						'raiting'=>$rating,
						'interests'=>$interests,
						'age'=>$age,
						'prof'=>$prof,
						'qmex_position'=>$user_position);
		
		/* Filter */
		foreach($submit as $k=>$v) 
		if(is_string($v)) $submit[$k] = htmlspecialchars($v);
		
		// Creation of user card //
		$one_user = new UserCard($submit);
		$all_content .= $one_user->createSearchCard(); 

		}
	echo $all_content;
	echo '<div class="q_pad_continue">Ещё >></div>';
	}
	
function CreateProposalsList($printed)
{
	if(trim($printed)=='') die(json_encode(array()));
	$printed = explode(' ',str_replace(',',' ',mysql_escape_string($printed)));
	$printed = mb_strtolower(trim($printed[sizeof($printed)-1]));
	
	$proposals = array();
	$db = new DB();
	$db->query("SELECT Login, DescrWords, Profession FROM qmex_users 
				WHERE Login LIKE '$printed%' OR DescrWords LIKE '%$printed%' OR Profession LIKE '%$printed%' LIMIT 15");
	while($data = $db->one())
	{
		$login = mb_strtolower($data[0]);
		$descr = explode(' ',$data[1]);
		$proof = mb_strtolower($data[2]);
		if(mb_strstr($login,$printed) && mb_strpos($login,$printed)==0) array_push($proposals, $login); else
		if(mb_strstr($proof,$printed)) array_push($proposals, $proof);
		foreach($descr as $val) 
		if(mb_strstr(mb_strtolower($val), $printed) &&
		   mb_strpos(mb_strtolower($val), $printed)==0) array_push($proposals, $val);
		}
		
	$proposals = array_unique(array_filter($proposals,'is_empty'));
	die( json_encode($proposals) );
	}