<?php

require_once("service/handlers_common.php");
$DB = new DB();

if(!empty($_GET))
{
	
}

else

if(!empty($_POST))
{
	if(isset($_POST['create-swop'])) 
		CreateSwop($_POST['create-swop'],$_POST['spectrum'], $_POST['theme'], $_POST['text'], $_POST['core']);
	if(isset($_POST['remove-swop'])) 
		RemoveSwop($_POST['remove-swop']);
	if(isset($_POST['link-conf-swop'])) 
		LinkConfSwop($_POST['link-conf-swop'], $_POST['swop-id']);
	if(isset($_POST['unlink-conf-swop'])) 
		UnLinkConfSwop($_POST['unlink-conf-swop']);
}


function UnLinkConfSwop($swop_id)
{
	global $DB;
	$swop_id = (int)$swop_id;
	$res = $DB->query("UPDATE qmex_conferences SET linked_with_type='' AND linked_with_id=0 
					   WHERE linked_with_type='swop' AND linked_with_id=$swop_id");
	die( $res ? '+':'-' );
	}

function LinkConfSwop($linked_conf_id, $swop_id)
{
	global $DB;
	$swop_id = (int)$swop_id;
	$linked_conf_id = (int)$linked_conf_id;
	$consumer = $_SESSION['id'];
	$DB->query("SELECT COUNT(*) FROM qmex_conferences WHERE id=$linked_conf_id AND creator=$consumer");
	if($DB->one(0)==0) die('!');
	$DB->query("SELECT COUNT(*) FROM qmex_conferences WHERE linked_with_id=$swop_id");
	if($DB->one(0)>0) die('~');
	$res = $DB->query("UPDATE qmex_conferences SET linked_with_type='swop', linked_with_id=$swop_id WHERE id=$linked_conf_id");
	die( $res ? '+':'-' );
	}

function CreateSwop($swop_type, $spectrum, $theme, $text, $swopcore)
{
	global $DB;
	$login     = (int)$_SESSION['id'];
	$swop_type = (int)$swop_type;
	$spectrum  = (int)$spectrum;
	$theme     = (int)$theme;
	$text      = mysql_real_escape_string(mb_substr(str_replace('\n','<br>',$text),0,255));
	$swopcore  = mysql_real_escape_string(mb_substr(trim($swopcore),0,50));
	
	if( !in_array($spectrum, array_values(Enum::$SWOP_SPECTRA)) || 
		!isset($_SESSION['interests']) || 
		( $swop_type==SWOP::SWOP_USER_PROVIDE && !in_array($theme, array_keys($_SESSION['interests']))) )  
	die( json_encode(array('result'=>'-')) );
	
	$DB->query("SELECT COUNT(*) FROM qmex_swop WHERE Login=$login AND SwopType=$swop_type");
	if((int)$DB->one(0) >= SWOP::$RESTRICTIONS[$swop_type]) 
	die( json_encode(array('result'=>'!')) );
	
	$DB->query("SELECT COUNT(*) FROM qmex_swop WHERE Login=$login AND SwopType=$swop_type AND 
				ServiceType=$spectrum AND Specialization=$theme");
	if($DB->one(0)>0) 
	die(json_encode(array('result'=>'*')) );

	$res = $DB->query("INSERT INTO qmex_swop(Login, SwopType, ServiceType, Specialization, Definition, SwopCore) 
					   VALUES($login, $swop_type, $spectrum, $theme, '$text', '$swopcore' )");
	$swop_id = SWOP::GetLastUserSwop($login);
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	$DB->query("SELECT Login FROM qmex_swop WHERE SwopType=".
	( $swop_type==SWOP::SWOP_USER_PROVIDE ? SWOP::SWOP_USER_NEED : SWOP::SWOP_USER_PROVIDE )." AND ServiceType=$spectrum AND 	
	Specialization=$theme");
	$inserts = array();
	while($responder = $DB->one(0)) 
		array_push($inserts, "INSERT INTO qmex_events(Type,Login,Maker,Essence,Action) 
							  VALUES(".EVENT::TYPE_SWOP_OCCURED.",$responder,$login,$swop_id,0)");
	foreach($inserts as $insert) $DB->query($insert);
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	if($res){
		$created_swop = SWOP::CreateSwopById($swop_id,true);
		die( json_encode(array('swop'=>$created_swop,'result'=>'+')) );
		}
				
	die( json_encode(array('result'=>'-')) );
	
	}
	
	
function RemoveSwop($sid)
{
	global $DB;
	$sid = (int)$sid;
	$login = (int)$_SESSION['id'];
	$DB->query("SELECT COUNT(*) FROM qmex_swop WHERE Login=$login AND ID=$sid");
	if($DB->one(0)==0) die('-');
	
	$res = $DB->query("DELETE FROM qmex_swop WHERE ID=$sid");
	die( $res ? '+' : '-' );
	}

?>