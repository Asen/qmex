<?php

require_once("service/handlers_common.php");
$db = new DB();

function filter($a)
{
	return (trim($a)!='') ? true:false;
	}

if(!empty($_GET))
{
if(isset($_GET["ui_settings"])) GetUserSettings($_GET["ui_settings"]);

if(isset($_GET["get_dialog_messages_count"])) GetDialogMessageCount($_GET["get_dialog_messages_count"]);
if(isset($_GET["ui_start_info"])) GetStartInfo($_GET["ui_start_info"]);
//if(isset($_GET["ui_i_peoples"])) GetNewPeopleCount();
if(isset($_GET["complete-message-for-dialog"])) CompleteMessage($_GET["id"]);
if(isset($_GET["get_i"])) GetKWInterests($_GET["key_i"]);
if(isset($_GET["last_note"])) GetLastNote();
if(isset($_GET["get_note_content"])) GetNoteContent($_GET["get_note_content"]);
if(isset($_GET["last_comment"])) GetLastComment($_GET['attachment']);
}

else

if(!empty($_POST))
{
	
if(isset($_POST["remove_event"])) RemoveEvent($_POST["id"]);	
if(isset($_POST["remove_event_all"])) RemoveEventAll();	
	
if(isset($_POST["ui_close_dialog"])) CloseDialog($_POST["ui_close_dialog"]);
if(isset($_POST["ui_fast_answer"])) FastMsgSender($_POST["answer_of"], $_POST["ui_msg"]);
if(isset($_POST["ui_exp_msg"])) 
ExpandedMsgSender($_POST["ui_exp_msg"], $_POST["to"], $_POST["cause"], $_POST["essence"], $_POST["answer_of"]);
if(isset($_POST["ui_msg_act"])) ActMsg($_POST["ui_msg_act"], $_POST["type"]);
if(isset($_POST["proposal"])) Proposal($_POST["uid"]);
if(isset($_POST["proposalRemove"])) ProposalRemove($_POST["uid"]);
if(isset($_POST["confirm_interest"])) ConfirmInterest($_POST['idi'],$_POST['uid']);
//if(isset($_POST["check_man"])) CheckFriend($_POST["check_man"],true);
//if(isset($_POST["refurse_man"])) CheckFriend($_POST["refurse_man"],false);
if(isset($_POST["note_case"])) MakeNote( $_POST["note_case"],$_POST["mean_case"],
										 $_POST["substance"],$_POST["theme"],
										 $_POST["type"],$_POST['ext'],$_POST['additional'] );

if(isset($_POST["remove_note"])) ActionNote($_POST["remove_note"],1);
if(isset($_POST["visible_note"])) ActionNote($_POST["visible_note"],2);
if(isset($_POST["invisible_note"])) ActionNote($_POST["invisible_note"],3);
if(isset($_POST["note_reg_ready"])) ActionNote($_POST["note_reg_ready"],4,$_POST["tb_content"],$_POST["tb_caption"],$_POST["tb_substance"]);
if(isset($_POST["make_note_intent"])) MakeIntent($_POST["make_note_intent"]);	
if(isset($_POST["curr_note_intent"])) RemoveIntent();	
if(isset($_POST["add_note_to_favorites"])) AddNoteToFavorite($_POST["add_note_to_favorites"]);	
if(isset($_POST["rem_note_from_favorites"])) RemNoteFromFavorite($_POST["rem_note_from_favorites"]);	

/*
if(isset($_POST["quer"])) QuerCreateNew($_POST);
if(isset($_POST["quer_delete"])) QuerDelete($_POST['quer_delete']);
if(isset($_POST["quer_to_draft"])) QuerDraft($_POST['quer_to_draft'],1);
if(isset($_POST["quer_from_draft"])) QuerDraft($_POST['quer_from_draft'],0);
*/

if(isset($_POST["post_comment"])) NewComment($_POST['type'],$_POST['text'],$_POST['attach']);
if(isset($_POST["delete-comment"])) RemComment($_POST['type'],$_POST['id'],$_POST['attach']);
if(isset($_POST["edit-comment"])) EditComment($_POST['id'],$_POST['text']);
if(isset($_POST["answer-comment"])) AnswerComment($_POST['id'],$_POST['text']); 
if(isset($_POST["quer_become_responder"]))   BecomeResponder($_POST["quer_become_responder"]);
if(isset($_POST["quer_refuse_responder"]))   RefuseResponder($_POST["quer_refuse_responder"]); 
if(isset($_POST["quer_get_responderslist"])) RespondersList($_POST["quer_get_responderslist"]);  

}
	
function CloseDialog($dialog)
{
	$dialog = (int)$dialog;
	$iam = $_SESSION['id'];
	global $db;
	$db->query("SELECT COUNT(*) FROM qmex_dialogs WHERE Member1=$iam OR Member2=$iam");
	if($db->one(0)>0){
		$res = $db->query("DELETE FROM qmex_dialogs WHERE id=$dialog");
		if($res) $res = $db->query("DELETE FROM qmex_dialogs_msg WHERE dialog_id=$dialog");
		if($res) die('+');
		}
	}
	
function RemNoteFromFavorite($id)
{
	$id = (int)$id;
	if(!isset($_SESSION['id'])) die('-');
	$user = $_SESSION['id'];
	global $db;
	$res = $db->query("DELETE FROM qmex_favorites WHERE Login=$user AND Type='note' AND Essence=$id");
	echo $res ? '+' : '-'; 
	}
	
function AddNoteToFavorite($id)
{
	$id = (int)$id;
	if(!isset($_SESSION['id'])) die('-');
	$user = $_SESSION['id'];
	global $db;
	$db->query("SELECT ID FROM qmex_favorites WHERE Login=$user AND Type='note' AND Essence=$id");
	if($db->rowCount()>0) die("-");
	$res = $db->query("INSERT INTO qmex_favorites(Login,Type,Essence) VALUES($user,'note',$id)");
	echo $res ? '+' : '-'; 
	}
	
function MakeIntent($noteId)
{
	$noteId = (int)$noteId;
	$user = $_SESSION['id'];
	global $db;
	$db->query("SELECT COUNT(*) FROM qmex_user_notes WHERE ID=$noteId AND Type=".Enum::$CONCEPTIONS['INTENT']." AND Login=$user");
	if( $db->one(0)==0 ) die("-");
	$res = $db->query("UPDATE qmex_users SET Target=$noteId WHERE ID=$user");
	if($res) $_SESSION["target"]=$noteId;
	echo ($res) ? '+':'-'; 
	}
	
function RemoveIntent()
{
	$user = $_SESSION['id'];
	global $db;
	$res = $db->query("UPDATE qmex_users SET Target=-1 WHERE ID=$user");
	if($res) $_SESSION["target"]=-1;
	echo ($res) ? '+':'-'; 
	}
	
function RemoveEvent($id)
{
	global $db;
	$login = $_SESSION['id'];
	$db->query("SELECT ID FROM qmex_events WHERE ID=$id AND Login=$login");
	if($db->rowCount()==0) {echo 0; return;}
	$res = $db->query("DELETE FROM qmex_events WHERE ID=$id"); 
	echo ($res) ? 1:0; 
	}
	
function RemoveEventAll()
{
	global $db;
	$login = $_SESSION['id'];
	$res = $db->query("DELETE FROM qmex_events WHERE Login=$login");
	echo ($res) ? 1:0; 
	}
	
function AnswerComment($id, $text)
{
	if(!isset($_SESSION['id'])) die('Необходимо войти в систему qMex.');
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	$id = (int)$id;
	if($id<0) exit;
	$text=trim(mysql_escape_string($text));
	
	if(mb_strlen($text)<5)   die('Ответ на комментарий слишком короткий');
	if(mb_strlen($text)>600) die('Ответ на комментарий слишком длинный');
	
	global $db;
	
	$res = $db->query("INSERT INTO qmex_comments(Owner,Attachment,Type,Text) VALUES($user,$id,'comment','$text')");
	if($res) {
			$db->query("SELECT Owner FROM qmex_comments WHERE ID=$id");
			$owner = $db->one(0);
			$db->query("SELECT ID FROM qmex_comments WHERE Owner=$user ORDER BY ID DESC LIMIT 1");
			$COID = $db->one(0);
			if($user!=$owner) Event::CreateEvent(Event::TYPE_COMMENT_ANSWER,$owner,$id,$COID);
			echo '+';
		}
	}
	

function EditComment($id, $text)
{
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	$id = (int)$id;
	if($id<0) exit;
	
	if(mb_strlen($text)<5) die('Комментарий слишком короткий');
	if(mb_strlen($text)>600) die('Комментарий слишком длинный');
	$text=trim(mysql_escape_string($text));
	
	global $db;
	
	$db->query("SELECT ID FROM qmex_comments WHERE ID=$id AND Owner=$user");
	if($db->rowCount()<=0) exit;
	
	$res = $db->query("UPDATE qmex_comments SET Text='$text' WHERE ID=$id");
	if($res) echo "<input type='hidden' tag='success-editing'>".str_replace("\\n","<br>",splitter(htmlspecialchars($text),70,0,false));
	}
	

function GetLastComment($attachment)
{
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	global $db;
	$inner_db = new db();
	$db->query("SELECT ID FROM qmex_comments WHERE Owner=$user ORDER BY ID DESC LIMIT 1");
	$data = $db->one();
	$comment_id=$data[0];
	echo '<div class="comment-abstraction">'.comment::getComment($comment_id,$attachment).'</div>';
	
	}


function RemComment($type,$qid,$attachment)
{
	
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	$qid = (int)$qid;
	if($qid<0) exit;
	global $db;
	
	$db->query("SELECT ID FROM qmex_comments WHERE Type='$type' AND Attachment=$attachment AND ID=$qid AND Owner=$user");
	if($db->rowCount()<=0) exit;
	
	$res = $db->query("DELETE FROM qmex_comments WHERE ID=$qid");
	if($res) {	
		
		function recursiveRemove($cid){		
			$dba = new db();	
			$cid=(int)$cid;
			$dba->query("DELETE FROM qmex_comments WHERE ID=$cid");			
			$dba->query("SELECT ID FROM qmex_comments WHERE Attachment=$cid AND Type='comment'");
			while($comment_id = $dba->one(0)) recursiveRemove($comment_id);	
			}
		recursiveRemove($qid);
					
	}
	if(trim($db->lastError())=='') echo '+'; else echo $db->lastError();
	
	}


function NewComment($type,$text,$attach_to)
{
	if(!isset($_SESSION['id'])) die('Необходимо войти в систему qMex.');
	$user = (int)@$_SESSION['id'];
	if($user<=0) exit;
	
	global $db;
	
	$text = trim(mysql_escape_string($text));
	$attach_to = (int)$attach_to;
	
	if(mb_strlen($text)<5) die('Комментарий слишком короткий');
	if(mb_strlen($text)>600) die('Комментарий слишком длинный');
	
	$res = $db->query("INSERT INTO qmex_comments(Owner,Attachment,Type,Text) VALUES($user,$attach_to,'$type','$text')");
	if($res){
		$db->query("SELECT ID FROM qmex_comments WHERE Owner=$user ORDER BY ID DESC LIMIT 1");
			$COID = $db->one(0);
		if($type==Comment::NOTE_COMMENT) {
			$db->query("SELECT Login FROM qmex_user_notes WHERE ID=$attach_to");
			$owner = $db->one(0);
			if($user!=$owner) Event::CreateEvent(Event::TYPE_NOTE_COMMENT,$owner,$attach_to,$COID);
			}
		}
	if(!$res) die('Произошла неизвестная ошибка при добавлении комментария.');
	}

function BecomeResponder($quer_id)
{
	$quer_id = (int)$quer_id;
	if(!isset($_SESSION['id'])) die('');
	$user = (int)$_SESSION['id'];
	global $db;
	$db->query("SELECT COUNT(*) FROM qmex_quer_responders WHERE quer_id=$quer_id AND user=$user");
	if($db->one(0)>0) die('-');
	$result = $db->query("INSERT INTO qmex_quer_responders(quer_id, user) VALUES($quer_id, $user)");
	die($result ? '+' : '');
	}
	
function RefuseResponder($quer_id)
{
	$quer_id = (int)$quer_id;
	if(!isset($_SESSION['id'])) die('');
	$user = (int)$_SESSION['id'];
	global $db;
	$result = $db->query("DELETE FROM qmex_quer_responders WHERE quer_id=$quer_id AND user=$user");
	die($result ? '+' : '');
	}

function RespondersList($quer_id)
{
	$quer_id = (int)$quer_id;
	if(!isset($_SESSION['id'])) die('');
	$user = (int)$_SESSION['id'];
	global $db;
	$db->query("SELECT user FROM qmex_quer_responders WHERE quer_id=$quer_id");
	while($id = $db->one(0))
	{
		$img = '<img src="'.Human::getUserPhoto($id).'" width="30px" />';
		$name = Human::getLoginById($id);
		$link = '/profile?id='.$name;
		echo '<a href="'.$link.'">
			  <table cellspacing="3px">
			  <tr>
			  <td>'.$img.'</td>
			  <td><a href="'.$link.'">'.$name.'</a></td>
			  </tr>
			  </table>
			  </a>';
		}
	}

/*
function QuerDraft($id,$draft)
{
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	$id = (int)$id;
	global $db;
	
	// Security of Update
	$db->query("SELECT ID FROM qmex_quer WHERE ID=$id AND Owner=$user");
	if($db->rowCount()<=0) exit;
	
	$db->query("UPDATE qmex_quer SET is_draft=$draft WHERE ID=$id AND Owner=$user");
	}

function QuerDelete($id)
{
	$user = (int)@$_SESSION['id'];
    if($user<=0) exit;
	
	$id = (int)$id;
	global $db;
	
	// Security of Update
	$db->query("SELECT ID FROM qmex_quer WHERE ID=$id AND Owner=$user");
	if($db->rowCount()<=0) exit;
	
	$db->query("DELETE FROM qmex_quer WHERE ID=$id AND Owner=$user");
	}

function QuerCreateNew($p)
{
	$user = (int)@$_SESSION['id'];
    if($user<=0) die("<script>show_wnd('Необходимо авторизоваться в системе!',100,true,2,'#ff5555',false)</script>");
	
	global $db;
	
	$id = NULL;
	$type = (int)$_POST['type'];
	
	// Security of Update
	if($type==1) 
	{
		$id = (int)$_POST['id'];
		$db->query("SELECT ID FROM qmex_quer WHERE ID=$id AND Owner=$user");
		if($db->rowCount()<=0) exit;
	}
	
	// Data get
	$caption = trim(mysql_escape_string($p['caption']));
	$content = trim(mysql_escape_string(format($p['content'])));
	$theme = (int)$p['theme'];
	$keywords = trim(mysql_escape_string($p['keywords']));
	$keywords = implode("|",array_filter(explode(" ",str_replace(","," ",$keywords)),"filter"));
	$city = trim(mysql_escape_string($p['city']));
	$country = trim(mysql_escape_string($p['country']));
	$actual_to = (int)trim($p['actual_t']);
	$actual_date_expired = time()+$actual_to*86400;
	$age_from = (int)trim($p['age_from']);
	$age_to = (int)trim($p['age_to']);
	$image = trim(mysql_escape_string($p['image']));
	
	if($caption!="" && $content!="" && $theme>0 && mb_strlen($keywords)>=1)
	if($age_from=='' or ($age_from<=150 && $age_to<=150) )
	if($actual_to>=3 && $actual_to<=180)
	{
		if($age_from>$age_to && $age_to>0) exit;
		switch($type)
		{
		case 0: $res = $db->query("INSERT INTO qmex_quer(Owner,Caption,Text,Theme,Keywords,Country,City, Age_From, Age_To, Image, ActualTo) VALUES('$user','$caption','$content',$theme,'$keywords','$country','$city',$age_from,$age_to,'$image',$actual_date_expired)");
		        echo ($res) ?  "<div id='SucMsg' style='padding:3px'>Запрос успешно создан.</div>":
		                       "<div id='ErrMsg'>Возникла ошибка при добавлении запроса...</div>";
							   break;
		case 1: $res = $db->query("UPDATE qmex_quer SET Caption='$caption', Text='$content', Theme=$theme, Keywords='$keywords', Country='$country', City='$city', Age_From=$age_from, Age_To=$age_to, Image='$image' WHERE ID=$id");
		        echo ($res) ?  "<div id='SucMsg' style='padding:3px'>Запрос успешно обновлен.</div>":
		                       "<div id='ErrMsg'>Возникла ошибка при обновлении запроса...</div>";
							   break;
		}
		} else echo "<div id='ErrMsg'>Некоторые данные указаны неверно! Убедитесь, что все поля заполнены в соответствии с правилами.</div>";
	}

*/
//////////////////////////// USER SETTINGS ////////////////////////////


function GetUserSettings($user)
{
	$settings = array();
	/// DEFAULTS ///
	$background = array();
	$background["type"] = "0";
	$background["b"] = "#DAF0FA";	
	$background_style = 1;
	$opacity = 1;
	
	$user = (int)$user;
	if(isset($_SESSION['id']))
	{
	$this_user = (int)($_SESSION['id']);
	if($user=='') $user = $this_user;
	}
	
	if($user!='')
	{
	global $db;
	$db->query("SELECT background, background_style, opacity, background_type FROM qmex_user_settings WHERE Login=$user");
	if($db->rowCount()>0)
	{
	$data = $db->one();
	if(trim($data[0])!='') $background["b"] = $data[0];
	if(trim($data[1])!='') $background_style = (int)$data[1];
	if(trim($data[2])!='') $opacity = number_format(floatval($data[2]),3,'.','');
	if(trim($data[3])!='') $background["type"] = (int)$data[3];
	}
	}
	
	$settings['bg'] = $background; 
	$settings['bg_style'] = $background_style;
	$settings['opacity'] = $opacity;
	echo json_encode($settings);
	}

///////////////////////////////////////////////////////////////////////

function GetLastNote()
{
	$id = (int)$_SESSION['id'];
	if($id>-1 && $_SESSION['login']!='><')
	{	
	global $db;
	$db->query("SELECT ID FROM qmex_user_notes WHERE Login=$id ORDER BY ID DESC LIMIT 1");
	if($db->rowCount()>0)
	{
		$id= $db->one(0);	
		echo  Note::showNote($id).
			 '<input type="hidden" id="note-identifier" value="'.$id.'" />';
	}
	}
	}
	
function GetNoteContent($ID)
{
	$ID = (int)$ID;
	global $db;
	$db->query("SELECT Note FROM qmex_user_notes WHERE ID=$ID");
	echo $db->one(0);
	}

function ActionNote($id, $t, $content = "", $caption = "", $substance="")
{
	$this_user = (int)$_SESSION['id'];
	$id = (int)$id;
	$content = mysql_real_escape_string(format($content));
	$caption = mysql_real_escape_string($caption);
	$substance = mysql_real_escape_string($substance);
	if(is_numeric($id))
	{
		global $db;
		$res = $db->query("SELECT * FROM qmex_user_notes WHERE ID=$id AND Login=$this_user");
		if($db->rowCount($res)>0)
		{
		switch($t)
		{
		case 1: $db->query("DELETE FROM qmex_user_notes WHERE ID=$id");
		        $db->query("DELETE FROM qmex_voites WHERE type='note' AND essence=$id");
				$db->query("DELETE FROM qmex_events WHERE Type=".EVENT::TYPE_HUB_OCCURED." AND Essence=$id");
				break;
		case 2: $db->query("UPDATE qmex_user_notes SET Is_Private=1 WHERE ID=$id");
				break;
		case 3: $db->query("UPDATE qmex_user_notes SET Is_Private=0 WHERE ID=$id");
				break;
		case 4: if(mb_strlen(strip_tags($content))<5 || mb_strlen($substance)<10) die("info_error");
		 		$updated_at = time();
		        $db->query("UPDATE qmex_user_notes SET Note='$content', Meaning='$caption', Substance='$substance', ".
				"updated_at=$updated_at WHERE ID=$id");
				break;
		}
	    }
	}
	}

function MakeNote($t,$m,$substance,$theme,$type,$ext,$additional)
{
	if(!isset($_SESSION['id']) || (int)$_SESSION['id']<0) 
	die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>Необходимо авторизоваться! </div>");
	
	$creator = (int)$_SESSION['id'];
	global $db;
	
	if(!ProcessTags($t)) 
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>Обнаружен некорректный контент!</div>");
	$note = mysql_escape_string(trim(format($t)));
	$len = mb_strlen(strip_tags($note));
	$mean = mb_substr(mysql_escape_string(trim($m)),0,50);
	$substance = mb_substr(mysql_escape_string(trim($substance)),0,150);
	$theme = (int)$theme;
	$type = (int)$type;
	$ext = (int)$ext;
	$updated_at = time();
	if(mb_strlen($substance)<3) 
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>
			 Необходимо сформулировать основную суть хаба.</div>");
	if($theme<1) 
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>
			 Необходимо указать, к какому из ваших интересов относится хаб</div>");
	if($type<1) 
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>Необходимо указать тип записи</div>");
	if( !isset($_SESSION['interests']) || !in_array($theme, array_keys($_SESSION['interests'])) ) 
	{	
		$IBASE = new Interests();
		$i = $IBASE->SelectName($theme);
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>
		<b>".$i."</b> не входит в спектр ваших специализаций.</div>");	
	}
	
	if($len<11) 
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>
		Хаб слишком короткий! (меньше 10 символов)</div>");
	if($len>=32768)
		die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'Хаб слишком длинный!</div>");


	$res = $db->query("INSERT INTO qmex_user_notes(Login,Note,Meaning,Substance,Theme,Type,Connection,updated_at) 	
					   VALUES($creator,'$note','$mean','$substance',$theme,$type,$ext,$updated_at)");
					   
	if($res)
	{
		$hub_id = $db->query("SELECT ID FROM qmex_user_notes WHERE Login=$creator ORDER BY ID DESC LIMIT 1");
		$hub_id = $db->one(0);
	
		if($type==Enum::$CONCEPTIONS['INTENT'] && (int)$additional==1 && 
		   $db->query("UPDATE qmex_users SET Target=(SELECT ID FROM qmex_user_notes 
					   WHERE Login=$creator ORDER BY ID DESC LIMIT 1)
					   WHERE ID=$creator")) $_SESSION["target"] = Human::getUserTarget($creator);
					   
		////////////////////////////////////////////////////////////////////////////////////////////////
		$db->query("SELECT u.ID FROM qmex_users AS u INNER JOIN qmex_user_interests AS i 
					ON i.Login=u.ID AND (NOT u.ID=$creator) AND i.key_interest=$theme");
		$inserts = array();
		while($responder = $db->one(0)) 
			array_push($inserts, "INSERT INTO qmex_events(Type,Login,Maker,Essence,Action) 
								  VALUES(".EVENT::TYPE_HUB_OCCURED.",$responder,$creator,$hub_id,0)");
		foreach($inserts as $insert) $db->query($insert);
		////////////////////////////////////////////////////////////////////////////////////////////////		
					   
		die("<div id='SucMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>Хаб создан!</div>");
	}
					  					   
	die("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px; padding-bottom:2px;'>Возникла непредвиденная ошибка!</div>");
	
}

/*
function CheckFriend($friend,$checked)
{
	global $db;
	$this_user = $_SESSION['id'];
	$friend=(int)$friend;
	$data=$db->query("SELECT Login FROM qmex_users WHERE ID=$friend");
	if($db->rowCount($data)>0)
	{
	$db->query("SELECT ID FROM qmex_user_friends_tmp WHERE ID2=$friend AND ID1=$this_user");
	if($db->rowCount($data)>0)
	{
	if($checked==true) {
		$res = $db->query("INSERT INTO qmex_user_friends(ID1,ID2) VALUES($this_user,$friend)");
	    if($res) $res = $db->query("DELETE FROM qmex_user_friends_tmp WHERE ID1=$this_user AND ID2=$friend");
	    if($res) echo '<script>$("#ui_acc_'.$friend.'").fadeOut("slow")</script>';
	}
	if($checked==false){
		$res = $db->query("DELETE FROM qmex_user_friends_tmp WHERE ID1=$this_user AND ID2=$friend");
		if($res)
		echo '<script>
		$("#ui_acc_'.$friend.'").hide(1000);
		show_wnd("Заявка отклонена!",100,true,2,"#ff5555",false);
		</script>';
		}
	}
	}
	}*/
	

function Proposal($uid)
{
	if( !Security::isCurrentAccountChecked() ) die('*');
	
	global $db;
	$uid = (int)$uid;
	$id = (int)@$_SESSION['id'];
	if($uid!=$id)
	{
		$db->query("SELECT ID FROM qmex_user_bookmarks WHERE ID1=$id AND ID2=$uid");
		if($db->rowCount()>0) die('0');
		$db->query("SELECT ID FROM qmex_user_bookmarks WHERE ID1=$id");
		if($db->rowCount()>=50) die('-');
		$res=$db->query("INSERT INTO qmex_user_bookmarks(ID1,ID2) VALUES($id,$uid)");
		if($res) {
			Human::addRating($uid, RATING::INTERESTING_USER, $db);
			Event::CreateEvent(Event::TYPE_ADD_TO_INTERESTS,$uid);
	        echo "1";
			
			$db->query("SELECT id FROM qmex_user_bookmarks WHERE ID1=$uid AND ID2=$id");
			if( $db->rowCount() == 1 ){
					Conference::EnvRoutine($id, $uid);
					Conference::EnvRoutine($uid, $id);
				}
			
		}
	}
}

function ProposalRemove($uid)
{
	global $db;
	$uid = (int)$uid;
	$id = (int)@$_SESSION['id'];
	$db -> query("SELECT ID FROM qmex_user_bookmarks WHERE ID1=$id AND ID2=$uid");
	if($db->rowCount()==0) return;
	$res = $db->query("DELETE FROM qmex_user_bookmarks WHERE ID1=$id AND ID2=$uid");
	if($res){
		Human::addRating($uid, -RATING::INTERESTING_USER, $db);
		Event::CreateEvent(Event::TYPE_DEL_FROM_INTERESTS,$uid);
		echo 1;
		}
	}


function ExpandedMsgSender($msg, $to, $cause, $essence, $answer_of)  // it is expanded message answer 
{
	if((int)$to<0) die('-');
	
	global $db;
	$iam = $_SESSION["id"];
	$to = (int)$to;
	$cause = (int)$cause;
	$essence = (int)$essence;
	$answer_of = (int)$answer_of;
	
	echo Communication::MakeConnection($to, $msg, $cause, $essence, $answer_of);
}

function FastMsgSender($answer_of, $msg) // answer on sent message in last by sender!
{
	global $db;
	if(!isset($_SESSION["id"])) exit();
	$iam = $_SESSION["id"];
	$answer_of = (int)$answer_of;
	$res = $db->query("SELECT msg.Creator, msg.dialog_id, dialogs.Cause, dialogs.EssenceID 
					   FROM qmex_dialogs_msg AS msg INNER JOIN qmex_dialogs AS dialogs ON dialogs.id=msg.dialog_id AND msg.id=$answer_of");
	if($db->rowCount()>0)
	{
		$cont = $db->one();
		$to = $cont[0];
		$dialog_id = $cont[1];
		$cause = $cont[2];
		$essence = $cont[3];
		
		echo Communication::MakeConnection($to,$msg,$cause,$essence,$answer_of);
		
		} else echo("<script>show_wnd('Ошибка.',100,true,2.5,'red');</script>");
}


function GetDialogMessageCount($dialog_id)
{
	if(isset($_SESSION["id"]))
	{
		global $db;
		$msgs = $db->query("SELECT COUNT(*) FROM qmex_dialogs_msg WHERE dialog_id=$dialog_id");
		die($db->one(0));
	}
	}

function GetStartInfo($dialog_id)
{
	if(isset($_SESSION["id"]))
	{
	global $db;
	$INFO = array();
	$array = array();
	$iam = $_SESSION["id"];
	$FROM_U = -1;
	$msgs = $db->query("SELECT COUNT(*) FROM qmex_dialogs_msg 
						WHERE IsRead=0 AND NOT Creator=$iam AND 
						dialog_id IN (SELECT id FROM qmex_dialogs WHERE Member1=$iam OR Member2=$iam)");
	$INFO['messages'] = (int)$db->one(0);
	$db->query("SELECT COUNT(*) FROM qmex_events WHERE Login=$iam");
	$INFO['events'] = (int)$db->one(0);
	$db->query("SELECT cash FROM qmex_users WHERE ID=$iam");
	$INFO['coins'] = (float)$db->one(0);
	$INFO['from_id'] = (int)$FROM_U;
	echo json_encode($INFO);
	}
	
	}
	
function CompleteMessage($dialog_id)
{
	global $db;
	$dialog_id = (int)$dialog_id;
	$db->query("SELECT id FROM qmex_dialogs_msg WHERE dialog_id=$dialog_id ORDER BY id DESC");
	die( Communication::ShowDialogMessage($db->one(0)) );	
	}

function ActMsg($id,$type)
{
	global $db;
	$id = (int)$id;
	$type = (int)$type;
	$iam = $_SESSION["id"];
	if($type==0) $res=$db->query("SELECT COUNT(*) FROM qmex_dialogs_msg WHERE id=$id AND Creator=$iam"); else
	if($type==1) $res=$db->query("SELECT COUNT(*) FROM qmex_dialogs_msg WHERE id=$id");
	if($type==2) $res=$db->query("SELECT COUNT(*) FROM qmex_dialogs_msg WHERE id=$id AND Thanked=0");
	if($db->one(0)>0)
	{
	$q = 1;
	switch($type)
	{
	case 0: $q = $db->query("DELETE FROM qmex_dialogs_msg WHERE id=$id");
	        die( $q==true ? '1':'0' );
	case 1: $q = $db->query("UPDATE qmex_dialogs_msg SET IsRead=1 WHERE id=$id");
	        die( $q==true ? '1':'0' );
	case 2: $cash = Human::getUserCash($iam, $db);
			if($cash<1) die('2');
			$db->query("SELECT Creator FROM qmex_dialogs_msg WHERE id=$id");
			$thank_user = $db->one(0);
			Human::addRating($thank_user, Rating::DIALOG_THANK, $db);
			Human::takeCash($iam, Rating::DIALOG_THANK, $db);
			$q = $db->query("UPDATE qmex_dialogs_msg SET Thanked=1 WHERE id=$id");
			Event::CreateEvent( Event::TYPE_DIALOGMSG_THANKED, $thank_user, $id, 1, $db );
			die('1');
	}
	}
	die('0');
	}

function GetKWInterests($interest_id)
{
	$interest_id = (int)$interest_id;
	global $db;
	$db->query("SELECT interest_keywords FROM qmex_user_interests WHERE key_interest=$interest_id");
	$return = array();
	for($i=$db->rowCount();$i>0;--$i)
	{
		$data=$db->one(0);
		$return = array_merge($return, explode(" ",str_replace(","," ",$data)) );
		}
	die( json_encode(array_filter(array_unique($return))) );
	}
	
function ConfirmInterest($idi,$uid)
{
	if( !Security::isCurrentAccountChecked() ) die('-');
	
	if(!isset($_SESSION["id"])) return;
	global $db;
	$this_user = $_SESSION["id"];
	$uid = (int)$uid;
	if($this_user==$uid) {echo 0; return;}
	$idi = (int)$idi;
	$db->query("SELECT ID FROM qmex_voites WHERE (Login=$uid AND Who=$this_user AND Type='interest' AND Essence=$idi)");
	if($db->rowCount()!=0) {echo -1; return;}
	$res = $db->query("INSERT INTO qmex_voites(Login,Who,Type,Essence,Voite) 
					   VALUES($uid,$this_user,'interest',$idi,1) ");
	if($res) {
		Human::addRating($uid, RATING::INTEREST_CONFIRM, $db);
		Event::CreateEvent(Event::TYPE_INTERESTVOITE,$uid,$idi);
		}
	echo ($res) ? 1:0;
	}
	
?>