<?php

session_start();
mb_internal_encoding("UTF-8");
include("../qmex_php_functions.php");
date_default_timezone_set("Europe/Moscow");

function __autoload($class)
{
	$class = mb_strtolower($class);
	include_once("../classes/$class.php");
	}

?>