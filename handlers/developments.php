<?php

require_once("service/handlers_common.php");
$db = new db();

if(isset($_GET))
{
	if(isset($_GET['get-proposals-list']))  CreateProposalsList($_GET['get-proposals-list']);
	if(isset($_GET['get-days-list']))  getDaysInMonth($_GET['year'],$_GET['month']);
	}
	
if(isset($_POST))
{ 	
	if(isset($_POST['create'])) createDevelopment($_POST);
	if(isset($_POST['turninto_member'])) BecameMember($_POST['turninto_member']);
	if(isset($_POST['change_cover_image'])) ChangeCoverImage($_POST['change_cover_image'],$_POST['dev']);
	if(isset($_POST['hand_achievement'])) HandAchievement($_POST['dev'],$_POST['login']);
	if(isset($_POST['express_thank'])) ExpressThank($_POST['dev'],$_POST['whom']);
	}
	
function ExpressThank($dev, $whom)
{	
	$dev = (int)$dev;
	$whom = (int)$whom;
	
	if(!Security::isAuthorized() || $whom==$_SESSION['id']) die('-');	
	if( in_array($whom, Development::getDevMembers($dev)) ) die('-');
	
	global $db;
	$db->query("SELECT COUNT(*) FROM qmex_voites WHERE Login=$whom AND Type='dev_thank' AND Essence=$dev");
	if($db->one(0)>0) die('-');
	if($db->query("INSERT INTO qmex_voites(Login,Type,Essence,Voite) VALUES($whom,'dev_thank',$dev,1)")){
		 Human::addRating($whom, Rating::DEV_THANK, $db);
		 die('+');
	}
	
	die('-');

	}
	
function getDaysInMonth($year, $month)
{
	$year = (int)$year;
	$month = (int)$month;
	die( ($year>0 && $month>0) ? (int)cal_days_in_month(0,$month,$year).'' : '0' );
	}
	
function HandAchievement($development, $login)
{
	if(!Security::isAuthorized()) 
		die( json_encode(array('result'=>"-","msg"=>'')) );
	
	global $db;	
	
	$user = $_SESSION['id'];
	$hand_to = Human::getIdByLogin($login);
		
	$db->query("SELECT COUNT(*) FROM qmex_developments WHERE id=$development AND creator=$user AND actual_time<=".qMexTime::getCurrent());
	if($db->one(0)==0) 
		die( json_encode(array('result'=>"-","msg"=>'!')) );
	
	$db->query("SELECT COUNT(*) FROM qmex_dev_members WHERE event_id=$development AND user=$hand_to");
	if($hand_to<0 || $db->one(0)==0)
		die( json_encode(array('result'=>"-","msg"=>'Этот пользователь не является участником конкурса.'.mysql_error())) );
	
	$db->query("SELECT bonus FROM qmex_achievements WHERE belong_to_dev=$development AND (NOT creator=$hand_to) AND belong_to_user=-1 ");
	if($db->rowCount()==0)
		die( json_encode(array('result'=>"-","msg"=>'Нельзя вручать достижение самому себе!')) );
		
	$bonus = $db->one(0);
		
	if( $db->query("UPDATE qmex_achievements SET belong_to_user=$hand_to WHERE belong_to_dev=$development") ){
			Human::addRating($hand_to, $bonus, $db);
			die( json_encode(array('result'=>"+","msg"=>'Вы вручили событие пользователю <b>'.$login.'</b>!')) );
		}
	
	}

function ChangeCoverImage($picture, $development)
{
    if(!Security::isAuthorized()) 
		die( json_encode(array('result'=>"-","msg"=>'')) );
	
	$picture = mysql_real_escape_string($picture);
	$development = (int)$development;
	$user = $_SESSION['id'];
	
	global $db;
	$db->query("SELECT COUNT(*) FROM qmex_developments WHERE id=$development AND creator=$user");
	if($db->one(0)>0) 
	die( $db->query("UPDATE qmex_developments SET picture='$picture' WHERE id=$development") ? '+':'-' );
	
	}


function createDevelopment($p)
{	
    if(!Security::isAuthorized()) 
		die( json_encode(array('result'=>"-","msg"=>'')) );
	
		
	qMexTime::setZone();
	
	
	function filter($a){ return (trim($a)!='') ? true:false; }
	
	// Data get
	$user = $_SESSION['id'];
	$caption = trim(mysql_real_escape_string($p['caption']));
	$content = trim(mysql_real_escape_string(format($p['content'])));
	$kind = (int)$p['kind'];
	$theme = (int)$p['theme'];
	$keywords = trim(mysql_real_escape_string($p['keywords']));
	$keywords = array_unique(array_filter(explode(" ",str_replace(","," ",$keywords)),"filter"));
	$city = trim(mysql_real_escape_string($p['city']));
	$country = trim(mysql_real_escape_string($p['country']));
	$latitude = (float)$p['latitude'];
	$longitude = (float)$p['longitude'];
	$image = trim(mysql_real_escape_string($p['image']));
	
	$year = (int)$p['year'];
	$month = (int)$p['month'];
	$day = (int)$p['day'];
	$hour = (int)$p['hour'];
	$minute = (int)$p['minute'];
	$duration = (int)$p['duration'];
	$actual_time = mktime($hour,$minute,0,$month,$day,$year);
	$finish = $actual_time + $duration*24*3600;
	
	$time_difference = $actual_time - qMexTime::getCurrent();
	
	$is_event_linked_with_conference = isset($p['linked_conference']);
	if($is_event_linked_with_conference)
	$linked_conference = (int)$p['linked_conference'];
	
	if( $kind == Enum::$DEVELOPMENT_TYPES['COMPETITION'] )
	{
		$achievement_descr = mysql_real_escape_string(trim($p['ach-descr']));
		$achievement_bonus = (int)$p['ach-bonus'];
	}
	
	if(mb_strlen($caption)<5 || mb_strlen(strip_tags($content))<5) 
		die( json_encode(array('result'=>'-', 'msg'=>'Описание события должно быть подробнее!')) );
	if(!mb_strstr($image,'://'))
		die( json_encode(array('result'=>'-', 'msg'=>'Необходимо выбрать корректное изображение события!')) );
	if(!in_array($kind, array_values(Enum::$DEVELOPMENT_TYPES)))
		die( json_encode(array('result'=>'-', 'msg'=>'Необходимо выбрать корректный тип события!')) );
	if($theme<1)
		die( json_encode(array('result'=>'-', 'msg'=>'Необходимо выбрать, с каким интересом связано событие!')) );
	if(sizeof($keywords)<3)
		die( json_encode(array('result'=>'-', 'msg'=>'Необходимо указать более 2 уникальных ключевых слов о событии!')) );
	if(trim($city)=='' || trim($country)=='')
		die( json_encode(array('result'=>'-', 'msg'=>'Необходимо установить место, для которого событие будет актуально!')) );
	if( $time_difference<3*3600 || $time_difference>6*30*24*3600 || !checkdate($month,$day,$year))
		die( json_encode(array('result'=>'-', 
		'msg'=>'Некорректное время события. 
		Время начала события должно быть не менее, чем через 3 часа и не более, чем через 6 месяцев от текущего момента.')) );
	if($duration<1 || $duration>150)
		die( json_encode(array('result'=>'-', 'msg'=>'Продолжительность события может быть в промежутке от 1 до 150 дней.')) );
	
	if( $kind == Enum::$DEVELOPMENT_TYPES['COMPETITION'] )
	{	
		if(mb_strlen($achievement_descr)<10)
			die( json_encode(array('result'=>'-', 'msg'=>'Информация о достижении слишком короткая!')) );	
		if($achievement_bonus<1 || $achievement_bonus > Human::getUserCash($user) )
			die( json_encode(array('result'=>'-', 'msg'=>'У вас не достаточно qCoins для создания такого большого бонуса.')) );
		/*if(max(2,$achievement_bonus/10) > Human::getUserRating($user))
			die( json_encode(array('result'=>'-', 'msg'=>'Недостаточно рейтинга, чтобы предоставлять такой большой бонус для достижения.')) );
		*/
	}
	
			
	$keywords = implode(' ', $keywords);	


	global $db;
	
	
	if($is_event_linked_with_conference)
	{
		$db->query("SELECT COUNT(*) FROM qmex_conferences WHERE id=$linked_conference AND creator=$user");
		if($db->one(0)<=0) 
			die( json_encode(array('result'=>"-","msg"=>'Обсуждение с таким № не существует либо вам не пренадлежит.')) );
			
		$db->query("SELECT COUNT(*) FROM qmex_conferences WHERE id=$linked_conference AND linked_with_type='event'");
		if($db->one(0)>0) 
			die( json_encode(array('result'=>"-","msg"=>'Обсуждение с таким № было создано для другого события.')) );	
		}
	
	$res = $db->query("
		INSERT INTO qmex_developments 
		(creator,caption,description,theme,keywords,country,city,latitude,longitude,picture,actual_time,finish,event_kind)
		VALUES($user,'$caption','$content',$theme,'$keywords','$country','$city',$latitude,$longitude,'$image',
			   $actual_time,$finish,$kind)");
			   
	if($res){
		$db->query("SELECT id FROM qmex_developments WHERE creator=$user ORDER BY id DESC LIMIT 1");
		$last_development_id = $db->one(0);	
	}
	
	if($res)
	if(!$is_event_linked_with_conference) {
		$content = strip_tags(trim(strip_tags(trim($content))));
		$res = $db->query("INSERT INTO qmex_conferences(creator,description,is_private,plan_start,vector_interest,words,img,last_activity) 
					  	   VALUES($user,'".$content."',0,-1,$theme,'$keywords','$image',".qMexTime::getCurrent().')');
		if($res){
			$db->query("SELECT id FROM qmex_conferences WHERE creator=$user ORDER BY id DESC LIMIT 1");
			$db->query("UPDATE qmex_conferences SET linked_with_id=$last_development_id, linked_with_type='event' 
	             	    WHERE id=".(int)$db->one(0));
		}
	} elseif($is_event_linked_with_conference){
						
		$res = $db->query("UPDATE qmex_developments SET belong_to_conf=$linked_conference WHERE id=$last_development_id");
		
		}		
		
	
	if($res)
	if( $kind == Enum::$DEVELOPMENT_TYPES['COMPETITION'] )
	{
		$ach_kind = Achievement::TYPE_DEV;
		$res = $db->query("INSERT INTO qmex_achievements(description,interest,bonus,creator,belong_to_dev,ach_kind) 
						   VALUES('$achievement_descr',$theme,$achievement_bonus,$user,$last_development_id,$ach_kind)");
		if($res) Human::takeCash($user, $achievement_bonus, $db);
		}
	 
			   
	die( json_encode( $res ? array('result'=>"+","msg"=>'Событие успешно создано!','id'=>$last_development_id) : 
			   array('result'=>"-","msg"=>'Произошла неизвестная ошибка. Повторите попытку') 
			   ));
	}


function BecameMember($event)
{
	if(!Security::isAuthorized()) die('~');
	
	global $db;
	$user = $_SESSION['id'];
	$event = (int)$event;
	$db->query("SELECT COUNT(*) FROM qmex_dev_members WHERE event_id=$event AND user=$user");
	if($db->one(0)>0) die('!');
	$res = $db->query("INSERT INTO qmex_dev_members(event_id, user) VALUES($event, $user)");
	die( $res ? '+' : '-' );
	}



function CreateProposalsList($printed)
{
	function is_empty($value){ return trim($value)!=''; }
	
	if(trim($printed)=='') die(json_encode(array()));
	$printed = explode(' ',str_replace(',',' ',mysql_real_escape_string($printed)));
	$printed = mb_strtolower(trim($printed[sizeof($printed)-1]));
	
	$proposals = array();
	global $db;
	$db->query("SELECT keywords FROM qmex_developments WHERE keywords LIKE '%$printed%' LIMIT 15");
	while($keys = $db->one(0))
	{
		foreach(explode(' ',$keys) as $val) 
		if(mb_strstr(mb_strtolower($val), $printed) &&
		   mb_strpos(mb_strtolower($val), $printed)==0) array_push($proposals, $val);
		}
		
	$proposals = array_unique(array_filter($proposals,'is_empty'));
	die( json_encode($proposals) );
	}
	

?>