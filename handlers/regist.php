<?php

require_once("../handlers/service/handlers_common.php");
$db = new db();
	
if(!isset($_SESSION["reg"])) $_SESSION["reg"] = array();


if(isset($_GET))
{
	
	}

if(isset($_POST))
{
	if(isset($_POST["login"]))         qLogin($_POST["login"]);
	if(isset($_POST["password"]))      qPass($_POST["password"]);
	if(isset($_POST["repassword"]))    qRePass($_POST["repassword"]);
	if(isset($_POST["key"]))           qKey($_POST["key"]);
	if(isset($_POST["check"]))         CheckStep1();
	
	if(isset($_POST["calendar"]))      qYear(@$_POST["year"],@$_POST["month"]); 
	if(isset($_POST["checkstep2"]))    CheckStep2();
	if(isset($_POST["skipstep2"]))     SkipStep2(); 
	
	if(isset($_POST["select_interest"]))       SelectInterest(); 
	if(isset($_POST["interest_add_keywords"])) AddKeywordsInterest(); 
	if(isset($_POST["checkstep3"]))  		   CheckInterests($_POST["checkstep3"]); 
	
	if(isset($_POST["email"]))         Email($_POST["email"]);  
	}
	
	
function is_empty($value)
{
	return trim($value)!='';
	}

function _login($login)
{
	global $db;
	$result = array();
	
	$login = mysql_real_escape_string(trim($login)); 
	$found = false;
	$db->query("SELECT Login FROM qmex_users WHERE LOWER(Login)='".mb_strtolower($login)."'");
	if($db->rowCount()==1) $found = true;

		if(trim($login)=='') 
			return array('login' =>array('msg'=>'Логин не указан', 'res'=>'-'));
		if($found==true)  
			return array('login' =>array('msg'=>'Этот логин уже занят :(', 'res'=>'-'));
		if($found==false) 
		{			
			$len = mb_strlen(trim(mb_strtolower($login)));
			
			if(!is_similar_by_service_reg($login))
				return array('login' => array('msg'=>'Введены недопустимые символы!', 'res'=>'-'));
			
			if($len>45)
				return array('login' => array('msg'=>'Логин слишком длинный...', 'res'=>'-'));
			
			if($len<2) 
				 return array('login' =>array('msg'=>'Логин слишком короткий...', 'res'=>'-'));
						
			return array('login' => array('msg'=>'Этот логин свободен', 'res'=>'+'));	
		}
	}

function _pass($pass)
{
	$passwd = mb_strlen(trim(mb_strtolower($pass)));
	if($passwd<=4) 
		return array('password' => array('msg'=>'Пароль ненадежен!', 'res'=>'-'));
	if($passwd>40) 
		return array('password' => array('msg'=>'Пароль слишком длинный!', 'res'=>'-'));  
	if(is_similar_by_service_reg($pass)==false) 
		return array('password' => array('msg'=>'Введены недопустимые символы!', 'res'=>'-')); 
		
	return array('password' => array('msg'=>'', 'res'=>'+')); 
	}
	
function is_pass_eq($pass1, $pass2)
{
	if($pass1!=$pass2) 
		return array('repassword' => array('msg'=>'Пароли не совпадают!' , 'res'=>'-')); 
	return array('repassword' => array('msg'=>'', 'res'=>'+')); 
	}
	
function _key($key)
{
	$kkey = mb_strlen(trim(mb_strtolower($key)));
	if($kkey<2) 
		return array('key' => array('msg'=>'Минимум - 2 символа!', 'res'=>'-')); 
	if($kkey>40)
		return array('key' => array('msg'=>'Ключевое слово слишком длинное!', 'res'=>'-')); 
	if(!is_similar_by_service_reg($key)) 
		return array('key' => array('msg'=>'Введены недопустимые символы!', 'res'=>'-')); 
	return array('key' => array('msg'=>'', 'res'=>'+'));
	}


function qLogin($login)
{
	$login =trim($login);
	echo json_encode( _login($login) );
	$_SESSION["reg"]["login"] = mysql_real_escape_string($login);
	}
	
	
function qPass($pass)
{
	$pass = trim(mb_strtolower($pass));
	echo json_encode( _pass($pass) );
	$_SESSION["reg"]["password"] = mysql_real_escape_string($pass);
	}
	
	
function qRePass($repass)
{
	$pass = $_SESSION["reg"]["password"] ;
	$repass = trim(mb_strtolower($repass));
	echo json_encode( is_pass_eq($pass, $repass) );	
	$_SESSION["reg"]["repassword"] = mysql_escape_string($repass);
	}
	
	
function qKey($key)
{
	$key = trim(mb_strtolower($key));
	echo json_encode( _key($key) );	
	$_SESSION["reg"]["key"] = $key; 
	}
	
	
function CheckStep1()
{
	$result = array();
	$login = @$_SESSION["reg"]["login"];
	$pass = @$_SESSION["reg"]["password"];
	$repass = @$_SESSION["reg"]["repassword"];
	$key = @$_SESSION["reg"]["key"];
	
	$login = _login($login);
	$passw = _pass($pass);
	$repass = is_pass_eq($pass,$repass);
	$key = _key($key);
	
	$result['login'] = $login['login'];
	$result['password'] = $passw['password'];
	$result['repassword'] = $repass['repassword'];
	$result['key'] = $key['key'] ;
	
	$checker = ($result['login']['res']=='+') + 
			   ($result['password']['res']=='+') + 
			   ($result['repassword']['res']=='+') + 
			   ($result['key']['res']=='+');
			   
	$checker = $checker == 4;
	
	if ($checker==false)
	{
		$_SESSION["reg"]["step1_succ"] = false;
		$result['result'] = array('msg'=>'Пожалуйста, исправьте допущенные ошибки!', 'res'=>'-');
	}
		
	if($checker==true)
	{
		unset($_SESSION["reg"]["repassword"]);
		$_SESSION["reg"]["step1_succ"] = true;
		$result['result'] = array('msg'=>'', 'res'=>'+');
		}
		
	echo json_encode( $result );
		
	}
	
/////////////////////////////////////////////////////////////////////////////

function qYear($year, $month)
{
	$answer = "";
	if($month < 32 && $month > 0 && $year>1900)
	{
		$days = cal_days_in_month(0,$month,$year);
		$answer.='<select class="qDate qChecker" style="background-color:#FFE8E8" id="day" name="_day"><option value="-1">День: </option>';
		for($i=1; $i<=$days; $i++) $answer.="<option value='$i'>$i</option>";
		$answer.='</select>';
	}
	die($answer);
}

function CheckStep2()
{
	function local_filter($a){ return (trim($a)!=''); }
	
	$name = mysql_real_escape_string(@$_POST["name"]);
	$sename = mysql_real_escape_string(@$_POST["sename"]);
	$country = mysql_real_escape_string(@$_POST["country"]);
	$city = mysql_real_escape_string(@$_POST["city"]);
	
	$descrwords = 
	implode(' ', array_unique(array_filter(explode(' ', str_replace(',', ' ', mb_strtolower(@$_POST["descrwords"]) )), 'local_filter')) );
	
	$latitude  = @$_POST["latitude"];
	$longitude = @$_POST["longitude"];
	
	$sex= @(int)$_POST["sex"];
	$year = @(int)$_POST["year"];
	$month = @(int)$_POST["month"];
	$day = @(int)$_POST["day"];
	
	$nowyear = date("Y");
	
	$checker = (is_similar_by_reg($name) + is_similar_by_reg($sename) + is_similar_by_reg($country) + 
				is_similar_by_reg($city) + is_similar_by_reg($descrwords)) +
			   (($year<=$nowyear && $year>=$nowyear-110) + ($month<=12 && $month>=1) + ($day>=1 && $day<=31) + ($sex==1 || $sex==2)) == 9;
	
	if($checker==false) 
	{
		$_SESSION["reg"]["step2_succ"] = false;
		die( json_encode( array('msg'=>'Некоторые поля не заполнены или заполнены неверно.', 'res'=>'-') ) );
	}
	

	$_SESSION["reg"]["step2_succ"] = true;
	$_SESSION["reg"]["name"] = $name;
	$_SESSION["reg"]["sename"] = $sename;
	$_SESSION["reg"]["country"] = $country;
	$_SESSION["reg"]["city"] = $city;
	$_SESSION["reg"]["latitude"] = $latitude;
	$_SESSION["reg"]["longitude"] = $longitude;
	$_SESSION["reg"]["sex"] = $sex;
	$_SESSION["reg"]["year"] = $year;
	$_SESSION["reg"]["month"] = $month;
	$_SESSION["reg"]["day"] = $day;	
	$_SESSION["reg"]["descrwords"] = $descrwords;
	die( json_encode(array('res'=>'+')) );	
}

function SkipStep2()
{
	$name=$sename=$country=$city=$sex=$year=$month=$day="";
	$_SESSION["reg"]["step2_succ"] = true;
	$_SESSION["reg"]["name"] = $name;
	$_SESSION["reg"]["sename"] = $sename;
	$_SESSION["reg"]["country"] = $country;
	$_SESSION["reg"]["city"] = $city;
	$_SESSION["reg"]["sex"] = $sex;
	$_SESSION["reg"]["latitude"] = 0;
	$_SESSION["reg"]["longitude"] = 0;
	$_SESSION["reg"]["year"] = $year;
	$_SESSION["reg"]["month"] = $month;
	$_SESSION["reg"]["day"] = $day;	
	$_SESSION["reg"]["descrwords"] = $day;	
	die( json_encode(array('res'=>'+')) );	
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////

function SelectInterest()
{
	if(!isset($_SESSION["interests"])) $_SESSION["interests"]=array();
	$key_interest = (int)$_POST["select_interest"];
	$type = trim(strtolower($_POST["type"]));
	if($type=='none') {$_SESSION["interests"][$key_interest]='';}
	if($type=='remove') 
	{
		$arrkeys = $_SESSION["interests"];
		$_SESSION["interests"]=array();
		foreach($arrkeys as $key=>$val) if($key!=$key_interest) $_SESSION["interests"][$key]=$arrkeys[$key];
		/*$rp = array_search($key_interest,$arrkeys);
		echo(">>>>>>>>>>>>>>>>".$_SESSION['interests'][$key_interest]."-".$key_interest."<<<<<<<<<<<<<<<<");
		array_splice($_SESSION["interests"],$rp,1);*/
	}
	$IBASE = new Interests();
	$have = 0;
	foreach($IBASE->SelectIBASE() as $val) if($IBASE->SelectKey($val)==$key_interest) {$have = 1;break;}
	echo($have);
	//print_r($_SESSION["interests"]);
	//unset($_SESSION["interests"]);
	}
	
function AddKeywordsInterest()
{
	$index = (int)$_POST["interest_add_keywords"];
	$content = strip_tags($_POST["content"]);
	$_SESSION["interests"][$index] = $content;
	}
	
function GetKWInterests($id)
{
	global $db;
	$id=(int)$id;
	$ftch = $db->query("SELECT interest_keywords FROM qmex_user_interests WHERE key_interest=$id");
	$ints = array();
	while($data = $db->one($ftch))
	{
		$s_int = str_replace(',',' ',trim($data[0]));
	    $s_int = explode(" ",$s_int);
		$s_int = array_filter($s_int,"filter");
		foreach($s_int as $single) array_push($ints,$single);
		}
		$ints = array_filter($ints,"filter");
		for($j=0;$j<count($ints);$j++)
		{
			$i = $ints[$j];
			echo "<a href='search#ints=".urlencode($i)."'>$i</a>";
			if($j<count($ints)-1) echo ', ';
			}
	}
	

function CheckInterests($id)
{
if(count(@$_SESSION["interests"])>10) 
{
	die('У вас слишком много интересов( более 10 ).<br> Постарайтесь выбрать лишь несколько основных интересов и дополнить их ключевыми словами.');
}
if(count(@$_SESSION["interests"])==0) 
{
	die('Не выбрано ни одного интереса. У вас должно быть хотя бы несколько интересов.');
}

if($id==0 && (!isset($_SESSION["entered"]) || $_SESSION["entered"]==false))
{
$can = 0;
if(isset($_SESSION["interests"]))
if(!empty($_SESSION["interests"])) $can = 1;	

switch ($can)
{
	case 0: $_SESSION["reg"]["step3_succ"]=false; echo("Вы не выбрали ни одного интереса.");break;
	case 1: $_SESSION["reg"]["step3_succ"]=true; echo("<script>top.location.href = 'register?step=last'</script>");break;
	}
} elseif($id==1 && $_SESSION["entered"]==true)
{
	$log = $_SESSION['id'];

	$db = new db();
	foreach($_SESSION["interests"] as $key=>$val)
	{
	$val = stripslashes( strip_tags(str_replace(array("'",'"'),array("",''),$val)) );
	$val = array_filter(explode(' ',str_replace(',',' ',$val)),'is_empty');
	$val = implode($val,' ');
	$db->query("SELECT ID FROM qmex_user_interests WHERE Login=$log AND key_interest=$key");
	if($db->rowCount()>0) 
	{
		$i_id = $db->one(0);
		$db->query("UPDATE qmex_user_interests SET interest_keywords='$val' WHERE ID=$i_id");
	} else $db->query("INSERT INTO qmex_user_interests(Login,key_interest,interest_keywords) VALUES($log,$key,'$val')");
	}
	
	/* HardCore */
	$db_ = new db();
	$log = $_SESSION['id'];
	
	$interests = array_keys($_SESSION['interests']);
	$db->query("SELECT ID,key_interest FROM qmex_user_interests WHERE Login=$log");
	while($data=$db->one())
	if(!in_array($data[1],$interests)) $db_->query("DELETE FROM qmex_user_interests WHERE ID=".$data[0]); 	
	
	/*$db->query("SELECT Essence FROM qmex_voites WHERE Login=$log AND Type='interest'"); 
	while($data=$db->one())
	{
		$i_id = $data[0];
		$db_->query("SELECT ID FROM qmex_user_interests WHERE ID=$i_id");
		if($db_->rowCount()==0) $db_->query("DELETE FROM qmex_voites WHERE Essence=$i_id");
		}*/
	/**/
		
	if(trim($db->lastError())=='') 
	echo '<div style="padding:5px; background-color:#D9FFD9; color:#009a2c; font-weight:bold">Интересы обновлены!</div>';
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

function Email($email)
{
	global $db;
	$email = strip_tags(trim(mb_strtolower($email,"UTF-8")));
	$em_len = mb_strlen($email,"UTF-8");
	if(preg_match('/^[a-zA-Zа-яА-Я0-9\-\._]+@[a-zA-Zа-яА-Я0-9\-\._]+\.[a-zA-Zа-яА-Я0-9\-\._]+$/ui',$email)) $can = 1; else $can = 0;		
	$db->query("SELECT Email FROM qmex_users");
	while($data = $db->one()){
		if(trim(strtolower($data[0]))==$email) {$can = 519; break;}
		}
	if(is_similar_by_reg($email)==false) $can = 2;
	switch ($can)
    {
	case 519: echo("Уже существует аккаунт, связанный с этим адресом электронной почты!<br><span class='regstep_subcaption' style='color:#0053a6'>Если вы забыли свой пароль, то вы можете <strong><a href='recover'>восстановить его.</a></strong></span>");break;
	case 0:   echo("Неверный формат");break;
	case 1:   {$_SESSION["reg"]["email"]=$email; echo("<script>top.location.href = 'register?step=finish'</script>"); break;}
	case 2:   echo("Обнаружены недопустимые символы!");break;
	}
	}