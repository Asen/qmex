﻿<?php

require_once("service/handlers_common.php");
$DB = new DB();

if(!empty($_GET))
{
	if(isset($_GET['count-time-for-starting'])) CountTime($_GET['days'], $_GET['hours'], $_GET['minutes']);
	if(isset($_GET['conference_get_last_created_opinion'])) GetLastCreatedOpinion();
	if(isset($_GET['conference_get_new_opinions'])) GetNewConferenceOpinions($_GET['conf'], $_GET['last']);
	if(isset($_GET['get-words'])) GetConferencesWords();
}

else

if(!empty($_POST))
{
	if(isset($_POST['create-conference'])) 		CreateConference($_POST);
	if(isset($_POST['new_conference_essence'])) CreateConferenceOpinion($_POST['conf_id'], $_POST['essence']);
	if(isset($_POST['change_conference_idea'])) ChangeConferenceIdea($_POST['conf_id'], $_POST['new_idea']);
	if(isset($_POST['change_conference_img']))  ChangeConferenceImage($_POST['conf_id'], $_POST['new_image']);
	if(isset($_POST['close_conference'])) 		CloseConference($_POST['conf_id']);
	if(isset($_POST['try_to_join'])) 		    TryJoinTo($_POST['conf_id']);
	if(isset($_POST['check_user_membership'])) 	CheckMemberShip($_POST['conf_id'], $_POST['user'], $_POST['result']);
}

function GetConferencesWords()
{
	function filter($a){
		return (mb_strlen(trim($a))>2) ? true:false;
		}
	
	$words = "";
	global $DB;
	$DB->query("SELECT words FROM qmex_conferences LIMIT 5000");
	while($word = $DB->one()) $words .= $word[0]." ";
	die(json_encode(array_filter(array_unique(explode(' ',$words)))));
	}

function CheckMemberShip($conf, $user, $result)
{
	$info = Conference::getConferenceInfo($conf);
	if( !Security::checkAuthority($info['creator']) ) return;
	

	global $DB;
	$conf = (int)$conf;
	$user = (int)$user;
	$result = (int)$result;
	$DB->query("SELECT members, members_not_joined FROM qmex_conferences WHERE id=$conf");
	$data = $DB->one();
	$members = $data[0];
	$members_nj = str_replace(';'.$user.';','',$data[1]);
	if($result==1) $members.=';'.$user.';';
	if($DB->query("UPDATE qmex_conferences SET members='$members', members_not_joined='$members_nj' WHERE id=$conf")) die('+');
	}

function TryJoinTo($conf)
{
	$info = Conference::getConferenceInfo($conf);
	if( !isset($_SESSION['id']) || $info['creator']==$_SESSION['id'] ) die('-');
	
	global $DB;
	$conf = (int)$conf;
	$this_user = (int)$_SESSION['id'];
	if($DB->query("UPDATE qmex_conferences SET members_not_joined=CONCAT(members_not_joined,';$this_user;') WHERE id=$conf")) die('+');
	}

function CloseConference($conf)
{
	$info = Conference::getConferenceInfo($conf);
	if( !Security::checkAuthority($info['creator']) ) return;
	
	global $DB;
	$conf = (int)$conf;
	$idea = mysql_real_escape_string($idea);
	$DB->query("UPDATE qmex_conferences SET is_closed=1 WHERE id=$conf");
	}
	
function ChangeConferenceImage($conf, $image)
{
	$info = Conference::getConferenceInfo($conf);
	if( !Security::checkAuthority($info['creator']) ) return;
	
	global $DB;
	$conf = (int)$conf;
	$image = mysql_real_escape_string($image);
	$DB->query("UPDATE qmex_conferences SET img='$image' WHERE id=$conf");
	}
	
function ChangeConferenceIdea($conf, $idea)
{
	$info = Conference::getConferenceInfo($conf);
	if( !Security::checkAuthority($info['creator']) ) return;
	
	global $DB;
	$conf = (int)$conf;
	$idea = mysql_real_escape_string($idea);
	$DB->query("UPDATE qmex_conferences SET description='$idea' WHERE id=$conf");
	}

function GetNewConferenceOpinions($conf, $last)
{
	global $DB;
	$conf = (int)$conf;
	$last = (int)$last;
	$DB->query("SELECT id FROM qmex_conferences_opinions WHERE conf_id=$conf AND id>$last ORDER BY id DESC LIMIT 1");
	while($oid = $DB->one(0)) Conference::ShowOpinion($oid); 
	}

function GetLastCreatedOpinion()
{
	$uid = (int)$_SESSION['id'];
	global $DB;
	$DB->query("SELECT id FROM qmex_conferences_opinions WHERE creator=$uid ORDER BY id DESC LIMIT 1");
	die( Conference::ShowOpinion($DB->one(0)) );
	}

function CreateConferenceOpinion($conf_id, $essence)
{
	if(!isset($_SESSION['id'])) die("*");
	
	$info = Conference::getConferenceInfo($conf_id);
	if( Conference::checkAvailability($conf_id)!=Conference::OK_AVAILABLE ) die('NA');
	
	global $DB;
	$creator = $_SESSION['id'];
	if(!$creator || $creator<0 || !ProcessTags($essence)) die("-");
	$conf_id = (int)$conf_id;
	$essence = mysql_real_escape_string(trim(format($essence)));
	$opinion_length = mb_strlen(strip_tags($essence));
	if($opinion_length>1000 || $opinion_length<=3) die('~');
	
	if($DB->query("INSERT INTO qmex_conferences_opinions(conf_id, creator, content) VALUES( $conf_id, $creator, '$essence' )") &&
	   $DB->query("UPDATE qmex_conferences SET last_activity=".qMexTime::getCurrent()." WHERE id=$conf_id"))
	   {
		   
		   $DB->query("SELECT members,linked_with_type FROM qmex_conferences WHERE id=$conf_id");
		   $data = $DB->one();
		   if($data[1]=='env'){
			   $DB->query("SELECT id FROM qmex_conferences_opinions WHERE creator=$creator ORDER BY ID DESC LIMIT 1");
			   $opinion_id = $DB->one(0);
			   $members = array_filter(explode(';',$data[0]));
			   foreach($members as $member) 
			   	$DB->query("INSERT INTO qmex_events(Type,Login,Maker,Essence,Action) 
							VALUES(".Event::TYPE_ENV_CONF_NEWOPINION.", ".(int)$member.", $creator, $opinion_id, 0)");
			   }
		   
		   }
	   die('+');
	   
	die('-');
	
	}

function CreateConference($data)
{
	
	function filter($a){
		return (mb_strlen(trim($a))>2) ? true:false;
		}
	
	$result = array('result'=>'-');
	$descr = mysql_real_escape_string(strip_tags($data['descr']));
	if(mb_strlen($descr)<5) die( json_encode($result) ); 
	$is_private = $data['is_private'];
	$is_planned = $data['is_planned'];
	$global_theme = $data['interest'];
	$words = implode(' ',array_filter(explode(' ',str_replace(',',' ',$data['words'])), 'filter'));
	$words = mysql_real_escape_string($words);
	$image = mysql_real_escape_string($data['image']);
	$last_activity = qMexTime::getCurrent();
	if(mb_strlen(trim($words))<3)  die( json_encode($result) ); 
	$creator = $_SESSION['id'];
	$plan_start = -1;
	if($is_planned=='true'){
		$days = (int)$data['days'];
		$hours = (int)$data['hours'];
		$minutes = (int)$data['minutes'];
		if( ($days<=0 && $hours<=0 && $minutes<=0) || 
			($days<0 || $hours<0 || $minutes<0)) die( json_encode($result) );
		$plan_start = qMexTime::getCurrent() + $days*24*60*60 + $hours*60*60 + $minutes*60;
		}
	
	global $DB;
	$res = $DB->query("INSERT INTO qmex_conferences(creator,description,is_private,plan_start,vector_interest,words,img,last_activity) 
					   VALUES($creator, '$descr', $is_private, $plan_start, $global_theme, '$words', '$image',$last_activity)");
	if($res) {
		$res = $DB->query("SELECT id FROM qmex_conferences WHERE creator=$creator ORDER BY id DESC LIMIT 1");
		$result['conf_id'] = $DB->one(0);
		$result['result'] = '+';
	}
	die( json_encode($result) );
	
	}

function CountTime($days, $hours, $minutes)
{
	$days = (int)$days;
	$hours = (int)$hours;
	$minutes = (int)$minutes;
	$time = qMexTime::getCurrent() + $days*24*60*60 + $hours*60*60 + $minutes*60;
	$data = array();
	$data['day'] = date('d', $time);
	$data['month'] = date('m', $time);
	$data['month-name'] = Enum::$MONTHES[(int)$data['month']-1];
	$data['year'] = date('Y', $time);
	$data['hour'] = date('H', $time);
	$data['minute'] = date('i', $time);
	$data['second'] = date('s', $time);
	$data['date'] = date("d.m.Y H:i:s", $time);
	die( json_encode($data) );
	}

?>