<?php

require_once("service/handlers_common.php");
$db = new DB();

if(!empty($_GET))
{
	}

else

if(!empty($_POST))
{
	if(isset($_POST["check_curr_pass"]))  checkPass($_POST["check_curr_pass"]);
	if(isset($_POST["cp"])) 			  changePass($_POST["cp"],$_POST["np"],$_POST["rnp"]);
	if(isset($_POST["change_email"]))     changeMail($_POST["change_email"]);
	if(isset($_POST["change_qid"]))       changeQID($_POST["change_qid"]);
	
	if(isset($_POST["ui_set_bg"]))        UISetBackGround(@$_POST["ui_set_bg"],@$_POST["bg_type"]);
	if(isset($_POST["ui_set_opacity"]))   UISetOpacity(@$_POST["ui_set_opacity"]);
	if(isset($_POST["ui_set_view"])) 	  UISetView(@$_POST["view-id"]);
	
	if(isset($_POST["ch_descrwords"]))    changeDescrWords($_POST["ch_descrwords"]);
	}

function changeDescrWords($descrwords)
{
	function local_filter($a){ return (trim($a)!=''); }
	
	global $db;
	$uid = $_SESSION["id"];
	$descrwords = 
	implode(' ', array_unique(array_filter(explode(' ', str_replace(',', ' ', mb_strtolower($descrwords) )), 'local_filter')));
	if(!is_similar_by_reg($descrwords)) die('<div id="ErrMsg">Вы ввели некоторые недопустимые символы.</div>');
	$descrwords = mysql_real_escape_string($descrwords);
	
	$res = $db->query("UPDATE qmex_users SET DescrWords='$descrwords' WHERE ID=$uid");
	if($res) echo '<div id="SucMsg">Ключевые слова о вас обновлены</div>'; else
		     echo '<div id="ErrMsg">Возникла неизвестная ошибка. Изменения не сохранены...</div>';
	}

function UISetView($vid)
{
	global $db;
	$vid = (int)$vid;
	if($vid<0 || $vid>4) $vid=1;
	$this_user = $_SESSION["id"];
	$res = $db->query("UPDATE qmex_user_settings SET pagescheme=$vid WHERE Login=$this_user");
	}


function UISetOpacity($opacity)
{
	global $db;
	$opacity = number_format(floatval($opacity),3,'.','');
	if($opacity>1 || $opacity<0.5) $opacity=1;
	$this_user = $_SESSION["id"];
	$res = $db->query("UPDATE qmex_user_settings SET opacity=$opacity WHERE Login=$this_user");
	}


function UISetBackGround($bg,$bg_type)
{
	global $db;
	$bg_type = (int)$bg_type;
	$this_user = $_SESSION["id"];
	$url = mysql_real_escape_string(trim(urldecode($bg)));
	$res = $db->query("UPDATE qmex_user_settings SET background='$url', background_style=$bg_type, background_type=1 WHERE Login=$this_user");
	if($res) echo '<div id="SucMsg">Цветовая схема успешно обновлена!</div>'; else
		     echo '<div id="ErrMsg">Возникла неизвестная ошибка. Изменения не сохранены...</div>';
	}



function changeQID($qid)
{
	$id = $_SESSION["id"];
	$curr_qid = $_SESSION["login"];
	$qid = trim($qid); 
	if($qid=='') 			die('<div id="ErrMsg" class="sett_notify" tag="1">Не введен qID.</div>');
	if(mb_strlen($qid)>45)  die('<div id="ErrMsg" class="sett_notify" tag="1">qID слишком длинный.</div>');
	if(mb_strlen($qid)<2)   die('<div id="ErrMsg" class="sett_notify" tag="1">qID слишком короткий.</div>');
	global $db;
	$db->query("SELECT ID FROM qmex_users WHERE Login='$qid' AND ID<>$id");
	if($db->rowCount()>0)  
			die('<div id="ErrMsg" class="sett_notify" tag="1">Этот qID уже занят. Попробуйте другой.</div>');

	
	if(!is_similar_by_service_reg($qid)) 
	die('<div id="ErrMsg" class="sett_notify" tag="1">Можно использовать только буквы( a-z ), цифры, точки, тире и символы подчеркивания.</div>');
	
	$qid = mysql_real_escape_string($qid);
	$res = $db->query("UPDATE qmex_users SET Login='$qid' WHERE ID=$id");
	if($res){
		echo '<div param="sett_success" id="SucMsg" class="sett_notify" tag="1">qID успешно изменен.</div>'; 
		$_SESSION['login'] = $qid;
		} else echo '<div id="ErrMsg" class="sett_notify" tag="1">Не удалось сменить qID =(.</div>';
	}
	

function changeMail($mail)
{
	$id = $_SESSION["id"];
	$curr_email = $_SESSION["email"];
	$mail = trim(mb_strtolower($mail)); 
	if($mail=='') {echo '<div id="ErrMsg" class="sett_notify" tag="1">Не введен E-Mail.</div>'; exit;}
	if(mb_strlen($mail)>45) {echo '<div id="ErrMsg" class="sett_notify" tag="1">E-Mail слишком длинный.</div>'; exit;}
	if(mb_strlen($mail)<2) {echo '<div id="ErrMsg" class="sett_notify" tag="1">E-Mail слишком короткий.</div>'; exit;}
	global $db;
	$db->query("SELECT Email FROM qmex_users WHERE Email='$mail' AND ID<>$id");
	if($db->rowCount()>0) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Этот почтовый ящик уже занят.</div>'; exit;}
	
	if(!preg_match('/^[a-zA-Zа-яА-Я0-9\-\._]+@[a-zA-Zа-яА-Я0-9\-\._]+\.[a-zA-Zа-яА-Я0-9\-\._]+$/ui',$mail))
	{echo '<div id="ErrMsg" class="sett_notify" tag="1">Неверный формат.</div>'; exit;}
	
	$mail_n = mysql_real_escape_string($mail);
	if( $mail_n==$curr_email ) return;
	$res = $db->query("UPDATE qmex_users SET Email='$mail_n', account_checked=0 WHERE ID=$id");
	if($res){
		echo '<div param="sett_success" id="SucMsg" class="sett_notify" tag="1">E-Mail успешно изменен.</div>'; 
		$_SESSION['email'] = $mail;
		} else echo '<div id="ErrMsg" class="sett_notify" tag="1">Не удалось сменить E-Mail =(.</div>';
	}


function checkPass($curr_pass)
{
	$curr_pass = trim(mb_strtolower($curr_pass));
	$_SESSION["sett_p"] = false;
	if($curr_pass!='')
	{
	$_SESSION["sett_p"] = ($_SESSION['password']==$curr_pass);
	if($_SESSION['password']==$curr_pass) echo '<div id="SucMsg" class="sett_notify" tag="1">Пароль введен верно.</div>'; else 
	                                      echo '<div id="ErrMsg" class="sett_notify" tag="1">Текущий пароль введен неверно.</div>';
	}
}

function changePass($cp,$np,$rnp)
{
	if(!isset($_SESSION["sett_p"])) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Необходимо заполнить все поля.</div>';exit;}
	if($_SESSION["sett_p"]==true)
	{
	$cp = trim(mb_strtolower($cp));
	$np = trim(mb_strtolower($np));
	$rnp = trim(mb_strtolower($rnp));
	
	if($cp==$np) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Текущий пароль совпадает с новым.</div>'; exit;}
	if($np!=$rnp) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Пароли не совпадают.</div>'; exit;}
	if(mb_strlen($np)<5) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Новый пароль слишком короткий.</div>'; exit;}
	if(mb_strlen($np)>45) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Новый пароль слишком длинный.</div>'; exit;}
	if($np==$rnp){
		if(!is_similar_by_reg($np)) {echo '<div id="ErrMsg" class="sett_notify" tag="1">Введены недопустимые символы!</div>';exit;}
		$id = $_SESSION["id"];
		global $db;
		$_SESSION['password'] = $np;
		$np=mysql_escape_string($np);
		$res = $db->query("UPDATE qmex_users SET Password='$np' WHERE ID=$id");
		if($res) {echo '<div id="SucMsg" class="sett_notify" tag="1">Пароль успешно изменен.</div>'; unset($_SESSION["sett_p"]);} else
		         echo '<div id="ErrMsg" class="sett_notify" tag="1">Не удалось сменить пароль =(.</div>';
		}
	
	}
	
	}

?>