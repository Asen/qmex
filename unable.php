﻿<?php

if(isset($_GET['access']))
if($_GET['access']=='root') { $_SESSION['access'] = true; 
							  echo '<meta http-equiv="refresh" content="0; URL=/">';
							  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="qmex_img/favicon.png">
<title>Система недоступна!</title>
</head>
<body>

<table cellpadding="0" cellspacing="0" border="0" style="width:100%; margin:0 auto;" id="unabled">
<tr>
<td>

<div class="browser_error_page">
<div id='NeutralMsg' style="height:65px">

<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
<td>
<img src="qmex_img/qmex.png" style="float:left; padding-right:10px" height="65px">
</td>
<td>
<div style='font-weight:bold; color:white; font-family:Arial, Helvetica, sans-serif; font-size:0.72em'>В данный момент система qMex недоступна для пользования.</div>
</td>
</tr>
</table>
</div>
<div class="badbrowser">
<div class="perform">
<table cellpadding="0" cellspacing="0" border="0" style="margin:0 auto; width:100%; ">
<tr>
<td style="padding:10%">
<?php if(!SYSINFO::IS_SYSTEM_CLOSED):?>
Над системой ведутся технические работы.<br>В скором времени работа qMex будет возообновлена!<br> Просим прощения за предоставленные неудобства. 
<?php else: ?>
<div style="font-family:Arial, Helvetica, sans-serif; font-size:0.7em">Система qMex в активной разработке.<br> Блог, в котором кратко описывается процесс:</div>
<a href='https://twitter.com/qMexDev' style="font-size:1.5em; font-family:Verdana, Geneva, sans-serif; color:black" target="_blank">qBLOG</a>
<?php endif; ?>
</td>
</tr>
<tr>
<td>
<hr color="#006699" size="1">
<a href='/'>qMex &copy;</a>
</td>
</tr>
</table>
</div>
</div>

</td>
</tr>
</table>

</body>
</html>

<style type="text/css">

body{background:url(qmex_img/bg.png) #000; }

a{ color:#00C; text-decoration:none; font-size:12px; font-family:Arial, Helvetica, sans-serif;}
a:visited{ color:#00C; }
a:hover{ text-decoration:underline; }

#NeutralMsg{
	 background-color:#069;
	 padding:10px;
	 color:white;
	 border:1px solid white;
	}

.browser_error_page
{
	width:50%;
	text-align:center;
	margin:0 auto;
	}
	
.badbrowser
{
	background-color:#FFF;
	width:100%;
	margin:0 auto;
	text-align:center;
	color:#000;
	//background-color:white;
	}
	
.perform{
	padding:10px;
	font-size:16px;
	font-weight:bold;
	color:#069;
	}

</style>

<script>
window.onload = function(){
	document.getElementById("unabled").style.height=window.innerHeight*0.95+"px"; 
	window.onkeydown = function(e){ if(e.keyCode==121) location.href=location.href+"?access=root"; }
	}
</script>