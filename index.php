<?php 

ini_set('display_errors', 'On');
error_reporting(E_ALL);
mb_internal_encoding("UTF-8");
//date_default_timezone_set("Europe/Moscow");
qMexTime::setZone();

//ini_set('session.cookie_lifetime', 5);
session_start();

//foreach($_SESSION as $key=>$val)
//unset($_SESSION[$key]);

require_once 'qmex_php_functions.php';

function __autoload($class)
{
	$class = mb_strtolower($class);
	include_once("classes/$class.php");
	}

$provider = new data_provider(); // Data Provider
$provider["start"] = true;

try{
	$provider['db'] = new DB();
}catch(Exception $e){
	die(include('unable.php'));
	}

///////////////////////////////// Page Creating ////////////////////////////////
$ip = trim($_SERVER['REMOTE_ADDR']);
if(SysInfo::IS_SYSTEM_ENABLE==true || in_array($ip,explode(" ",SysInfo::ALLOWED_IP)) || isset($_SESSION['access']))
{
	$router = new Router($provider);
	Router::Execute();
} else include('unable.php');
////////////////////////////////////////////////////////////////////////////////

session_write_close();
//echo '<script>console.log("magic_quotes: '.ini_get('magic_quotes_gpc').'");</script>';

?>
