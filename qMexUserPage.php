<?php
	
	if(!isset($_GET["user"]))
		die(include('controllers/error.php'));
	
	Smarty::$DATA['userpage'] = new UserPage($_GET["user"]);
	if(Smarty::$DATA['userpage']->LoadUserInfo()==NULL)
		die(include('controllers/error.php'));  

	include("views/page-layouts/layout.php");
	
?>