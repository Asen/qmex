<?php
	
function ProcessTags($html)
{
	$html = str_replace('&','','<doc>'.$html.'</doc>');
	return !(@simplexml_load_string($html)==NULL);
	}

function ProcessNode(&$node)
{
	if(mb_strtolower(trim($node->nodeName))=='a')      $node->setAttribute("target","_blank");
	if(mb_strtolower(trim($node->nodeName))=='iframe') 
	{
		if(!mb_strstr($node->getAttribute("src"),"www.youtube.com/embed")) $node->setAttribute("src","");
	}
	
	$attributes = $node->attributes;
	if(!$attributes) return;
	$removal = array();
	$n = 0;
	while($attr = $attributes->item($n))
	{
		$attrName = $attr->nodeName;
		$search = mb_strpos($attrName,'on');
		if($search!==false && $search==0) array_push($removal,$attrName);
		++$n;
		}
	foreach($removal as $attr) $node->removeAttribute($attr);
	}

function StartRoutine(&$tag)
{
	if($tag->hasChildNodes()) {
		
		$first = $tag->firstChild;
		ProcessNode($first);
		if($first->hasChildNodes()) StartRoutine($first);
		
		$next = $first;
		while($next = $next->nextSibling) 
		{
			ProcessNode($next);
			if($next->hasChildNodes()) StartRoutine($next);
			}
		
		}
	}
	
function format($content)
{
	// Protection level <<>> here is BUG!
	$content = str_replace(
				array("<script","</script>","<applet","</applet>","<frame","</frame>","<p></p>","'"),
				array("","","","","","","",""),
				$content);
				
	$content = "<!DOCTYPE HTML><html><head>
				<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
				</head><body>".$content."</body></html>";
				
	$dom = new DOMDocument();
	$dom->formatOutput = true;
	$dom->loadHTML($content);
	$tags = $dom->getElementsByTagName('html');
	
	StartRoutine(@$tags->item(0));
	
	return $dom->saveHTML() ;
	}

function addFrameStyles($frame_html, $additional_styles = NULL)
{
	//(SysInfo::SITE."views/main/styles/iframe.css")
	$styles_path = 'views/main/styles/iframe/';
	$standards   = "<link rel=\"stylesheet\" href=\"".$styles_path.'standards.css'."\" />";
	$additional  = $additional_styles==NULL ? '' : 
				   "<link rel=\"stylesheet\" href=\"".$styles_path.$additional_styles.'.css'."\" />";
	return $standards.$additional.$frame_html;
	}

function site($site)
{
	echo "<script>location.href='$site';</script>";
	}

function translate_date($str, $pattern = '')
{
	//<a href='/qmex-time' class='underlined'>(по qMexTime)</a>
	if($pattern=='') $pattern = "[day] [month] [year] в [time]";
	
	$str = trim($str);
	$str = explode(" ",$str);
	$str[0] = str_replace('.','-',$str[0]);
	$date = explode("-",$str[0]);
	$time = @explode(":",$str[1]);
	
	if((int)@$date[2]<=0 || (int)@$date[1]<=0) return "";
	return mb_strtolower( str_replace( 
		array('[day]','[month]','[time]','[year]'), 
		array((int)$date[2], Enum::$MONTHES[((int)$date[1])-1], $time[0].":".$time[1],$date[0]),
		$pattern
		)); 
	}

function translate_timestamp($timestamp)
{
	date_default_timezone_set(SysInfo::qMexTime);
	return translate_date( date('Y-m-d H:i:s',$timestamp) );
	}
	
function translate_timestamp_shorten($timestamp)
{
	date_default_timezone_set(SysInfo::qMexTime);
	return translate_date( date('Y-m-d H:i:s',$timestamp), '[day] [month]' ).
		   translate_date( date('Y-m-d H:i:s',$timestamp), '`[year]' );
	}
	
function calculate_time_diff($timestamp1, $timestamp2)
{
	$diff = abs($timestamp1 - $timestamp2);
	$days = (int)($diff / (24*60*60));
	$diff = (int)($diff % (24*60*60));
	$hours = (int)($diff / 3600);
	$diff = (int)($diff % 3600);
	$minutes = ceil($diff / 60);
	
	$buffer = '';
	$i=0;
	if($days>0 && ++$i)    $buffer .= $days." дней";
	if($hours>0 && ++$i)   $buffer .= (mb_strlen($buffer)>0 ? ' и ':'').$hours." часов";
	if($minutes>0 && $i<2) $buffer .= (mb_strlen($buffer)>0 ? ' и ':'').$minutes." минут";
	
	return $buffer;
	
	}

function make_normal($word,$split_count = 20)
{
	if(!$split_count) $split_count = 20;
	$word = trim($word);
	$word = splitter($word,$split_count);
	$word_m = (mb_strlen($word)>50) ? mb_substr($word,0,46):$word;
    if(mb_strlen($word_m)<mb_strlen($word)) $word_m.='.....';
	return $word_m;
	}

function bad_user_action($str,$col,$mod)
{
	$symb = ($mod==true) ? '-':'';
	for($i=1;$i<mb_strlen($str);$i++)
	{
		if(($i%$col)==0) 
		{
			$part1 = mb_substr($str,0,$i);
			$part2 = mb_substr($str,$i,mb_strlen($str)-$i+1);
			$str = $part1.$symb.'<br>'.$part2;
			}
		}
		return $str;
	}
		 

	function splitter($str,$col,$pos=0,$mod=true)
	{
		if(mb_strlen($str)>$col && !mb_strstr($str,' ')) return bad_user_action($str,$col,$mod);
		$symb = $mod ? "-":"";
		$buffer = '';
		$i = 0;
		foreach(String::mb_str_split($str) as $char)
		{					
			$buffer .= $char;	
			if((++$i)%$col==0)
				if(!strpbrk(mb_substr($buffer, mb_strlen($buffer)-$col, $col ),"\r\n ")) $buffer.=$symb."\n";
			}
		return $buffer;
		}
		
		
		function is_valid_url($url)
		{
			    $res_url = $url;
			    if(!strstr($res_url,'://')) $res_url='http://'.$res_url;
				if(preg_match('/(http|https|ftp):\/\/[a-zA-Zа-яА-Я0-9=_\-:\?\&\%]+\.[a-zA-Zа-яА-Я0-9]+/ui',$res_url)) return true;
				return false;
				/*$SUCCESS_STATE = array( 200, 201, 202, 203, 204, 205, 206, 207, 226, 301, 302, 303, 305, 307);
                $headers = @get_headers($res_url); 
			    for($i=0;$i<count($SUCCESS_STATE);$i++)
			    if(strstr($headers[0],$SUCCESS_STATE[$i])) {$url=$res_url;return true; break;}/*/
			}
			
		
		function get_not_escaped($str)
		{
			return htmlspecialchars(stripslashes($str));
			}
			
		function get_escaped($str)
		{
		   /*$str = strip_tags(trim($str));
	       $str = str_replace("'",'"',$str);
	       $str = mysql_escape_string($str);*/
		   //$str = str_replace("\"",'\'',$str);
		   return mysql_escape_string($str);
			}
			
		function is_similar_by_reg($text)
		{
			return (preg_match('/^[a-zA-Zа-яА-Я0-9_\.\-\^\[\]#*%!~@\+\s\|$:\(\)\{\}]+$/ui',$text));
			}
			
		function is_similar_by_service_reg($text)
		{
			return (preg_match('/^[a-zA-Z0-9_\.\-]+$/ui',$text));
			}
			

?>