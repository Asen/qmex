<?php 
Security::checkAccess();
?>

<style type="text/css">
.ui_setting_item{ 
	color:#069; background-color:#97CBFF; 
	ursor:pointer; padding:5px; 
	font-size:12px; border-left:1px solid #069; border-right:1px solid #069 }
.ui_setting_item:hover{background-color:#127AC7; color:white}
a:hover{text-decoration:none;}
.clarification-box{ padding:5px; font-size:0.7em; color:#D7EBFF; background-color:#069 } 
</style>

<div style="text-align:left; height:30px">

<table style="text-align:center;" cellspacing="0">
<tr>
<td><a href="/settings?t=private"><div class="ui_setting_item" id="private_settings">Личные настройки</div></a></td>
<td><a href="/settings?t=cscheme"><div class="ui_setting_item" id="else_settings">Оформление</div></a></td>
<td><a href="/settings?t=yousearch"><div class="ui_setting_item" id="search_settings">Поиск</div></a></td>
</tr>
</table>

<hr color="#0066CC" size="3">
</div>


<?php

if(!isset($_GET['t'])) $_GET['t'] = 'private';

$type = strip_tags(trim(mb_strtolower($_GET['t'])));
switch($type)
{
	case 'private':   include('controllers/service/settings/private.php');break;
	case 'cscheme':   include('controllers/service/settings/cscheme.php');break;
	case 'yousearch': include('controllers/service/settings/yousearch.php');break;
	default: include('controllers/service/settings/private.php');
	}
	
?>