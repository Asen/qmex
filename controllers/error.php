<div class="WContent" style="width:95%; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
<div style="background-color:#C66; border:black 1px solid; padding:10px; margin:0 auto; color:#FFF; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold" id="ErrMsg">
Такой страницы не существует.
<div style="float:right; margin:-5px auto">
<img src="/qmex_img/warning.png" width="25px" height="25px">
</div>
</div>

<table cellspacing="10px">
<tr>
<td valign="top"><img src="/qmex_img/info.png" width="50px" height="50px"></td>
<td>
<div class="WContent">
Страница 
<span style="color:red; font-weight:bold; padding:2px">"<?php echo "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>"</span> 
не существует!<br><br>
Скорее всего, вы допустили ошибку при вводе URL-адреса в адресную строку своего браузера.<br>
Вследствие этого, сервер не смог правильно обработать запрос и выдал данную ошибку.<br>
Проверьте корректность URL-адреса.<br><br>
Если вы уверены, что ошибки нет, а сервер все равно выдает эту ошибку, вместо нужной вам страницы, то обратитесь, пожалуйста, за помощью в нашу <b><a class="underlined" href="/support">службу поддержки</a></b>
<div style="text-align:center; padding-top:50px">
<a href="/"><button class="qButton" style="width:100%; height:40px">Перейти на главную страницу</button></a>
</div>
</div>
</td>
</tr>
</table>

</div>

<script language="javascript">
document.title="404 | Страница не найдена";
</script>