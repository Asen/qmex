<style>

#NO-VOITES{
	padding: 15px;
	background-color:#9CF;
	color:#000;
	font-weight:bold;
	font-size:12px;
}

</style>

<?php

$id = (isset($_GET["id"])) ? (int)$_GET["id"] : $_SESSION['id'];
$sort = (isset($_GET["sort"])) ? mb_strtolower(trim($_GET["sort"])) : '';
if($sort!='note' && $sort!='page' && $sort!='interest' && $sort!='fans') $sort='';

$login = Human::getLoginById($id);
$rating = Human::getUserRating($id);
$color = ($rating>0) ? "#009966" : "#CC6666";
if($rating==0) $color="#CCC";
$photo = Human::getUserPhoto($id);

$page_rait = Rating::getPageRating($id);
$public_rait = Rating::getPublicRating($id);
$interests_rait = Rating::getInterestsRating($id);
$fans_count = Rating::getFansCount($id);

$hubster_types_rating = Rating::getHubsterTypesRating($id);
$voites_positive = Rating::countPositiveVoites($id);
$voites_negative = Rating::countNegativeVoites($id);
$voites_pos_per = number_format((float)$voites_positive/max(array($voites_negative+$voites_positive,1)),2)*100;
$voites_neg_per = 100-$voites_pos_per;

$attitude = Human::getUserRatingPosition($id);

$interests_rating = array();
$interests_rating_sum = 0;
$DB = new db();
$DB->query("SELECT Essence FROM qmex_voites WHERE Login=$id AND Type='interest'");
while($key=$DB->one(0)) {
	if(!isset($interests_rating[$key])) $interests_rating[$key]=1; else $interests_rating[$key]++;
	$interests_rating_sum++;
}
?>

<style>

.rait-type-href{ font-family: tahoma, arial, verdana, sans-serif, Lucida Sans; font-size:12px; color:#00F; text-decoration:underline; }
.rait-type-href:hover{ color:#00F; text-decoration:none; }
.hubster-type-rait{ background-color:#CCC; padding:5px; color:#000; text-align:center}

</style>

<div class="WCaption">История значимости</div>
<div style='font-family:Arial, Helvetica, sans-serif; font-size:1.7em; background-color:#FFF; border-bottom:1px solid #06f; padding:5px'>
<a href='profile?id=<?php echo $login; ?>' style="color:#06F; "><?php  echo $login;  ?></a>
</div>
<table cellpadding="0" cellspacing="0px" border="0" style="width:100%;">
<tr>
<td width="30%" valign="top">

<?php if($sort==''):?>

<a class='qHint rait-type-href' tag='Подробнее...' href='rating?id=<?php echo $id?>&sort=fans'>
<div style="padding:5px; background-color:#FFF; width:105%;">
Окружение людьми : <b><?php echo $fans_count; ?></b> (?)
</div>
</a>
</div>

<? else: ?>

<a class='qHint rait-type-href' tag='Просмотр истории рейтинга' href='rating?id=<?php echo $id?>'>
<div style="padding:5px; background-color:#FFF; width:105%">
История значимости
</div></a>

<? endif ?>

<div style='background-color:#FFF; border: 1px solid #CCC; padding:0'>
<img src="<?php echo $photo?>" width="100%" style="padding:0">
</div>

<div style="color:white; background-color:#000; border-bottom:1px dashed white; padding:5px; font-size:9pt; font-weight:bold">
Значимость в системе
</div>
<div style="color:white; background-color:#333; font-size:36px; text-align:center; padding:10px;">
<span style='color:<?php echo $color;?>'><?php echo $rating;?></span>
<div style="font-size: 12px; color:white; font-weight:normal; padding:5px; border-top:1px dotted white">
<span style="font-weight:bold; font-family:'Arial Black', Gadget, sans-serif">№_<?php echo $attitude; ?></span> 
в qMex ( <a href='/search' class="underlined" style="color:#AED7FF">qN</a> )
</div>
</div>
<br>

<?php if($voites_negative+$voites_positive > 0): ?>
<table cellpadding="0" cellspacing="0" border="0" style="width:100%; height:12px; border:1px solid #777; text-align:center; font-family:Georgia, 'Times New Roman', Times, serif; font-size:12px; font-weight:bold; color:#000">
<tr>
<td class="qHint" tag="положительные мнения" width="<?php echo $voites_pos_per;?>%" style="background-color:#096">
<?php echo $voites_pos_per;?>%
</td>
<td class="qHint" tag="негативные мнения" width="<?php echo $voites_neg_per;?>%" style="background-color:#C66; border-left:2px solid #CCC">
<?php echo $voites_neg_per;?>%
</td>
</tr>
</table>
<?php endif; ?>
 
<div style="font-family:Georgia, 'Times New Roman', Times, serif; font-size:12px; padding-top:10px; cursor:pointer">

<span class="qHint" tag="Общее количество положительных мнений">
<img src='qmex_img/l_view.png' width="10px" height="10px"> <?php echo $voites_positive;?>
</span> | 
<span class="qHint" tag="Общее количество негативных мнений">
<img src='qmex_img/disl_view.png' width="10px" height="10px"> <?php echo $voites_negative;?>
</span>

</div>
<br><hr color="#069" size="2">
<div style="font-size:11px; padding:5px; color:#069;">
<table cellpadding="0" cellspacing="10px" border="0">
<tr>
<td>Рейтинг страницы:</td>
<td><div class='ui-voite-select-interest'><?php echo $page_rait; ?></div></td>
<td><a class='qHint' tag='Подробнее...' href='rating?id=<?php echo $id?>&sort=page'>>></a></td>
</tr>
<tr>
<td>Рейтинг в Hubster:</td>
<td><div class='ui-voite-select-interest'><?php echo $public_rait; ?></div></td>
<td><a class='qHint' tag='Подробнее...' href='rating?id=<?php echo $id?>&sort=note'>>></a></td>
</tr>
<tr>
<td colspan="3">
<div style="padding-left:20px">
<table>
<?php
foreach(Enum::ConceptionsCharacters() as $key=>$val) 
echo '<tr><td align="right">'.$val.'</td><td class="hubster-type-rait">'.$hubster_types_rating[$key].'</td></tr>'
?>
</table>
</div>
</td>
</tr>
<td>Рейтинг навыков:</td>
<td><div class='ui-voite-select-interest'><?php echo $interests_rait; ?></div></td>
<td><a class='qHint' tag='Подробнее...' href='rating?id=<?php echo $id?>&sort=interest'>>></a></td>
</tr>
</table>
</div>
<hr color="#069" size="2" />
<div style="font-size:11px; padding:5px; color:#003;">
<b>Активность:</b><br><br>
<table cellpadding="0" class="full-width" cellspacing="10px" border="0">
<?php

$IBASE = new Interests();
foreach($interests_rating as $key=>$val)
{
	$v = number_format($val/$interests_rating_sum,2)*100.0;
	echo '<tr><td>'.
		 $IBASE->SelectName($key)." (умение:".$v."%)".
		 "<div class='ui-voite-select-interest' style='width:".$v."%'>".$val."</div>".
		 "</td></tr>";
	}

?>
</table>
</div>
</td>
<td width="70%" valign="top" bgcolor="#FFFFFF">
<div class="ui-rating-events-box">
<?php


if($sort!='fans'){
	if($sort!='') $sort=" AND Type='$sort' ";
	$DB->query("SELECT ID FROM qmex_voites WHERE Login=$id ".$sort." ORDER BY ID DESC");
	while($id=$DB->one(0)) echo Rating::CreateEvent($id);
	if($DB->rowCount()==0) echo '<div id="NO-VOITES">Данные о значимости отсутствуют</div>';
	
} else{
	
	$searching_ids = array();
	$DB->query("SELECT ID1 FROM qmex_user_bookmarks WHERE ID2=$id");
	while($id=$DB->one(0)) array_push($searching_ids,(int)$id);
	
	echo '<div class="WCaption" style="padding:5px; border-bottom:1px solid black; font-size:11px">Люди, заитересованные в 
	<span style="font-family:Courier, monospace; color:#CCC">'.$login.'</span> ('.($DB->rowCount()).') :</div>';
	
	$searching_ids = implode(',',$searching_ids);
	$DB->query("SELECT Login,Photo FROM qmex_users WHERE ID IN (".$searching_ids.")");
	while($data=$DB->one())
	{
		$login = $data[0];
		$photo = (trim($data[1])=='') ? 'qmex_img/qmex_human.png':$data[1];
		
		echo '<div style="float:left; padding:10px; text-align:center;"><a href="profile?id='.$login.'">
		      <img src="'.$photo.'" height="50px" /><br>'.$login.'</div>';
		
		}
	
	}

?>
</div>
</td>
</tr>
</table>