<?php ob_start();?>

<style type="text/css">
a:hover{text-decoration:underline;}
.dev-kind{ text-align:center; padding:10px; font-weight:bold; cursor:pointer; }
.dev-kind:hover{ background-color:#09C }
</style>


<table class="full-width" cellspacing="0">
<tr>
    <?php
    
    $dev_kinds = Enum::DevelopmentTypes();
	$blue_coeff=0;
	foreach( $dev_kinds as $key=>$val){
		$blue_coeff+=20;
		echo "
		<td style='background:rgb(0,102,".(105+$blue_coeff).")'>
		<a onclick='dev_kind(".$key.");return false;' style='color:white' class='non-underlined'>
		<div class='dev-kind'>".
		$val.' '.( isset($_GET['ki']) && $_GET['ki']==$key ? '<img src="/qmex_img/UI/actuals/check.png" width="15px" />' : '' ).
		"</div></a></td>";
	}
    ?>
</tr>
</table>

<div id="ad_searchbar" style="padding-top:10px">
<table cellspacing="5px" style="width:100%" border="0">
<tr>
<td width="70%">
<input type="text" class="UI-input" style="width:80%; padding:5px" id="dev-search-line" onkeypress="_key_pressed(event)" onkeyup="_key_up(event)" placeholder="Поиск интересных событий" value="<?php echo @$_GET['keys'];?>"> <button class='qButton' style="border-radius:2px; height:26px; width:26px" onclick="quer_srch()"><img src="qmex_img/search.png" width="13px" height="13px"></button>
</td>
<td align="right" width="30%">
<a onclick="actualsView();return false;">
<button class="qButton" style=" padding:5px">
Самое актуальное <?php if(isset($_GET['actuals'])) echo '<img src="/qmex_img/UI/actuals/check.png" width="15px" />'; ?>
</button>
</a>
</td>
<td align="right" width="30%">
<a href='?view=new'><button class="qButton" style="width:150px; padding:5px">Создать событие + </button></a>
</td>
</tr>
</table>
</div>
<table cellspacing="5px" style="width:100%" border="0">
<tr>
<td style="width:80%" valign="top">


<div style="background-color:#91C8FF; font-size:12px;">
<table class="full-width" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="left">
	<div style="padding:10px;">
        <input type="checkbox" onchange="geoPreference(this)" <?php if(isset($_GET['geo'])) echo 'checked'?>> 
        События рядом со мной
    </div>
</td>
<td align="center">
	<a onclick="mapView();return false;" class="non-underlined">
    	<div class="UI-simple-menu-item" style="padding:10px;">
        	<?php echo isset($_GET['map']) ? 'Лента событий':'Карта событий'; ?>
        </div>
    </a>
</td>
</tr>
</table>
</div>


<div id="quer-queries">

<?php
include("controllers/service/developments/showall.php");
$c = $_SESSION['quer_notes_count'];
if($c==0) echo '<div style="padding:10px" t>Таких событий пока что нет.</div>';
?>

<div style="padding:10px; width:100%; text-align:center" id="quer-nav"></div>

</div>
</td>
<td valign="top" width="20%">
    <div style="background-color:#06F; font-size:0.8em; padding:5px; border:1px #069 solid">
    <?php
    
    $IBASE = new Interests();
    
    echo "<div style='border-bottom:1px dotted #CEE6FF; color:#CEE6FF'> События о : </div>";
    foreach($IBASE->SelectIBASE() as $ITEM)
    {
        $KEY = $IBASE->SelectKey($ITEM);
        $NAME = $IBASE->SelectName($KEY);
		
		if(isset($_GET['i']) && (int)$_GET['i']==$KEY) echo '<img src="/qmex_img/UI/actuals/check.png" width="15px" /> ';
        echo "<a onclick='quer_spec(".$KEY.");return false;' style='color:#FFF' class='underlined'>".$NAME.'</a><br>';
    }
    ?>
    </div>
</td>
</tr>
<tr>

</tr>
</table>

<?php

if(isset($_GET["view"]))
{
	$view= $_GET["view"];
	if($view=="new")  {ob_clean();include("controllers/service/developments/new.php");}
	if($view=="my")   {ob_clean();include("controllers/service/developments/my.php");}
	if($view=="edit") {ob_clean();include("controllers/service/developments/new.php");}
	if($view=="show") {ob_clean();include("controllers/service/developments/single.php");}
	if($view=="draft") {ob_clean();include("controllers/service/developments/draft.php");}
	if($view=="actuals") {ob_clean();include("controllers/service/developments/actuals.php");}
	}

?>

<script language="javascript">

$(function(){
	
	COMMON.AddPagedNav("#quer-nav");	
	COMMON.IFrameTest(".quer-iframe");
	COMMON.CreateFastScroll();
	
	// Removal of query
	$(".ui-quer-remove").click(function(){
		var id = $(this).attr("point");
		WINDOW.Confirm("Вы уверены, что данный запрос для вас более не актуален и вы хотите его удалить?", function(){						
			SendDynamicMsg(['quer_delete'],[id],null,'handlers/uiHandler.php','POST');
			$("#quer_"+id).slideUp(1000);			
			})
		});
	
		$(".ui-quer-to-draft").click(function(){
		id = $(this).attr("point");
		SendDynamicMsg(['quer_to_draft'],[id],null,'handlers/uiHandler.php','POST');
		$("#quer_"+id).hide(1000);
		});
		
		$(".ui-quer-from-draft").click(function(){
		id = $(this).attr("point");
		SendDynamicMsg(['quer_from_draft'],[id],null,'handlers/uiHandler.php','POST');
		$("#quer_"+id).hide(1000);
		});
	
	});
	
function actualsView()
{
	var q = QUERY.splitGetQuery(location.href);
	q['actuals'] = <?php echo isset($_GET['actuals']) ? '""':'1'; ?>;
	location.href = '/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
function mapView(o)
{
	var q = QUERY.splitGetQuery(location.href);
	q['map'] = <?php echo isset($_GET['map']) ? '""':'1'; ?>;
	location.href = '/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
function geoPreference(o)
{
	var q = QUERY.splitGetQuery(location.href);
	q['geo'] = o.checked ? '1':'';
	location.href = '/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
	
function quer_spec(key)
{
	var q = QUERY.splitGetQuery(location.href);
	var current = <?php echo isset($_GET['i']) ? $_GET['i'] : -1 ?>;
	q['i'] = current==key ? '' : key;
	location.href = '/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
function dev_kind(key)
{
	var q = QUERY.splitGetQuery(location.href);
	var current = <?php echo isset($_GET['ki']) ? $_GET['ki'] : -1 ?>;
	q['ki'] = current==key ? '' : key;
	location.href = '/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
function quer_srch()
{
	var q = QUERY.splitGetQuery(location.href);
	q['keys']=$("#dev-search-line").val();
	location.href='/developments?'+QUERY.completeQueryFromHash(q, true)
	}
	
function _key_pressed(e)
{
	var key = e.charCode||e.keyCode;
	if(key==13) quer_srch();	
	}
	
function _key_up(e)
{
	var key = e.charCode||e.keyCode;
	if(key==38 || key==40) return;		
	var o = $("#dev-search-line").first()
	var printed = $(o).val();
	var proposals = SendDynamicMsg(['get-proposals-list'], [printed], null, 'handlers/developments.php', 'GET', 'json');
	DATALIST.Create( o, proposals, {position:'absolute'} );
	}

</script>
