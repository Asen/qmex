<?php 
if( isset($_SESSION["entered"]) && $_SESSION["entered"]==true && !isset($_GET['justr']) ) 
die("<script>location.href='/'</script>");
?>


<link rel="stylesheet" href="views/main/styles/registration.css" />
<style>
#reg-steps-preview{ font-size:12px; }
.topper_href{ color:#EEE; text-decoration:underline }
.topper_href:hover{ text-decoration:none }

.sg
{
	color:#090;
	font-size:12px;
	font-weight:bold;
}
	
.eg
{
	color:#C40000;
	font-size:12px;
	font-weight:bold;
}

</style>


<div class="WCaption">Регистрация</div>
<table cellpadding="0" cellspacing="0" border="0" id='reg-steps-preview'>
<tr id='steps-preview'>
<td style="background:#2989D8" tag='#2989D8'><a href='register?step=passwd' class="topper_href">Создание логина и пароля</a></td>
<td style="background:#2681CC" tag='#2681CC'><a href='register?step=personal' class="topper_href">Личные данные</a></td>
<td style="background:#206EB1" tag='#206EB1'><a href='register?step=interests' class="topper_href">Выбор интересов</a></td>
<td style="background:#1B5E94" tag='#1B5E94'><a href='register?step=last' class="topper_href">Указание E-Mail адреса</a></td>
<td style="background:#00557D; font-weight:bold" tag='#00557D'>Финиш!</td>
</tr>
<tr id='current-step'>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>

<?php

$effect = false;
$index = 0;
if(isset($_GET["step"]))
{
	switch($_GET["step"])
	{
		case 'passwd':     include("controllers/service/register/step1.php"); break;
		case 'personal':   include("controllers/service/register/step2.php"); $index=1; break;
		case 'interests':  include("controllers/service/register/step3.php"); $index=2; break;
		case 'last': 	   include("controllers/service/register/step4.php"); $index=3; break;
		case 'finish': 	   include("controllers/service/register/step5.php"); $index=4; break;
		case 'success':    include("controllers/service/register/success.php"); $effect=true; $index=4; break;
		case 'failed': 	   include("controllers/service/register/failed.php"); $effect=true; $index=4; break;
		default: 		   include("controllers/service/register/step1.php");
		}	
} else include("controllers/service/register/step1.php");

?>

<script>
$(function(){
	var reg_index = <?php echo $index ?>;
	if(reg_index>=0 && reg_index<5){
		var color = $("#steps-preview > td").eq(reg_index).attr('tag');
		$("#steps-preview > td").eq(reg_index).css({'font-weight':'bold'});
		$("#current-step > td").eq(reg_index).html("<div style='padding:2px; background-color:"+color+"'></div>");
		}
	});
</script>
