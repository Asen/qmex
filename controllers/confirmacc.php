<?php 

if(Security::isCurrentAccountChecked())
 	die('<script>location.href="/"</script>');

?>

<div class='WCaption'>Подтверждение своей действительности</div>
<div class='WContent'>
<?php

if(isset($_GET['accid']) && isset($_GET['ussid'])) Confirm(); 
if(isset($_GET['token']) && isset($_GET['t'])) createConfirmation();


function Confirm()
{
	$accid = $_GET['accid'];
	$ussid = $_GET['ussid'];	
	$ussid = (int)base64_decode(urldecode($ussid)) - 888;
	
	$operation_succeed = false;
	$login = "";
	
	$db = new DB();
	$db->query("SELECT Email, Login FROM qmex_users WHERE ID=$ussid");
	if( $db->rowCount() > 0 )
	{
		$data = $db->one();
		$email = $data[0];
		$login = $data[1];
		if($accid == md5($email.$ussid))
		if($db->query("UPDATE qmex_users SET account_checked=1 WHERE ID=$ussid")) $operation_succeed=true;		
		}
	
	if($operation_succeed): ?>   
    	<div style="padding:10px; color:#096; font-size:14px">Действительность qID <b><?php echo $login ?></b> успешно подтверждена!</div> 
    <?php else: ?>
    	<div style="padding:10px; color:#933; font-size:14px">
        Не удалось подтвердить действительность. Пожалуйста, <a href='/confirmacc' class="underlined">попробуйте снова</a>.
        </div>   
    <? endif;
	}

function createConfirmation()
{
	
if(!Security::isAuthorized()){
	echo '<a class="underlined" href="/login">Авторизуйтесь</a>, пожалуйста, чтобы подтвердить действительность своего qID.';
	return;
	}
	
$token = $_GET['token'];
$time = (float)base64_decode(urldecode($_GET['t'])) * 100.0;
if( $token != md5($_SESSION['login'].$_SESSION['id']) || 
	( isset($_SESSION['validate_profile_time']) && ($time-$_SESSION['validate_profile_time'])<5 ) )
	{
		$_SESSION['validate_profile_time'] = $time;
		echo 'Это действие разрешено не чаще, чем 1 раз в 5 секунд.<br>Перейдите в свой профиль и попробйте снова.';
		return;
		}

$_SESSION['validate_profile_time'] = $time;

	
$accid = md5($_SESSION['email'].$_SESSION['id']);
$ussid = urlencode(base64_encode((int)$_SESSION['id']+888));
$email = $_SESSION['email'];

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset: utf8\r\n";
$headers .= "From: qMex Community <admin@qmex.ru>\r\n";
$headers .= "X-Mailer: PHP/" . phpversion();


$body = file_get_contents(SYSINFO::SITE."/data/confirm_letter.php");
$body = str_replace("[HREF]", SYSINFO::SITE."/confirmacc?accid=".$accid."&ussid=".$ussid, $body);
$res = mail($email, "Account", $body, $headers);

$email = explode("@",$email);
$email = $email[0][0].'*****@'.$email[1];

if($res): ?>

На ваш почтовый ящик <b><?php echo $email ?></b> было отправлено письмо с ссылкой для подтверждения действительности своего qID.
<br><br>
<b>Письмо придет в течение 15 минут.<br>Проверьте, пожалуйста, свой почтовый ящик.</b>

<?php endif; 
	
	}

?>

</div>