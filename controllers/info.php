
<style>

@font-face{
	font-family:About;
	src:url(/views/main/fonts/q.ttf);
	}

html, body{
	background-color:#000;
	background-image:url(qmex_img/bg.png);
	background-attachment:fixed;
	background-repeat:repeat;
	font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;
	font-family: About;
	}

#qmex_content_block{ background:rgba(0,0,20,0.8); }

</style>

<?php

if(isset($_GET['intro'])) 
	include("controllers/service/info/intro.php");
elseif(isset($_GET["page"]))
{
	switch($_GET["page"])
	{
		case "about":   include_once("controllers/service/info/about.php");
		                break;

		case "blog":    include_once("controllers/service/info/blog.php");
		                break;
					  
		case "ads":     include_once("controllers/service/info/ads.php");
		                break;

		case "support": include_once("controllers/service/info/support.php");
		                break;
											
		default: 		include_once("controllers/service/info/about.php");
		}
} else include_once("controllers/service/info/about.php");
?>