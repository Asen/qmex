<div class="WContent" style="width:95%; font-family:Arial, Helvetica, sans-serif; font-size:13px;">
<div style="background-color:#C66; border:black 1px solid; padding:10px; margin:0 auto; color:#FFF; font-family:Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold" id="ErrMsg">
Доступ запрещен.
<div style="float:right; margin:-5px auto">
<img src="/qmex_img/warning.png" width="25px" height="25px">
</div>
</div>

<table cellspacing="10px">
<tr>
<td valign="top"><img src="/qmex_img/info.png" width="50px" height="50px"></td>
<td>
<div class="WContent">
Вы попытались получить доступ к функциям сайта, предусмотренным лишь для авторизованных пользователей. Для того, чтобы эти функции были доступны и вам, войдите, пожалуйста, в систему, используя свой qID и пароль, указанные при регистрации.
<br><br>
<div><a href='/register' class="underlined" style="color:#06F">Еще нет аккаунта и собственного qID?</a></div>
<div style='padding-top:2px'><a href='/login' class="underlined" style="color:#06F;">Войти в систему?</a></div>
<div style="text-align:center; padding-top:60px">
<a href="/"><button class="qButton" style="width:100%; height:40px">Перейти на главную страницу</button></a>
</div>
</div>
</td>
</tr>
</table>

</div>




<script language="javascript">
document.title="Доступ запрещен";
</script>