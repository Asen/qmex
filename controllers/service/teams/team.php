<?php

$id = (int)@$_GET["id"];

Smarty::$DATA['db']->query("SELECT ID, Name, Description, Year, Motto, Sphere, Sphere_keys, Logo, Owner, Date, Web FROM qmex_teams WHERE ID=$id");
$data = Smarty::$DATA['db']->one();

$id = (int)$data[0];
$name = htmlspecialchars($data[1]);
$description = splitter(format($data[2]),60);
$year = (int)$data[3];
$motto = splitter(htmlspecialchars($data[4]),60);
$sphere = htmlspecialchars($data[5]);
$keys = htmlspecialchars($data[6]);
$keys = implode(", ",array_filter(explode(" ",$keys)));
$logo = ($data[7]=='') ? '/qmex_img/quer/needle.png':htmlspecialchars($data[7]);
$owner = $data[8];
$date = $data[9];
$web = $data[10];

?>

<div class="WContent ui-team">
<table style="width:100%" cellpadding="10px" cellspacing="10px" border="0">
<tr>
<td width="200px" valign="top"><img src='<?php echo $logo;?>' class="ui-team-logo"></td>
<td valign="top">
<div class="WCaption"><?php echo $name;?></div>
<div class="ui-team-motto" style="font-size:14px; padding:10px"><?php echo $motto;?></div> 
<div class="ui-team-description" style="font-size:14px; padding-top:10px"><?php echo $description;?></div>
<div class="ui-team-site" style="font-size:14px; padding-top:10px; margin-top:10px"><b>Веб-сайт: </b> <a href="<?php echo $web;?>"><?php echo $web;?></a></div>
</td>
</tr>
</table>
</div>


<script>

$(".ui-team-logo").hide();
$(function(){
	
	window.setInterval(function(){
		
		$(".ui-team-logo").each(function(index, element) {
           if(element.complete) {$(element).show(); zoom(element,200,null);} 
        });
		
		},100);
	
	});
	
</script>