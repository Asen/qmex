<?php

Smarty::$DATA['db']->query("SELECT ID, Name, Description, Year, Motto, Sphere, Sphere_keys, Logo, Owner, Date 
							FROM qmex_teams ORDER BY ID DESC");
while($data = Smarty::$DATA['db']->one())
{
	$id = (int)$data[0];
	$name = htmlspecialchars($data[1]);
	$description = splitter(format($data[2]),60);
	$year = (int)$data[3];
	$motto = splitter(htmlspecialchars($data[4]),60);
	$sphere = htmlspecialchars($data[5]);
	$keys = htmlspecialchars($data[6]);
	$keys = implode(", ",array_filter(explode(" ",$keys)));
	$logo = ($data[7]=='') ? '/qmex_img/quer/needle.png':htmlspecialchars($data[7]);
	$owner = $data[8];
	$date = $data[9];
	$IBASE = new Interests();
	
	$href = "teams?action=team&id=$id";
	
	echo '
	
	<div class="ui-team-box" id="'.$id.'">
	<a href="'.$href.'" class="ui-team-link"><div class="ui-team-caption">'.$name.'</div></a>
	<div class="ui-team-motto">'.$motto.'</div>
	<div class="ui-team-info">
	<table class="ui-team-info-table" cellspacing="10px">
	<tr>
	<td width="100px" valign="top"><a href="'.$href.'" class="ui-team-link"><img src="'.$logo.'" class="ui-team-logo"></a></td>
	<td valign="top"><div class="ui-team-description">'.$description.'</div></td>
	</tr>
	<tr>
	<td valign="top" colspan="2">
	<table style="width:100%" cellspacing="10px">
	<tr>
	<td>
	<div class="ui-team-sphere">'.$IBASE->SelectName($sphere).'</div>
	<div class="ui-team-keys">'.$keys.'</div>
	</td>
	<td>
	<div class="ui-team-go"><a href="'.$href.'"><button class="qButton ui-team-link"> Исследовать >> </button></a></div>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	</div>
	</div>
	
	';
	
	}

?>

<script>

$(".ui-team-logo").hide();
$(function(){
	
	window.setInterval(function(){
		
		$(".ui-team-logo").each(function(index, element) {
           if(element.complete) {$(element).show();  zoom(element,100,null);} 
        });
		
		},100);
	
	});

</script>