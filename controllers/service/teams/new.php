
<style>
.command_label{ font-size:12px; font-family:Arial, Helvetica, sans-serif }
</style>

<div class='WCaption'>
Создание новой команды
</div>
<div class='WContent'>

<table style="width:100%;" cellpadding="10px" cellspacing="15px" border="0">

<tr>
<td colspan="2" width="75%"></td>
<td valign="top" width="25%" style="border-left:1px dashed #069; padding:5px; font-size:11px; font-family:Arial, Helvetica, sans-serif" rowspan="100">
<b>5 вещей, которые следует знать при создании собственной команды:</b>
<hr size="1" color="#CCC"><br>
<b>1.</b> Команда - это мини объединение пользователей с общими интересами и намерениями.
<br><br>
<b>2.</b> Каждая команда имеет основную сферу деятельности. Cоздается для того, чтобы найти людей для работы над общей целью/задачей, распределив при этом между ними должности. 
<br><br>
<b>3.</b> Присоединиться к команде сможет любой, кому будет интересна её деятельность. Если человек нужен команде, и он будет одобрен её лидером, то такой человек получает членство и должность. 
<br><br>
<b>4.</b> По умолчанию лидер команды - это её создатель. Но лидер может меняться в процессе путем голосования участников или по его собственному желанию.
<br><br>
<b>5.</b> Команды могут взаимодействовать с другими объединениями, сотрудничать с их участниками по взаимному соглашению лидеров.
Все команды имеют свой новостной блог, обратную связь с независимыми пользователями, списки планов и задач и многое другое.
</td>
</tr>

<tr>
<td colspan="2">
<div class="command_label">Название команды:</div>
<input type="text" style="width:90%" class="setter_field" id="name" placeholder='[Musical doodles.]'>
</td>
</tr>

<tr>
<td>
<div class="command_label">Описание:</div>
<textarea style="width:90%; height:100px" class="setter_field" id="descr" placeholder="[Мы занимаемся созданием абстрактных музыкальных композиций для поднятия настрения!]"></textarea>
</td>
</tr>

<tr>
<td>
<table style="width:100%">
<tr>
<td width="20%">
<div class="command_label">Год основания:</div>
<select class="setter_field" style="width:100px" id="year">
<?php
for($i=date("Y");$i>=date("Y")-300;$i--) echo "<option>$i</option>";
?>
</select>
</td>
<td width="80%">
<div class="command_label">Слоган:</div>
<input type="text" style="width:85%" class="setter_field" id="motto" placeholder='[ Музыка - глушитель печали. ]'>
</td>
</tr>
</table>
</td>
</tr>

<tr>
<td>
<table style="width:100%" border="0">
<tr>
<td width="35%">
<div class="command_label">Cфера деятельности:</div>
<select class="setter_field" style="width:100%" id="sphere">
<option value="-1" id="a-stuff">Музыка</option>
<?php
$IBASE = new Interests();
foreach($IBASE->SelectIBASE() as $int) 
{
	$KEY = $IBASE->SelectKey($int);
	echo "<option value='".$KEY."'>".$IBASE->SelectName($KEY)."</option>";
}
?>
</select>
</td>
<td width="65%">
<div class="command_label">Пояснение сферы деятельности:</div>
<input type="text" style="width:85%" class="setter_field" id="keys" placeholder="[ абстракционизм, каракули, мелодии, ... ]">
</td>
</tr>
</table>
</td>
</tr>

<tr>
<td>
<div class="command_label">Логотип( ссылка на изображение ):</div>
<input type="text" style="width:90%" class="setter_field" id="logotype">
</td>
</tr>

<tr>
<td>
<div class="command_label">Официальный сайт( если имеется ):</div>
<input type="text" style="width:90%" class="setter_field" id="web" placeholder="[ http://www.musical-doodles.com ]">
</td>
</tr>

<tr>
<td style="text-align:center">
<table style="width:90%" cellspacing="5px" border="0">
<td width="70%">
<div id="creation_notify"></div>
</td>
<td width="30%">
<div style="width:90%; text-align:right">
<button class='qButton' style="padding:5px" onclick='create_team()'>Создать команду</button>
</div>
</td>
</table>
</td>
</tr>

</table>

</div>

<script>

$(function(){
	/*$(".setter_field").css({'color':"#CCC"});
	$(".setter_field").one('mousedown',function(){
		 $(this).val('');
		 $(this).css({'color':'#039'});
		 if($(this).attr("id")=="sphere") $("#a-stuff").remove(); 
		 });*/
	});
	
function create_team()
{
	name = $("#name").val().trim();
	descr = $("#descr").val().trim(); 
	year = $("#year").val().trim(); 
	motto = $("#motto").val().trim();
	sphere = parseInt($("#sphere").val());
	keys = $("#keys").val().trim(); 
	logo = $("#logotype").val().trim();
	web = $("#web").val().trim();
	
	if(name!='' && descr!='' && year!='' && motto!='' && sphere!='')
	{
	var res = SendDynamicMsg(['team','name','descr','year','motto','sphere','keys','logo','web'],
						 [1,name,descr,year,motto,sphere,keys,logo,web],
						 null,
						 'handlers/team.php'); 
	if(res.indexOf("<success>")!=-1){
		res = res.replace("<success>","");
		WINDOW.ShowMessage("Ваша команда создана!",WINDOW.C.SUCCESS);
		window.setTimeout(function(){ location.href='teams?action=team&id='+res },1500);
		} 
	}
						 else 
	WINDOW.ShowMessage("Вы не указали ничего о своей новой команде!<br>Как же так?",WINDOW.C.ERROR)
	
	}

</script>