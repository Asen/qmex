<div class="WCaption">Мои открытые диалоги</div>
<br><br>

<div id="dialogs-collection">
<?php

$iam = $_SESSION["id"];
Smarty::$DATA['db']->query("SELECT id FROM qmex_dialogs WHERE Member1=$iam OR Member2=$iam ORDER BY updated DESC");
if(Smarty::$DATA['db']->rowCount()==0) echo "<div id='NeutralMsg' style='font-size:9pt'>У вас нет открытых диалогов.</div>";
else
while($dialog_id = Smarty::$DATA['db']->one(0)) 
echo Communication::ShowDialogSign($dialog_id);

?>
</div>

<script language="javascript">

$(function(){
	$('.ui-dialog-close-dialog').live('click', function(){ closeDialog(this); })
	dialogsAlign();
	})

function dialogsAlign()
{
	$(".ui-dialog-box:odd").css("margin-left","20%");
	}

function closeDialog(o)
{
	var dialog = $(o).attr('tag');
	WINDOW.Confirm('Вся переписка будет полностью удалена. Вы точно желаете закрыть этот диалог?',
					function(){ 
					var res = SendDynamicMsg(['ui_close_dialog'],[dialog],null,"handlers/uiHandler.php","POST"); 
					if(res=='+') $("#dialog-box-"+dialog).slideUp(300);
					});
	}
</script>