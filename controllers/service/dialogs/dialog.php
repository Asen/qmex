<?php
if(!isset($_GET["dialog"])) site('/');
$dialog = (int)$_GET["dialog"];
?>

<div class="WCaption">
Диалог
</div>
<a style="font-family:Arial, Helvetica, sans-serif;font-size:11px;" href='/dialogs'>Вернуться к диалогам >> </a>
<br><br>

<div id='dialog-message-collection'>
<?php

Smarty::$DATA['db']->query("SELECT id FROM qmex_dialogs_msg WHERE dialog_id=$dialog ORDER BY id DESC");	
while($msg_id = Smarty::$DATA['db']->one(0))
echo Communication::ShowDialogMessage($msg_id);	

?>
</div>


<script>

msg_c = null;
var $DIALOG_ID=<?php echo (int)$_GET["dialog"] ?>;

$(function(){
		fast_sender_init();
		COMMON.IFrameTest(".message-frame");
		$(".ui_fast_msg_sender").live('click',function(){ui_fast_answer(this)});
		$(".ui_removing_msg").live('click',function(){ui_act_msg(this,0)});
		$(".ui_check_msg").live('click',function(){ui_act_msg(this,1)});
		$(".ui_graduate_msg").live('click',function(){ui_act_msg(this,2)});
		window.setInterval(function(){ui_check_new_message();},1000);
	});
	
function ui_act_msg(obj,type)
{
	var id = $(obj).attr("tag");
	var res = SendDynamicMsg(['ui_msg_act','type'],[id,type],null,"handlers/uiHandler.php","POST");
	res = parseInt(res);
	
	if(res==0){ 
		WINDOW.ShowMessage("Нельзя произвести это действие.", WINDOW.C.ERROR); 
		return; 
	}
	
	switch(type){
		case 0: $("#ui-message-"+id).slideUp('slow'); 
				break;
		case 1: $("#uiCheckMessage_"+id).hide('slow');
				break;
		case 2: if(res==2){
					WINDOW.ShowMessage("Недостаточно qCoins!", WINDOW.C.ERROR);
					break;
				}
				$("#ui-msg-gratitude"+id).hide('slow');
				break;
				}
	}

function fast_sender_init()
{
	$(".ui_fast_sender").toggle(function(){ui_write_fast_msg_show(this)},function(){ui_write_fast_msg_hide(this)});
	}
	
function ui_write_fast_msg_show(obj)
{
	$(obj).children('.ui_f_txt').text('Отмена');	
	var id = $(obj).attr('tag');
	$('#ui_sent_bar_'+id).stop();
	$('#ui_sent_bar_'+id).slideDown('slow');
	$('.uiUserMessageBox').focus();
	}
	
function ui_write_fast_msg_hide(obj)
{
	$(obj).children('.ui_f_txt').text('Ответить');	
	var id = $(obj).attr('tag');
	$('#ui_sent_bar_'+id).stop();
	$('#ui_sent_bar_'+id).slideUp('slow')
	$('.uiUserMessageBox').focus()
	}
	
function ui_fast_answer(obj)
{
	var id = $(obj).attr("tag");
	var msg = $("#uiFastAnswer_"+id).val();
	$("#ui_snd_"+id).click();
	var current = $(".ui-message").length;
	answer = SendDynamicMsg(['ui_fast_answer','answer_of','ui_msg'],[1,id,msg],null,"handlers/uiHandler.php","POST");
	
	$('body').append(answer);
	
	$('#ui-msg-answer-'+id).trigger('click');
	$("#uiFastAnswer_"+id).val("");
	
	}

function ui_check_new_message()
{
	$.ajax({
		   type:'GET',
		   data:"get_dialog_messages_count="+$DIALOG_ID,
		   url:"handlers/uiHandler.php",
		   dataType:"text",
		   async:true,
		   error:function(e,err){/*alert(err)*/},
		   success:function(msg_count)
		   {
			   if(parseInt(msg_count)>parseInt(msg_c))
			   {		  
				   $.ajax({
						   type:"GET",
						   url:"handlers/uiHandler.php",
						   data:"complete-message-for-dialog=1&id="+$DIALOG_ID,
						   async:false,
						   dataType:"html",
						   success:function(newMsg)
						   {
							   $("#dialog-message-collection").prepend(newMsg);
							   $(".ui-message:first").slideUp(0);
							   $(".ui-message:first").slideDown('slow');
							   fast_sender_init();
							   COMMON.IFrameTest(".message-frame");
							   }
				          });
			   }
			   msg_c = parseInt(msg_count);
			   }
		   });
	}


</script>