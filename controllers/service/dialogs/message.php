<?php

include_once("tools/sided/veditor.php"); 
//if(!isset($_GET["whom"]) && !isset($_GET["deal-swop"])) site('/dialogs');

$cause   =   isset($_GET["cause"])     ? (int)$_GET["cause"] : -1;
$essence =   isset($_GET["id"])        ? (int)$_GET["id"] : -1;
$answer_of = isset($_GET['answer_of']) ? (int)$_GET["answer_of"] : -1;

$dialog_id = -1;

if($answer_of>0){
	$db = new DB();
	$db->query("SELECT d.Cause, d.EssenceID, d.Member1, d.Member2, d.id FROM qmex_dialogs_msg AS m 
				INNER JOIN qmex_dialogs AS d ON d.id=m.dialog_id AND m.id=$answer_of ");
	$data = $db->one();
	
	$iam     = $_SESSION['id'];
	$cause   = $data[0];
	$essence = $data[1];
	$member1 = $data[2];
	$member2 = $data[3];
	$dialog_id = $data[4];
	if($member1!=$iam && $member2!=$iam)  die(site('/'));
	$causeInfo = Communication::getInfoForMessage(Communication::CAUSE_MSG_ANSWER, $answer_of);
	}
	else
$causeInfo = Communication::getInfoForMessage($cause, $essence);



function getSmileCollection($type)
{
$simple_smiles_dir = "qmex_img/smiles/".$type."/";
$name = ($type=='smiles_advanced') ? "face":"icon";
$ext = ($type=='smiles_advanced') ? '.png':'.gif';
$_sz = ($type=='smiles_advanced') ? 'width="50px"':"";
$smiles = array();
$i = 1;
$filename = $simple_smiles_dir.$name.$i.$ext;
while(file_exists($filename))
{
array_push($smiles,$filename);
$i++;
$filename = $simple_smiles_dir.$name.$i.$ext;
}

$i=0;
echo "<tr>";
foreach($smiles as $smile)
{
	if($i%5==0) echo "</tr><tr>";
	echo "<td><img class='ui_single_simple_smile' src='$smile' $_sz></td>";
	$i++;
	}
	}

?>

<style>
.qSlider{
	width:20px; 
	margin:5px;
	}
</style>


<div class="WContent">
<div class="WCaption"><img src="/qmex_img/expanded_msg.png" height="10px" style="padding-right:5px">Написать сообщение</div>
<div class="WUnderCaption">
<?php echo $causeInfo['caption']; ?>
<br>
<?php if($dialog_id>-1) echo '<a href="/dialogs?dialog='.$dialog_id.'">Перейти к диалогу >></a><br>'; ?>
<?php echo $causeInfo['additional']; ?>
<?php if($cause==Communication::CAUSE_SWOP_CONNECTION) SWOP::PrintJSRoutine(); ?>

</div>
<table style="width:100%; margin:20px auto" border="0" cellspacing="5px">
<tr>
<td style="width:70%" valign="top">
<textarea rows="7" style="width:95%" class="uiExpandedMessager" id="MainMessageBox"></textarea>
<div style="padding-top:20px; width:100%; text-align:left; margin:0 auto;">
<input type="button" class="qButton" value="Отправить сообщение" onclick="expand_msg_sender(this)">
<input type="button" class="qButton_silver" value="Отмена" 
onclick="location.href='/dialogs'">
</div>
<div style="width:200px" id="ui_expanded_cap"></div>
</td>
<td style="width:30%" valign="top" id="smile-tool-box">


<div id="ui_smiles_box">
<div id="ui_smile_tools"><input type="button" class="qButton qEdit qSlider" value="+" tag='Развернуть'> Простые смайлы</div>
<div id="ui_simple_smiles" class="ui_sbox">
<table id="ui_simple_smiles_table" border="0">
<?php
getSmileCollection("smiles");
?>
</table>
</div>

<div id="ui_smile_tools"><input type="button" class="qButton qEdit qSlider" value="+" tag='Развернуть'> Расширенные</div>
<div id="ui_simple_smiles" class="ui_sbox">
<table id="ui_simple_smiles_table" border="0">
<?php
getSmileCollection("smiles_better");
?>
</table>
</div>

<div id="ui_smile_tools"><input type="button" class="qButton qEdit qSlider" value="+" tag='Развернуть'> Эмоции</div>
<div id="ui_simple_smiles" class="ui_sbox">
<table id="ui_simple_smiles_table" border="0">
<?php
getSmileCollection("smiles_advanced");
?>
</table>
</div>


</div>

</td>
</tr>
</table>

</div>


<script language="javascript">

$(document).ready(function(){
	
	InitEditor("#MainMessageBox",($("#ui_simple_smiles").height()*3)+"px");
	
	$(".ui_single_simple_smile").on('click',function(){
		var img = "<img src='"+$(this).attr("src")+"' />";
		try{
			tinyMCE.execCommand('mceInsertContent',false, img);
		}catch(e){
			$(GetEditor("MainMessageBox")).append(img);
			}
		});
		$('.qSlider').toggle(function(){$(this).parent("div").next(".ui_sbox").css({height:"auto"}); 
		                                $(this).attr("value","-");$(this).attr("tag","Свернуть")},
		                     function(){$(this).parent("div").next(".ui_sbox").animate({height:"60px"},500);
							            $(this).attr("value","+");$(this).attr("tag","Развернуть")});
	});


function expand_msg_sender(obj)
{
	var msg =  GetEditor('MainMessageBox').getContent();
	if(msg.length>4500) show_wnd('Сообщение слишком длинное!',100,true,2.5,'red'); else
	var result = SendDynamicMsg(['ui_exp_msg','to','cause','essence','answer_of'],
								[msg,<?php echo $causeInfo['id-to'] ?>,<?php echo $cause ?>,<?php echo $essence ?>,<?php echo $answer_of?>],
								"ui_expanded_cap",
								"handlers/uiHandler.php");
	if(result.indexOf("send-success"))
	{
		GetEditor("MainMessageBox").setContent("");
		}
	}

</script>