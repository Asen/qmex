<style>
#pageview-list{ cursor:pointer; font-weight:bold;}
#apply-view{ padding:7px }
.view-select:hover{ background-color:#88C4FF }
.view{width:100%;}
.scheme-name{padding:5px};
.selected-scheme{background-color:#069; color:white;}
</style>

<?php

$db = new db();
$this_user = @$_SESSION["id"];
$background = "";
$scheme = 1;
if(trim($this_user)!='')
{
$db->query("SELECT background, background_style, opacity, pagescheme FROM qmex_user_settings WHERE Login=$this_user");
	if($db->rowCount()>0)
	{
	$data = $db->one();
	if(trim($data[0])!='') $background = $data[0];
	                       $opacity = number_format(floatval($data[2]),3,'.','');
						   $bg_style = $data[1];
						   $scheme = $data[3];
	}
}

?>

<div class="clarification-box">
Здесь вы можете настроить стилизацию своей страницы
</div>

<table style="margin:20px auto; border:none; width:80%; font-family:Arial, Helvetica, sans-serif; font-size:11px" cellspacing="15px" border="0" t>
<tr>
<td colspan="2">Изменить фоновое изображение страницы qMex<br><hr class="subline"></td>
</tr>
<tr>
<td valign="top"><div class="ui_setter_caption">URL изображения:</div><input type="text" class="setter_field" id="ui_sett_bg" style="width:500px" value="<?php echo $background?>"><br>
<span id="mark">(Фоновое изображение будет видно всем пользователям системы qMex. Советуем вам устанавливать на фон изображения с размерами более 1000x1000 пикселей)</span>
</td>
</tr>
<tr>
<td colspan="2"><input type="checkbox" class="bg-type" <?php if($bg_style==2) echo 'checked="checked"';?>><span style="padding-left:3px;padding-right:10px">Растянуть</span>
<input type="checkbox" class="bg-type" <?php if($bg_style==1) echo 'checked="checked"';?>><span style="padding-left:3px;">Замостить</span></td>
</tr>
<tr>
<td colspan="2" id="ui_bg_tools" align="right"><input type="button" class="qButton" value="Применить" id="sett_change_bg"><br>
<div id="ChangeBgBox" style="padding-top:5px; height:20px"></div><br>
</td>
</tr>

<tr>
<td colspan="2">Прозрачность профиля( настраивается совместно с фоновым изображением )<br><hr class="subline"></td>
</tr>
<tr>
<td>
<input type="range" min="0" max="500" step="1" value="<?php echo ($opacity-0.5)*1000; ?>" style="cursor:pointer" id="sett_opacity_val">
<div id="ChangeOpacityBox" style="padding-top:5px; height:20px"><?php echo $opacity;?></div> 
</td>
</tr>
<tr>
<td colspan="2" align="right"><input type="button" class="qButton" value="Применить" id="sett_opacity"><br>
<div id="ChangeBgBox" style="padding-top:5px; height:20px"></div><br>
</td>
</tr>

<tr>
<td colspan="2">Вид страницы<br><hr class="subline"></td>
</tr>
<tr>
<td>

<table cellpadding="0" cellspacing="10" border="0" id="pageview-list" class="full-width">
<tr>
<td class='view-select' tag='0'>
<img src="/qmex_img/service/cscheme-views/scheme-1.png" class="view">
</td>
<td class='view-select' tag='1'>
<img src="/qmex_img/service/cscheme-views/scheme-2.png" class="view">
</td>
<td rowspan="15" valign="top" align="right"><input type="button" class="qButton" value="Применить" id='apply-view'></td>
</tr>
<tr>
<td class="scheme-name" tag='0'>Схема-1</td>
<td class="scheme-name" tag='1'>Схема-2</td>
</tr>
<tr>
<td class='view-select' tag='2'>
<img src="/qmex_img/service/cscheme-views/scheme-3.png" class="view">
</td>
<td class='view-select' tag='3'>
<img src="/qmex_img/service/cscheme-views/scheme-4.png" class="view">
</td>
</tr>
<tr>
<td class="scheme-name" tag='2'>Схема-3</td>
<td class="scheme-name" tag='3'>Схема-4</td>
</tr>
<tr>
<td class='view-select' tag='4'>
<img src="/qmex_img/service/cscheme-views/scheme-5.png" class="view">
</td>
</tr>
<tr>
<td class="scheme-name" tag='4'>Схема-5</td>
</tr>
</table>

</td>
</tr>

</table>

<script language="javascript">
$("#else_settings").css({'background-color':'#1D66AF','color':'white'});


$(function(){
	if($("#ui_sett_bg").val().length>3) 
	{
		$("#ui_bg_tools").prepend('<input type="button" class="qButton_silver" value="Удалить" id="sett_change_bg_rem"> ');
		$("#sett_change_bg_rem").click(function(){
		SendDynamicMsg(['ui_set_bg','bg_type'],["",0],'ChangeBgBox','/handlers/settings.php');
		window.setTimeout(function(){location.href='/';},500);
		});	
	}
	
	selected=<?php echo $scheme;?>;
	$(".scheme-name").eq(selected).css({"background-color":"#069","color":"white"});
	
	$(".view-select, .scheme-name").click(function(){
		    selected = parseInt($(this).attr('tag'));
			$(".scheme-name").css({"background-color":"transparent","color":"#069"});
			$(".scheme-name").eq(selected).css({"background-color":"#069","color":"white"});
			});
	$("#apply-view").click(function(){
		SendDynamicMsg(['ui_set_view','view-id'],["",selected],'ChangeView','/handlers/settings.php');
		window.setTimeout(function(){location.href='/';},500);
		});
	
	});

$("#sett_change_bg").click(function(){
	var bg_type = (typeof $($(".bg-type")[0]).attr("checked")==="undefined") ? 1:2;
	var url = encodeURIComponent($("#ui_sett_bg").val());
	a=SendDynamicMsg(['ui_set_bg','bg_type'],[url,bg_type],'ChangeBgBox','/handlers/settings.php');
	window.setTimeout(function(){location.href='/';},500);
	});
	
$("#sett_opacity").click(function(){
	var opacity = $("#ChangeOpacityBox").text();
	a=SendDynamicMsg(['ui_set_opacity'],[opacity],null,'/handlers/settings.php');
	window.setTimeout(function(){location.href='/';},500);
	});


$(".bg-type").click(function(){$(".bg-type").removeAttr("checked"); $(this).attr("checked","checked");});
$("#sett_opacity_val").change(function(){
	var pos = parseInt($(this).val(),10)/1000+0.5;
	$("#ChangeOpacityBox").text(pos.toFixed(3).toString());
	$("#qmex_site").css({"opacity":pos.toString()});
	});
</script>