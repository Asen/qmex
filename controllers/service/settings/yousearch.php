<div class="clarification-box">
Здесь вы можете настроить систему поиска вас другими пользователями.<br>
Например, указать, по каким словам вас можно будет отыскать.
</div>

<?php

$uid = $_SESSION['id'];
Smarty::$DATA['db']->query("SELECT DescrWords FROM qmex_users WHERE ID=$uid");
$descrwords = Smarty::$DATA['db']->one(0);

?>

<table style="margin:20px auto; border:none; width:80%; font-family:Arial, Helvetica, sans-serif; font-size:11px" cellspacing="15px" border="0" t>
<tr>
<td colspan="2">Ключевые слова о вас, вашей деятельности и интересах, по которым вас можно будет легко найти другим пользователям qMex:<br><hr class="subline"></td>
</tr>
<tr>
<td colspan="2">
<input type="text" class="setter_field" maxlength="255" style="width:70%" id="descrwords" placeholder="вы не указали ни одного слова..." value="<?php echo $descrwords; ?>">
<input type="button" class="qButton" value="Применить слова" onclick="update_descrwords()">
<div id='mark'>( для разделения используйте запятую или пробел )</div>
<div id="ChangeDescrWords" style="padding-top:5px; height:20px"></div>
</td>
</tr>
</table>


<script>

$("#search_settings").css({'background-color':'#1D66AF','color':'white'});

function update_descrwords()
{
	var descrwords = $("#descrwords").val();
	var res = SendDynamicMsg(['ch_descrwords'],[descrwords],'ChangeDescrWords','/handlers/settings.php')
	}

</script>