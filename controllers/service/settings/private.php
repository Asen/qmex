<div class="clarification-box">
Здесь вы можете изменить свои авторизационные данные.<br>Например, свой <b>qID( логин )</b> или <b>пароль</b>.
</div>

<form method="post">

<table style="margin:20px auto; border:none; width:80%; font-family:Arial, Helvetica, sans-serif; font-size:11px" cellspacing="15px" border="0" t>
<tr>
<td colspan="2">Изменить qID<br><hr class="subline"></td>
</tr>
<tr>
<td colspan="2">
<b>qID( qMex ID )</b> - ваше уникальное имя в системе<br>
<b>Текущий qID: <span style="color:#555"><?php echo $_SESSION["login"];?></span></b><br><br>
<div class="ui_setter_caption">Новый qID:</div>
<input type="text" class="setter_field" maxlength="40" id="sett_qid">
<input type="button" class="qButton" value="Готово" id="sett_change_qid">
<br>
<div id="ChangeQID" style="padding-top:5px; height:20px"></div>
</td>
</tr>

<tr>
<td colspan="2">Изменить пароль<br><hr class="subline"></td>
</tr>
<tr>
<td rowspan="2" valign="top"><div class="ui_setter_caption">Текущий пароль:</div><input type="password" class="setter_field" id="sett_curr_pass"><br>
<div id="CheckPassBox" style="padding-top:5px"></div>
</td>
<td><div class="ui_setter_caption">Новый пароль:</div><input type="password" class="setter_field" id="sett_new_pass"></td>
</tr>
<tr>
<td><div class="ui_setter_caption">Повторите пароль:</div><input type="password" class="setter_field" id="sett_new_repass"></td>
</tr>
<tr>
<td colspan="2"><input type="button" class="qButton" value="Сменить пароль" id="sett_change_pass"><br>
<div id="ChangePassBox" style="padding-top:5px; height:20px"></div>
</td>
</tr>

<tr>
<td colspan="2">Изменить E-Mail<br><hr class="subline"></td>
</tr>
<tr>
<td colspan="2">
<b><u>Текущий E-Mail:</u> <?php echo $_SESSION["email"];?></b><br><br>
<div class="ui_setter_caption">Новый E-Mail:</div>
<input type="text" class="setter_field" id="sett_email">
<input type="button" class="qButton" value="Готово" id="sett_change_email">
<br>
<div id="ChangeMail" style="padding-top:5px; height:20px"></div>
</td>
</tr>
</table>

</form>
<script language="javascript">
$("#private_settings").css({'background-color':'#1D66AF','color':'white'});

$("#sett_curr_pass").live('change',function(){
	$('.sett_notify').remove();
	SendDynamicMsg(['check_curr_pass'],[$(this).val()],'CheckPassBox','handlers/settings.php','POST');
	});
$("#sett_change_pass").live('click',function(){
	$('.sett_notify').remove();
	SendDynamicMsg(['cp','np','rnp'],[$("#sett_curr_pass").val(),$("#sett_new_pass").val(),$("#sett_new_repass").val()],'ChangePassBox','handlers/settings.php','POST');
	});
$("#sett_change_email").live("click",function(){
	SendDynamicMsg(['change_email'],[$("#sett_email").val()],'ChangeMail','handlers/settings.php','POST');
	if(req.indexOf("sett_success")>0) window.setTimeout(function(){location.href=location.href;},1000);
	});
$("#sett_change_qid").live("click",function(){
	var req = SendDynamicMsg(['change_qid'],[$("#sett_qid").val()],'ChangeQID','handlers/settings.php','POST');
	if(req.indexOf("sett_success")>0) window.setTimeout(function(){location.href=location.href;},1000);
	});

window.setInterval(function(){$(".sett_notify[tag='1']").slideDown(400);},300);

</script>