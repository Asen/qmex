<?php

$limit = 15;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
if($page==0) $page=1;
$page--;
$page *= $limit;

$IS_SHOW_ALL = ($is_helpers && $_GET['helpers']=='y')   ||   ($is_clients && $_GET['clients']=='y');
if($IS_SHOW_ALL){
	if($is_helpers && $_GET['helpers']=='y') $glob_caption = 'Предложения от людей:'; else
	if($is_clients && $_GET['clients']=='y') $glob_caption = 'Потребности и желания людей:';
	}

$DB = new DB();

if(!$IS_SHOW_ALL)
{
	$swop_id = $is_clients ? (int)$_GET['clients'] : (int)$_GET['helpers'];
	$DB->query("SELECT ServiceType, Specialization, Login FROM qmex_swop WHERE ID=$swop_id");
	if($DB->rowCount()==0) die('Такой Swop не существует!');
	$swop_info = $DB->one();
	$necessary_servicetype    = $swop_info[0];
	$necessary_specialization = $swop_info[1];
	$swop_creator = $swop_info[2];
	
	$type = $is_helpers ? Enum::SwopHelpersSentencies() : Enum::SwopClientsSentencies();
	$type = $type[$necessary_servicetype];
	$spec = new Interests();
	$spec = $spec->SelectName($necessary_specialization); 
	
	$glob_caption = $type.' <span style="color:#09F">"'.$spec.'"</span> : ';
}

$necessary_swoptype = $is_clients ? SWOP::SWOP_USER_NEED : SWOP::SWOP_USER_PROVIDE;

?>

<style>
.swop-owner-name-label{
	padding:5px;
	border-radius: 0px;
	font-size:1.7em;
	color:#06F;
	background-color:#C4E1FF;
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#A4D1FF', endColorstr='#E6F2FF',GradientType=0 );
	background: -moz-linear-gradient(bottom, #E6F2FF 0%, #A4D1FF 99%);
	background: -webkit-linear-gradient(bottom, #E6F2FF 0%, #A4D1FF 99%);
	background: -o-linear-gradient(bottom, #E6F2FF 0%, #A4D1FF 99%);
	background: -ms-linear-gradient(bottom, #E6F2FF 0%, #A4D1FF 99%);
	background: linear-gradient(to bottom, #A4D1FF 0%, #E6F2FF 99%);
	}
.swop-user-rait-box{
	padding:5px;
	background-color:#069;
	color:#CCC;
	font-size:1.5em;
	text-align:center;
	}
.swop-bad-rait{
	background-color: #FFC4C4;	
	color: #C00;
	}
.swop-good-rait{
	background-color: #B9FFB9;	
	color: #093;
	}
.nobody-found{
	padding:50px;
	background-color:#B0D8FF;
	color:#009;
	font-size:2em;
	font-weight:bold;
	}
.additional-info{
	padding:10px;
	color:#06F;
	font-size:0.7em;
	text-decoration:overline;
	}
</style>

<div style="padding:5px; font-size:2em; color:#00F; border-bottom:2px solid #09F">
<?php echo $glob_caption; ?>
</div>

<div class='WCaption' style="border-top: 1px solid #09F; padding-top:5px; font-size:0.8em; font-weight:normal">
<input type="checkbox" onchange="setGeo(this)" <?php if(isset($_GET['geo'])) echo 'checked' ?>> 
Показывать <?php echo $necessary_swoptype==SWOP::SWOP_USER_NEED ? 'потребности':'предложения'?>, которые актуальны для моего текущего местоположения.
</div>

<table style="width:100%" id="swoppers" cellspacing="10px" border=0>
<?php

$S = 1.5;
if(isset($_GET['geo'])) $geoInfo = Human::getGeoInfo(@$_SESSION['id']);
$geo = isset($_GET['geo']) ? " ( ((Users.latitude BETWEEN ".($geoInfo['latitude']-$S)." AND ".($geoInfo['latitude']+$S).") AND 
								 (Users.longitude BETWEEN ".($geoInfo['longitude']-$S)." AND ".($geoInfo['longitude']+$S).") ) OR 
								 (Users.Country LIKE '%".$geoInfo['country']."%' AND Users.City LIKE '%".$geoInfo['city']."%') ) " :
								   '1';
								   
$concrete = $IS_SHOW_ALL ? '' : "
			AND (NOT Login=$swop_creator) AND  ServiceType=$necessary_servicetype AND Specialization=$necessary_specialization
			";
$DB->query("SELECT Users.Login, Users.photo, Users.rating, MINFO.SwopID, Users.Country, Users.City 
			FROM qmex_users as Users, (SELECT ID as SwopID, Login as UserID, Date AS DT FROM qmex_swop 
			WHERE SwopType=$necessary_swoptype ".$concrete.") AS MINFO 
		    WHERE Users.ID=MINFO.UserID AND ".$geo." ORDER BY MINFO.SwopID DESC LIMIT $page, $limit");
		   
//echo mysql_error();
		   
if($DB->rowCount()==0)
echo '<div class="nobody-found">'.( $page==0 ? 'К сожалению, таких людей пока нет.' : 'Больше людей не найдено' ).'</div>';

while($info = $DB->one())
{
	$login = $info[0];
	$image = Human::ResolvePhoto($info[1]);
	$rating = $info[2];
	$swopID = $info[3];
	$country = $info[4];
	$city = $info[5];
	
	$rait_class = $rating<0 ? 'swop-bad-rait' : 'swop-good-rait';
	
	echo '<tr><td colspan="2">
	<a href="profile?id='.$login.'" class="underlined"><div class="swop-owner-name-label">'.String::mb_ucfirst($login).'</div></a></td>
	</tr>';
	echo '<tr>';
	echo '<td width="20%" valign="top">
		  <img src="'.$image.'" class="swop-owner-image" width="0px" />
		  <div class="swop-user-rait-box '.$rait_class.'">'.$rating.'</div>
		  </td>';
	echo '<td align="left" valign="top">'.
		 '<div style="width:70%">'.Swop::CreateSwopById($swopID, true).'</div>'.
		 '<table style="width:100%"><tr>'.
		 '<td>'.
		 '<div style="text-align:left"><a href="profile?id='.$login.'#swop">
		 <button class="qButton">Больше подробностей >></button></a></div>'.
		 '</td><td align="right">'.
		 '<div class="additional-info">'.
		 $country.', '.$city.
		 '</div>'.
		 '</td>'.
		 '</tr></table>'.
		 '</td>';
	echo '</tr>';
	}

?>
</table>

<div id="swop-navigator" style="text-align:center"></div>

<?php  SWOP::PrintJsRoutine(); ?>

<script>

$(function(){
	
	COMMON.AddPagedNav("#swop-navigator");
	Resize2();
	$(window).resize(Resize2);
	
	});
	
function Resize2()
{
	var ContentW = $("#swoppers").width();
	$(".swop-owner-image").attr("width",(ContentW*0.2)+"px")
	}

function setGeo(o)
{
	var q = QUERY.splitGetQuery(location.href);
	q['geo'] = (o.checked) ? 'yes' : '';
	location.href = 'swop?'+QUERY.completeQueryFromHash(q, true);
	}

</script>