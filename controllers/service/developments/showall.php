<?php

$find_bit = "";

$bit     = (isset($_GET["bit"]))     ? mysql_escape_string(trim(mb_strtolower($_GET["bit"]))) : '';
$actuals = (isset($_GET["actuals"])) ? true : false ;

$PAGE_ITEMS_COUNT = 20;
$page = (((isset($_GET["page"])) ? (int)$_GET["page"] : 1) - 1) * $PAGE_ITEMS_COUNT;

$now = qMexTime::getCurrent();

$search = array();
if(isset($_GET["i"]))        array_push($search, "theme=".(int)$_GET["i"]);
if(isset($_GET["ki"]))       array_push($search, "event_kind=".(int)$_GET["ki"]);
if(isset($_GET["useful"]))   array_push($search, "id IN ( SELECT essence FROM qmex_voites 
												  WHERE Type='dev_thank' AND Login=".(int)$_GET["useful"].")");
if(isset($_GET["actuals"]))  array_push($search, "( (actual_time > $now AND actual_time-$now < ".(3*24*3600).") OR 
												  ($now BETWEEN actual_time AND finish) )");
if(isset($_GET['keys'])) {
	$sql = array();
	foreach(explode(" ",str_replace(","," ",$_GET['keys']." ")) as $key) 
		$sql[] = "keywords LIKE '%$key%'"; 
	array_push($search, '('.implode(" AND ",$sql).')');
}
if(isset($_GET['geo']) && Security::isAuthorized()) {
	$geoInfo = Human::getGeoInfo($_SESSION['id']);
	$S = 1.5;
	array_push($search, "(  ((latitude BETWEEN ".($geoInfo['latitude']-$S)." AND ".($geoInfo['latitude']+$S).") AND 
							(longitude BETWEEN ".($geoInfo['longitude']-$S)." AND ".($geoInfo['longitude']+$S).")) OR 
							(country='".$geoInfo['country']."' AND city='".$geoInfo['city']."') 
						 )");
}

$search = sizeof($search)>0 ? ' WHERE '.implode(' AND ',$search) : '';
$limit = (isset($_GET['map'])) ? '' : "LIMIT $page,$PAGE_ITEMS_COUNT";

Smarty::$DATA['db']->query("SELECT id FROM qmex_developments ".$search." ORDER BY ID DESC ".$limit);
$_SESSION['quer_notes_count'] = Smarty::$DATA['db']->rowCount();

$collector = array();
while($dev_id = Smarty::$DATA['db']->one(0))
!isset($_GET['map']) ? array_push($collector,$dev_id) : 
					   array_push($collector, Development::generatePlaceMark($dev_id));


if(!isset($_GET['map'])): ?>

<table class="full-width">
<tr>
<td valign="top" width="50%">
	<?php for($i=0;$i<count($collector);$i+=2) Development::Show($collector[$i]);  ?>
</td>
<td valign="top" width="50%">
	<?php for($i=1;$i<count($collector);$i+=2) Development::Show($collector[$i]);  ?>
</td>
</tr>
</table>
<?php Development::printJSRoutine() ?>

<?php
else:
?>

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 400px"></div>


<script>

var Map;
var placemarks = <?php echo json_encode($collector); ?>;
ymaps.ready(init);

function init(){ 

	navigator.geolocation.getCurrentPosition(initMap, GEO.wasNotFoundLocation, { timeout:3500 });
	
	function initMap(pos)
	{
		var latitude = pos.coords.latitude;
		var longitude = pos.coords.longitude;
		Map = new ymaps.Map("map", {
			center: [latitude,longitude],
        	zoom: 9,
			controls: ['zoomControl','typeSelector','geolocationControl']
			}); 
		
		for(var i in placemarks)
			//placemarks[i].coords[1]=parseFloat(placemarks[i].coords[1])+(Math.random()/1000)*( i%2==0 ? -1:1 );
			Map.geoObjects.add( 
				new ymaps.Placemark(placemarks[i].coords, 
				{
					'hintContent' : placemarks[i].hint,
					'balloonContentBody' : placemarks[i].description,
            		'balloonContentFooter' : placemarks[i].footer,
            		'balloonContentHeader' : placemarks[i].header
					},
				{
					preset: 'islands#icon',
					iconColor: '#069'
					}
					));
		
		$(".ymaps-copyright").remove();
				
		}	
}


</script>

<?php endif; 

unset($collector);

?>
