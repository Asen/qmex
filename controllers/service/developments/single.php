
<style>
.content{
	background: -webkit-linear-gradient(top, #FFF 0%, #E1F0FF 90%);
	}
	
#change-cover-button{
	padding:5px; color:#FFF; 
	background-color:#000; 
	text-align:center;
	font-size:0.8em; cursor:pointer;
	}
#change-cover-button:hover{
	background-color:#111;
	color:#09F;
	}

.thank-button{ font-size:12px; font-weight:normal; padding:2px }
	
</style>

<?php 

if( !isset($_GET["id"]) ) die("StupID?");
$id = (int)$_GET["id"];
$user = @$_SESSION["id"];

$db = new DB();
$db->query("SELECT description,creator,caption,theme,keywords,country,city,picture,actual_time,belong_to_conf,event_kind,finish
			FROM qmex_developments WHERE id=$id");
if($db->rowCount()>0): ?>

<?php 
	$data = $db->one() ;
	$is_already_started = qMexTime::getCurrent() > $data[8];
	
	$linked_conference = (int)$data[9];
	if($linked_conference < 1)
	{
		$db->query("SELECT id FROM qmex_conferences WHERE linked_with_type='event' AND linked_with_id=$id LIMIT 1");
		$linked_conference = $db->one(0);
	}
	
	$db->query("SELECT COUNT(*) FROM qmex_dev_members WHERE event_id=".$id);
	$member_count = $db->one(0);
	
	$db->query("SELECT COUNT(*) FROM qmex_dev_members WHERE event_id=$id AND user=$user");
	$is_user_connected = $db->one(0) == 1;
	
	$is_owner = $data[1]==$user;
	$creator_info = Human::getUserInfo($data[1]);
	
	$is_achievement = $data[10] == Enum::$DEVELOPMENT_TYPES['COMPETITION'];	
	if($is_achievement)
	{
		$db->query("SELECT id FROM qmex_achievements WHERE belong_to_dev=$id");
		$attached_achievement = $db->one(0);
		$owner = Achievement::getOwner($attached_achievement);
		}	
		
	$finish = $data[11];
	$is_already_finished = qMexTime::getCurrent() > $finish;
		
?>


<div id="dev-kind" style="background-color:#000; opacity:0.75; padding:15px; font-weight:bold; font-size:1.3em; color:#FFF;">
<?php 
if($data[10]>0)
{
	$ET = Enum::DevelopmentTypes();
	echo $ET[$data[10]];
}
?>
</div>


<img src="<?php echo $data[7];?>" id="dev-image" width="100%" style="max-height:400px; margin-bottom:-5px">


<?php if($is_owner): ?>
    <div id="change-cover-button">
    Изменить обложку события
    </div>
<?php endif; ?>


<div id="dev-head" style="background-color:#06F; padding:15px; font-weight:bold; font-size:1.6em; color:#EEE">
<?php echo $data[2];?>
</div>


<div style="padding:5px; background-color:#FFF; color:#06F; font-size:1.1em; font-weight:bold">
<table class="full-width" border="0">
<tr>
<td valign="top" align="left">
	<?php if(!$is_user_connected && !$is_already_finished && !$is_owner): ?>
		<button class='qButton' onclick='connectTo()' style="padding:5px">Присоединиться к событию!</button>
    <?php else: ?>
    	<?php if($is_user_connected): ?> <div style="font-size:12px">Вы участник этого события.</div> <?php endif ?>
        <a onclick="return false;" id="connectors-roll" style="font-size:0.7em; font-weight:normal" class="underlined">
            Всего присоединились <b><?php echo $member_count ?></b> людей
            <img src="/qmex_img/arrow_down_black.png" width="10px">
        </a>
        <div id="connectors-list" style="font-size:0.7em; padding:5px; border-left:1px solid #DDD">
        	<?php 
				$db->query("SELECT user FROM qmex_dev_members WHERE event_id=".$id." LIMIT 10"); 
				while($u_iter = $db->one(0)){
					$u_name = Human::getLoginById($u_iter);
					$u_img = Human::getUserPhoto($u_iter);
					$was_thanked = Development::wasThanked($id,$u_iter);
					echo '<a href="/profile?id='.$u_name.'" style="font-size:12px">
					<img src="'.$u_img.'" width="25px" height="25px" style="border-radius:40px; border:1px solid #777; margin-right:5px" />'.
					$u_name.'</a>'.
					($is_owner && !$was_thanked? ' <button class="qButton thank-button" tag="'.$u_iter.'">Поблагодарить</button>' : '').
					($was_thanked ? '<span style="font-size:12px; color:#CCC; padding-left:10px">Выражена благодарность</span>':'').
					'<br>';
				}
			?>
        </div>
    <?php endif; ?>
</td>
<td valign="top">
	<span style="font-size:14px; font-weight:normal"><?php echo $data[5] ?></span> , 
	<?php echo $data[6]  ?>
</td>
<td align="right" valign="top">
	<?php echo translate_timestamp_shorten($data[8]).' - '.translate_timestamp_shorten($finish) ?><br>
    <span style="font-size:0.7em; color:<?php echo $is_already_finished ? '#900':'#093' ?>">
    <?php echo Development::getReadableTimeResidue($data[8], $finish) ?> 
    </span>
</td>
</tr>
</table>
</div>

<div style="background-color:#FFF; padding:20px" class="content">
<table class="full-width" cellspacing="5px" cellpadding="0" border="0">
<tr>
	<td width="75%" valign="top">    	
    	<iframe srcdoc='<?php echo addFrameStyles($data[0]) ?>' class='development-frame' 
        frameborder='0' height='0px' style="max-height:450px" scroll='no' seamless></iframe> 
           
		<div style="padding-top:10px; border-top:1px solid #CCC">
        <a href="profile?id=<?php echo $creator_info['login'] ?>">
        	<table class="qHint" tag="Создатель" cellspacing="5px" cellpadding="0" border="0">
            <tr>
            	<td><img src="<?php echo $creator_info['pic'] ?>" height="35px" style="border-radius:5px"></td>
                <td><div style="font-size:20px"><?php echo $creator_info['login'] ?></div></td>
            </tr>
        	</table>
        </a>    
    	</div>
                    
    </td>
    <td width="25%" valign="top">
    	<?php if($is_achievement): ?>
        	<div id="attached_achievement">
                <div style="padding-bottom:5px; font-weight:bold; font-size:12px">Предлагаемое достижение</div>
                <?php Achievement::ShowBox($attached_achievement); ?>
                
                <?php 					
					if($owner>0):
					$owner = Human::getLoginById($owner);
				?>
                	<div style="padding-top:10px; padding-bottom:10px; font-size:12px; font-weight:bold; color:#C00">
                    	Достижение было вручено<br>
                        пользователю <a style="font-size:15px" href='/profile?id=<?php echo $owner?>'><?php echo $owner ?></a>
                    </div>
                <?php else:?>
                    <div style="padding-top:5px; padding-bottom:5px; font-size:11px">
                    	Будет вручено лучшему участнику создателем конкурса.
                    </div>
                <?php endif ?> 
                               
				<?php if($is_already_started && $is_owner && $owner<0):?>
                    <button class="qButton" style="width:100%; padding:10px" onclick="HandAchievement()">Вручить достижение</button>
                <?php elseif($owner<0): ?>
                	<div style="padding-top:5px; padding-bottom:5px; font-size:11px; font-weight:bold">
                    	<?php echo $is_owner ? 'Достижение  можно будет вручить самому ценному участнику после того, как конкурс начнется.' :
						 'Достижение будет вручено самому ценному участнику в процессе конкурса.'?>                      
                    </div>
                <?php endif ?>
                
        	</div>
        <?php endif ?>
    </td>
</tr>
</table>
</div>

<a href='/talks?conf=<?php echo $linked_conference?>' class="non-underlined">
    <div style="background-color:#069; opacity:0.6; color:#D7EBFF; padding:5px; font-size:0.8em; text-align:center; " id="goto-conference">
    <!--Обсудить это событие-->
    Перейти к обсуждению события >>
    </div>
</a>


<div style="padding:10px; background-color:#069">

<div style="padding:0px; color:#FFF; font-weight:bold; font-size:1.1em">
<?php 
$IBASE = new Interests();
$IBASE->SelectIBASE();
echo $IBASE->SelectName($data[3]); ?>
</div>
<div style="padding:0px; color:#DDD; font-size:0.8em"><?php echo str_replace(' ',', ',$data[4]); ?></div>

</div>

<?php endif; ?>


<script language="javascript">

COMMON.IFrameTest(".development-frame");
var $isRespondersQueried = false;

$(function(){
	
	//$("#dev-image").height( $("#dev-head").height() )
	//$("#dev-image").width( $("#dev-image").height() )
	$("#dev-image").css('margin-top', -$("#dev-kind").innerHeight() )
	$("#change-cover-button").on('click',changeCover)
	
	$("#goto-conference").on('mouseover', function(){
		$(this).stop(true,true,true);
		$(this).animate({'opacity':0.9},500);
		});
	$("#goto-conference").on('mouseout', function(){
		$(this).stop(true,true,true);
		$(this).animate({'opacity':0.6},500);
		});
	$("#connectors-list").slideUp(0);
	
	$("#connectors-roll").toggle(function(){ $("#connectors-list").stop(true,true,true); $("#connectors-list").slideDown(500); },
								 function(){ $("#connectors-list").stop(true,true,true); $("#connectors-list").slideUp(500); });
	
	$(".thank-button").on('click', function(){ expressMembershipThank(this) } )
	
	
	$("#responders-list").slideUp(0);
	$("#become-responder").click(function(){
		var res = SendDynamicMsg(['quer_become_responder'],[<?php echo $id ?>],null,'handlers/uiHandler.php','POST');
		if(res.length>0) $("#become-responder").slideUp(1000); 
		if(res=='+') WINDOW.ShowMessage("Вы стали участником запроса.", WINDOW.C.SUCCESS); else
		if(res=='-') WINDOW.ShowMessage("Вы уже участник запроса!", WINDOW.C.ERROR); else
		if(res=='') WINDOW.ShowMessage("Для этого необходимо авторизоваться в системе!", WINDOW.C.ERROR);
		});
	$("#refuse-responder").click(function(){
		var res = SendDynamicMsg(['quer_refuse_responder'],[<?php echo $id ?>],null,'handlers/uiHandler.php','POST');
		if(res.length>0) $("#refuse_responder").slideUp(1000); 
		if(res=='+') WINDOW.ShowMessage("Участие отменено.", WINDOW.C.SUCCESS);
		});
	$("#responders-listview").toggle(function(){
		if(!$isRespondersQueried)
			res = SendDynamicMsg(['quer_get_responderslist'],[<?php echo $id ?>],null,'handlers/uiHandler.php','POST');
		$isRespondersQueried = true;
		$("#responders-list").html("<div style='font-family:Arial, Helvetica, sans-serif; font-size:12px;'><div style='padding:2px; border-bottom: 1px solid #069'><b>Участники запроса :</b></div><br>"+res+"</div>");
		$("#responders-list").stop();
		$("#responders-list").slideDown(1000);
		},
		function(){
			$("#responders-list").stop();
			$("#responders-list").slideUp(1000);
			});
	})

function HandAchievement()
{
	var hand = function(login){
		var res = SendDynamicMsg(['hand_achievement','dev','login'],
								 [1,<?php echo $id ?>,login],null,'handlers/developments.php','POST','json');
		if(res['result']=='-') WINDOW.ShowMessage(res['msg'], WINDOW.C.ERROR);
		if(res['result']=='+'){
			 WINDOW.ShowMessage(res['msg'], WINDOW.C.SUCCESS);
			 $("#attached_achievement").slideUp(500);
			 window.setTimeout(function(){ $("#attached_achievement").remove() }, 500);
		}
		}
	
	WINDOW.Prompt("Кому вручить достижение?", "Введите qID(логин) участника конкурса:", hand);
	}

function expressMembershipThank(o)
{
	var whom = $(o).attr("tag");
	var res = SendDynamicMsg(['express_thank','whom','dev'],[1,whom,<?php echo $id ?>],null,'handlers/developments.php','POST');
	if(res=='+') $(o).hide(300);
	}

function changeCover()
{
	var startChangingProcess = function(url)
	{
		var res = SendDynamicMsg(['change_cover_image','dev'],[url,<?php echo $id ?>],null,'handlers/developments.php','POST');
		if(res=='+') {
			$("#dev-image").fadeTo(500,0.25);
			window.setTimeout(function(){
				$("#dev-image").fadeTo(500,1);
				$("#dev-image").attr('src',url);
				},500);
			WINDOW.ShowMessage("Обложка успешно изменена!", WINDOW.C.SUCCESS);
		}else WINDOW.ShowMessage("Произошла неизвестная ошибка!", WINDOW.C.ERROR);
		}
	
	WINDOW.Prompt("Укажите изображение новой обложки", "Ссылка на новое изображение(URL):", startChangingProcess);
	}

function connectTo()
{
	var res = SendDynamicMsg(['turninto_member'],[<?php echo $id ?>],null,'handlers/developments.php','POST');
	if(res=='+') WINDOW.ShowMessage("Вы присоединились к событию!", WINDOW.C.SUCCESS);
	if(res=='!') WINDOW.ShowMessage("Вы уже присоединились ранее", WINDOW.C.ERROR);
	if(res=='~') WINDOW.ShowMessage("Необходимо авторизоваться!", WINDOW.C.ERROR);
	}

function post_comment()
{
	text = $("#newcomment").val();
	id = <?php echo $id;?>;
	a = SendDynamicMsg(['post_comment','type','text','attach'],[1,'quer_query',text,id],'creation-result','handlers/uiHandler.php','POST','html');
	if(a=="")
	{
		a = SendDynamicMsg(['last_comment','attachment'],[1,id],null,'handlers/uiHandler.php','GET','html');
		$("#newcomment").val("");
		$("#quer-comments-box").prepend(a);
		$(".comment-abstraction:first").animate({opacity:1},1000);
		}
	}

</script>

