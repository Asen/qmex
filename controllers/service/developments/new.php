<?php
Security::checkAccess();
include 'tools/sided/veditor.php';

$user_cash = Human::getUserCash($_SESSION['id']);

?>

<style type="text/css">
.event-field{ padding:5px; color:#09F; border:1px solid #06F }
.dev-clarification{ padding-bottom:10px; padding-top:10px; color:#000; font-size:0.7em }
.dev-caption{ font-weight:bold; }
.time-field{ width:70px }
#quer-help-notify{font-family:Arial, Helvetica, sans-serif;font-size:11px;padding:10px;}
</style>


<div class="WCaption">
Создание нового события
<?php echo isset($_GET['talk']) ? ' для участников конференции №'.(int)$_GET['talk'] : ''?>
</div>

<table class="full-width" id="event-creation-layout" cellspacing="10px" border="0">
    <tr>
        <td width="70%" valign="top">
            <input type="text" class="event-field full-width" style="padding:10px; font-size:1.2em" id="event-caption"
            maxlength="60" placeholder="Что за событие?">
            <div class="dev-clarification" style="padding-top:3px">
            ( краткое представление события, в котором должна быть точно отражена суть самого события ) 
            </div>
            
            <br>
            
            <div style="background-color:#88C4FF; padding:10px">
            <table class="full-width" id="event-picture-layout">
            <tr>
            <td width="25%">
            	<img src="qmex_img/error.png" width="0px" id="event-image-picture">
            </td>
            <td width="75%" valign="top">
            	<input type="text" class="event-field full-width" id="event-image" maxlength="300" 
            	placeholder="Обложка события( URL изображения )">
                <div class='dev-clarification'>
                	<span class="dev-caption">Изображение обложки события</span> - 
                    это графический идентификатор события, визуально выражающий идею и суть.<br>
                    Выбирайте изображения крупного формата для четкости обложки. 
                </div>
            </td>
            </tr>
            </table>
            </div>
            
            
            <div class='dev-clarification'>
            <span class="dev-caption">Описание события</span><br>
            ( предоставьте людям информацию о событии, изображения, связанные с этим событием, место на карте и все, что угодно ) :
            </div>
            <div id="event_disign">
            	<textarea cols="60" rows="8" class="event-field full-width" id="event-content"></textarea>
            </div>
            
            <br><br>
            
            <div style="background-color:#88C4FF; padding:10px">
                <table class="full-width">
                 <tr>
                    <td width="25%">
                    <div class='dev-clarification dev-caption'>Что это за событие: </div>
                    </td>
                    <td>
                    <select class="event-field" style="color:#09F" onchange="defineKind(this)" id="event-kind">
                    <?php 
                    $event_types = Enum::DevelopmentTypes();			
                    foreach($event_types as $key=>$val)
                        echo '<option value="'.$key.'">'.$val.'</option>';                                         
                    ?>
                    </select>
                    </td>
                </tr>
                <tr>
                	<td colspan="2">
                	<div style="background-color:#069; padding:20px" id="achievement-box">
                    <div style="padding:5px; color:white; border-bottom:1px solid #CCC; font-size:0.8em; font-weight:bold">
                    	Создайте достижение для лучшего участника:
                    </div>
                        <table cellspacing="10px" class="full-width" style="color:white; font-size:0.8em">
                        <tr>
                            <td width="20%">Достижение: </td>
                            <td>
                                <textarea style="width:85%; height:50px" 
                                placeholder="например, &quot;разработчик мобильного приложения&quot;, 
                                &quot;лучший игрок в гольф&quot; и т.д" id="achievement-description"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" valign="top">Бонус к значимости: </td>
                            <td>
                            	<input type="range" style='width:50%' min="1" max="<?php echo $user_cash ?>" id="achievement-bonus"
                                step="1" onchange="setBonusValue(this)"><div style="width:50%; text-align:center" id="bonus_value">0</div>
                            	<!--<input type="text" class='event-field' onchange="checkRCorrect(this)" 
                                placeholder="число от 10 до 1000" id="achievement-bonus" />
                                <div style="font-size:10px; color:#CCC">
                                	Cколько рейтинга предоставляет это достижение?<br>
                                    <u>Одна десятая часть будет взята у вас.</u>
                                </div>-->
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td width="25%">
                    <div class='dev-clarification dev-caption'>С каким интересом связано событие: </div>
                    </td>
                    <td>
                    <select class="event-field" style="color:#09F" id="event-theme">
                    <?php 
                    $IBASE = new Interests();				
                    foreach($IBASE->SelectIBASE() as $ITEM)
                    {
                        $KEY = $IBASE->SelectKey($ITEM);
                        $NAME = $IBASE->SelectName($KEY);
                        echo '<option value="'.$KEY.'" id="interest-'.$KEY.'">'.$NAME.'</option>';
                        }
                    
                    ?>
                    </select>
                    </td>
                </tr>
                <tr>
                	<td colspan="2">
                    	<input type="text" class="event-field full-width" id="event-keywords" 
                        style="width:100%" maxlength="100" placeholder="Ключевые слова, по которым можно будет отыскать это событие">
                    </td>
                </tr>
                </table>
            </div>
            
            <br><br>
            
            <div>
            <div class='dev-clarification dev-caption'>Для какого места актуально событие?</div>
            	<table cellspacing="10px">
                <tr>
                	<td>
                    	<input type="text" id="country" class="event-field geo-info-field" placeholder="Страна" />
                    </td>
                    <td>
                    	<input type="text" id="city" class="event-field geo-info-field" placeholder="Город" />
                        <input type="hidden" id="latitude" value='0' />
                        <input type="hidden" id="longitude" value='0' />
                    </td>
                    <td>
                    	<button class="qButton" onclick="updLocation()">Актуально для моего текущего положения</button>
                    </td>
                </tr>
                </table>
            </div>
            
            <br><br>
            
            <div style="background-color:#88C4FF; padding:10px">
            <div class='dev-clarification dev-caption'>Время начала события( по qMex-Time ):</div>
            	<table cellspacing="10px" class="full-width" border="0">
                <tr>
                	<td>
                    	<div class='dev-clarification'>Дата:</div>
                    	<select id="year">
                        	<option value="-1">год</option>
                            <option value="<?php echo date('Y') ?>"><?php echo date('Y') ?></option>
                            <option value="<?php echo date('Y')+1 ?>"><?php echo date('Y')+1 ?></option>
                        </select>
                        <select id="month">
                        	<option value="-1">месяц</option>
                            <?php
                            	foreach(Enum::$MONTHES as $id=>$month) 
									echo '<option value="'.($id+1).'">'.$month.'</option>';
							?>
                        </select>
                        <select id="day"><option value="-1">день</option></select>
                    </td>   
                    <td valign="top" align="right">
                    	<div class='dev-clarification'>Текущее время qMex-Time :</div>
                        <div id="current-qmextime-bar" style="font-size:0.8em; font-weight:bold"></div>
                    </td>
                </tr>
                <tr>
                	<td valign="middle">
                    	<div class='dev-clarification'>Время:  <span style="font-size:12px; font-weight:bold" id='timing'>в 12:00</span></div>
                    	<input type="range" id='time' onmousemove="updateTime(this)" min="0" max="47" value="24">
                    </td>
                </tr>
                <tr>
                	<td colspan="4">
                    	<div class='dev-clarification'>
                    		Продолжительность события: 
                        	<input type="number" class="event-field" id="event-duration" placeholder="в днях" />
                        </div>
                    </td>
                </tr>
                </table>
            </div>
            
            
            <?php if(isset($_GET['talk'])):?>
            <div class='dev-clarification dev-caption'>
            	Это событие будет привязано к конференции №<?php echo (int)$_GET['talk'] ?>.<br>
                Событие будет общедоступным.
            </div>
            <?php endif?>
            
            
        </td>
        <td width="30%" valign="top">
            <div class="dev-clarification">
            	<img src="qmex_img/info.png" style="float:left; padding:5px" width="25px" />
                Вы можете создать( организовать ) собственное событие, которое помогло бы отыскать и объединить множество людей ради
                действия во благо общих целей, намерений и интересов.
            </div>  
            <br>
            <div class="dev-clarification">
            	<img src="qmex_img/warning.png" style="float:left; padding:5px" width="25px" />
                <strong>Информацию о событии нельзя будет изменять в будущем!</strong><br>
                Этот запрет наложен с целью предотвращения изменения сути события для потенциальных участников данного события.
            </div>
        </td>
    </tr>
    <tr>
    	<td>
        	<button class="qButton" style="width:100%; padding:15px" id="create-event">
            <?php echo isset($_GET['talk']) ? 'Привязать' : 'Создать' ?> событие!
            </button>
        </td>
    </tr>
</table>


<script language="javascript">

$(function(){
	
InitEditor("#event-content","200px");
$("#event-image-picture").width($("#event-picture-layout").width()*0.2+"px")
$("#event-image").on('change', function(){ $("#event-image-picture").attr("src", $(this).val()) });
$("#create-event").on('click',createEvent)
$("#achievement-box").slideUp(0);
$(".geo-info-field").on('change',updLocationCoords)
$("#year, #month").on('change', updateDayList )
window.setInterval(updqMexTime, 1000);

});

function setBonusValue(o)
{
	var bonus = $(o).val();
	$("#bonus_value").html( "<div style='font-size:16px; font-weight:bold'>"+bonus+"</div>" );
	$("#bonus_value").append( "У вас будет взято <b>"+bonus+"</b> qCoins" );
	}

function updateDayList()
{
	var year = $("#year").val();
	var month = $("#month").val();
	var days = SendDynamicMsg(['get-days-list','year','month'],[1,year,month],null,'handlers/developments.php','GET');
	$("#day").html('<option value="-1">день</option>');
	for(i=1;i<=parseInt(days);++i)  
	$("#day").append("<option value='"+i+"'>"+i+"</option>");
	}

function updateTime(o)
{
	var val = parseInt($(o).val());
	$("#timing").text("в "+(parseInt(val/2))+":"+(val%2==0 ? '00':'30'));
	}

/*
function checkRCorrect(o)
{
	var offer = parseInt($(o).val());
	var avail = <?php echo Security::isAuthorized() ? $user_cash : 0 ?>;
	if(offer<1 || offer>avail) 
		WINDOW.ShowMessage("Достижение должно предоставлять от 10 до 1000 единиц рейтинга.", WINDOW.C.ERROR);
	if(Math.max(2,offer/10)>avail) 
		WINDOW.ShowMessage("У вас отсутствует "+parseInt(offer/10)+" единиц рейтинга. Предложите меньше единиц рейтинга.", WINDOW.C.ERROR);
}
*/

function defineKind(o)
{
	var val = $(o).val();
	if(val == <?php echo Enum::$DEVELOPMENT_TYPES['COMPETITION'] ?>) 
		$("#achievement-box").slideDown(500); else
		$("#achievement-box").slideUp(500);
	}


function ConfTypeCH(o)
{
	if(o.checked) $("#event-conf-id").attr("disabled","disabled"); else
	$("#event-conf-id").removeAttr("disabled");
	}

function updLocation()
{
	function wasFound(pos)
	{
		var latitude = pos.coords.latitude;
		var longitude = pos.coords.longitude;
		var location = GEO.DefCountryCity({'latitude':latitude, 'longitude':longitude});
		$("#country").val(location.country);
		$("#city").val(location.city);
		$("#latitude").val(latitude);
		$("#longitude").val(longitude);
		}
	
	$("#country,#city").val('...');
	navigator.geolocation.getCurrentPosition(wasFound, GEO.wasNotFoundLocation, { timeout:3500 });
	}
	
	
function updLocationCoords()
{
	var country = $("#country").val();
	var city = $("#city").val();
	var coords = GEO.DefCoords({'country':country, 'city':city});
	$("#latitude").val(coords.latitude);
	$("#longitude").val(coords.longitude);
	}
	

function updqMexTime()
{
	SendDynamicSync({ 
		type : 'GET', 
		url : 'handlers/ask.php', 
		dataType : 'json', 
		data : {'qmex-time':1}, 
		func : function($time){ 
			$("#current-qmextime-bar").text( parseInt($time['day'])+' '+$time['month-name']+", "+$time['year']+" год " );
			$("#current-qmextime-bar").append(' , '+$time['hour']+' : '+$time['minute']+" : "+$time['second'] );
		}
		})
	}

function createEvent()
{	
	var dev = new Array();
	dev['caption'] = $("#event-caption").val();
	dev['content'] = GetEditor("event-content").getContent();
	dev['kind'] = parseInt($("#event-kind").val());
	dev['theme'] = parseInt($("#event-theme").val());
	dev['keywords'] = $("#event-keywords").val();
	dev['image'] = $("#event-image").val();
	dev['city'] = $("#city").val();
	dev['country'] = $("#country").val();
	dev['latitude'] = $("#latitude").val();
	dev['longitude'] = $("#longitude").val();
	dev['year'] = $("#year").val();
	dev['month'] = $("#month").val();
	dev['day'] = $("#day").val();
	dev['hour'] = parseInt(parseInt($("#time").val())/2);
	dev['minute'] =  parseInt($("#time").val())%2==0 ? 0:30;
	dev['duration'] = $("#event-duration").val();
	if(dev['kind'] == <?php echo Enum::$DEVELOPMENT_TYPES['COMPETITION'] ?>)
	{
		dev['ach-descr'] = $("#achievement-description").val();
		dev['ach-bonus'] = $("#achievement-bonus").val();
	}
	<?php if(isset($_GET['talk'])): ?>
		dev['linked_conference'] = <?php echo (int)$_GET['talk'] ?>;
	<?php endif ?>
	
	
	var allowed = {'latitude':1, 'longitude':1};
	for(var key in dev) 
		if(!(key in allowed) && (dev[key]+"").trim().length==0) {
				WINDOW.ShowMessage("Для привлечения людей нужно указать все данные о событии.", WINDOW.C.ERROR);
				return;
			}
	
	var query = getQueryString(dev);
	
		$.ajax({
			url:"handlers/developments.php",
			type:"POST",
			dataType:"json",
			async:false,
			data:'create=1&'+query,
			success:function(msg)
			{
				WINDOW.ShowMessage(msg['msg'], msg['result']=='-' ? WINDOW.C.ERROR : WINDOW.C.SUCCESS);
				if(msg['result']=='+')
					window.setTimeout(function(){
						location.href = '/developments?view=show&id='+msg['id']
						}, 1500);
			}
			});

	}
	
</script>