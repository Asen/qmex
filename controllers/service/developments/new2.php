<style type="text/css">
.dev-clarification{ padding-bottom:10px; color:#06F; font-size:0.7em }
.quer_mandatory{color:red; font-size:18px; padding:10px}
#quer-help-notify{font-family:Arial, Helvetica, sans-serif;font-size:11px;padding:10px;}
</style>

<?php

include 'tools/sided/veditor.php';

$data = array();

if(isset($_GET['view']))
{
	$view = $_GET['view'];
	if($view=='edit') 
	if(isset($_GET['query']) && (int)$_GET['query']>0)
	{
		$id = (int)$_GET['query'];
		$user = (int)$_SESSION['id'];
		global $db;
		$db->query("SELECT ID FROM qmex_quer WHERE ID=$id AND Owner=$user");
		if($db->rowCount()==1) 
		{
			$query = new quer();
			$query->get($id);
			$data = $query->data();
			}
		}
	
	}
	
$is_new = @$_GET['view']=='new';
	
?>

<div class="MainCaption" t><?php echo ($is_new) ? 'Создание нового запроса':'Обновление запроса'; ?></div>
<?php echo ($is_new) ? '':'<div style="font-size:13px"><a href="quer?view=show&id='.(int)@$_GET['query'].'">Вернуться к запросу</a></div>'; ?>
<table style='width:100%;padding:5px;margin-top:20px;' cellspacing="5px" border="0" t>
<tr>
<td width="50%" colspan="2">
<input type="text" class="setter_field quer-field full-width" id="quer-caption"
maxlength="60" value="<?php echo @$data['caption'];?>" placeholder="Что за событие?">
</td>
<td width="5%"><div class="quer_mandatory">*</div></td>
<td rowspan="10" width="30%" valign="top" style="border-left:1px dashed blue">
<div style="text-align:center"><img src='/qmex_img/quer/quer.png' /></div>
<div id="quer-help-notify" t>
Запросы помогут вам организовать с другими людьми сообщества, объединения,  мероприятия, сборы и команды для дальнейшего взаимодействия.<br><br> Ваш запрос может существовать не более, чем 6 месяцев и не менее, чем 3 дня. За 36 часов до уничтожения он будет переведен в категорию актуальных( Actuals ).
<br><br><span class="quer_mandatory" style="padding:0px;">*</span> Поля, помеченные звездочкой  обязательны для заполнения, все остальные поля являются уточняющими для запроса, они заполняются по вашему усмотрению.
<br><br>
</div>
</td>
</tr>
<tr>
<td colspan="2">
<div class='dev-clarification'>Описание события:</div>
<div id="quer_disign">
<textarea cols="60" rows="8" class="ui_official_textbox quer-field" id="quer-content" 
style="width:100%" maxlength="5000"><?php echo @$data['text'];?></textarea>
</div>
<br>
</td>
<td><div class="quer_mandatory">*</div></td>
</tr>
</table>



<div>
<table class="full-width" border=1>
<tr>
<td width="100px">
<div class='dev-clarification'>С чем связано событие: </div>
</td>
<td>
<select class="setter_field quer-field" style="color:#09F" id="quer-theme">
<?php 
$IBASE = new Interests();

if(isset($data['theme'])) echo '<option value="'.$data['theme'].'">'.$IBASE->SelectName($data['theme']).'</option>';

foreach($IBASE->SelectIBASE() as $ITEM)
{
	$KEY = $IBASE->SelectKey($ITEM);
	$NAME = $IBASE->SelectName($KEY);
	echo '<option value="'.$KEY.'" id="interest-'.$KEY.'">'.$NAME.'</option>';
	}

?>
</select>
</td>
<td><div class="quer_mandatory">*</div></td>
</tr>
<tr>
<td colspan="2">
<input type="text" class="setter_field quer-field full-width" id="quer-keywords" style="width:100%" maxlength="100" 
value="<?php echo str_replace('|',',',@$data['keywords']);?>" placeholder="Ключевые слова, по которым можно будет отыскать это событие">
</td>
<td><div class="quer_mandatory">*</div></td>
</tr>
</table>
</div>



<table>
<tr>
<td colspan="2">
<input type="text" class="setter_field quer-field" id="quer-image" style="width:100%" maxlength="300" 
value="<?php echo str_replace('|',',',@$data['image']);?>" placeholder="Изображение, связанное с событием( URL изображения )">
</td>
</tr>

<tr>
<td colspan="2">
<table cellspacing="10px" border="0">
<tr>
<td>
<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px" t>Страна:</div>
<div><input type="text" class="setter_field quer-field" id="quer-country" maxlength="50" value="<?php echo @$data['country'];?>"></div>
</td>
<td>
<div style="font-family:Arial, Helvetica, sans-serif; font-size:12px" t>Город:</div>
<div><input type="text" class="setter_field quer-field" id="quer-city" maxlength="50" value="<?php echo @$data['city'];?>"></div>
</td>
</tr>
<tr>
<td colspan="2" align="left">Возраст:</td></tr>
<td colspan="2" align="left">
от <input type="number" class="setter_field quer-field" id="quer-age-from" style="width:100px" maxlength="3" value="<?php echo @$data['age_from'];?>"> 
до <input type="number" class="setter_field quer-field" id="quer-age-to" style="width:100px" maxlength="3" value="<?php echo @$data['age_to'];?>">
</td>
</tr>
</table>
</td>
</tr>

<tr><td colspan="2"><hr size="2" color="#0000CC" width="100%"></td></tr>
<tr>
<td colspan="2">

<table cellspacing="2px" cellpadding="0px" border="0" style="width:100%">
<tr>
<td width="70%"><div style="width:100%" id="quer_error_box"></div></td>
<td width="30%" align="right">
<input type="button" class="qButton" value="<?php echo ($is_new) ? 'Создать':'Обновить'; ?>" onclick="<?php echo ($is_new) ? 'quer_create()':'quer_update()'; ?>" style="text-align:center; padding-top:5px; padding-bottom:5px; width:100%" tag='<?php echo @$data['id'];?>' id="upcreate">
</td>
</tr>
</table>

</td>
</tr>

</table>


<script language="javascript">

$(function(){
	
InitEditor("#quer-content","200px");
	
$(".quer-field").bind("keydown",function(){v=$(this).val(); l=$(this).attr("maxlength")-1; 
    if(v.length>l) $(this).val(v.substring(0,l));
	});
	
	// TYPICAL CONTENT...BAD...
	$(".quer_caption").addClass("typical_text");
	/////////////////////////////////////////////
	
});

function quer_create(t)
{
	if(!t || t==null) t=0;
	
	quer_ = "a";
	quer = new Array();
	quer['caption'] = $("#quer-caption").val();
	quer['content'] = GetEditor("quer-content").getContent();
	quer['theme'] = parseInt($("#quer-theme").val());
	quer['keywords'] = $("#quer-keywords").val();
	quer['city'] = $("#quer-city").val();
	quer['country'] = $("#quer-country").val();
	quer['actual_t'] = parseInt($("#quer-actual").val());
	quer['image'] = $("#quer-image").val();
	quer['id'] = $("#upcreate").attr("tag");
	quer['age_from'] = $("#quer-age-from").val();
	quer['age_to'] = $("#quer-age-to").val();
	
	query = getQueryString(quer);
	if(quer['caption'].trim()!="" && quer['content'].trim()!="" && quer['theme']>0 && quer['actual_t']>0 && quer['keywords'].trim()!="") 
	{
		quer_="quer";
		$.ajax({
			url:"handlers/uiHandler.php",
			type:"POST",
			dataType:"html",
			async:true,
			data:'&'+quer_+'=1&type='+t+'&'+query,
			success:function(msg)
			{
				$("#quer_error_box").html(msg);
				href = "";
				switch(t)
				{
					case 0: href = "quer"; break; 
					case 1: href = "quer?view=show&id="+quer['id']; break;
					}
				if(msg.indexOf("SucMsg")>0) window.setTimeout(function(){location.href=href;},1500);
				}
			});
	} else $("#quer_error_box").html("<div id='ErrMsg'>Не заполненны некоторые обязательные поля.</div>");
	
	}
	
function quer_update()
{
	quer_create(1);
	}


</script>