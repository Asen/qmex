<?php
if(!isset($_SESSION["names"]["rLogin"])) header("Location: recover");
?>
<form method="post" action="recover?pchange" name="checker">
<table style="margin:0 auto; width:90%;" class="Qtrifle" cellspacing="15px" border="0">
<th style="border-bottom:#069 solid 1px; text-align:left; height:30px"><span style="font-size:18px" t>Сменить пароль?</span><div style="float:right; font-size:13px; text-decoration:overline;" t>Аккаунт пользователя <span style="opacity:0.6">"<?php echo $_SESSION["names"]["rLogin"]?>"</span></div></th>
<tr>
<td>
<div style="padding-top:15px; padding-bottom:15px; font-size:15px" t>Аккаунт, пароль от которого был утерян, определен. Теперь выберите, каким образом вы желаете восстановить доступ.</div>
<div style="margin:20px auto"></div>
<div id="line"></div>
<div id="sub_caption" t>Восстановить пароль</div>
<div id="line"></div>
<br>
<div style="margin:10px auto">
<div style="max-width:50%; padding-bottom:5px; padding-top:5px" t>
Введите ключевое слово, указанное вами при регистрации:
</div>
<div style="height:30px">
<input type="text" class="qmex_former" name="rRestore" id="rope">
</div>
</div>
<div id="KeyWordError" style="padding-top:4px;padding-bottom:4px;"></div>
</td>
</tr>
<tr>
</tr>
<tr>
<td>
<div style="margin:0 auto"></div>
<div id="line"></div>
<div id="sub_caption" t>Изменить пароль</div>
<div id="line"></div>
<br>

<div style="max-width:50%; padding-bottom:5px; padding-top:5px" t>
Введите новый пароль:
</div>
<div style="height:30px; float:left">
<input type="password" class="qmex_former" name="rPass" id="rpass">
</div>
<div style="float:left; padding-left:15px" id="qPass_result">
</div><br><br>
<div style="max-width:50%; padding-bottom:5px; padding-top:5px" t>
Повторите пароль:
</div>
<div style="height:30px; float:left" id="qRepass">
<input type="password" class="qmex_former" name="rRepass" id="rrepass">
</div>
<div style="float:left; padding-left:15px" id="PasswordError">
</div><br><br>

</td>
</tr>
<tr>
<td style="text-align:right">
<input type="button" class="qButton_silver" value="Назад" name="find_backer" onClick="location.href = '../recover.php'">
<input type="button" class="qButton" value="Готово" name="find_forg" onClick="some_p()">
</td>
</tr>
</table>
</form>
<script>
function some_p()
{
	kw = $("#rope").val().trim();
	pass = $("#rpass").val().trim();
	repass = $("#rrepass").val().trim();
	if(kw=="" && pass=="" && repass==""){
		WINDOW.ShowMessage('Необходимо заполнить любой из двух пунктов!', WINDOW.C.ERROR);
		return;
	}
	
	var msg = (kw!="") ? SendDynamicMsg(['recovering_keyword'], [kw], null, 'handlers/ask.php', 'GET', 'json'):
						 SendDynamicMsg(['recovering_password','recovering_repassword'], 
						 				[pass, repass], null, 'handlers/ask.php', 'GET', 'json')
	var error_box = kw!="" ? "KeyWordError" : "PasswordError";					 
						 
	if(msg['result']=='-')
	{
		$('#ErrMsg').remove();
		$('#'+error_box).html('<div id="ErrMsg">'+msg['msg']+'</div>');
		$('#'+error_box).slideUp(0);
		$('#'+error_box).slideDown(500);
		}
		
	if(msg['result']=='+') document.forms['checker'].submit();
	
	}
</script>
