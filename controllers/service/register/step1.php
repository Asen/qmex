<style>
.q_reg_note{
	width:80px;
	font-size:12px;
	font-family:Arial, Helvetica, sans-serif;
	}
.dd{ border:red 1px solid }
</style>

<div class="WContent">
<div class="step">
<span class="step-name">Шаг первый:</span>
<span class="step-descr"> создание персонального qID и пароля</span>
</div>
<form method="post" style="margin:20px auto" id="qForm">
<table width="80%" border="0" cellspacing="15px" class="qformer">
<tr>
<td>Придумайте qID: <br><span class="reg-label">( уникальное имя в системе, логин )</span></td>
<?php
$q1 = @$_SESSION["reg"]["login"];
$q2= @$_SESSION["reg"]["password"];
$q3 = @$_SESSION["reg"]["key"];
?>
<td><input type="text" class="qmex_former" name="login" onchange="qParams_setoff(this)" value="<?php echo get_not_escaped($q1)?>"></td>
<td><div id="qLogin_result" class="q_reg_note"></div></td>
</tr>
<tr>
<td>Придумайте пароль:</td>
<td><input type="password" class="qmex_former" name="password" onchange="qParams_setoff(this)" value="<?php echo get_not_escaped($q2)?>"></td>
<td><div id="qPass_result" class="q_reg_note"></div></td>
</tr>
<tr>
<td>Повторите пароль:</td>
<td><input type="password" class="qmex_former" name="repassword" onchange="qParams_setoff(this)"></td>
<td><div id="qRePass_result" class="q_reg_note"></div></td>
</tr>

<tr>
<td>
<div style="padding-left:5px; border-left:2px solid #09F">
Ключевое слово: <br><span class="reg-label">( Необходимо на случай утери пароля )</span>
</div>
</td>
<td><input type="text" class="qmex_former" name="key" onchange="qParams_setoff(this)" value="<?php echo get_not_escaped($q3)?>"></td>
<td><div id="qKey_result" class="q_reg_note"></div></td>
</tr>

<tr>
<td>

<div style="text-align:center; width:200px" onmouseup="qParams_setoff(0)">
<div class="qButton full-width">
Дальше<img src="qmex_img/arrow_right.png" width="10px" height="10px" style="margin:0 0 0 10px">
</div>
</div>

</td>
<td colspan="2"><div id="Qres"></div></td>
</tr>
<tr>
<td colspan="3">

<div style="border-top:solid 3px #069">
<table style="width:100%">
<tr>
<td valign="top">
<img src="qmex_img/warning.png" width="20px">
</td>
<td>
<div id="mark">
Можно использовать только буквы латинского алфавита ( A-Z ), цифры, точки, знаки подчеркивания и тире.<br>
Длина содержимого каждого из полей должна быть не менее 2-ух символов и не более 45.<br><br>
Длина пароля должна быть не менее 5 символов. Постарайтесь придумать сложный пароль.
</div>
</td>
</tr>
</table>
</div>

</td>
</tr>
</table>
</form>
</div>
<script language="javascript">
  
function qParams_setoff(selfpad)
{
	var response = new Object();
	if(selfpad==0) 
		response = SendDynamicMsg(['check'],[1],null,'handlers/regist.php',"POST","json"); else
		response =SendDynamicMsg([selfpad.name],[selfpad.value],null,'handlers/regist.php',"POST","json"); 
	completeResponse(response);
}

function completeResponse(response)
{
	if( !(response instanceof Object) ) return;
	
	var login = ('login' in response) ? 
				('<span class="'+(response.login['res']=='-' ? 'eg' : 'sg')+'">'+response.login['msg']+'</span>') : 'non';
	var passw = ('password' in response) ? 
				('<span class="'+(response.password['res']=='-' ? 'eg' : 'sg')+'">'+response.password['msg']+'</span>') : 'non';			
	var repas = ('repassword' in response) ? 
				('<span class="'+(response.repassword['res']=='-' ? 'eg' : 'sg')+'">'+response.repassword['msg']+'</span>') : 'non';
	var key =   ('key' in response) ? 
				('<span class="'+(response.key['res']=='-' ? 'eg' : 'sg')+'">'+response.key['msg']+'</span>') : 'non';
			
	if(login!='non') 
		$("#qLogin_result").html(login);		
	if(passw!='non') 
		$("#qPass_result").html(passw);		
	if(repas!='non') 
		$("#qRePass_result").html(repas);		
	if(key!='non')   
		$("#qKey_result").html(key);	
	
	if( ('result' in response) )
	if( response.result['res']=='+' ) 
		top.location.href="/register?step=personal"; else
		$("#Qres").html('<span style="color:#C00; font-size:14px;">'+response.result['msg']+'</span>');			
				

	}
</script>