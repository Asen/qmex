﻿<div style=" padding:15px;" id="qFail">
<div style="margin:20px auto;color:#BF0000; font-size:20px;">
<strong>Регистрация почему-то не удалась...</strong>
</div>
<div style="font-size:16px" t>
<strong>Это, скорее всего, произошло по одной из следующих причин:</strong>
<div style="margin:0px auto; float:right" align="center">
<img src="qmex_img/error.png" width="150px" height="150px">
</div>
<div style="margin:10px 0 0 40px">
<li>вы попытались повторно ввести одни и те же данные</li>
<li>сбой системы из-за большой нагрузки на сервер</li>
<li>ведутся технические работы над системой <strong>qMex</strong></li>
<li>проблемы с нашим оборудованием</li>
</div><br><br>
Но мы обязательно с этим разберемся! И вы нам очень поможете, если напишите в нашу службу поддержки чуть-чуть поподробнее о данной проблеме. 
</div>
</div>
<div style="padding:10px; font-size:14px; font-weight:bold; border-top:#006 1px solid;" t>
Просим прощения за предоставленные неудобства...
</div>

<script>
$("#steps-preview > td:last").text("Ошибка");
</script>