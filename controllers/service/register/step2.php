﻿<?php
if(!isset($_SESSION["reg"]["step1_succ"]) || !$_SESSION["reg"]["step1_succ"]) 
echo "<script>location.href='register?step=passwd'</script>";

$q1=@$_SESSION["reg"]["name"];
$q2=@$_SESSION["reg"]["sename"];
$q3=@$_SESSION["reg"]["country"];
$q4=@$_SESSION["reg"]["city"];
$q5=str_replace('|',' ',@$_SESSION["reg"]["descrwords"]);

$q6 = @$_SESSION["reg"]["latitude"];
$q7 = @$_SESSION["reg"]["longitude"]

?>


<style>
.qformer{ font-size:12px; }
.qmex_former{ width:auto }
.qformer_label{ height:20px; }
.qformer td{ vertical-align:top; font-size:12px; height:0px; }
</style>


<div class="WContent">
<div class="step">
<span class="step-name">Шаг второй:</span>
<span class="step-descr"> указание личных данных</span>
</div>


<table style="" border="0" cellspacing="10px" class="qformer full-width">
<tr>
<td valign="top" width="60%">

    <table id="NameSename full-width" cellspacing="1px">
    <tr>
    <td width="50%">
    <div class="qformer_label">Ваше имя:</div>
    <input type="text" class="qmex_former" name="name" id="name" value="<?php echo get_not_escaped($q1)?>"></div>
    </td>
    <td width="50%">
    <div class="qformer_label">Ваша фамилия:</div>
    <input type="text" class="qmex_former" name="sename" id="sename" value="<?php echo get_not_escaped($q2)?>" >
    </td>
    </tr>
    </table>
    <div class="reg-label">(Наверняка другим пользователям будет интересно знать ваше имя.)</div>
    
    <br><br>
    
    <div class="qformer_label">Дата рождения:</div>
    <select class="qDate" id="year" onchange="qMonthes()">
    <option value="-1">Год: </option>
    <?php
    $year = date("Y");
    for($i=$year; $i>=$year-110; $i--) echo "<option value='$i'>$i</option>";
    ?>
    </select>
    <select class="qDate" onChange="qMonthes()" id="month">
    <option value="-1">Месяц: </option>
    <?php
    for($i=0; $i<=11; $i++) echo "<option value='$i'>".Enum::$MONTHES[$i]."</option>";
    ?>
    </select>
    <span id="days_block">
    <select class="qDate" id="day">
    <option value="-1">День: </option>
    <?php
    for($i=1; $i<=31; $i++) echo "<option value='$i'>$i</option>";
    ?>
    </select>
    </span>
    
    <br><br><br>

    <select class="qDate" id="sex" style="width:200px;">
    <option value="-1">Пол: </option><option value="1">Мужской</option><option value="2">Женский</option>
    </select>
    
    <br>
    
    <div style="margin-top:20px; padding:5px; border:1px solid #CCC ">
    <input type="text" class="full-width" style="padding:5px" maxlength="255" name="descrwords" id="descrwords" 
    placeholder="ключевые слова о вас" value="<?php echo $q5; ?>">
    <span class="reg-label">(Основные слова, характеризующие вашу деятельность, ваши увлечения, интересы и навыки.)</span>
    </div>

</td>
<td valign="top" width="40%">

    <div style="padding:5px; border:1px solid #CCC;">
    <div style="padding-bottom:5px">
    Текущее местоположение:
    <button class="qButton" style="padding:3px" onclick="whereIam()">Определить?</button>
    </div>
    <table id="CountryCity" cellspacing="10px">
    <tr>
    <td>
    <input type="text" class="qmex_former geo-info-field" name="country" id="country" placeholder="Страна" value="<?php echo $q3?>">
    </td>
    </tr><tr>
    <td>
    <input type="text" class="qmex_former geo-info-field" name="city" id="city" placeholder="Город" value="<?php echo $q4?>">
    <input type="hidden" id='latitude'  value="<?php echo $q6; ?>">
    <input type="hidden" id='longitude' value="<?php echo $q7; ?>">
    </td>
    </tr>
    </table>
    <span class="reg-label">( Для получения информации, актуальной для вашего местонахождения. )</span>
    </div>

</td>
</tr>
<tr>
<td colspan="2">

    <table cellpadding="0px" style="padding-top:10px" cellspacing="5px">
    <tr><td colspan="2" width="50%"><div id="Qres" style="max-width:95%; height:20px; color:#C00; font-weight:bold"></div></td></tr>
    <tr>
    <td>
    <div style="float:left;text-align:center; width:200px" onmouseup="location.href='register?step=passwd'">
    <div class="qButton" style="font-weight:100">
    <img src="qmex_img/arrow_left.png" width="10px" height="10px" style="padding-right:10px">Назад
    </div>
    </div>
    </td>
    <td>
    <div style="float:right; text-align:center; width:200px" onmouseup="Qcheck()">
    <div class="qButton" style="font-weight:100">
    Далее<img src="qmex_img/arrow_right.png" width="10px" height="10px" style="margin:0 0 0 10px">
    </div>
    </div>
    </td>
    </tr>
    <tr>
    <td></td>
    <td>
    <div style="float:right; text-align:center; width:200px" onmouseup="SkipStep()">
    <div class="qButton_silver qHint" tag="Эту информацию можно будет указать и потом." style="font-weight:100">Пропустить этот шаг</div>
    </div>
    </td>
    </tr>
    </table>

</td>
</tr>
</table>



<div style="border-top:solid 3px #069">
<table style="width:100%">
<tr>
<td valign="top">
<img src="qmex_img/warning.png" width="20px">
</td>
<td>
<div id="mark">
Личные данные будут отображаться только в вашем профиле и использоваться при поиске вас. При желании вы сможете изменить эти данные в настройках своего профиля в любой момент времени.<br><br>
<u>Можно использовать только буквы.</u><br>
Длина содержимого каждого из полей <u>должна быть</u> не менее 2-ух символов и не более 45.<br>
<u>При желании, этот шаг можно пропустить.</u>
</div>
</td>
</tr>
</table>
</div>


<script language="javascript">

$(function(){
	$(".geo-info-field").on('change',updLocationCoords)
	})

function whereIam()
{
	function wasFound(pos)
	{
		var latitude = pos.coords.latitude;
		var longitude = pos.coords.longitude;
		var location = GEO.DefCountryCity({'latitude':latitude, 'longitude':longitude});
		$("#country").val(location.country);
		$("#city").val(location.city);
		$("#latitude").val(latitude);
		$("#longitude").val(longitude);
		}
	
	navigator.geolocation.getCurrentPosition(wasFound, GEO.wasNotFoundLocation, { timeout:3500 });	
	}
	
function updLocationCoords()
{
	var country = $("#country").val();
	var city = $("#city").val();
	var coords = GEO.DefCoords({'country':country, 'city':city});
	$("#latitude").val(coords.latitude);
	$("#longitude").val(coords.longitude);
	}

function qMonthes()
{
	index=document.getElementById('day').selectedIndex;
	obj = document.getElementById("year");
	yearValue = obj.options[obj.selectedIndex].innerHTML;
	monthValue = document.getElementById("month").selectedIndex;
	if(monthValue==0 || (parseInt(yearValue) || 0)==0) return;
	var request = SendDynamicMsg(['calendar','year','month'],[1,yearValue,monthValue],null,"handlers/regist.php","POST");
	$("#days_block").html(request);
	if($('#day option').length<index) document.getElementById('day').selectedIndex = 0; else 
		                              document.getElementById('day').selectedIndex = index;
	}
	
function SkipStep()
{
	var request = SendDynamicMsg(['skipstep2'],[1],null,"handlers/regist.php","POST");
	$("#Qres").empty();
	location.href="/register?step=interests";
	}
	
	
function Qcheck()
{
	var name = document.getElementById("name").value;
	var sename = document.getElementById("sename").value;
	var sex = document.getElementById("sex").selectedIndex;
	var year = document.getElementById("year");
	    year = year.options[year.selectedIndex].innerHTML;
	var month = document.getElementById("month").selectedIndex;
	var day = document.getElementById("day");
	    day = day.options[day.selectedIndex].innerHTML;
	var country = document.getElementById("country").value;
	var city = document.getElementById("city").value;
	var latitude = document.getElementById("latitude").value;
	var longitude = document.getElementById("longitude").value;
	var descrwords = document.getElementById("descrwords").value;
	
	var request = SendDynamicMsg(
		['checkstep2','name','sename','sex','year','month','day','country','city','latitude','longitude','descrwords'],
		[1,name,sename,sex,year,month,day,country,city,latitude,longitude,descrwords], 
		null,"handlers/regist.php","POST", 'json');
		
	if(request['res']=='-')
		$("#Qres").html(request['msg']); else
		location.href="/register?step=interests";						 
	}
		
</script>