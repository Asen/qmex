﻿<?php
if(isset($_SESSION["reg"]["email"]))
{
	$db = new db();
	$success = true;
	$login = trim($_SESSION["reg"]["login"]);
	$db->query("SELECT ID From qmex_users WHERE Login='$login'");
	$found = ($db->rowCount()>0);
	if($found==false)
	{
	$login = $_SESSION["reg"]["login"];
	$pass = $_SESSION["reg"]["password"];
	$keypad = $_SESSION["reg"]["key"];
	$email = $_SESSION["reg"]["email"];
	$name = $_SESSION["reg"]["name"];
	$sename = $_SESSION["reg"]["sename"];
	$birth_year = (trim($_SESSION["reg"]["year"])!='') ? $_SESSION["reg"]["year"]:-1;
	$birth_month = (trim($_SESSION["reg"]["month"])!='') ? $_SESSION["reg"]["month"]:0;
	$birth_day = (trim($_SESSION["reg"]["day"])!='') ? $_SESSION["reg"]["day"]:0;
	$sex = (trim($_SESSION["reg"]["sex"])!='') ? $_SESSION["reg"]["sex"]:0;
	$country = $_SESSION["reg"]["country"];
	$city = $_SESSION["reg"]["city"];
	$descrwords = $_SESSION["reg"]["descrwords"];
	$ip = $_SERVER['REMOTE_ADDR'];
	$dt = date("Y-m-d H:i:s");
	
	$latitude  = (float)@$_SESSION["reg"]['latitude'];
	$longitude = (float)@$_SESSION["reg"]['longitude'];
	
	$values = "'$login', '$pass', '$email', '$name', '$sename', $birth_year, $birth_month, $birth_day, $sex, '$country', '$city', '$ip',
	           '$keypad', '$dt', '$descrwords', $latitude, $longitude";
	$fields = "Login, Password, Email, Name, Sename, birth_year, birth_month, birth_day, Sex, Country, City, IP, key_restore, 
			   last_enter, DescrWords, latitude, longitude";			   
	$result = $db->query("INSERT INTO qmex_users(".$fields.") VALUES(".$values.")");
	
	if(!$result)
	{
		$info = Logs::getError($db->lastError());
		Logs::writeLogs(C::LOG_ERROR,$info);
	}
	else
	{	
		$id = Human::getIdByLogin($login);
		$arrkeys = array_keys($_SESSION["interests"]);
		for($i=0;$i<count($arrkeys);$i++)
		{
			$key = $arrkeys[$i];
			$val = $_SESSION['interests'][$key];
			$res = $db->query("INSERT INTO qmex_user_interests(Login, key_interest, interest_keywords) VALUES($id, $key, '$val')");
			if(!$res) { $success=false; break; }
		}
		
		if($success) 
		{
			$_POST["q_inter_login"]=$login;
			$_POST["q_inter_pass"]=$pass;
			ob_start();
			require 'controllers/login.php';
			ob_clean();
			}
			
		}
		
	}
	    unset($_SESSION["reg"]);
		echo $success ? '<script>location.href="register?step=success&justr=1"</script>' : 
						'<script>location.href="register?step=failed&justr=1"</script>';
	}
?>