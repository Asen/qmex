﻿<link rel="stylesheet" href="views/main/styles/registration.css">

<style>
.highlight-item{
	background-color:#069;
	color:#FFF;
	}
</style>

<?php
$i_exists = isset($_SESSION["entered"]);
switch($i_exists)
{
	case true: $forward_text = "Готово. Обновить интересы.";
	           $func_ready_change = "Qcheck(1)";
			   break;
	case false:$forward_text = "Далее";
	           $func_ready_change = "Qcheck(0)";
			   break;
	}

if($i_exists==false)
if(!isset($_SESSION["reg"]["step2_succ"]) || !$_SESSION["reg"]["step2_succ"])
echo "<script>location.href='register?step=personal'</script>";
?>
<div class="WContent">
<div class="step">
<?php if($i_exists==false) echo '<span class="step-name">Шаг третий:</span><span class="step-descr"> определение интересов</span>';
else echo '<span class="step-descr">Изменение интересов и специализаций</span>';?>
</div>

<div class="regstep_subcaption" style="padding:10px" t>
• Выберите свои основные специализации, интересы и увлечения.<br> Также вы можете уточнить, чем именно вы занимаетесь / интересуетесь в каждой из выбранных областей.<br>
<b>Например:</b><br>
Гаджеты → смартфоны, планшеты, gps<br>
Биология → зоология, анатомия, мозг человека и т.д
<div style="border-top:1px dashed #069"></div>
• Совокупности интересов образует специализации. Вот пример:<br>
Торговля + Экономика = Инвестор <br>
Бизнес + Сотрудничество = Бизнесмен <br>
</div>
</div>

<div style="padding-bottom:10px; font-size:12px; color:#06F">
Нажимайте на изображения подходящих вам интересов:
</div>

<div style="background:rgba(0,0,0,0.1); padding:10px">
<table class='full-width' style="text-align:center;" cellspacing="0" cellpadding="0" border="0" id="layout-interests">
<script>
function FuncPad(ob,type,cont)
{
$(function(){
tagid = parseInt($(ob).attr('tagid'));
if(type==1)
{
$(ob).addClass("highlight-item");
if($("#key-words-table").html()=="") 
$("#key-words-table").append("<th align='left' style='border-bottom:#069 groove 2px; height:40px;color:#036;font-size:16px;' colspan='3'>Также вы можете пояснить, чем конкретно вы занимаетесь в выбранных областях и что именно вам интересно.<br><span id='mark' style='color:#069;font-size:12px;'>(Ключевые понятия через пробел или через запятую. Понятия, содержащие более 1 слова, следует отделять тире)</span></th>");
$("#key-words-table").append("<tr id='"+$(ob).text()+"' class='qType'><td>Ключевые слова к '"+$(ob).text()+"' :</td><td style='width:60%'><input type='text' class='Tkeywords' value='"+cont+"' tagid='"+tagid+"'></td></tr>");
$(ob).toggle(function(){$(this).css({'background-color':'transparent',"color":"#03C"}); hd(this);},
function(){$(this).css({'background-color':'#069',"color":"white"}); sh(this);});
}
if(type==0)
	$(ob).toggle(function(){$(this).css({'background-color':'#069',"color":"white"}); sh(this);},
    function(){$(this).css({'background-color':'transparent',"color":"#03C"}); hd(this);});
});
	}
</script>
<?php

$IBASE = new Interests();

$cn = 5;
$counter = $cn;

foreach($IBASE -> SelectIBASE() as $val) 
{
$key = $IBASE->SelectKey($val);
if($counter==$cn) {echo('<tr>');$counter=0;}
$counter++;
echo('<td>');
$pp = Interests::IMG_PATH.$val[1];
$tt = $val[0];
$int_id = $key;
echo("<img src='$pp' class='TImage' tag='".$key."'><div class='TImageCapt' id='icaption-".$key."' tagid='$int_id'>$tt</div>");
if(isset($_SESSION["interests"])) $ark = array_keys($_SESSION["interests"]);
if(isset($_SESSION["interests"]) && in_array($int_id,$ark))
{
$content = $_SESSION["interests"][$int_id];
echo('<script>FuncPad($(".TImageCapt:last"),1,"'.$content.'");</script>'); 
} else echo('<script>FuncPad($(".TImageCapt:last"),0,"")</script>');

echo('</td>');
if($counter==$cn) {echo('</tr>');$counter=0;}
}
?>
<tr>
<td colspan="5">
<div id='qinput'  style="padding:5px;min-height:30px;text-align:right; font-family:Arial, Helvetica, sans-serif;font-size:14px;" class="ea"></div>

<?php
if($i_exists==false) echo '<div style="float:left;text-align:center; width:200px" onmouseup="location.href=\'register?step=personal\'">
<div class="qButton" style="font-weight:100"><img src="qmex_img/arrow_left.png" width="10px" height="10px" style="padding-right:10px">Назад</div>
</div>';
?>

<div style="float:right; text-align:center; width:200px" onmouseup="<?php echo $func_ready_change?>">
<button class="qButton full-width" style="font-weight:100">
<?php echo $forward_text?> <img src="qmex_img/arrow_right.png" width="10px" height="10px">
</button>
</div>

</div>

</td>
</tr>
<script>

$(function(){
	w = parseInt($("#layout-interests").width() / 6);
	$(".TImage").width(w);
	$(".TImage").height(w);
	});

function sh(obj)
{
	var tt = parseInt($(obj).attr("tagid"));
	var request = SendDynamicMsg(['select_interest','type'],[tt,'none'],null,"handlers/regist.php","POST");
	if(request=='1') {
		if($("#key-words-table").html()=="") 
		$("#key-words-table").append('<th align="left" style="border-bottom:#069 groove 2px; height:40px;color:#036;font-size:16px;" colspan="3">Также вы можете пояснить, чем конкретно вы занимаетесь в выбранных областях областях и что именно вам интересно.<br><span id="mark" style="color:#069;font-size:12px;">(Ключевые понятия через пробел или через запятую. Понятия, содержащие более 1 слова, следует отделять тире)</span></th>');
		$("#key-words-table").append("<tr id='"+$(obj).text()+"' class='qType'><td>Ключевые слова к '"+$(obj).text()+"' :</td><td style='width:60%'><input type='text' class='Tkeywords' tagid='"+$(obj).attr("tagid")+"'></td></tr>");
		}
}
	
function hd(obj)
{
	var tt = parseInt($(obj).attr("tagid"));
	var request = SendDynamicMsg(['select_interest','type'],[tt,'remove'],null,"handlers/regist.php","POST");
	$("#"+$(obj).text()).remove();
	if($("#key-words-table .qType").length<1) $("#key-words-table").empty();
	}

function Qadd(obj)
{
	var qid = $(obj).attr("tagid");
	var request = SendDynamicMsg(['interest_add_keywords','content'],[qid,obj.value],null,"handlers/regist.php","POST");
	}
	
	
function Qcheck(id)
{
	var request = SendDynamicMsg(['checkstep3'],[parseInt(id)],null,"handlers/regist.php","POST");
	$("#qinput").html(request);
	window.setTimeout(function(){ $("#qinput").empty(); }, 2000);
	}
	
$(".TImage").live("mousedown",function(){ $("#icaption-"+$(this).attr("tag")).click(); })
$(".Tkeywords").live("keydown",function(){if(this.value.length>44) this.value=this.value.substr(0,44);});
$(".Tkeywords").live("change",function(){Qadd(this);});
</script>

<tr>
<td colspan="10" style="padding-top:30px; text-align:left">

<div style="border-top:solid 3px #069">
<table style="width:100%">
<tr>
<td valign="top">
<img src="qmex_img/warning.png" width="20px">
</td>
<td>
<div id="mark">
Специализации и интересы - одна из самых важный частей системы qMex. Постарайтесь выбрать самые важные для вас.<br>
<u>Предпочтительное количество интересов - от 3 до 8.</u>
</div>
</td>
</tr>
</table>
</div>

</td>
</tr>

</table>
<table style="margin-top:0px" cellspacing="10px" id="key-words-table" border="0"></table>
</div>