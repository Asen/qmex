﻿<?php
if(!isset($_SESSION["reg"]["step3_succ"]) || !$_SESSION["reg"]["step3_succ"]) 
echo "<script>location.href='register?step=interests'</script>";
?>

<style type="text/css">

#td1{
	text-align:right;
	opacity:0.4;
	}
	
#td2{
	text-align:left;
	}

#q_email_info{
	background-color:#FFE6E6;
	border:#D70000 double 1px;	
	font-size:12px;
	}
</style>

<div class="WContent">
<div class="step">
<span class="step-name">Завершающий шаг:</span><span class='step-descr'> указание электронной почты</span>
</div>


<table class="qLastRegStep" align="center" cellspacing="5px" border="0" t>
<tr>
<td>E-Mail:</td>
<td style="height:30px"><input type="text" class="qmex_former" placeholder="email@email.com" style="width:80%" id="Qemail"></td>
</tr>
<tr style="height:50px">
<td colspan="2"><div id="q_email_info"></div></td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:100%">
<tr>
<td align="left">
<div style="float:left;text-align:center; width:200px" onmouseup="location.href='register?step=interests'">
<div class="qButton" style="font-weight:100"><img src="qmex_img/arrow_left.png" width="10px" height="10px" style="padding-right:10px">Назад</div>
</div>
</td>
<td align="right">
<div style=" text-align:center; width:200px" onmouseup="Qcheck()">
<div class="qButton" style="font-weight:100">Завершить регистрацию<img src="qmex_img/arrow_right.png" width="10px" height="10px" style="margin:0 0 0 10px"></div>
</div>
</td>
</tr>

<tr><td colspan="3">
<?php $is = !$_SESSION["reg"]["name"]==''; $nop = ' - '; ?>
<div align="left" class='step' style="margin-top:50px">
<img src="qmex_img/warning.png" width="20px">
Вот часть информации, введенной вами за предыдущие 3 шага:
</div>
<table width="100%" border="0" align="left" cellspacing="10px" class="reg_result" t>
<tr>
<td id="td1">Ваш qID( логин ): </td>
<td id="td2"><?php echo splitter(get_not_escaped($_SESSION["reg"]["login"]),50);?></td>
</tr>

<tr>
<td id="td1">Ваше имя: </td>
<td id="td2"><?php echo ($is) ? splitter(get_not_escaped($_SESSION["reg"]["name"]),50):$nop?></td>
</tr>

<tr>
<td id="td1">Ваша фамилия: </td>
<td id="td2"><?php echo ($is) ? splitter(get_not_escaped($_SESSION["reg"]["sename"]),50):$nop?></td>
</tr>

<tr>
<td id="td1">Ваша страна: </td>
<td id="td2"><?php echo ($is) ? splitter($_SESSION["reg"]["country"],50):$nop?></td>
</tr>

<tr>
<td id="td1">Ваш город: </td>
<td id="td2"><?php echo ($is) ? splitter($_SESSION["reg"]["city"],50):$nop?></td>
</tr>

<tr>
<td id="td1">Пол: </td>
<td id="td2"><?php echo ($is) ? Enum::$SEXES[$_SESSION["reg"]["sex"]-1]:$nop?></td>
</tr>

<tr>
<td id="td1">Слова, описывающие<br>вашу деятельность<br>и интересы: </td>
<td id="td2"><?php echo ($is) ? str_replace(' ',', ',$_SESSION["reg"]["descrwords"]):$nop?></td>
</tr>

<tr>
<td id="td1">Дата рождения: </td>
<td id="td2"><?php echo ($is) ? $_SESSION["reg"]["day"]." ".Enum::$MONTHES[$_SESSION["reg"]["month"]-1].", ".$_SESSION["reg"]["year"].' г.':$nop?></td>
</tr>
</table>
</td></tr>
</table>

</div>

<script language="javascript">

$(function(){
	$("#q_email_info").slideUp(0);
});

function Qcheck()
{
	if($("#Qemail").val()!="")
	{
		var request = SendDynamicMsg(['email'],[$("#Qemail").val()],null,"handlers/regist.php","POST");
		$("#q_email_info").empty();
		$("#q_email_info").append(request);
		$("#q_email_info").slideDown(600);
		  } else
		  {
			  $("#q_email_info").empty();
			  $("#q_email_info").append("Укажите, пожалуйста, свой почтовый ящик.");
			  $("#q_email_info").slideDown(600);
			  }
	}
</script>