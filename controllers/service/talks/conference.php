<style>
#qmex_content_block{  background-color:#DFDFDF }
#talk-box{ padding:10px; background-color:#007EFD; }
#talk-box-new{ display:none }
#essence-box{ overflow-y:scroll; padding:20px }
#conf-number{ color:white; font-size:1.8em; font-weight:bold; padding:5px; }
#blank-opinion{ background-color:#B7DBFF; padding:10px; font-size:0.85em; font-weight:bold; color:#06F; text-align:center }
#no_new_members{ padding:10px; font-size:0.7em; background-color:#EEE; color:#000; }
#linked-sign{ padding:4px; background-color:#06F; color:#EEE; font-size:0.7em; text-align:center; cursor:pointer }
#linked-sign:hover{ background-color:#09F }
#linked-essences{ background-color:#9DCEFF; font-size:0.8em; padding:15px }
.conf-created-for{ color:#333; font-weight:bold }
.user-conf-position{ font-weight:bold; color:#000; }

</style>

<?php include_once("tools/sided/veditor.php"); ?>
<?php

$CONF_ID = (int)$_GET['conf'];
$CONF_INFO = Conference::getConferenceInfo((int)$_GET['conf']);
$PARTICIPANTS_COUNT = Conference::getConferenceParticipantsCount($CONF_ID);
$CREATOR_INFO = Human::getUserInfo($CONF_INFO['creator']);
$THIS_USER = isset($_SESSION['login']) ? $_SESSION['id'] : -1;
$IS_CONFERENCE_OWNER = $CONF_INFO['creator']==$THIS_USER;
$AVAILABILITY = Conference::checkAvailability($CONF_ID);
$DB = new DB();

?>


<?php if(!$CONF_INFO['is_exists']): ?>
	<div style="padding:15px; background-color:#99F; color:#000; font-size:12px">
    	Этой конференции еще не существует или она была удалена.
    </div>
<?php else: ?>


<div id="talk-box">
<table class="full-width" cellpadding="0"  cellspacing="0">
<tr>
<td width="25%" valign="top">


<?php if($AVAILABILITY==Conference::OK_AVAILABLE): ?>
    <button class="qButton" style="border-color:#EEE" id="make-opinion">
    <div id='make'>
    Я хочу сказать..<br>
    <span style="font-weight:lighter; font-size:11px">Войти в диалог.</span>
    </div>
    <div id='remove' style="display:none">
    Отказаться от выражения мнения
    </div>
    </button>
<?php elseif( $AVAILABILITY==Conference::NA_NOT_MEMBER  &&  $CONF_INFO['linked_with_type']=='env' ): ?>
	<div style="color:#FFE1E1; font-weight:bold">Для участия необходимо войти в окружение инициатора.</div>
<?php elseif( $AVAILABILITY==Conference::NA_NOT_MEMBER &&
			  !Conference::isMembershipRequired((int)$_GET['conf'])) : ?>
	<button class="qButton" style="border-color:#EEE" onclick="JoinTo()">
    <span style="font-weight:lighter; font-size:10px">Эта конференция непубличная.</span>
    <br>Присоединиться?
    </div>
    </button>
<?php elseif($AVAILABILITY==Conference::NA_NOT_MEMBER): ?>
	<div style="color:#FFE1E1; font-weight:bold">Инициатор пока не подтвердил ваше участие.</div>
<?php elseif($AVAILABILITY==Conference::NA_NOT_STARTED): ?>
	<div style="color:#FFE1E1; font-weight:bold">Начало:<br><?php echo translate_timestamp($CONF_INFO['plan_start']); ?></div>
<?php elseif(!isset($_SESSION['id'])): ?>
	<div style="color:#FFE1E1; font-weight:bold">Необходимо войти в систему.</div>
<?php else: ?>
	<div style="color:#FFE1E1; font-weight:bold">Эта конференция была закрыта.</div>
<?php endif; ?>


<div style="display:none" id="send-opinion-box">
<hr size="5" color="#000000">
<button class="qButton full-width" id="send-opinion-to-conf" style="padding:10px; border-color:#B7DBFF">Сказать!</button>
<div id="mark" style="color:#EEE; padding-top:15%">
Для удобства восприятия вашего мнения,
<u>не следует использовать</u> плохочитаемые цвета, крупные шрифты и изображения большого размера ( более 500x500 пикселей )<br><br>
<img src='qmex_img/UI/talks/warning.png' width='15px' />
<strong>Будьте внимательны: выраженное мнение нельзя будет изменить или удалить!</strong>
</div>
</div>
</td>
<td width="75%" valign="top">
<div id="conf-number">Конференция <?php echo (int)$_GET['conf']; ?></div>
<div id="talk-box-new"><textarea rows="3" id="talk-thread"></textarea></div>
</td>
</tr>
</table>
</div>

<div id="linked-sign">Что связано с этой конференцией?</div>
<div id='linked-essences'>

<?php 

$Types = Enum::DevelopmentTypes();

if(mb_strlen(trim($CONF_INFO['linked_with_type']))>0)
{
	if($CONF_INFO['linked_with_type']=='event'){
		$DB->query("SELECT caption,event_kind FROM qmex_developments WHERE id=".$CONF_INFO['linked_with_id']);
		if($DB->rowCount()>0) {
			$data = $DB->one();
			$type = $data[1]>0 ? $Types[$data[1]].': ' : '';
			echo '<div class="conf-created-for">Конференция создана для общения на тему события: </div>';
			echo '<a class="underlined" href="/developments?view=show&id='.$CONF_INFO['linked_with_id'].'">'.
			'<b>'.$type.'</b>'.$data[0].'</a>';
		}
		}
	if($CONF_INFO['linked_with_type']=='swop'){
		echo '<div class="conf-created-for">Конференция создана для общения о Swop: </div>';
		SWOP::CreateSwopById($CONF_INFO['linked_with_id']);
		SWOP::PrintJsRoutine();
	}
	if($CONF_INFO['linked_with_type']=='env')
	{
		$login = Human::getLoginById($CONF_INFO['linked_with_id']);
		echo '<div>Конференция предназначена для коммуникации с окружением
			  <a class="underlined" href="/profile?id='.$login.'"><b>'.$login.'</b></a>
			  </div>';
		}
} else echo '<div class="conf-created-for">Независимая конференция</div>';


$DB->query("SELECT id, caption, event_kind FROM qmex_developments WHERE belong_to_conf=".$CONF_ID.' ORDER BY ID DESC');
if($DB->rowCount()>0) echo '<div class="conf-created-for">События: </div>';
while($event_info = $DB->one())
{
	$type = $event_info[2]>0 ? $Types[$event_info[2]].': ' : '';
	echo '<a class="underlined" href="/developments?view=show&id='.$event_info[0].'"><b>'.$type.'</b>'.$event_info[1].'</a><br>';
}

?>

</div>

<table cellspacing="10" class="full-width" border="0" cellpadding="0">
<tr>
<td width="75%" valign="top">
<div id="essence-box">
<?php
$DB = new DB();
$DB->query("SELECT id FROM qmex_conferences_opinions WHERE conf_id=".(int)$_GET['conf']." ORDER BY datetime DESC");
if($DB->rowCount()>0)
	while($id=$DB->one(0))Conference::ShowOpinion($id);
else
	echo '<div id="blank-opinion">Диалог конференции пока пуст. Добавьте первое мнение!</div>';
Conference::printJSOpinionRoutine();
?>
</div>
</td>
<td width="25%" valign="top">

<img src="<?php echo $CONF_INFO['img'] ?>" width="100%" id="conference-image" />
<div style="text-align:center; padding-bottom:0px">
<?php if($IS_CONFERENCE_OWNER): ?>
<button class='qButton' id='ch-image' onclick="changeConfImage()">Изменить изображение?</button>
<?php endif?>
</div>


<div style="padding:5px 0 5px 0; font-size:1.1em; color:#9CF; font-weight:bold; background-color:#000; text-align:center">
<?php
$IBASE = new Interests();
$IBASE->SelectIBASE();
echo $IBASE->SelectName($CONF_INFO['vector']);
?>
</div>
<div style="color:#06C; font-size:0.75em">
<b>Конференция про...</b><br>
<div id='conference-idea'>
<span id='conference-idea-text'><?php echo $CONF_INFO['idea'] ?></span>
</div>
</div>

<div style="font-weight:bold; font-size:0.73em; color:#03F; background-color:#CCC; padding:5px">
<?php echo implode(', ',explode(' ',$CONF_INFO['words'])) ?>
</div>

<div style="text-align:right; padding-top:10px">
<?php if($IS_CONFERENCE_OWNER): ?>
<button class='qButton' id='ch-descr' onclick="changeConfIdea()">Изменить описание</button>
<?php endif?>
</div>

<?php if($IS_CONFERENCE_OWNER && $AVAILABILITY!=Conference::NA_WAS_CLOSED): ?>
<br>
<hr size="1" color="#0066CC">
	<?php if($CONF_INFO['linked_with_type']!='event'): ?>
    <div style='padding-top:10px'>
    <button class='qButton full-width' style="padding:5px" onclick="linkActualEvent()">
    <div style="font-weight:normal; font-size:0.8em">Намечается событие для участников?</div>
    Добавить новое событие
    </button>
    </div>
    <?php endif ?>
<div style='padding-top:10px'>
<button class='qButton_silver full-width' style="padding:5px" onclick="closeConference()">
<div style="font-weight:normal; font-size:0.7em">Тема конеренции больше не актуальна?</div>
Закрыть конференцию
</button>
</div>
<?php endif; ?>

<br>
<hr size="1" color="#0066CC">

<div style="padding-top:20px; font-size:0.75em; color:#06C; font-weight:bold">Инициатор конференции:</div>
<table style="color:#069; font-size:1.2em; font-weight:bold; padding-top:2px">
<tr>
<td valign="top"><img src="<?php echo $CREATOR_INFO['pic']?>" width="30px"/></td>
<td valign="middle">
<a href="<?php echo HTML::user_link($CREATOR_INFO['login']); ?>">
<?php echo splitter(String::mb_ucfirst($CREATOR_INFO['login']),10); ?>
</a>
</td>
</tr>
</table>

<?php if($IS_CONFERENCE_OWNER && $CONF_INFO['is_private']==1): ?>
<br>
<hr size="1" color="#0066CC">
<div style="background-color:#9CF">
<div style="padding:5px; font-size:0.75em; color:#06C; font-weight:bold">Желающие присоединиться к конференции:</div>
<?php
    $is_exists = false;
	$DB->query("SELECT members_not_joined FROM qmex_conferences WHERE id=$CONF_ID");
	foreach(explode(';',$DB->one(0)) as $val)
	if(mb_strlen(trim($val))>0){
		$is_exists = true;
		$uinfo = Human::getUserInfo((int)$val);
		echo '<div id="membership-'.(int)$val.'"><a href="'.HTML::user_link($uinfo['login']).'">
			  <img src="'.$uinfo['pic'].'" width="20px"> '.$uinfo['login'].'</a>
			  <button class="qButton check_join" tag="'.(int)$val.'">✔</button>
			  <button class="qButton_silver refuse_join" tag="'.(int)$val.'">×</button>
			  </div>';
		}
	if(!$is_exists) echo '<div id="no_new_members">Нет пользователей, желающих присоединиться к конференции.</div>';
?>
<?php endif ?>
</div>

<br>
<hr size="1" color="#0066CC">
<br>
<div style='color:#06C; font-size:0.75em; font-weight:bold; text-decoration:underline'>Участвующих лиц: <?php echo $PARTICIPANTS_COUNT ?></div>
<div style='color:#06C; font-size:0.75em; font-weight:bold'>Активность участников:</div>
<div style="padding:5px; height:200px; overflow-y:scroll;">
<?php
$DB->query("SELECT ops.creator, 
		   (SELECT IFNULL(SUM(v.Voite),0) FROM qmex_voites AS v WHERE v.Type='conf_opinion' AND v.Essence IN 
		   (SELECT id FROM qmex_conferences_opinions WHERE creator=ops.creator AND conf_id=$CONF_ID) ) AS Raiting
		   FROM qmex_conferences_opinions AS ops WHERE ops.conf_id=$CONF_ID GROUP BY ops.creator ORDER BY Raiting DESC");
while($data = $DB->one())
{
	$user_info = Human::getUserInfo($data[0]);
	$raiting = (int)$data[1];
	echo '<table class="full-width" border=0 cellspacing="2px"><tr>
		  <td width="10%"><img src="'.$user_info['pic'].'" width="20px" /></td>
		  <td width="70%"><a href="'.HTML::user_link($user_info['login']).'">'.splitter($user_info['login'],10).'</a></td>
		  <td width="20%" align="right"><div class="user-conf-position">'.$raiting.'</div></td>
		  </tr></table>';
	}

?>
</div>

</td>
</tr>
</table>


<script>

$(function(){
	InitEditor("#talk-thread","150px");
	
	$("#linked-essences").slideUp(0);
	$("#linked-sign").toggle(function(){ $("#linked-essences").stop(true,true,true); $("#linked-essences").slideDown(500) },
							 function(){ $("#linked-essences").stop(true,true,true); $("#linked-essences").slideUp(500) } )
	
	$("#make-opinion").toggle(newOpinion, removeOpinionBox);
	$("#send-opinion-to-conf").on('click', sendOpinion);
	$("#essence-box").height( $(window).height()*0.8+"px" );
	$(".check_join").on('click', function(){  CheckUserJoin(this,1) });
	$(".refuse_join").on('click', function(){ CheckUserJoin(this,0) })
	window.setInterval(Follow, 1200);
	});

function CheckUserJoin(o, type)
{
	var user = $(o).attr('tag');
	var ret  = SendDynamicMsg(['check_user_membership', 'conf_id', 'user', 'result'], 
			   [1, <?php echo $CONF_ID ?>, user, type],null,"handlers/talks.php","POST");
    if(ret=='+') $("#membership-"+user).slideUp(250);
	}

function Follow()
{
	var last = $(".ui_conference_opinion").length>0 ? $(".ui_conference_opinion:first").attr('tag') : 0;
	
	$.ajax({
		type:'GET',
		data:"conference_get_new_opinions=1&conf=<?php echo $CONF_ID ?>&last="+last,
		url:"handlers/talks.php",
		dataType:"text",
		async:true,
		success:function(ret){
				
				if(ret.trim()!='') {
					var block = $("<div></div>");
					$(block).html(ret);
					$("#essence-box").prepend(block);
					ConferenceClass.Routine.Reform();
					$(block).slideUp(0);
					$(block).slideDown(750);
					$("#blank-opinion").slideUp(750);
				}
						
			}
	});
	
	}

function sendOpinion()
{
	var new_opinion = GetEditor('talk-thread').getContent();
	if(new_opinion.length<3) {
		WINDOW.ShowMessage("Мнение слишком маленькое!", WINDOW.C.ERROR);
		return;
		}
	var conf_id = <?php echo $CONF_ID ?>;
	var res = SendDynamicMsg(['new_conference_essence',"conf_id","essence"],
							 [1,conf_id,new_opinion],null,"handlers/talks.php","POST");
	if(res=='+') $("#make-opinion").click(); else
	if(res=='~') WINDOW.ShowMessage("Мнение должно содержать от трех до тысячи букв.", WINDOW.C.ERROR); else
	if(res=='*') WINDOW.ShowMessage("Необходимо войти в систему qMex, чтобы быть участником конференции.", WINDOW.C.ERROR)
	}

var ANIM_TIME = 500;
function newOpinion()
{
	$("*").stop(true,true,true);
	$("#make-opinion").removeClass("qButton");
	$("#make-opinion").addClass("qButton_silver");
	$("#make").hide(ANIM_TIME);
	$("#conf-number").hide(ANIM_TIME);
	$("#remove").show(ANIM_TIME);	
	$("#talk-box-new").show(ANIM_TIME);
	$("#send-opinion-box").show(ANIM_TIME);
	GetEditor('talk-thread').focus();
	}
	
function removeOpinionBox()
{
	$("*").stop(true,true,true);
	$("#make-opinion").removeClass("qButton_silver");
	$("#make-opinion").addClass("qButton");
	$("#make").show(ANIM_TIME);
	$("#conf-number").show(ANIM_TIME);
	$("#remove").hide(ANIM_TIME);
	$("#talk-box-new").hide(ANIM_TIME);
	$("#send-opinion-box").hide(ANIM_TIME);
	}
	
function changeConfImage()
{
	var changeImage = function(new_image, conf_id){
		SendDynamicMsg(['change_conference_img',"conf_id","new_image"],
				   [1,conf_id,new_image],null,"handlers/talks.php","POST");
		$("#conference-image").fadeOut(400);
		window.setTimeout(function(){
			$("#conference-image").attr('src', new_image);
			$("#conference-image").fadeIn(400)
			}, 400);
		};
	
	var conf_id = <?php echo $CONF_ID ?>;
	WINDOW.Prompt("Изменить изображении конференции", "URL нового изображения:", 
				  function(e){changeImage(e, conf_id)} )
	
	}
	
function changeConfIdea()
{
	var idea = $("#conference-idea").text();
	$("#conference-idea-text").fadeOut(200);
	$("#conference-idea").append("<textarea class='full-width' id='descr-changer' style='height:200px; padding:5px'>"+idea+"</textarea>");
	$("#conference-idea").append("<button class='qButton' onclick='changeIdea(this)'>Готово!</button>");
	$("#ch-descr").hide(0);
	}
	
function changeIdea(sender)
{
	var conf_id = <?php echo $CONF_ID ?>;
	var new_idea = $("#descr-changer").val();
	SendDynamicMsg(['change_conference_idea',"conf_id","new_idea"],
				   [1,conf_id,new_idea],null,"handlers/talks.php","POST");
	$("#ch-descr").show(0);
	$("#conference-idea-text").fadeIn(200);
	$("#conference-idea-text").text(new_idea);
	$(sender).remove();
	$("#descr-changer").remove();
	}
	
function linkActualEvent()
{
	location.href='/developments?view=new&talk='+<?php echo $CONF_ID ?>;
	}
	
function closeConference()
{
	var CloseConf = function(){
		var conf_id = <?php echo $CONF_ID; ?>;
		SendDynamicMsg(['close_conference',"conf_id"],[1,conf_id],null,"handlers/talks.php","POST");
		location.href=location.href;
		}
			
	WINDOW.Confirm("Конференцию нельзя будет открыть вновь!<br>"+
				   "Вы действительно желаете закрыть конференцию?",
				   CloseConf);	
	}

function JoinTo()
{
	var conf_id = <?php echo $CONF_ID ?>;
	var trying = SendDynamicMsg(['try_to_join',"conf_id"],[1,conf_id],null,"handlers/talks.php","POST");
	if(trying=='+'){
		WINDOW.ShowMessage("Инициатор конференции получил вашу заявку на участие.", WINDOW.C.SUCCESS);
		window.setTimeout(function(){ location.href = location.href; }, 1000);
	}else WINDOW.ShowMessage("Необходимо авторизоваться!", WINDOW.C.ERROR);
	}

</script>


<?php endif; ?>

