
<style>
	.search-bit{ float:left; padding:2px; font-size:0.8em }
</style>

<div style="position:fixed; top:0; background-color:#2D96FF; width:70%" id='top-line'>
<table class="full-width" cellspacing="10px" border="0">
<tr>
<td>
    <select style="height:auto; color:#09F; padding:0" onchange="selectInterest()" id='confInterestList'>
    <?php
        $IBASE = new Interests();
		if(isset($_GET['i'])) echo '<option value="0" id="interest-default">'.$IBASE->SelectName((int)$_GET['i']).'</option>';
		echo '<option value="" id="interest-default">Интересно все?</option>';
        foreach($IBASE->SelectIBASE() as $ITEM){
            $KEY = $IBASE->SelectKey($ITEM);
            $NAME = $IBASE->SelectName($KEY);
            echo '<option value="'.$KEY.'" id="interest-'.$KEY.'">'.$NAME.'</option>';
            }
    ?>
    </select>
    <input type="text" class="std" style="margin-left:30px; font-weight:bold; text-align:center" 
    onkeyup="cid_checker(this)" id='cid' placeholder="№ конференции" />
    <button class="qButton qHint" tag='Поиск по ID' onclick="cid()"><img src="qmex_img/search.png" height="14px" /></button>
</td>
<td>

<table>
<tr>
<td><a onclick="ttype('pub');return false;"><button class="qButton">Общедоступные конференции</button></a></td>
<td><a onclick="ttype('pri');return false;"><button class="qButton_silver">Закрытые обсуждения</button></a></td>
<td><a onclick="ttype('plan');return false;"><button class="qButton">Запланированные</button></a></td>
</tr>
</table>

</td>
</tr>
</table>
</div>

<table style="padding-top:50px" id='conferences_box' width="100%" border="0" cellspacing="10px">
<tr>
<td width="60%" valign="top">
<?php

$conditions = array();

$filter = (isset($_GET['t'])) ? $_GET['t'] : '';
switch($filter){
	case 'pub':  array_push($conditions, 'is_private=0');
				 break;
	case 'pri':  array_push($conditions, 'is_private=1');
				 break;
	case 'plan': array_push($conditions, 'plan_start>-1');
				 break;
	}
if(isset($_GET['i'])) array_push($conditions, 'vector_interest='.(int)$_GET['i']);
if(isset($_GET['cid'])) array_push($conditions, 'id='.(int)$_GET['cid']);
if(isset($_GET['human'])) array_push($conditions, 'creator='.(int)$_GET['human']); 
if(isset($_GET['upart'])) array_push($conditions, "id IN (SELECT conf_id FROM qmex_conferences_opinions 
												   WHERE creator=".(int)$_GET['upart']." GROUP BY conf_id)");
if(isset($_GET['words'])){
	$sql = array();
	foreach(explode(" ",str_replace(","," ",$_GET['words']." ")) as $key) 
		$sql[] = "words LIKE '%$key%'"; 
	array_push($conditions, implode(" AND ",$sql));	
}

$conditions = (sizeof($conditions)>0) ? ' WHERE '.implode(' AND ', $conditions) : '';

$limit = 10;
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
if($page==0) $page=1;
$page--;
$page *= $limit;

$DB = new DB();
$DB->query("SELECT id FROM qmex_conferences".$conditions." ORDER BY last_activity DESC LIMIT $page, $limit");
while($cid = $DB->one(0))
Conference::ShowConference($cid);

if($DB->rowCount()==0):
?>

	<div style="padding:15px; background-color:#99F; color:#000; font-size:12px">
    	Ничего не найдено.
    </div>

<?php endif; ?>

</td>
<td valign="top" align="center" width="40%">
<div style='background-color:#FFF; padding:10px'>
<div style='color:#555; font-size:0.8em; text-align:left; '>
<img src='/qmex_img/info.png' width="20px" style="float:left; padding:5px">
Здесь сконцентрированы все тематические обсуждения и разговоры минисообществ пользователей,
объединенных общим интересом или задачей.<br><br>Вступайте в интересные конференции, поддерживайте 
дискуссии и организовывайте собственные для интерактивного взаимодействия с обществом.
</div><br>
<a href="talks?create"><button style="width:220px" class="qButton">Создать новую конференцию или обсуждение +</button></a>
</div>

<div class="UI-simple-menu-item" id="search-interesting">
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td valign="middle" align="center"><span id='search-word'>Поиск интересного</span></td>
<td valign="middle" align="center"><img src="qmex_img/search.png" width="20px"></td>
</td>
</tr>
</table>
</div>

<div style="text-align:left; font-size:0.7em; display:none; padding:5px" id="words-sign">
Выберите с чем должно быть связано обсуждение:
</div>
<div style="padding:10px;" id="words"></div>

</td>
</table>

<div id="talks-navigator" style="text-align:center"></div>

<script>

$(function(){
	
	__resize();
	$("#qcontent").resize(__resize);
	COMMON.AddPagedNav("#talks-navigator");
	$("#search-interesting").bind('click', PrepareSearching);
	
	});
	
function __resize()
{
	var ll = $("#qmex_head").width();
	var ww = $(window).width() - ll;
	$("#top-line").css({'left':ll+"px"});
	$("#top-line").width(ww+"px");
	
	var hh = $("#top-line").height();
	$("#conferences_box").css({'padding-top': hh+10+"px" });
	}

function PrepareSearching()
{
	var result = SendDynamicMsg(['get-words'],[1],null,'handlers/talks.php','GET','json');
	$("#words").html("")
	$("#words").slideUp(0);
	for(var key in result)
		$("#words").append("<div class='search-bit'><input type='checkbox' class='word-bit' word='"+result[key]+"'>"+
							result[key]+"&nbsp;</div>");
	$("#words").slideDown(500);
	$("#words-sign").show(100);
	$("#search-word").text("Начать поиск");
	$("#search-interesting").unbind('click');
	$("#search-interesting").bind('click', startWordsSearching);
	}
	
function startWordsSearching()
{
	var words = "";
	$(".word-bit:checked").each(function(index, element) {
        words += $(element).attr('word')+" ";
    });
	var params = QUERY.splitGetQuery(location.href);
	params["words"] = words;
	location.href="talks?"+QUERY.completeQueryFromHash(params,true);
	}

function selectInterest()
{
	var k = $("#confInterestList").val();
	var params = QUERY.splitGetQuery(location.href);
	params["i"] = k;
	location.href="talks?"+QUERY.completeQueryFromHash(params,true);
	}
	
function ttype(t)
{
	var params = QUERY.splitGetQuery(location.href);
	params["t"] = t;
	location.href="talks?"+QUERY.completeQueryFromHash(params,true);
	}
	
function cid_checker(o)
{
	var v = $(o).val();
	var v_p = '';
	for(var k in v) 
	if(v[k].charCodeAt(0)>47 && v[k].charCodeAt(0)<58) v_p+=v[k];
	$(o).val(v_p);
	}
	
function cid()
{
	var cid = parseInt($("#cid").val());
	location.href='talks?cid='+cid;
	}

</script>