<?php
Security::checkAccess();
?>

<style>
.sign{ color:#06F; font-size:14px; padding:3px }
.clare{ color:#555 }
.date-essence{ padding:1px; width:50px }
.qMT{ color:#CCC }
textarea{ font-family:Arial, Helvetica, sans-serif }
select{ background-color:#FFF; }
</style>

<div class="BigCaption">Создание конференции</div>
<div class="WCaption" style="font-weight:100; font-size:0.7em">Важную информацию о конференциях и обсуждениях нельзя будет изменять в дальнейшем!<br>Это сделано в целях соблюдения компетентности. </div>

<table class="full-width" id="conference-creation-layout" style="margin-top:50px" cellspacing="15px" cellpadding="0">
<tr>
<td width="70%">
<div class='sign'>Какова тема конференции?</div>
<textarea class='full-width' style="padding:5px; color:#06F; border: 1px solid #06F" rows="5" 
placeholder="краткое описание конференции" id='conf-descr'></textarea>
</td>
<td width="30%" valign="top" rowspan="100">
<img src="qmex_img/UI/talks/undefined.png" id="conference-image">
</td>
</tr>

<tr>
<td>
<input type="text" style="padding:5px;" class='full-width' id="image" 
placeholder="уникальное изображение, характеризующее конференцию( URL )" />
</td>
</tr>

<tr>
<td>
<div class='sign' style="background-color:#FFF; padding:20px">Свойства конференции:<br>
<hr color='#0066FF' size=1>

    <div style="padding:5px">
    	<input type="radio" class='radio' id="public"> 
        <strong>публичная</strong> <span class="clare">( открыта для всех )</span><br>
    	<input type="radio" class='radio' id='private'> 
        <strong>приватная</strong> <span class="clare">( общение доступно лишь ограниченному кругу лиц )</span>
    </div>
 
    <br>
    <div style="padding:5px; background-color:#DDF">
    	<input type="checkbox" id='planning' /> Запланирована на конкретное время
        <div id='mark'>(Конференция начнется в заданное вами время)</div>
        <div id='plan-box' style="padding:15px">
            <table style="color:#069">
            <tr>
            <td width="25%"><strong>Начало через :</strong><br>
            <div id='mark'>( по <a onclick="qMex_Time();return false;" class="underlined">qMex-Time</a> )</div>
            </td>
            <td width="25%">
            	<div style="border:1px solid #06F; padding:5px">
                	<table>
            		<tr>
                    <td>Дней:</td><td><input type="number" class="date-essence" id='days' min="0" max="180" /></td>
                    </tr>
                    <tr>
                    <td>Часов:</td><td><input type="number" class="date-essence" id='hours' min="0" max="24" /></td>
                    </tr>
                    <tr>
                    <td>Минут:</td><td><input type="number" class="date-essence" id='minutes' min="0" max="60" /></td>
                    </tr>        
                    </table>   
            	</div>
            </td>
            <td width="50%" align="right">
            	<div id="time-start" style="color:#000"></div>
            </td>
            </tr>
            </table>
            <br>
            <div  id='qmex-time-box'>
               <div style="font-weight:bold; font-size:0.8em; border:1px dotted #069">Текущее время по qMex-Time :</div>
               <div style="background-color:#069; padding:10px; color:#FFF; border-radius:3px">
               		<span id='qmex-time' style="color:#B0D8FF; font-size:16px"></span> ; <span id='qmex-monthday' style="color:#CCC"></span>
               </div>
            </div>
        </div>
    </div>
    <br>
    <div style="padding:5px">
        Основное направление:<br>
        <div style="padding-top:5px;">
            <select style="height:auto; color:#09F; " class="qmex_former search_bit" id="interests-list">
            <option value="" id="interest-default"><Не выбрано></option>
            <?php
            
            $IBASE = new Interests();
            foreach($IBASE->SelectIBASE() as $ITEM)
            {
                $KEY = $IBASE->SelectKey($ITEM);
                $NAME = $IBASE->SelectName($KEY);
                echo '<option value="'.$KEY.'" id="interest-'.$KEY.'">'.$NAME.'</option>';
                }
            
            ?>
            </select>
        </div>
        <span id="mark">( интерес, с которым связана конференция )</span>
    </div>

<br>
<div>
<input type="text" style="padding:5px; width:90%" id="words" placeholder="Ключевые слова, характеризующие конференцию" />
<br>
<span id="mark">( главные слова, описывающие идею конференции / обсуждения )</span>

</div>

</div>
</td>
</tr>

<tr>
<td align="right">
<hr size="3px" color="#0066FF">
<button class="qButton" style="padding:5px; margin-top:5px; width:30%" onclick="Create()">Готово!</button>
</td>
</tr>

</table>

<script>

$(function(){
	
	$("#conference-image").width($("#conference-creation-layout").width()*0.3)
	$("#image").on('change', function(){ $("#conference-image").attr('src', $(this).val()) })
	
	$("#plan-box").slideUp(0);
	
	$('.radio').on('change', function(){ 
		$('.radio').each( function(index, element){ element.checked=false; });
		this.checked=true;
	});
	
	$("#planning").on('change', function(){
		$("#plan-box").stop(true,true,true);
		this.checked ? $("#plan-box").slideDown(500) : 	$("#plan-box").slideUp(500);
		if(this.checked){
			window.setTimeout(Time, 0);
			window.setInterval(Time, 1000);
			} 
	});
	
	$(".date-essence").on('change', function(){
		var days = $("#days").val();
		var hours = $("#hours").val();
		var minutes = $("#minutes").val();
		var $time = SendDynamicMsg(['count-time-for-starting','days','hours','minutes'],[1, days, hours, minutes],
								null,'handlers/talks.php','GET','json');
		$("#time-start").html("<b><u>Начало конференции:</u></b><br><div style='color:#069'>"+
							  $time['day']+" "+$time['month-name']+" "+$time['year']+" в "+$time['hour']+" : "+$time['minute']+
							  "</div><b>по qMex-Time</b>")					
		});
	
	
	});

function Time()
{
	$time = SendDynamicMsg(['qmex-time'],[1],null,'handlers/ask.php','GET','json');
	$("#qmex-monthday").text( parseInt($time['day'])+' '+$time['month-name']+", "+$time['year']+" год " );
	$("#qmex-time").text( $time['hour']+' : '+$time['minute']+" : "+$time['second'] );
	}
	
function qMex_Time()
{
	$("#qmex-time-box").css({'background-color':'#09F'});
	window.setTimeout(function(){ $("#qmex-time-box").css({'background-color':'transparent'}); }, 250);
	}

function Create()
{
	var data = {};
	data['descr'] = $("#conf-descr").val();
	data['is_private'] = $("#private:checked").length>0 ? true : false;
	data['is_planned'] = $("#planning:checked").length>0;
	if(data['is_planned']){
		data['days'] = parseInt($("#days").val());
		data['hours'] = parseInt($("#hours").val());
		data['minutes'] = parseInt($("#minutes").val());
		}
	data['words'] = $("#words").val();
	data['image'] = $("#image").val();
	data['interest'] = $("#interests-list").val();
	var keys = ['create-conference'];
	var vals = [1];
	for(var k in data) { keys.push(k); vals.push(data[k]); }
	$res = SendDynamicMsg(keys,vals,null,'handlers/talks.php','POST','json');
	if($res['result']=='+') {
		var conf_id = $res['conf_id']; 
		WINDOW.ShowMessage("Конференция успешно создана!<br>ID созданной конференции = "+
							conf_id+'<br><a href="/talks?conf='+conf_id+'" class="underlined">(войти в конференцию)</a>', 
							WINDOW.C.SUCCESS);
		$("#conf-descr").val('');
	} else 
	  WINDOW.ShowMessage("Некоторые поля заполены неверно. Пожалуйста, проверьте все параметры конференции.", WINDOW.C.ERROR);
	}

</script>