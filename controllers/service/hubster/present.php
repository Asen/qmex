<?php if(isset($_GET['random'])){include("controllers/service/hubster/random.php"); exit;} ?>
<?php  include_once("tools/sided/veditor.php"); ?>


<style>
.note_type{ font-family:Arial, Helvetica, sans-serif; font-size:0.7em; color:#777; padding:5px; border-bottom:1px #999 dashed; cursor:pointer;; font-size:11px;}
.note_type:hover{ background-color:#06C; color:#FFF }
.only_interest{ font-family:Arial, Helvetica, sans-serif; font-size:0.7em; color:#000; padding:5px; cursor:pointer;}
.only_interest:hover{ background-color:#06C; color:#FFF }
.note_spec{border-bottom:1px dotted #006699; padding:3px; font-size:11px}
.note_spec:hover{background-color:#80BFFF}

.choser{ background:transparent; border-radius:3px; color:#000; font-weight:bold; padding:5px; border:1px solid #999; cursor:pointer;}
.choser:hover{ border:1px solid #555; }

.UI-simple-menu-item{ background:#69F }
</style>

<?php

$from_userman = (isset($_GET['u'])) ? Human::GetLoginById((int)$_GET['u']) : -1; 

$sort_by = isset($_GET['sort']) ? $_GET['sort'] : 'date';
$allowed_sort_types = array('date','rait','update','random');
if(!in_array($sort_by,$allowed_sort_types)) $sort_by='date';
$sort_by_flag = '';
switch($sort_by)
{
	case 'date':    $SORT = ' ID '; 
					$sort_by_flag = 'Свежесть'; 
					break;
	case 'rait':    $SORT = ' Rating '; 
					$sort_by_flag = 'Крутизна'; 
					break;
	case 'update':  $SORT = ' updated_at '; 
					$sort_by_flag = 'Время изменения'; 
					break;
	case 'random':  $SORT = ' RAND() '; 
					$sort_by_flag = 'Случайность'; 
					break;
	
	}

$w = Enum::ConceptionsWords();
$conception = (isset($_GET['type']) && (int)$_GET['type']>0) ? $w[(int)$_GET['type']] : '';

?>

<div style="background-color:#069; border-radius:2px; color:white; padding:5px; font-size:11px; font-family:Arial, Helvetica, sans-serif">
<table border="0" cellspacing="5px">
<tr>
<td><img src="/qmex_img/UI/profile/public.png" width="30" height="30" style="padding:0px;"></td>
<td>
<strong>Hubster</strong> - концентратор полезной информации. Здесь можно получить информацию о занятиях других людей, об их намерениях и проектах. Также, почитать об интересных и важных вам вещах или получить советы. 
</td>
</tr>
</table>

<table cellspacing="2px" cellpadding="0" class='UI-simple-menu-tab'>
<tr>
<td class="UI-simple-menu-item" onclick="location.href='/#hub'">Создать хаб ?</td>
<td class="UI-simple-menu-item" onclick="location.href='/hubster?random'">Случайный хаб</td>
</tr>
</table>

<br>

</div>


<div>
<input type="text" class="UI-text-box full-width" id="kw-search" value="<?php echo isset($_GET['kw']) ? $_GET['kw'] : '' ?>" 
placeholder="Поиск интересного" />
</div>

<div id='search-bar' style="max-height:90px; overflow:hidden">
<table style="width:100%;" cellspacing="0" border="0">
<tr>
<td width="90%">
<div style="padding:10px; color:#069; ">
<?php

function __predicate_singles_remove($s)
{
	return mb_strlen(trim($s))>2 ? true:false;
	}
	
function __predicate_formatter($s)
{
	return str_replace(array('"',"'","!","?",".","«","»",',',':',';'), 
					   array('','','','','','','','','',''), 
					   $s);
	}

$db = new DB();
$db->query("SELECT Meaning, Substance FROM qmex_user_notes LIMIT 1000");
$line = "";
while($data = $db->one())
{
	$line .= " ".trim(mb_strtolower($data[0]))." ".trim(mb_strtolower($data[1]));
	if(mb_strlen($line)>32678) break;
	}
	
unset($data);
$line = array_unique( array_filter( array_map('__predicate_formatter', explode( ' ', $line ) ), '__predicate_singles_remove' ) );

foreach($line as $val)
echo '<div style="float:left; padding:4px"><input type="checkbox" n="'.$val.'" class="kw-box">'.$val.'</div>';

?>
</div>

</td>
<td width="10%" align="center" valign="top" id='search-button-bar'>
<button class='qButton qHint' id='search_by_kw' tag='Начать поиск хабов' 
style="border-radius:2px; margin-top:15px; border:1px solid white;" onclick="search()">
<img src="qmex_img/search.png" width="0px" id="search-img">
</button>
</td>
</tr>
</table>
</div>



<div style="font-size:1.1em; background-color:transparent; color:#036; padding:20px 0 20px 5px; border-top: 3px double #069; 
border-bottom: 3px double #069">
<?php  
$IBA = new Interests();
$IBA->SelectIBASE();
echo isset($_GET['s']) ? 
	'Текущие занятия и новости в сфере <b>"'.($IBA->SelectName((int)$_GET['s'])).'"</b> у' : 
	'<u>'.$conception.'</u>';
echo ' ';
?>
<?php 
if($from_userman!=-1) 
echo '<a href="/profile?id='.$from_userman.'" class="underlined" style="color:#069">'.String::mb_ucfirst($from_userman).'</a>' 
?>
</div>

<table cellpadding="0" cellspacing="5px" border="0" style="width:100%; padding-top:0px">
<tr>
<td valign="top" width="80%" id="QPublic">

<?php

function rem_empty($a)
{
	return (trim($a)!='') ? true:false;
	}

$IS_ENTERED = isset($_SESSION["entered"]);
$IAM = isset($_SESSION["id"]) ? $_SESSION["id"] : -1;

//global $db;
$req = array();

if(isset($_GET['favorites']) && $IS_ENTERED) 
{
	$essences = array();
	$db->query("SELECT Essence FROM qmex_favorites WHERE Login=$IAM AND Type='note'");
	while($ess = $db->one(0)) array_push($essences,$ess);
	if(count($essences)==0) array_push($essences, -1);
	array_push($req,'ID IN ('.implode(',',$essences).')');
}

$isfavorite = (isset($_GET['oi']) && (int)$_GET['oi']==1  && $IS_ENTERED);
if($isfavorite)
{
	$my_interests_keys = isset($_SESSION["interests"]) ? array_keys($_SESSION["interests"]) : array();
	$my_interests = implode(', ',$my_interests_keys);
	array_push($req,'Theme IN ('.$my_interests.')');
	}
	
if(isset($_GET['u'])) array_push($req,'Login='.(int)$_GET['u']);
if(isset($_GET['type']) && trim($_GET['type'])!='') array_push($req, 'Type='.(int)$_GET['type']);
if(isset($_GET['s']) && trim($_GET['s'])!='') array_push($req,'Theme='.(int)$_GET['s']);
if(isset($_GET['kw']) && trim($_GET['kw'])!=''){
	$sql = array();
	foreach(explode(" ",str_replace(","," ",$_GET['kw']." ")) as $key) 
		$sql[] = "CONCAT(LOWER(Meaning),LOWER(Substance)) LIKE '%$key%'"; 
	array_push($req, implode(" AND ",$sql));
}

$page = (isset($_GET['page'])) ? (int)($_GET['page']-1) : 0;
if($page<0) $page = 0;

$key = '';
if(!empty($req)) $key="WHERE ";
$key .= implode(" AND ",$req);


Smarty::$DATA['db']->query("SELECT ID, (SELECT IFNULL(SUM(Voite),0) FROM qmex_voites WHERE Essence=qmex_user_notes.ID) AS Rating 
			FROM qmex_user_notes $key ORDER BY ".$SORT." DESC LIMIT ".($page*10).",10");
if(Smarty::$DATA['db']->rowCount()>0)
{
	while($data=Smarty::$DATA['db']->one())
	{
		//if(!isset($kw) || mb_strstr(strip_tags( trim(mb_strtolower($data[1]))),$kw)) 
		Note::showNote($data[0]);
	}
}
else {
	echo "<div style='text-align:center; font-size:14px;font-family:Arial, Helvetica, sans-serif; font-size:12px; background-color:#CCC; 
	padding:10px' id='none_notes'>Ничего подобного не найдено.</div>";
	$NOT_FOUND = 1;
}

Note::JSRoutine();
Comment::showRoutine();

?>
<div style="text-align:center; padding:10px" id="public-nav"></div>
</td>
<td valign="top" width="20%">

<div>
<div style="padding:3px; font-size:0.9em; font-weight:bold; color:#069">Предпочтение:</div>
<button class='choise-selector choser full-width' items="Свежесть|Крутизна|Время изменения|Случайность" 
func='sb_date|sb_rait|sb_update|sb_random'> 
<?php echo $sort_by_flag; ?> <img src="qmex_img/arrow_down_black.png" width="10px"> 
</button>
</div>

<br>

<?php if($IS_ENTERED): ?>
<div class="only_interest" ><input type="checkbox" id='only_fav' <?php if($isfavorite) echo 'checked' ?>> отображать только интересные мне хабы</div>
<?php endif ?>


<div class="WCaption">Что вам интересно в данный момент?</div>
<div class="note_type" value=""><b>Все</b></div>
<?php

$IBASE = new Interests();
foreach(Enum::ConceptionsWords() as $key=>$val)
echo '<div class="note_type" value="'.$key.'">'.$val.(isset($_GET['type']) && $key==(int)$_GET['type'] ? 
'<div style="float:right">'.HTML::image_tag("qmex_img/UI/checker.png",array('width'=>'10px','height'=>'10px')).'</div>' : '' ).'</div>';

?>

<br>

<div class="WCaption">Какая тема вас интересует?</div>
<div class="note_spec" onclick="checkSpec(0);"><a onclick="return false;"><b>Любая тема</b></a></div>

<?php

$IBASE = new Interests();
foreach($IBASE->SelectIBASE() as $val)
{
	$key = $IBASE->SelectKey($val);
	$val = $IBASE->SelectName($key);
	if( ($isfavorite && in_array($key,$my_interests_keys)) || !$isfavorite) 
	echo '<div class="note_spec"><a onclick="checkSpec('.$key.');return false;">'.$val.(isset($_GET['s']) && $key==(int)$_GET['s'] ? 
'<div style="float:right">'.HTML::image_tag("qmex_img/UI/checker.png",array('width'=>'10px','height'=>'10px')).'</div>' : '' ).'</a></div>';
	}

?>
</td>
</table>

<script language="javascript">

//////////////////////////////////////////////////////////////
function sb_date()
{
	var params = QUERY.splitGetQuery(location.href);
	params["sort"] = 'date';
	params["page"] = '1';
	location.href="hubster?"+getQueryString(params);
	}
	
function sb_rait()
{
	var params = QUERY.splitGetQuery(location.href);
	params["sort"] = 'rait';
	params["page"] = '1';
	location.href="hubster?"+getQueryString(params);
	}
	
function sb_update()
{
	var params = QUERY.splitGetQuery(location.href);
	params["sort"] = 'update';
	params["page"] = '1';
	location.href="hubster?"+getQueryString(params);
	}
	
function sb_random()
{
	var params = QUERY.splitGetQuery(location.href);
	params["sort"] = 'random';
	params["page"] = '1';
	location.href="hubster?"+getQueryString(params);
	}
//////////////////////////////////////////////////////////////

function search()
{
	var params = QUERY.splitGetQuery(location.href);
	params["kw"] = $("#kw-search").val();
	params["page"] = '1';
	params["type"] = '';
	params["s"] = '';
	location.href="hubster?"+QUERY.completeQueryFromHash(params,true);
	}

function checkSpec(key)
{
	key = parseInt(key);
	if(!key) key='';
	var params = QUERY.splitGetQuery(location.href);
	params["s"] = key;
	params["page"] = '1';
	location.href="hubster?"+QUERY.completeQueryFromHash(params,true);
	}

$(function(){
	
	COMMON.CreateFastScroll();
	<?php if(!isset($NOT_FOUND)):?>
	COMMON.AddPagedNav("#public-nav");
	<?php endif; ?>
	
	$(".note_type").on('click',function(){ 
		var params = QUERY.splitGetQuery(location.href);
		params["type"] = $(this).attr("value");
		params["page"] = '1';
		location.href="hubster?"+QUERY.completeQueryFromHash(params,true);
	 });
		
	$("#kw-search").keydown(function(e){
		if(e.keyCode==13) search();
		});
		
	$(".kw-box").on('change', function(){
		var val = $(this).attr('n');
		var line = $("#kw-search").val();
		line = (!this.checked) ? line.replace(" "+val,"") : line+" "+val;
		$("#kw-search").val( line )
		});
		
	$("#only_fav").on('click',function(){
		var params = QUERY.splitGetQuery(location.href);
		params["oi"] = this.checked ? 1 : 0;
		params["page"] = '1';
		location.href="hubster?"+QUERY.completeQueryFromHash(params,true);
		});
		
	$("#search-img").width($("#search-button-bar").width()*0.5+"px")	
	$("#search-bar").on('click', function(){ $(this).css({'overflow':'visible','max-height':'none'}) })
	
	/*
	$(window).scroll(function(){
		
		var scroll_pos = window.pageYOffset;
		var max_scroll =(document.documentElement.scrollHeight - document.documentElement.clientHeight) 
		if(scroll_pos>=max_scroll) {
			var last_tagged = parseInt($(".public-note:last").attr("tag"));
			var ret = SendDynamicMsg(['loadPublic','params'],[last_tagged,location.href+"&"],null,'handlers/loader.php',"GET");
			$("#QPublic").append(ret);
			//COMMON.IFrameTest(".public-frame");
			}
		
		});
*/
	});

</script>