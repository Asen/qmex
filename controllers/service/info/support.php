<style>

.support-field{ 
	padding:10px; 
	border-radius:3px; 
	border: 2px solid #069; 
	color:#06F; 
	font-weight:bold ;
	font-family:Arial, Helvetica, sans-serif;
}	
.support-field:active{ 
	border: 2px solid #09F ;
}
.transmission-succeed{
	color: #000;
	background-color:#9FC;
	padding:10px;
}
</style>

<form method="post">
<div class="WCaption">Помощь и поддержка</div>


<?php

if(isset($_POST['email'])){
	
	$email = Security::isAuthorized() ? $_SESSION['email'] : $_POST['email'];
	$text  = $_POST['text'];
	$name = Security::isAuthorized() ? $_SESSION['login'] : 'Unknown';
	
	$headers = "\r\nFrom: $name<$email>\r\n";
	
	mail(SysInfo::EMAIL, "qMex-Support", $text, $headers);
	echo '<div class="transmission-succeed">Сообщение отправлено! Ожидайте ответное письмо на указанный вами Email.</div>';
	
	}

?>


<div style="padding:100px 10px 0 20px;">

<div style="padding:0 0 25px 0; color:#CCC">Отправить сообщение в службу поддержки : </div>

<div style="padding:0 10px 0 0;">
<input type="text" name="email" placeholder="ваш Email" value="<?php if(Security::isAuthorized()) echo $_SESSION['email']  ?>" 
class="support-field" <?php if(Security::isAuthorized()) echo 'readonly' ?> style="width:60%" />
</div>

<div style="padding:10px 10px 0 0;">
<textarea class="support-field" name="text" style="width:60%" rows="5" placeholder="Текст сообщения"></textarea>
</div>

<input type='submit' class="qButton" value="Отправить" />
</div>
</form>