<style type="text/css">

@font-face{
	font-family:About;
	src:url(/views/main/fonts/q.ttf);
	}

*{
	/*font-size:0.8em;*/ 
	color:#AAA;
	}

html, body{
	background-color:#000;
	background-image:url(qmex_img/bg.png);
	background-attachment:fixed;
	background-repeat:repeat;
	font-family: 'lucida grande',tahoma,verdana,arial,sans-serif;
	font-family: About;
	}

#qmex_content_block{ background:rgba(0,0,20,0.8); }

#register-button{
display:none;	
}
#label{ 
border:#069 dotted 0px; 
color:#FFF; 
padding:7px; 
font-size:18px; 
border-radius: 0px; 
background:url(/qmex_img/presentation/grate.png) ;
font-family: Arial, Helvetica, sans-serif;
}
#secondarylabel{ 
font-size:16px; 
font-weight:bold;
color:#69C; 
font-family: Arial, Helvetica, sans-serif;
}
#third-party-label{
color:#69C; 
}

.present-icon{ float:left; padding:8px }

.expression{
	font-size:11pt;
	color:#DDD;
	}
	
.subexpress{
	color:#AAA;
	font-size:13px;
	}
	
.subexpress table td{ vertical-align:top; }

.captionexpress{
	font-weight:bold; 
	border-bottom:#777 1px solid; 
	padding-bottom:5px;
	color:#CEE6FF;
	font-family:Arial, Helvetica, sans-serif
	}
	
.examplecaption{ color:#DDD; font-size:13px;  }
.examplebody{ color:#999; font-size:12px; margin-bottom:20px; padding:5px 0 0 10px}
	
.present-feature{ float:left; padding:8px }

</style>

<div class='WContent' id="qAbout" style="margin:50px auto">

<table cellspacing="10px" class="full-width" border="0">
<tr>
<td width="60%">
    <div id="secondarylabel">
    С помощью qMex у вас появится уникальная возможность выразить все свои ценные навыки, 
    знания и достижения, привлечь к себе новых людей и  новые возможности для достижения целей!
    </div>
</td>
<td width="40%">
    <div id="label">
    <span style="font-weight:bold; color:#C1E0FF;">qMex</span> 
    поможет вам достичь поставленных целей и воплотить свои намерения.
    </div>
</td>
</tr>
</table>

<br>

<!--

<span style="font-weight:bold; color:#CEE6FF">qMex</span> - это мультифункциональное средство для достижения поставленных целей и реализаций намерений.

схема достаточно проста:  вы создаете профиль, демонстрируете все, что знаете и умеете, привлекая таким образом к себе людей
// это место, где собираются люди, обладающие полезными и интересными знаниями и навыками в самых разных сферах деятельности.
С помощью qMex у вас появится уникальная возможность показать все свои ценные навыки, знания и достижения, привлечь к себе новых людей и  новые возможности для достижения целей! 
qMex создан для того, чтобы помогать людям реализовывать идеи и намерения в жизни путем привлечения своими умениями и навыками других людей. В qMex каждый человек - это новая возможность, потому как каждый обладает полезными знаниями, умениями и навыками.
<strong>Так расскажите же и о своих интересах, увлечениях и навыках, привлекайте к своей личности все больше и больше новых людей и сопутствующих возможностей для достижения целей!</strong>
-->

<div id="third-party-label">
Здесь собираются активные люди, обладающие полезными знаниями и умениями в самых разных сферах деятельности и открытые для взаимодействия.
Присоединяйтесь к активному обществу и начинайте выражать себя прямо сейчас.
</div>

<br> 

<div id='register-button'>
<?php if(!isset($_SESSION["entered"])): ?>
    <div style="padding-top:10px; padding-bottom:16px; text-align:center">
    <button class="qButton" onclick="makeRegAnim(this)" style="width:200px; height:30px">
    Присоединиться <img src="qmex_img/arrow_right.png" width="7px" height="7px">
    </button>
    </div><br>
<?php endif; ?>
</div>

<div class="captionexpress">Какие возможности предоставляет qMex?</div>

<br>

<div style="margin:0 0 0 25px; width:90%">

<li>
<div class="expression">-Hubster-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/hubster.png" width="30px" class='present-feature'></td>
<td>Расскажите людям о том, чем вы занимаетесь, какие проблемы перед вами стоят и как их можно преодолеть.
Вы можете рассказать о своих проектах, планах, намерениях и привлечь к себе новых людей, заинтересованных вашей деятельностью.</td>
</tr>
</table>
</div>
<br>
</li>

<li>
<div class="expression">-Swop-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/swop.png" width="30px" class='present-feature'></td>
<td>Создавайте возможности и предложения для других, выражайте свои желания и потребности для достижения целей!</td>
</tr>
</table>
</div>
<br>
</li>

<li>
<div class="expression">-Обсуждения и конференции-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/talks.png" width="30px" class='present-feature'></td>
<td>Участвуйте в интересных обсуждениях и конференциях, отвечайте на профессиональные вопросы и задавайте новые! 
Демонстрируйте свои знания и свой опыт в тех областях, которые интересны вам, и развивайте тем самым свой статус.</td>
</tr>
</table>
</div>
<br>
</li>

<li>
<div class="expression">-Множество различных интересов и увлечений-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/interests.png" width="30px" class='present-feature'></td>
<td>Чем увлекаются другие люди? 
Вполне вероятно, вы сможете найти новых людей для общения, чьи интересы и увлечения во многом совпадают с вашими.</td>
</tr>
</table>
</div>
<br>
</li>

<li>
<div class="expression">-Актуальные события и достижения-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/actuals.png" width="30px" class='present-feature'></td>
<td>Находите интересные события, присоединяйтесь к людям и действуйте вместе!
Создавайте собственные события, помогающие организовывать множество людей на определенные действия, 
и действуйте во благо общих интересов!</td>
</tr>
</table>
</div>
<br>
</li>

<li>
<div class="expression">-Поиск нужных и полезных людей-</div>
<div class="subexpress">
<table class="full-width">
<tr>
<td><img src="/qmex_img/presentation/search.png" width="30px" class='present-feature'></td>
<td>Вы можете быстро и точно отыскать полезных людей, которые занимаются определенной деятельностью, 
которые находятся недалеко от вас и которые были бы не против с вами взаимодействовать во благо общих интересов.</td>
</tr>
</table>
</div>
<br>
</li>


</div>

<br>

<div class="captionexpress">Чем поможет qMex?</div><br>
<div style="margin:0 0 0 40px; ">
<table class="full-width">
<tr>
<td width="70%">
<ul class="listing">
<li>Рассказать людям о своей текущей и глобальной деятельности.</li>
<li>Представить свои умения, знания, навыки, интересы и достижения.</li>
<li>Найти полезных единомышленников для совместной деятельности.</li>
<li>Почерпнуть новые идеи и начать делать то, что действительно интересно.</li>
</ul>
</td>
<td width="30%" align="center"><div id='why'><img src="/qmex_img/presentation/why.png" height="70px"></div></td>
</tr>
</table>
</div>

<br><br>

<div class="captionexpress">Например...</div><br>
<div style="margin:0 0 0 40px; ">

<div class="examplecaption">Все знакомые разъехались и не с кем завтра поиграть в футбол?</div>
<div class="examplebody">
Не беда - qMex поможет это исправить! Всего лишь создайте новое событие, актуальное для вашего местоположения, и желающие найдутся сами!
</div>

<div class="examplecaption">Среди знакомых нет таких, кто бы разделял ваши интересы и увлечения?</div>
<div class="examplebody">
В таком случае достаточно просто ввести в поиске свои интересы, выбрать любого подходящего человека и открыть с ним диалог.
</div>

<div class="examplecaption">Есть предложения для общества? Или, может, вы в поисках предложений?</div>
<div class="examplebody">
Можно создать новый Своп, описывающий ваше желание или предложение, и вы сразу же сможете узнать, кому это было бы интересно и кто готов помочь.
</div>

<div class="examplecaption">Создаете свою компанию и вам необходимы заинтересованные люди?</div>
<div class="examplebody">
Просто создайте новый Хаб о своей деятельности. Заинтересованные люди сразу же получат уведомление об этом, и начнут с вами взаимодействовать!
</div>

<div class="examplecaption">Разрабатываете что-то новое?</div>
<div class="examplebody">
Тогда почему бы не рассказать об этом обществу? Расскажите об этом в своем профиле, установите текущую цель. И все будут уведомлены о вашей текущей деятельности! А, может, уже кто-то другой начал разработку похожего продукта. В таком случае почему бы вам не объединиться и не работать вместе?
</div>

<br>
<div class="examplecaption" style="text-align:center; color:white">...И многое другое в qMex!</div>

</div>

<br><br>

<div style="border:#CCC dashed 1px; padding:15px;">
В данное время общество <span style="font-weight:bold">qMex</span> только начинает пополняться активными людьми.<br>Поэтому мы просим, чтобы вы, по возможности, приглашали в <span style="font-weight:bold">qMex</span> своих знакомых людей. <strong>
<br>Вместе</strong> мы сделаем qMex таким местом, где каждая цель и любое намерение станут достижимы. <strong>Спасибо вам!</strong>   
</div>  

</div>

<script>

$(function(){
	$("#qcontent").css({'font-size':"13px"});
	Animation();
	});
	
function MakeAnim(object, animation, time)
{
	window.setTimeout(function(){ $(object).addClass(animation) }, time);
	}
	
function Animation()
{
	MakeAnim( "#label", 'rubberBand animated' , 0 );
	MakeAnim( "#why", 'flip animated infinite', 0 );
	MakeAnim( "#register-button", 'wobble animated', 1000 );
	window.setTimeout(function(){ $("#register-button").fadeIn(1000); }, 1000);
	}

function makeRegAnim(o)
{
	MakeAnim( o, 'hinge animated', 0 );
	window.setTimeout(function(){ location.href='/register?step=passwd' }, 2000);
	}

</script>
