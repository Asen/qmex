﻿<div class='BigCaption'>Текущее время в системе qMex (qMex-Time):</div>

<div style='text-align:center; border: 1px dotted #09F'>
<div id='time' style="padding:50px;">
<div id='qmex-time' style="color:#09F; font-size:100px"></div>
<div id='qmex-monthday' style="color:#069; font-size:20px;"></div>
</div>
</div>

<div style="background-color:#069; border-radius:3px; color:white; padding:5px; font-size:11px; font-family:Arial, Helvetica, sans-serif">
<table border="0" cellspacing="5px">
<tr>
<td><img src="/qmex_img/UI/qtime.png" width="30" height="30" style="padding:0px;"></td>
<td>
<strong>qMex-Time</strong> - универсальное время, действующее в системе qMex, созданное для удобства согласования пользоватей во времени.
<br>qMex-Time не зависит от местоположения и от текущего часового пояса - это время, являющееся одинаковым в данный момент для всех пользователей.
</td>
</tr>
</table>
</div>

<script>

$(function(){
	
	window.setTimeout(Time, 0);
	window.setInterval(Time, 1000);
	
	});

function Time()
{
	$time = SendDynamicMsg(['qmex-time'],[1],null,'handlers/ask.php','GET','json');
	$("#qmex-monthday").text( parseInt($time['day'])+' '+$time['month-name']+", "+$time['year']+" год " );
	$("#qmex-time").text( $time['hour']+' : '+$time['minute']+" : "+$time['second'] );
	}

</script>