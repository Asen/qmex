<?php
Security::checkAccess();
?>

<?php
$events = Event::ExtractUserEvents($_SESSION["id"]);
$event_count = count($events);
?>

<style>
.event{ background-color:#FFF; padding:0px}
</style>

<div class="WCaption">События с момента моего последнего просмотра</div>

<table cellpadding="0" cellspacing="0px" border=0 style="width:100%" id='event-list'>
<tr>
<td style='padding-bottom:7px; padding-top:15px' colspan="2">
<span style="font-size:12px">Всего событий за прошедшее время : </span><b><u><?php echo $event_count; ?></u></b>
<?php if($event_count>0):?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<button class="qButton" onclick="RemoveAll()"><img src="qmex_img/UI/checker-active.png" width="15px" height="15px">&nbsp;Удалить все</button>
<?php endif; ?>
</td>
</tr>
<th align="center" style='padding:5px; background-color:#82C0FF; color:#000'>Подтвердить</th>
<th align="center" style='padding:5px; background-color:#2B95FF; color:#FFF'>Событие</th>
<?php

if($event_count>0){
	$events = Event::genEventsList($events);
	foreach($events as $id=>$event)
	echo '<tr id="event-slot-'.$id.'">
		  <td width="20%" align="center"><img src="qmex_img/UI/checker.png" class="ui-event-check-img" tag="'.$id.'"></td>
		  <td width="80%"><div class="event" id="event-'.$id.'">'.$event.'</div></td>
		  </tr>';
	}
else echo '<tr><td colspan="2"><div style="padding:25px; font-weight:bold;">За последнее время активности не наблюдалось.</div></td></tr>';

?>
</table>

<script>

$(function(){
	$(".ui-event-check-img").on('mouseover', function(){$(this).attr('src','qmex_img/UI/checker-active.png')});
	$(".ui-event-check-img").on('mouseout', function(){$(this).attr('src','qmex_img/UI/checker.png')});
	var sz = $("#event-list").width() * 0.07; 
	$(".ui-event-check-img").width(sz+"px");
	$(".ui-event-check-img").height(sz+"px");
	$(".ui-event-check-img").click(function(){
		var id = $(this).attr("tag");
		res = SendDynamicMsg(['remove_event','id'],[1,id],null,'handlers/uiHandler.php','POST');
		if(res==1)
		{
		$("#event-"+id).slideUp(500);
		$(this).hide(300);
		window.setTimeout(function(){ $("#event-slot-"+id).remove(); },490);
		} else show_wnd('Возникла ошибка.',100,true,2.5,'#FF5555',false);
		});
	});

function RemoveAll()
{
	WINDOW.Confirm("Удалить ВСЕ уведомления?", function(){
	res = SendDynamicMsg(['remove_event_all'],[1],null,'handlers/uiHandler.php','POST');
	if(res==1) $("#event-list").fadeOut(500); else show_wnd('Возникла ошибка.',100,true,2.5,'#FF5555',false);
	});
}

</script>