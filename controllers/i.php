<div class="WCaption">Всевозможные интересы в системе qMex</div>
<div class="WContent">

<style>
.interest-picture{ border:1px solid #CCC }
.keyword-href{ font-size:12px; text-decoration:underline; color:#039; cursor:pointer;  font-weight:bold }
.keyword-href:hover{ text-decoration:none; color:#09F }
.main-key{ text-decoration:underline; color:#069; }
.main-key:hover{ text-decoration:none; color:#09F }
</style>

<table class="full-width" cellspacing="10px" border="0">
<?php

$db = new db();

$db->query("SELECT DISTINCT key_interest FROM qmex_user_interests");
$interests = array();
$IBASE = new Interests();
while($key = $db->one(0))
	$interests[$key] = array($IBASE->SelectName($key), $IBASE->SelectImage($key));
	
asort($interests);

foreach($interests as $key=>$info)
if(true): ?>

<tr>
<td width="20px"><div class="qButton ui_expand_i" id="int_<?php echo $key ?>">+</div></td>
<td width="50px"><img src="/qmex_img/inter/<?php echo $info[1] ?>" width="30px" class="interest-picture"></td>
<td>
<div id="ui_single_i-<?php echo $key ?>">
<a class="main-key" href="search#keyinterest=<?php echo $key ?>"><?php echo $info[0] ?></a>
</div>
</td>
</tr><tr>
<td colspan="2"></td>
<td>
<div class='ui_kw_i' id='ui_kw_i-<?php echo $key ?>'></div>
</td></tr>

<?php endif ?>
</table>
</div>

<script language="javascript">
$(document).ready(function(){
	$('.ui_expand_i').toggle(function(){showing(this)}, function(){hiding(this)});
	});
	
function showing(obj)
{
	var key = $(obj).attr("id").split("_")[1];
	$(obj).text('-');
	$.ajax({
		type:"GET",
		url:'handlers/uiHandler.php',
		data:"get_i=1&key_i="+key,
		error:function(e,err){alert(err)},
		dataType:"json",
		success:function(words){
			$("#ui_kw_i-"+key).html("");
			var data = [];
			for(var word in words)
				data.push('<a class="keyword-href" href="search#keyinterest='+key+'&keywords='+words[word]+'">'+words[word]+'</a>');
			$("#ui_kw_i-"+key).append(data.join(', '))
			$("#ui_kw_i-"+key).stop(false,false,false);
			$("#ui_kw_i-"+key).slideDown(300);
			}
		});
	}
	
function hiding(obj)
{
	var id = $(obj).attr("id").split("_")[1];
	inter = $("#ui_single_i-"+id.toString()).text();
	$(obj).text('+');
	$("#ui_kw_i-"+id).stop(false,false,false);
	$("#ui_kw_i-"+id).slideUp(300);
	}
</script>