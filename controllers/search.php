<style>
.search-field{ padding:3px; box-sizing:border-box; -moz-box-sizing: border-box; width:100%; }
#search-text{ padding:10px 5px 10px 5px; background-color:#9CF; color:#006 }
</style>

<div class="WCaption">Поиск нужных людей</div>
<div class="WContent">

<table style="width:100%; margin:0 auto" cellspacing="5px" border="0">
<tr>

<td valign="top" style="width:70%; background-color:#FFF">
<div id="search-text"></div>
<div id="usersearch"></div>
</td>

<td valign="top" style="width:30%">

<div style="font-size:12px; background-color:#000; color:white; padding:3px">
Эта форма поможет вам найти,<br> кого нужно
</div>

<div id="searchPanel">

<table style="margin:0 auto; color:#FFF; font-size:0.8em" border="0" cellspacing="10px" id="concrete">
<tr>
<td>

<div>
<button class='qButton' style="width:100%; padding:5px">
<img src="qmex_img/search.png" height="21px">
</button>
</div>

<table cellpadding="0" cellspacing="5px" border="0" style="width:100%; border:1px dotted white; ">
<tr><td>Увлечения или специализации:</td></tr>
<tr><td>
<select name="keyinterest" style="color:#09F; font-size:12px" class="search-field search_bit" id="Q_con_key_interest">
<option value="" id="interest-default"><Любые></option>
<?php

$IBASE = new Interests();
foreach($IBASE->SelectIBASE() as $ITEM)
{
	$KEY = $IBASE->SelectKey($ITEM);
	$NAME = $IBASE->SelectName($KEY);
	echo '<option value="'.$KEY.'" id="interest-'.$KEY.'">'.$NAME.'</option>';
	}

?>
</select>
</td></tr>
<tr><td>
<textarea name="keywords" rows="5" class="search_bit full-width" id="Q_con_keywords_interest" 
style="font-family:Arial, Helvetica, sans-serif; padding:5px; font-size:10pt" 
placeholder="здесь можно указать, что именно вас интересует в выбранной деятельности или выбранном увлечении">
</textarea>
</td></tr>
</tr>
</table>

</td>
</tr>

<tr>
<td>
qID (Логин) :
</td>
</tr>
<tr>
<td>
<input type="text" name="qid" class="search-field search_bit" id="Q_con_login">
</td>
</tr>

<tr>
<td>
Деятельность:
</td>
</tr>
<tr>
<td>
<input type="text" name="biz" class="search-field search_bit" id="Q_con_biz">
</td>
</tr>

<tr>
<td>
Имя:
</td>
</tr>
<tr>
<td>
<input type="text" name="name" class="search-field search_bit" id="Q_con_name">
</td>
</tr>

<tr>
<td>
Фамилия:
</td>
</tr>
<tr>
<td>
<input type="text" name="sename" class="search-field search_bit" id="Q_con_sename">
</td>
</tr>

<tr>
<td>
Возраст:
</td>
</tr>
<tr>
<td>
от <input type="text" name="f_age" class="search-field search_bit" style="width:30%" id="Q_first_age"> - до <input type="text" name="to_age" class="search-field search_bit" style="width:30%" id="Q_second_age"> 
</td>
</tr>

<tr>
<td>
Пол:
</td>
</tr>
<tr>
<td>
<input type="radio" name="sex" id="Q_con_sex search_bit" class="search_bit" value="1"> Мужской 
</td>
</tr>
<tr>
<td>
<input type="radio" name="sex" id="Q_con_sex search_bit" class="search_bit" value="2"> Женский
</td>
</tr>
<tr>
<td>
<input type="radio" name="sex" id="Q_con_sex search_bit" class="search_bit" value=""> Не важно
</td>
</tr>

<!--
<tr>
<td>
Страна:
</td>
</tr>
<tr>
<td>
<input type="text" name="country" class="qmex_former search_bit" id="Q_con_country">
</td>
</tr>

<tr>
<td>
Город:
</td>
</tr>
<tr>
<td>
<input type="text" name="city" class="qmex_former search_bit" id="Q_con_city">
</td>
</tr>
-->


<tr>
<td>
<div style="border-top: 1px solid #09F; padding-top:5px;">
<input type="checkbox" class='search_bit' id='geo' name='geo' value='1'> Показывать тех людей, которые находятся недалеко от моего текущего местоположения ( в одном городе и т.д )
</div>
</td>
</tr>

</table>
</div>

</td>
</tr>
</table>
<input type="hidden" id="hide" value="">
</div closed='WContent'>

<!-- JavaScript Code Section here -->
<script language="javascript" src='/tools/controls/search.js'></script>