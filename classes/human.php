<?php
class human{
		
	public static function getLoginById($id)
	{
		$db=new db();
		$db->query("SELECT Login FROM qmex_users WHERE ID=$id");
		return $db->one(0);
		}
		
	public static function getIdByLogin($login)
	{
		$db=new db();
		$login = mysql_real_escape_string($login);
		$db->query("SELECT ID FROM qmex_users WHERE Login='$login'");
		return $db->rowCount()>0 ? $db->one(0) : -1;
		}
		
	public static function getUserRating($id)
	{
		$db=new db();
		$db->query("SELECT Rating FROM qmex_users WHERE ID=$id");
		return $db->one(0);
		}
		
	public static function getUserRatingPosition($uid)
	{
		$db=new DB();
		$db->query("SELECT row_number FROM (
					SELECT ID, @row := @row+1 AS row_number FROM qmex_users JOIN(SELECT @row:=0) r 
					ORDER BY rating DESC ) AS c WHERE ID=$uid");
		return $db->one(0);
		}
	
	public static function ResolvePhoto($photo)
	{
		return (trim($photo)=='') ? 'qmex_img/qmex_human.png' : $photo;
		}
		
	public static function getUserPhoto($id)
	{
		$db=new db();
		$db->query("SELECT Photo FROM qmex_users WHERE ID=$id");
		return Human::ResolvePhoto($db->one(0));
		}
		
	public static function getUserInfo($id)
	{
		$db=new db();
		$db->query("SELECT Login, Photo FROM qmex_users WHERE ID=$id");
		$data = $db->one();
		$info = array();
		
		$info['login'] = $data[0];
		$info['pic'] = Human::ResolvePhoto($data[1]);
		
		return $info;
		}
		
	public static function getUserTarget($id)
	{
		$db=new db();
		$db->query("SELECT Target FROM qmex_users WHERE ID=$id");
		return $db->one(0);
		}
		
	public static function getInterestingPeople($id)
	{
		$db=new db();
		$db->query("SELECT ID2 FROM qmex_user_bookmarks WHERE ID1=$id");
		$like_users = array();
		while($like_user = $db->one(0)) array_push($like_users,$like_user);
		return $like_users;
		}
		
	public static function getGeoInfo($id)
	{
		$db=new DB();
		$db->query("SELECT Country, City, latitude, longitude FROM qmex_users WHERE ID=$id");
		$data = $db->one();
		
		return array('country'=>$data[0], 'city'=>$data[1], 'latitude'=>$data[2], 'longitude'=>$data[3]);
		}
		
	public static function getCreatedConferenceCount($id)
	{
		$db=new DB();
		$db->query("SELECT COUNT(*) FROM qmex_conferences WHERE creator=$id");
		return $db->one(0);
		}
		
	public static function getParticipationConferences($id)
	{
		$db=new DB();
		$db->query("SELECT id FROM qmex_conferences_opinions WHERE creator=$id GROUP BY conf_id");
		return $db->rowCount();
		}
		
	public static function getSuccessfulEvents($id)
	{
		$db=new DB();
		$db->query("SELECT COUNT(*) FROM qmex_voites WHERE Login=$id AND Type='dev_thank'");
		return $db->one(0);
		}
		
	public static function defineDefaultSpec($id)
	{
		$db=new DB();
		$keys = array();
		$db->query("SELECT key_interest FROM qmex_user_interests WHERE Login=$id");
		while($i = $db->one(0))
			array_push($keys, $i);
			
		return Interests::DefineSpecialization($keys);
			
		}
	
	public static function getUserCash($user, $DBH=NULL)
	{
		if($DBH == NULL) $DBH = new DB();	
		$DBH->query("SELECT cash FROM qmex_users WHERE ID=$user");
		return $DBH->one(0);
		}
	
	public static function addRating($user, $add, $DBH=NULL)
	{
		if($DBH == NULL) $DBH = new DB();	
		$DBH->query("UPDATE qmex_users SET rating=rating+$add, cash=cash+$add WHERE ID=$user");		
		}
		
	public static function takeCash($user, $number, $DBH=NULL)
	{
		if($DBH == NULL) $DBH = new DB();	
		$DBH->query("UPDATE qmex_users SET cash=cash-$number WHERE ID=$user");		
		}
	
		
	}
?>