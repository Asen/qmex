<?php

class Smarty
{	
	////////////////////////////////////////////////
	public static $DATA;
	////////////////////////////////////////////////
			
	public static function StartCreation($display_content, $data, $view)
	{
		Smarty::$DATA = clone $data;
		Smarty::$DATA['current-page-content'] = $display_content;
				
		require_once 'views/intro-php.php';		
		include('views/'.$view.'/template.php');	
		}	
	
	//////////////////////// Template Methods ////////////////////////
	
	public static function getExpression()
	{
		$data = explode(SysInfo::DATA_EXPR_DELIMITER, file_get_contents('data/qmex/expressions.txt'));
		echo $data[ rand() % sizeof($data) ];
		}
		
	public static function getRegistrationCaption()
	{
		$url = $_SERVER['REQUEST_URI'];
		$should_display = trim($url)!='/' && !mb_strstr($url, "/register");
		if(!Security::isAuthorized() && $should_display ): ?>
        
        	<a href='/register' class="underlined" style="color:#00F">
        	<div id="designed-reg-sign">
            Присоединиться к сообществу qMex >> 
            </div>
            </a>
        
        <? endif;
		}
		
	public static function getDescription()
	{
		echo 'qMex предоставляет уникальную возможность выразить свои ценные навыки, знания, 
			  достижения и привлечь новые возможности для достижения целей. Выражай себя с помощью qMex!';
		}
		
	public static function getTitle()
	{
		echo Security::isAuthorized() ? 
			"qMex | ".$_SESSION["login"] : 
			"qMex | "."Добро пожаловать в qMex!";
		}
		
		
	public static function getUserBar()
	{
		include Security::isAuthorized() ? 
			'views/fragments/loginboxes/logined.php' : 
			'views/fragments/loginboxes/unlogined.php';
		}
		
		
	public static function getServiceMessage()
	{
		if(isset(Smarty::$DATA["msg"])) 
			echo Smarty::$DATA["msg"];
		}
		
		
	public static function getSearchBar()
	{
		include "views/fragments/search/searcher.php";
		}
		
		
	public static function getMainMenu()
	{
		if(true): ?>
        
        <a href="/i" class="non-underlined">
        <div class="ui_upper_part_menu_item qHint" tag="Все интересы и увлечения людей">Интересы и занятия</div>
        </a>
        <a href="/developments" class="non-underlined">
        <div class="ui_upper_part_menu_item">Актуальные события</div>
        </a>
		<a href="/talks" class="non-underlined">
        <div class="ui_upper_part_menu_item">Обсуждения и конференции</div>
        </a>
		<a href="/hubster?start" class="non-underlined">
        <div class="ui_upper_part_menu_item qHint" tag="Концентратор полезной информации">Хабстер</div>
        </a>
        <!--FROZEN <a href="/teams" class="non-underlined"><div class="ui_upper_part_menu_item">Команды</div></a> FROZEN-->
		<!--FROZEN <a href="/quer" class="non-underlined"><div class="ui_upper_part_menu_item">Quer</div></a> FROZEN-->
		<a href="/swop" class="non-underlined">
        <div class="ui_upper_part_menu_item qHint" tag="Возможности и предложения">Своп</div>
        </a>
        
        <?php endif;
		}
			
				
	public static function getMainContent()
	{
		include Smarty::$DATA['current-page-content'];
		}
		
		
	public static function getAuthorizedSign()
	{
		echo Security::isAuthorized() ? 
			"<div id='authorized-user-sign'></div>" : 
			'';
		}
		
}
?>