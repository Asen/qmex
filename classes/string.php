<?php

class String{
	
	public static function mb_ucfirst($text)
	{
		return mb_strtoupper(mb_substr($text, 0, 1)).mb_substr($text, 1);
		}
	
	public static function mb_str_split($str)
    {
        preg_match_all('#.{1}#uis', $str, $out);
        return $out[0];
    }
	
	public static function ReplaceLinks($text, $color='#B0D8FF')
	{
		return preg_replace('#((?:http|https):\/\/[^\s]+)#i',
						    '<a href="$1" class="underlined" style="color:'.$color.'" target="_blank">$1</a>', $text);
		}
		
	public static function title($text)
	{
		return String::mb_ucfirst(mb_strtolower($text));
		}
	
	}

?>