<?php

class Logs
{	
	private static $error = "logs/errors.txt";
	private static $inputs = "logs/inputs.txt";
	private static $logs = "logs/logs.txt";
	
	public static function getVisitorInfo()
	{
		$datetime = date("d/m/Y H:i:s");
		$useragent = @$_SERVER['HTTP_USER_AGENT'];
		$ip = @$_SERVER['REMOTE_ADDR'];
		$port = @$_SERVER['REMOTE_PORT'];
		$req_url = @$_SERVER['PHP_SELF'];
		$params = @$_SERVER['QUERY_STRING'];
		$lang = @$_SERVER['HTTP_ACCEPT_LANGUAGE']."  |  ".@$_SERVER['HTTP_ACCEPT_ENCODING']."  |  ".@$_SERVER['HTTP_ACCEPT_CHARSET'];
		$referer = @$_SERVER['HTTP_REFERER'];
		$info = "[ $datetime ] \r\nIP: $ip:$port \r\nUser-Agent: $useragent \r\nEncoding / Language: $lang \r\nURI: $req_url$params \r\nReferer( From ): $referer"; 
		 
		return $info;
			
		}
		
	public static function getError($error)
	{
		$date = date("d/m/Y H:i:s");
		$ip = $_SERVER['REMOTE_ADDR'];
		$info = "-------------------------\r\n[ $date ]   $ip\r\n$error\r\n";
		return $info;
		}
		
	
	public static function writeLogs($file,$info)
	{
		if($file==C::LOG_ERROR) $file=Logs::$error;else
		if($file==C::LOG_INPUTS) $file=Logs::$inputs;else
		if($file==C::LOG_LOGS) $file=Logs::$logs;
		try
		{
			$f = @fopen($file,"a+");
			fwrite($f,$info);
			fwrite($f,"\r\n\r\n##################\r\n\r\n");
			fclose($f);
		}catch(Exception $e){ fclose($f); }
		}
	

	
	}

?>