<?php

class Router
{
	private $provider = NULL;
	private $path = "";
	private $bits = array();
	private static $router = NULL;
	
	function __construct($registry)
	{
		$this->provider = $registry;
		if(Router::$router==NULL) Router::$router = $this; else return Router::$router;
		}
	
	private function Delegate($start_path = 'controllers')
	{
		function filter($bit){
			return (trim($bit)=='') ? true:false;
			}
			
		$this->path = (isset($_GET["route"])) ? $start_path.'/'.$_GET["route"] : $start_path;
		$path_pieces = explode("/", mb_strtolower(trim($this->path)));
		array_filter($path_pieces,"filter");
		$this->path = implode("/",$path_pieces);
		
		if(is_file($this->path.'.php')) 
			$this->path .= ".php"; else
		if(is_dir($this->path) && is_file($this->path.'/main.php'))  
			$this->path .= "/main.php"; else
		$this->path = 'controllers/error.php';
		
		if(mb_strstr($this->path,$start_path.'/service')) 
			$this->path = 'controllers/denied.php';

		
		/*$tok = strtok($path,'/');
		while($tok)
		{
			$t = mb_strpos($tok,'.');
			if($t>0) $tok=mb_substr($tok,0,$t);
			array_push($this->bits,mb_strtolower(trim($tok)));
			$tok = strtok('/');
			}
			
			array_filter($this->bits,"filter");
			
			$this->path=implode('/',$this->bits);
			if(is_file($this->path.'.php')) $this->path=$this->path.'.php'; else
			if(is_dir($this->path)) $this->path=$this->path.'/main.php'; else
			$this->path='controllers/error.php';
			
			if(mb_strstr($this->path,$start_path.'/service')) header('Location: /');
			*/
		}
		
		private function PrintPage()
		{
			Smarty::StartCreation($this->path, $this->provider, 'main');
			}
		
		public static function Execute()
		{
			Router::$router->Delegate();
			Router::$router->PrintPage();
			}
		
	}

?>