<?php

class check{
	
	public static function Password($password)
	{
		$password_length = mb_strlen(trim(mb_strtolower($password)));
		if($password_length<5) 
			return array("result"=>"-", "msg"=>"Пароль ненадежен!");
		if($password_length>40) 
			return array("result"=>"-", "msg"=>"Пароль слишком длинный!");
		if(!is_similar_by_service_reg($password)) 
			return array("result"=>"-", "msg"=>"Введены недопустимые символы!"); 
		return array("result"=>"+", "msg"=>"");
		}
	
	public static function PasswordEquals($password1, $password2)
	{
		if($password1!=$password2) return array("result"=>"-", "msg"=>"Пароли не совпадают!");
		return array("result"=>"+", "msg"=>"");
		}
	
	}

?>