<?php

class achievement{
	
	#####################################
	const TYPE_DEV = 1;
	const TYPE_INTEREST = 2;
	#####################################
	
	public static function ShowBox($ach_id, $is_return = false)
	{
		$db = new DB();
		$db->query("SELECT description, interest, bonus, belong_to_dev FROM qmex_achievements WHERE id=$ach_id");
		$data = $db->one();
		
		$descr = $data[0];
		$interest = $data[1];
		$bonus = $data[2];
		$att_dev = $data[3];
		
		$IBASE = new Interests();
		$IBASE->SelectIBASE();
		$interest = $IBASE->SelectName($interest);
		unset($IBASE);
		
		ob_start();
		if(true): ?>
        
        <div class='ui-achievement-box'>
        <div class='ui-achievement-descr'>
        	<table class="full-width" cellspacing="3px">
            <tr>
            	<td valign="top"><img src="/qmex_img/UI/achievements/check-light.png" width="20px" ></td>
                <td><?php echo htmlspecialchars($descr) ?></td>
            </tr>
            </table>
		</div>
        <div class='ui-achievement-line'><?php echo $interest ?></div>    
        <div class='ui-achievement-bonus'>+<?php echo $bonus ?></div>
        </div>
        
        <? endif;
		$ACHIEVEMENT = ob_get_contents();
		ob_clean();
		
		if($is_return) return $ACHIEVEMENT; else echo $ACHIEVEMENT;
		
		}
		
		
		
	public static function ShowProfileVariantAchievement($ach_id, $is_return = false)
	{
		$db = new DB();
		$db->query("SELECT description, interest, bonus, belong_to_dev FROM qmex_achievements WHERE id=$ach_id");
		$data = $db->one();
		
		$descr = $data[0];
		$interest = $data[1];
		$bonus = $data[2];
		$att_dev = $data[3];
		
		$IBASE = new Interests();
		$IBASE->SelectIBASE();
		$interest = $IBASE->SelectName($interest);
		unset($IBASE);
		
		$href = $att_dev>-1 ? '/developments?view=show&id='.$att_dev : '#';
		
		ob_start();
		if(true): ?>
        
        <a href='<?php echo $href ?>' class="non-underlined">
        <div class='ui-achievement-box-profile'>
        <div class='ui-achievement-descr-profile'>
        	<table class="full-width" cellspacing="3px">
            <tr>
            	<td valign="top"><img src="/qmex_img/UI/achievements/check.png" width="40px" ></td>
                <td><?php echo htmlspecialchars($descr) ?></td>
            </tr>
            </table>
		</div>
        <div class='ui-achievement-line-profile'>
			<u><?php echo $interest ?></u>&nbsp;
            <?php if($bonus>0): ?>
            	<span class='ui-achievement-bonus-profile'>+<?php echo $bonus ?>
            <?php endif ?>
        </div>
        </div>
        </a>
        
        <? endif;
		$ACHIEVEMENT = ob_get_contents();
		ob_clean();
		
		if($is_return) return $ACHIEVEMENT; else echo $ACHIEVEMENT;
		
		}
	
		
	
	public static function getOwner($ach_id)
	{
		$DB = new DB();
		
		$DB->query("SELECT belong_to_user FROM qmex_achievements WHERE id=$ach_id");
		return $DB->one(0);
		
		}
	
	
	public static function CheckInterests($interests, $user_id)
	{
		$DB = NULL;
		$user_id = (int)$user_id;
		$ach_kind = Achievement::TYPE_INTEREST;
		
		foreach($interests as $key=>$value)
		{
			$interest = $value->interest_id;
			$rating = $value->rating;
			
			if($rating>=10){
				
				if($DB==NULL) $DB = new DB();
				
				$DB->query("SELECT COUNT(*) FROM qmex_achievements 
							WHERE belong_to_user=$user_id AND interest=$interest AND ach_kind=$ach_kind");
				if((int)$DB->one(0) > 0) continue;
				
				$description = 'Знание сферы';
				$concl = $DB->query("INSERT INTO qmex_achievements(description,interest,bonus,creator,belong_to_dev,belong_to_user,ach_kind) 
						   			 VALUES('$description',$interest,0,-1,-1,$user_id,$ach_kind)");
				
				$DB->query("SELECT id FROM qmex_achievements 
							WHERE belong_to_user=$user_id AND ach_kind=$ach_kind ORDER BY id DESC LIMIT 1");
				$last_id = $DB->one(0);
				
				if($concl) 
				Event::CreateEvent( Event::TYPE_INTEREST_ACHIEVEMENT, $user_id, $last_id, 0);
				
				} 
			
			}
		}
	
	
	}

?>