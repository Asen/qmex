<?php

class html{
	
	private static function completeParams($params)
	{
		$attributes = ""; 
		if($params) 
			foreach($params as $key=>$val) 
			$attributes .= $key."=\"".$val."\""; 
		return $attributes;
		}
	
	public static function image_tag($image,$params = NULL)
	{
		$attrs = HTML::completeParams($params);
		return "<img src='$image' $attrs />";
	}
	
	public static function link_to($href, $word, $params = NULL)
	{
		$attrs = HTML::completeParams($params);
		return "<a href='$href' class='underlined' $attrs >$word</a>";
		}
	
	public static function div_block($content, $params = NULL)
	{
		$attrs = HTML::completeParams($params);
		return "<div $attrs >$content</div>";
		}
	
	public static function user_link($uid)
	{
		return '/profile?id='.$uid;
		}
	
	}

?>