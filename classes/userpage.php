<?php

class userpage{
	
	private $login = NULL;
	public $data = NULL;
	
	public function __construct($user_login)
	{
		$this->login = mysql_real_escape_string($user_login);
		$this->data = array();
		}
	
	public function LoadUserInfo()
	{
		$login = $this->login;
		$query = "SELECT 
						Login, Email, 
						Name,Sename, 
						birth_year, birth_month, 
						birth_day, Sex, 
						Country, City, 
						photo, current_business, 
						About, Profession, 
						Site, Target, 
						caption, datetime_reg, 
						last_enter, rating, ID,
						account_checked  
				 FROM qmex_users WHERE Login = '$login'";
				  
		if(!Smarty::$DATA['db']->query($query) || Smarty::$DATA['db']->rowCount()==0) 
			return NULL;
		$data = Smarty::$DATA['db']->one();
		$id = $data[20];
		$is_iam = ($id==@$_SESSION['id']);
		
		$this->data['authorized_user'] = isset($_SESSION["id"]) ? $_SESSION["id"]:-1;
		$this->data['login'] = get_not_escaped($data[0]);
		$this->data['email'] = mb_strtolower($data[1]);
		$this->data['name'] = splitter(String::title(get_not_escaped($data[2])),30);
		$this->data["sename"] = splitter(String::title(get_not_escaped($data[3])),30);
		$this->data["by"] = $data[4];
		$this->data["bm"] = ($data[5]>0) ? Enum::$MONTHES[$data[5]-1]:'день';
		$this->data["bd"] = $data[6];
		$this->data["sex"] = $data[7];
		$this->data["country"] = splitter(String::title(get_not_escaped($data[8])),25);
		$this->data["city"] = splitter(String::title(get_not_escaped($data[9])),25);
		$this->data["avatar"] = htmlentities(Human::ResolvePhoto($data[10]),ENT_QUOTES,"UTF-8");
		$this->data["current_business"] = get_not_escaped(mb_strlen(trim($data[11]))==0 && $is_iam ? 
								"Несколько слов о своих текущих занятиях и делах" : splitter($data[11],50)); 
		$this->data["about"] = str_replace("\r\n","<br>", splitter(htmlspecialchars($data[12]),35));
		$this->data["interests"] = new interests();
		$this->data["interests"]->get_interests($id);
		$this->data["proof"] = splitter(get_not_escaped( $data[13] ),25);
		$this->data["site"] = get_not_escaped( $data[14] );
		$this->data["target"] = $data[15];
		$this->data["common_raiting"] = (float)$data[19];
		$this->data["caption"] = trim($data[16])=='' ? '?' : get_not_escaped($data[16]);
		$this->data["datetime_reg"] = $data[17];
		$this->data["last_enter"] = $data[18];
		$this->data["id"] = $id;
		$this->data["conference_count"] = Human::getCreatedConferenceCount($id);
		$this->data["conference_participant"] = Human::getParticipationConferences($id);
		$this->data['success_events'] = Human::getSuccessfulEvents($id);
		$this->data['account_checked'] = $data[21];
		
	    ////////////////////////////////////////////////////////////////////////////////////////////	
		 
		$query = "SELECT pagescheme FROM qmex_user_settings WHERE Login=".$id;
		Smarty::$DATA['db']->query($query);
		if(Smarty::$DATA['db']->rowCount()==0){
			Smarty::$DATA['db']->query("INSERT INTO qmex_user_settings(Login) VALUES($id)");
			Smarty::$DATA['db']->query($query);
			}
			
		$data = Smarty::$DATA['db']->one();			
		$this->data["SCHEME"] = $data[0];
			
		Smarty::$DATA['db']->query("SELECT (SELECT COUNT(*) FROM qmex_voites WHERE Login=$id AND Type='page' AND Voite=1) AS l,
								           (SELECT COUNT(*) FROM qmex_voites WHERE Login=$id AND Type='page' AND Voite=-1) AS dl");
		$data = Smarty::$DATA['db']->one();
		$this->data["likes"] = (int)$data[0];
		$this->data["dislikes"] = (int)$data[1];
		
		////////////////////////////////////////////////////////////////////////////////////////////
		
		return true;
	
		}




public function isOwner()
{
	$page_user = $this->data["id"];
	return (Security::isAuthorized() && $_SESSION['id'] == $page_user);
	}
		
		
		
		
public function createCaption()
{
##################################################################
$uid = $this->data["id"];
$login = $this->data["login"];
$caption = $this->data["caption"];
$last_enter = $this->data["last_enter"];
$account_checked = $this->data["account_checked"];
$tok = md5($login.$uid);
##################################################################
if(true): ?>
	
<div id="qUserPageHighCaption">
<table cellspacing="0" class='full-width'>
<tr><td>
<?php echo $login ?> → [<span style='border:#FFF 0px outset; padding:1px;'><?php echo $caption?></span>]
<?php if($this->isOwner()):?>
<img src="qmex_img/edit.png" class="qHint" tag=" Кто я " width="17px" style="padding-left:5px" id="ChangeCaption">
<?php endif ?>
</td>
<td align="right" style="font-size:10px; font-family:Tahoma, Geneva, sans-serif">
Последнее посещение<br>
<?php echo translate_date($last_enter) ?>
</td></tr>
</table>
</div>

<?php if($account_checked==0): ?>
<div id="Non-Checked_Account">
<?php if($this->isOwner()): ?>
	В системе qMex очень важны настоящие пользователи и их настоящие интересы.<br>
    Именно поэтому необходимо подтвердить свою действительность.
    <a href="/confirmacc?token=<?php echo $tok ?>&t=<?php echo urlencode(base64_encode(time()/100.0)) ?>" class='underlined'>
    Подтвердить сейчас?</a>
<?php else: ?>
	Действительность этого пользователя не подтверждена.
<?php endif; ?>
</div>
<?php endif; ?>
    
<?php endif;
}
		
	

public function createTarget()
{
if($this->isOwner()) return;
##################################################################
$target	= $this->data["target"];
##################################################################
if(true):?>

<?php 
$db = new DB();		
$db->query("SELECT Substance, Meaning FROM qmex_user_notes WHERE ID=".$target);
$is_exists = $db->rowCount()>0;
$data = $db->one();
$substance = mb_substr(strip_tags($data[0]),0,15);
$meaning = htmlspecialchars(mb_substr($data[1],0,15));
$intent_complex = (mb_strlen($meaning)>0) ? $meaning : $substance;
$intent = htmlspecialchars(trim(mb_substr($intent_complex,0,50)));
if( mb_strlen($intent_complex)>mb_strlen($intent) ) $intent.='...';	
$tag = ($target>0) ? '<span style=\'color:white\'>Текущая цель/намерение:</span><hr><div style=\'padding-top:10px; padding-bottom:10px\'>'.trim(mb_substr(strip_tags($data[0]),0,95)).'...</div>' : 'Основная цель не определена.';
?>

<div class="qHint" tag="<?php echo $tag; ?>" id="qUTarget">
<?php if($is_exists && $target>0):?>
	<a href="note?id=<?php echo $target ?>" class="qUTarget_exists">
	<img src="qmex_img/UI/hubster/idea-curr.png" style="float:left; padding-right:5px" width="15px"><?php echo $intent ?>
    </a>
<?php else: ?>
	<img src="qmex_img/UI/hubster/no-idea-curr.png" style="float:left; padding-right:5px" width="15px">
	<span class="qUTarget_none">На данный момент основная цель не определена.</span>
<?php endif?>
</div>
    
<?php endif;   
}
	
	
	
	
public function createUserPicture()
{
##################################################################
$avatar	= $this->data["avatar"];
##################################################################
if(true): ?>

<div id="qmex_ui_photo">
<img src='<?php echo $avatar?>' id="QuserPhoto" width="0px">
</div>

<?php endif;
	} 
	
	
	
	
public function createLikeSystem()
{
include('views/page-layouts/userpage_bits/like.php');
}
	
	
		
public function createConnectionSystem()
{
include('views/page-layouts/userpage_bits/connection.php');
}
	
	
	
public function createLoginLabel()
{
##################################################################
$login = $this->data["login"];
##################################################################
if(true):?>

<div style="padding:4px; padding-left:10px; text-align:left; border-bottom:#069 solid 2px;" >
<span id='main-login-caption'>
<?php 
$postfix = mb_strlen($login) > 29 ? '...' : '';
echo mb_substr($login,0,29).$postfix;
?>
</span>
</div>

<?php endif;
}
	
	
	
	
public function createCurrentBusiness()
{
##################################################################
$current_business = $this->data["current_business"];
##################################################################
if(true): ?>

<?php if($this->isOwner()): ?>
	<div style="float:right;">
	<img src="qmex_img/edit.png" width="20px" height="20px" class="qHint" tag="Изменить информацию о своих теущих делах" id="ChangePosition">
	</div>
<?php endif ?>
<div style="padding:4px; font-size:0.7em; width:430px; color:#069; font-style:italic"><?php echo $current_business ?></div>
<?php if(trim($current_business)!=''): ?> 
	<div style="border-top:#069 1px solid;"></div>
<?php endif ?>

<?php endif;
}




public function getInterestsInfo()
{
##################################################################
$id = $this->data["id"];
$ints = $this->data["interests"];
$proof = $this->data["proof"];
$page_scheme = $this->data['SCHEME'];
##################################################################

$INFO = array('boxes' => array(),
			  'specializations' => array()
			  );

for($i=0;$i<$ints->counter;$i++)
{
	$subval = $ints->spec_inter($i);
	$imain = $subval->interest;
	$rating = $ints->spec_inter($i)->rating;
	$interest_id = $ints->spec_inter($i)->interest_id;	
	array_push($INFO['specializations'],$interest_id);	
	
	$keywords_data = array_filter(explode(",",$subval->key_words));
	$keywords = array();
	foreach($keywords_data as $keyword)
		array_push($keywords, "<a href='/hubster?u=".$id."&s=".$interest_id."&kw=".urlencode(trim($keyword))."' 
		style='color:#DDD; font-size:12px'>".$keyword."</a>");
	$keywords = implode(', ',$keywords);	
	
	$business_tag = $this->isOwner() ? 'Мои дела в этой сфере...' : 'Дела в этой сфере...';
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	$structures = array();
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	$structures[0] = 
	"<table class='UI-item' border=0 cellpadding='0' cellspacing='0'>
	<tr><td><div class='UI-item-text qHint' tag='[BUSINESS_TAG]'>
	<a href='/hubster?u=[USER_ID]&s=[INTID]' style='color:white'>[MAIN]</a></div></td>
	<td class='UI-item-rating qHint' tag='Уровень опыта' align='center' r='[RATING]' idi='[INTID]'>[RATING]</td>
	</tr><tr><td colspan='2'><div class='UI-item-text-additional'>[KEYWORDS]</div></td></tr></table>";
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	$structures[1] = 
	"<table class='UI-item full-width' style='font-size:15px;' border=0 cellpadding='0' cellspacing='0'>
	<th class='UI-item-rating qHint' style='border-bottom:2px #005984 solid' tag='Уровень опыта' align='center' 
	r='[RATING]' idi='[INTID]'>[RATING]</th>
	<tr><td class='UI-item-text qHint' tag='[BUSINESS_TAG]'>
	<a href='/hubster?u=[USER_ID]&s=[INTID]' style='color:white'>[MAIN]</a></td></tr>
	<tr><td colspan='2'><div class='UI-item-text-additional'>[KEYWORDS]</div></td></tr></table>";
	////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	$structire_id = 0;
	if(in_array($page_scheme,array(2,3))) $structire_id=1;
	
	$box = str_replace(
		array('[INTID]','[MAIN]','[RATING]','[KEYWORDS]','[USER_ID]','[BUSINESS_TAG]'), 
		array($interest_id,$imain,$rating,$keywords,$id,$business_tag), 
		$structures[$structire_id]
		);
	
	
	if( mb_strlen($keywords)==0 ) array_push($INFO['boxes'],$box); else array_unshift($INFO['boxes'],$box);
	
}	

$specialization = mb_strlen($proof)==0 ? Interests::DefineSpecialization($INFO['specializations']) : $proof;
$img = mb_strlen($proof)==0 ? '<img src="/qmex_img/UI/specialization.png" height="15px" style="padding-right:5px" />' : '';
$additional = $this->isOwner() ? 
'<hr color=\'#999\'><div style=\'color:white; font-size:11px;\'>Можно изменить в настройках профиля</div>' : '';
$INFO['specialization'] = 
'<div class="UI-specialization qHint" tag="Специализация на базе интересов'.$additional.'">'.$img.$specialization.'</div>
<div style="margin:0 auto; text-align:center">↓↓</div>';

return $INFO;

}
	
	
public function createInfo()
{
##################################################################
$id = $this->data['id'];
$name = $this->data["name"];
$sename = $this->data["sename"];
$by = $this->data["by"];
$bm = $this->data["bm"];
$bd = $this->data["bd"];
$city = $this->data['city'];
$country = $this->data['country'];
$sex = $this->data['sex'];
$regtime = $this->data['datetime_reg'];
$conferences = $this->data['conference_count'];
$conf_part = $this->data['conference_participant'];
$success_events = $this->data['success_events'];
##################################################################
	
if(true): ?>

<div id="caption">
<img src="qmex_img/UI/profile/info.png" width='20px' height="20px" style="float:left; padding-right:5px">INFO
<?php if($this->isOwner()): ?> 
<img src="qmex_img/edit.png" style="float:right" width="20px" class="qHint" tag="Редактировать" 
onclick="location.href='/editprofile#info'">
<?php endif ?>
</div>

<table id="Qinfo" style="width:100%" cellspacing="0px" border="0">
<tr>
<td class="cap_td1">Имя / Фамилия: </td>
<td id="cap_td2">
<span title="<?php echo strip_tags($name) ?>"><?php echo mb_substr($name,0,20) ?></span>
<span title="<?php echo strip_tags($sename) ?>"><?php echo mb_substr($sename,0,20) ?></span>
<span id="q_lim_div"><img src="/qmex_img/UI/profile/<?php echo ($sex-1)==0 ? 'gender_m.png' : 'gender_f.png' ?>" width="12px"></span>
</td>
</tr>
<tr>

</tr>
<tr>
<td class="cap_td1">Дата рождения: </td>
<td id="cap_td2">
<?php if($bd>0) echo($bd." ".$bm); if($by>0) echo(" ".$by.' г.');?><br>
<?php echo "(Возраст : ".(date("Y")-$by).")" ?>
</td>
</tr>
<tr>
<td class="cap_td1">Местоположение: </td>
<td id="cap_td2"><div id="q_lim_div"><?php echo $country?>, <?php echo $city?></div></td>
</tr>
<tr>
<td class="cap_td1">В системе: </td>
<td id="cap_td2"><div id="q_lim_div">
<?php
$bits = explode(' ',$regtime);
$bits_n = explode('-',$bits[0]);
$bits_n[1]-=1;
if($bits_n[1]>0) echo 'c '.(int)$bits_n[2].' '.Enum::$MONTHES[$bits_n[1]].' '.$bits_n[0];
?>
</div>
</td>
</tr>
<tr>
<td class="cap_td1">Организованные конференции: </td>
<td id="cap_td2"><div id="q_lim_div">
<a href='talks?human=<?php echo $id;?>' class="underlined"><?php echo $conferences;?></a>
</div></td>
</tr>
<tr>
<td class="cap_td1">Участие в обсуждениях: </td>
<td id="cap_td2"><div id="q_lim_div">
<a href='talks?upart=<?php echo $id;?>' class="underlined"><?php echo $conf_part;?></a>
</div></td>
</tr>
<tr>
<td class="cap_td1">Ценное участие в событиях: </td>
<td id="cap_td2"><div id="q_lim_div">
<a href='developments?useful=<?php echo $id;?>' class="underlined"><?php echo $success_events ?></a>
</div></td>
</tr>
</table>

<?php endif;	
	}
	
	
	
public function createAbout()
{
##################################################################
$about = $this->data['about'];
$about = str_replace("---<br>","<div style='border-top:1px dashed #069; margin:7px 0 7px 0'></div>",$about);
$about = str_replace("---","<div style='border-top:1px dashed #069; margin-top:5px'></div>",$about);
$about = String::ReplaceLinks($about, "#00D");
##################################################################
if(true): ?>

<div id="caption">
<img src="qmex_img/UI/profile/about.png" width='20px' height="20px" style="float:left; padding-right:5px" />О деятельности
<?php if($this->isOwner()): ?> 
<img src="qmex_img/edit.png" style="float:right" width="20px" class="qHint" tag="Рассказать о деятельности" 
onclick="location.href='/editprofile#activity'">
<?php endif ?>
</div>
<div id="qInfoAbout">
<?php 
echo ($this->data['about']=="" && $this->isOwner()) ? 
"<div class='BlankProfileMsg'>Расскажите о том, чем вы занимаетесь.<br><a href='editprofile'>Рассказать>></a></div>" : $about;
?>
</div>

<?php endif;

	}


public function createResourcesData()
{
##################################################################
$webs = $this->data['site'];
##################################################################

$sites_data = array();
foreach(explode("||",$webs) as $item){
	$it = explode(";;",$item);
	if(count($it)>=2) $sites_data[$it[0]]=$it[1];
	}	

if(true): ?>

<div class="qResources">
<div id="caption">
<img src="qmex_img/UI/profile/data.png" width='20px' height="20px" style="float:left; padding-right:5px">Особые данные
<?php if($this->isOwner()): ?> 
<img src="qmex_img/edit.png" style="float:right" width="20px" class="qHint" tag="Добавить особую информацию" 
onclick="location.href='/editprofile#specdata'">
<?php endif ?>
</div>
<table cellspacing="0px" border="0" class="full-width">
<?php 
if(count($sites_data)>0)
{
	foreach($sites_data as $comment=>$url)
	{
		echo('<tr>');
		echo "<td width='30%' class='user-resourse-name' style='padding:5px;'>".splitter($comment,30,0,false).":</td>";
		
		echo('<td class="user-resourse-value">');
		$w=false;
        if(is_valid_url($url)) $w=true;
		$suburl = urldecode(str_replace('\n','',$url));
		if(!strstr($suburl,'://')) $suburl='http://'.$suburl;
		
        if($w==true) echo("<a href='$suburl' class='user-profile-data-href' target='_blank'>");
        echo splitter(urldecode($url),80,0,false);
        if($w==true) echo("</a>");
		echo('</td></tr>');
		}
	}	
	elseif($this->isOwner()) 
	echo '<div class="BlankProfileMsg">Добавьте свою особую информацию.<br><a href="editprofile">Добавить>></a></div>';
	elseif(!$this->isOwner())
	echo '<div class="BlankProfileMsg">Особая информация отсутствует</div>';
	
?>
</table>
</div>

<?php endif;
	}

	
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public function createSwopTop()
{
include 'views/page-layouts/userpage_bits/swopTop.php';
}

public function createSwopNeeds()
{
include 'views/page-layouts/userpage_bits/swop/needs.php';
}

public function createSwopProvides()
{
include 'views/page-layouts/userpage_bits/swop/provides.php';
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////



public function createAchievements()
{

if(true): ?>

<div id="caption">
<img src="qmex_img/UI/profile/achievements.png" width='20px' height="20px" style="float:left; padding-right:5px">Достижения
</div>
<?php
include('views/page-layouts/userpage_bits/achievements.php');
?>

<?php endif;
}




public function createUsefulPeopleList()
{
##################################################################
$id = $this->data['id'];
##################################################################
if(true): ?>

<div class="ipeople-header qHint" tag="Заглянуть">
<table style="width:100%;" cellpadding="0" cellspacing="0">
<tr>
<td id='ipeople-box-roll'>
<img src="qmex_img/UI/profile/people.png" width='20px' height="20px" style="float:left; padding-right:5px" />
<a href='/search#t=ipeople&u=<?php echo $id ?>' class='underlined' style="font-size:14px; font-weight:bold; color:#FFF" 
id='ui-peoples-search'>Окружение</a>
</td>
</tr>
</table>
</div>
<div style="background-color:#09F; text-align:center; cursor:pointer" id='people-list-roll' class='qHint' tag='Показать'>
<img src="qmex_img/arrow_left.png" width="10px" height="10px" id='people-list-arrow'>
</div>
<div id='people-list'><?php include('views/page-layouts/userpage_bits/env.php');?></div>


<?php endif;
}




public function createHubster()
{

if(true): ?>

<div id="caption">
<img src="qmex_img/UI/profile/public.png" width='20px' height="20px" style="float:left; padding-right:5px">Hub
</div>
<?php
include('views/page-layouts/userpage_bits/hubster.php');
?>

<?php endif;
}

		
	}

?>