<?php

class rating{
	
	const INTERESTING_USER = 3;
	const INTEREST_CONFIRM = 1;
	const INTERESTING_PAGE = 2;
	const NOTE_VOITE       = 2;
	const CONF_OPINION     = 0.25;
	const DEV_THANK        = 4;
	const DIALOG_THANK     = 1;
	
	
	public static function getRating($user_id,$type)
	{
		$user_id = (int)$user_id;
		$rait = 0;
		$DB = new db();
		$DB->query("SELECT Voite FROM qmex_voites WHERE Login=$user_id AND Type='$type'");
		while($voite = $DB->one(0)) $rait += $voite;
		return $rait;
		}
		
	public static function countPositiveVoites($uid)
	{
		$uid = (int)$uid;
		$DB = new db();
		$DB->query("SELECT COUNT(*) FROM qmex_voites WHERE Login=$uid AND Voite>-1");
		return $DB->one(0);
		}
	
	public static function countNegativeVoites($uid)
	{
		$uid = (int)$uid;
		$DB = new db();
		$DB->query("SELECT COUNT(*) FROM qmex_voites WHERE Login=$uid AND Voite<0");
		return $DB->one(0);
		}
		
	public static function getPageRating($user_id)
	{
		return Rating::getRating($user_id,'page');
		}
		
	public static function getPublicRating($user_id)
	{
		return Rating::getRating($user_id,'note');
		}
		
	public static function getInterestsRating($user_id)
	{
		return Rating::getRating($user_id,'interest');
		}
		
	public static function getFansCount($user_id)
	{
		$user_id = (int)$user_id;
		$DB = new db();
		$DB->query("SELECT COUNT(*) FROM qmex_user_bookmarks WHERE ID2=$user_id");
		return $DB->one(0);
		}
	
	public static function getHubsterTypesRating($user_id)
	{
		$user_id = (int)$user_id;
		$DB = new db();
		$DB->query("SELECT notes.Type as Type, voites.Voite as Voite FROM qmex_voites AS voites, qmex_user_notes AS notes WHERE voites.Type='note' AND voites.Login=".$user_id." AND voites.Essence=notes.ID");
		$type_rating = array();
		foreach(ENUM::ConceptionsWords() as $key=>$val) $type_rating[$key] = 0;
		while($item = $DB->one())
		{
			$type_rating[$item[0]] += (int)$item[1];
			}
		return $type_rating;
		}
	
	public static function CreateEvent($voite_id)
	{
		$DB = new db();
		$DB->query("SELECT Type, Essence, Voite, DateTime FROM qmex_voites WHERE ID=$voite_id");
		$data = $DB->one();
		$type = $data[0];
		$essence = $data[1];
		$noteName = Note::getNoteSlogan($essence);
		if(trim($noteName)!='') $noteName="\"".$noteName."\"";
		$voite = $data[2];
		$datetime = translate_date($data[3]);
		if($datetime=='') $datetime="<неизвестно>";
		$IBASE = new Interests();
		
		$rew = ($voite==1) ? "Положительное" : "Отрицательное";
		$rew = '<u><b>'.$rew.'</b></u>';
		$sign = ($voite>0) ? "+" : "-";
		$color = ($voite>0) ? "#093" : "#C40000";
		
		$descr = '';
		switch($type)
		{
			case 'page': 
				$descr = "<b>Актуальность</b>"; 
				$rait = " | ".$sign.RATING::INTERESTING_PAGE; 
				break;
			case 'note': 
				$descr = $rew." мнение о <b><a href='note?id=".$essence."'>хабе ".$noteName."</a></b> в Public"; 
			    $rait = " | ".$sign.RATING::NOTE_VOITE; 
				break;
			case 'interest': 
				$descr = "Подтверждение интереса <b>'".$IBASE->SelectName($essence)."'</b>";
			    $rait = " | ".$sign.RATING::INTEREST_CONFIRM; 
				break;
			case 'conf_opinion': 
				$DB->query("SELECT conf_id, content FROM qmex_conferences_opinions WHERE id=$essence");
				$data = $DB->one();
				$conf_id = $data[0];
				$content = mb_substr(strip_tags($data[1]),0,50).'...';	
				$descr = "Оценка мнения в конференции <b><a href='talks?conf=".$conf_id."'>№".$conf_id."</a></b>:<br>'<i>".$content."</i>'";
			    $rait = " | ".$sign.RATING::CONF_OPINION; 
				break;
			case 'dev_thank': 
				$descr = "Полезный участник <a href='/developments?view=show&id=".$essence."'>события</a>";
			    $rait = " | ".$sign.RATING::DEV_THANK; 
				break;
			}
		
		echo '
		
		<div class="ui-voite-card">
		<div class="ui-voite-card-caption">'.$datetime.'<div style="float:right;color:'.$color.'">'.$rait.'</div></div>
		<div class="ui-voite-card-description" style="color:'.$color.'">'.$descr.'</div>
		</div>
		
		';
		
		}
	
	}

?>