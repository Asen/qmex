<?php

class Data_provider implements ArrayAccess
{
	private $vars = array();
	
	public function offsetGet($offset)
	{
		$offset = trim(mb_strtolower($offset));
		return (isset($this->vars[$offset])==true) ? $this->vars[$offset]:NULL;
		}
		
	public function offsetExists($offset)
	{
		return (isset($this->vars[$offset])) ? true:false;
		}
		
	public function offsetSet($offset, $value)
	{
		$offset = trim(mb_strtolower($offset));
		$this->vars[$offset] = $value;
		}
		
	public function offsetUnset($offset)
	{
		if(isset($this->vars[$offset])==true) unset($this->vars[$offset]);
		}

	}
	
?>