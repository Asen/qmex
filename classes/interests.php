<?php


class Interest
{
	public $id;
	public $interest;
	public $interest_id;
	public $key_words;
	public $rating;
	};


class interests
{
	
	public static function getName($id)
	{
		$IBASE = new Interests();
		$IBASE->SelectIBASE();
		return $IBASE->SelectName($id);
		}
	
	const IMG_PATH = "qmex_img/inter/"; 	
	public static $BASE_INTERESTS = array();
	private $IBASE = array();
	
	public $interests;
	public $counter;
	function __construct($langid = SYSINFO::LANGID_RU){
		$this->interests = array();
		$this->Init($langid);
		}
		
		
	function get_interests($user_id)
	{
		$db = new db();
		$voite_db = new db();
		$res = $db->query("SELECT key_interest, interest_keywords, ID FROM qmex_user_interests WHERE Login=".$user_id);
		while($data = $db->one())
		{
			$data[1] = str_replace(","," ",$data[1]);
			$data[1] = implode(', ',array_filter(explode(' ',$data[1])));
			$interest = new Interest();
			$interest->interest_id=$data[0];
			$interest->interest=$this->SelectName($data[0]);
			$interest->key_words=$data[1];
			$interest->id=$data[2];
			
			$voite_db->query("SELECT SUM(Voite) FROM qmex_voites WHERE Login=$user_id AND Type='interest' AND Essence=".$data[0]);
			$interest->rating = (float)$voite_db->one(0) * RATING::INTEREST_CONFIRM;
			$voite_db->query("SELECT SUM(v.Voite) FROM qmex_conferences_opinions AS ops
							  INNER JOIN qmex_voites AS v INNER JOIN qmex_conferences AS conf
							  ON v.Login=$user_id AND v.Essence=ops.id AND v.Type='conf_opinion' 
							  AND ops.conf_id=conf.id AND conf.vector_interest=".$data[0]);
			$interest->rating += (float)$voite_db->one(0)*RATING::CONF_OPINION;
			$voite_db->query("SELECT SUM(bonus) FROM qmex_achievements WHERE belong_to_user=$user_id AND interest=".$data[0]);
			$interest->rating += (float)$voite_db->one(0);
			
			array_push($this->interests,$interest);
			unset($interest);
			$this->counter++;
		}
		
		Achievement::CheckInterests($this->interests, $user_id);
		
	}
		
	function spec_inter($i) 
	{
		return $this->interests[$i];
		}
	
	private function Init($LANGID)
	{
		Interests::$BASE_INTERESTS = array(
             array(1,'architect.jpg'),
             array(2,'auto.jpg'),
			 array(3,'math.jpg'),
			 array(4,'phys.jpg'),
             array(5,'chemi.jpg'),
			 array(6,'gadghets.jpg'),
			 array(7,'children.jpg'),
			 array(8,'design.jpg'),
			 array(9,'food.jpg'),
			 array(10,'beast.jpg'),
			 array(11,'bio.png'),
			 array(12,'space.jpg'),
			 array(13,'book.jpg'),
			 array(14,'game.jpg'),
			 array(15,'internet.jpg'),
			 array(16,'history.jpg'),
			 array(17,'world.jpg'),
			 array(18,'film.jpg'),
			 array(19,'comp.png'),
			 array(20,'law.jpg'),
			 array(21,'culture.jpg'), 
			 array(22,'medicine.jpg'),
			 array(23,'fashion.png'),
			 array(24,'moto.jpg'),
			 array(25,'music.jpg'),
			 array(26,'eco.jpg'),
			 array(27,'creation.jpg'),
			 array(28,'news.jpg'),
			 array(29,'study.jpg'),
			 array(30,'society.jpg'),
			 array(31,'relations.jpg'),
			 array(32,'politic.jpg'),
			 array(33,'nature.jpg'),
			 array(34,'programming.jpg'),
			 array(35,'psychology.jpg'),
			 array(36,'journey.jpg'),
			 array(37,'biz.JPG'),
			 array(38,'ads.jpg'),
			 array(39,'sport.png'),
			 array(40,'tech.jpg'),
			 array(41,'phylosofy.jpg'),	 
			 array(42,'fitness.jpg'),
			 array(43,'photo.jpg'),
			 array(44,'trade.JPG'),
			 array(45,'economy.jpg'),
			 array(46,'humor.jpg'),
			 array(47,'people.jpg'),
			 array(48,'art.jpeg'),
			 array(49,'geo.png'),
			 array(50,'secret.jpg'),
			 array(51,'lingua.jpg'),
			 array(52,'video.jpg'),
			 array(53,'cookery.jpg'),
			 array(54,'finance.jpg'),
			 array(55,'astronomy.jpg'),
			 array(56,'archeology.jpg')
            );
			
			switch($LANGID){
				case SYSINFO::LANGID_RU : $this->Complete_RU();
				                          break;
			}
	}
	
	private function Complete_RU()
	{			
		$lang = array(
             1=>'Архитектура',
             2=>'Автомобили',
			 3=>'Математика',
			 4=>'Физика',
             5=>'Химия',
			 6=>'Гаджеты',
			 7=>'Дети',
			 8=>'Дизайн',
			 9=>'Еда',
			 10=>'Животные',
			 11=>'Биология',
			 12=>'Космос',
			 13=>'Литература',
			 14=>'Игры',
			 15=>'Интернет',
			 16=>'История',
			 17=>'Мир',
			 18=>'Кино',
			 19=>'Компьютеры',
			 20=>'Закон',
			 21=>'Культура', 
			 22=>'Медицина',
			 23=>'Мода',
			 24=>'Мото', //!
			 25=>'Музыка',
			 26=>'Экология',
			 27=>'Творчество',
			 28=>'Новости',
			 29=>'Обучение',
			 30=>'Общество',
			 31=>'Сотрудничество',
			 32=>'Политика',
			 33=>'Природа',
			 34=>'Программирование',
			 35=>'Психология',
			 36=>'Путешествия',
			 37=>'Бизнес',
			 38=>'Маркетинг',
			 39=>'Спорт',
			 40=>'Технологии',
			 41=>'Философия', 
			 42=>'Фитнес',
			 43=>'Фото',
			 44=>'Торговля',
			 45=>'Экономика',
			 46=>'Юмор',
			 47=>'Люди',
			 48=>'Художество',
			 49=>'География',
			 50=>'Тайное',
			 51=>'Языки',
			 52=>'Видео',
			 53=>'Кулинария',
			 54=>'Финансы',
			 55=>'Астрономия',
			 56=>'Археология'
            );
						
		foreach(Interests::$BASE_INTERESTS as $interest) {
					$interest_id = $interest[0];
					array_push($this->IBASE,array($lang[$interest_id],$interest[1],$interest_id));
					}
					
		array_multisort($this->IBASE);
	}
	
	public function SelectIBASE()
	{
		return $this->IBASE;
		}
	
	public function SelectName($i)
	{
		foreach($this->IBASE as $arr)
		if($arr[2]==$i) return $arr[0];
		}
		
	public function SelectImage($i)
	{
		foreach($this->IBASE as $arr)
		if($arr[2]==$i) return $arr[1];
		}
		
	public function SelectKey($array)
	{
		return $array[2];
		}
	
	public static function DefineSpecialization($specializations)
	{
		$data_RU = array( "Архитектор"=>array(1,3,4),
						  "Бизнесмен"=>array(37,31),
						  "Геймер"=>array(14,19),
						  "Политик"=>array(32,20),
						  "Юрист"=>array(30,20),
						  "Программист"=>array(34,19),
						  "Компьютерщик"=>array(19,40),
						  "Художник"=>array(48,27),
						  "Мыслитель"=>array(41,30),
						  "Мыслитель"=>array(41,47),
						  "Меломан"=>array(25),
						  "Музыкант"=>array(25,27),
						  "Изобретатель"=>array(4,40),
						  "Странник"=>array(36,17),
						  "Киноман"=>array(18,47),
						  "Комик"=>array(46,47),
						  "Трейдер"=>array(44,45),
						  "Физик"=>array(3,4),
						  "Автомобилист"=>array(2,24),
						  "Доктор"=>array(11,22),
						  "Модельер"=>array(8,23),
						  "Гумманитарий"=>array(13,16),
						  "Ботаник"=>array(11,29),
						  "Гик"=>array(6,19),
						  "Дизайнер"=>array(8,27),
						  "Учитель"=>array(7,29),
						  "Финансист"=>array(45,54),
						  "Интернетчик"=>array(15,37),
						  "Блоггер"=>array(15,30),
						  "Уфолог"=>array(12,50),
						  "Лингвист"=>array(29,51),
						  "Фотограф"=>array(8,43),
						  "Кулинар"=>array(9,53),
						  "Спортсмен"=>array(39,42),
						  'Социофил'=>array(30,47),
						  'Писатель'=>array(13,27),
						  'Актёр'=>array(18,27)
						   );
						   
		$specializations_collection = array();
		foreach($data_RU as $key=>$array)
		if(sizeof(array_intersect($specializations,$array)) == sizeof($array)){
			array_push($specializations_collection, $key);
		}
			
		return (sizeof($specializations_collection)==0) ? false : implode(' | ',array_unique($specializations_collection));			   
		}
	
	};

?>