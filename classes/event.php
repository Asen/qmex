<?php

class Event{
	
	const TYPE_PAGEVOITE = 0;
	const TYPE_HUBVOITE = 1;
	const TYPE_INTERESTVOITE = 2;
	const TYPE_ADD_TO_INTERESTS = 3;
	#####
	const TYPE_DEL_FROM_INTERESTS = 5;
	const TYPE_NOTE_COMMENT = 6;
	const TYPE_COMMENT_ANSWER = 7;
	const TYPE_SWOP_OCCURED = 9;
	const TYPE_HUB_OCCURED = 10;
	const TYPE_INTEREST_ACHIEVEMENT = 11;
	const TYPE_ENV_CONF_NEWOPINION = 12;
	const TYPE_DIALOGMSG_THANKED = 13;
	
	public static function CreateEvent($TYPE, $WHOSE, $ESSENCE=0, $ACTION=0, $DB = NULL)
	{
		$LOGIN = (int)$_SESSION["id"];
		$TYPE = (int)$TYPE;
		$WHOSE = (int)$WHOSE;
		$ESSENCE = (int)$ESSENCE;
		$ACTION = (int)$ACTION;
		if($DB == NULL) $DB = new DB();
		$RES = $DB->query("INSERT INTO qmex_events(Type,Login,Maker,Essence,Action) VALUES($TYPE,$WHOSE,$LOGIN,$ESSENCE,$ACTION)");
		return ($RES) ? 1:0;
		}
	
	
	public static function ExtractUserEvents($LOGIN)
	{
		$LOGIN = (int)$LOGIN;
		$DB = new db();
		$RESULT = array();
		$DB->query("SELECT ID FROM qmex_events WHERE Login=$LOGIN");
		while($data = $DB->one())
		array_push($RESULT,$data[0]);
		
		return $RESULT;
		}
		
		
	public static function genEventsList($id_collection)
	{
		if( !is_array($id_collection) ) return array();
		$DATA = array();
		$DB = new db();
		$IDB = new db();
		$DB->query("SELECT Type,Maker,Essence,Action,DateTime,ID FROM qmex_events 
					WHERE ID IN (".implode(',',$id_collection).") ORDER BY ID DESC");
		while($data = $DB->one())
		{		
			$type = $data[0];
			$maker = $data[1];
			$maker_login = Human::getLoginById($maker);
			$essence = $data[2];
			$action = $data[3];
			$datetime = translate_date($data[4]);
			$id = $data[5];
			
			$rating = 0;
			$text = '';
			$caption = '';
			$color = 'white';
			$BAD_COLOR = '#D50000';
			$GOOD_COLOR = '#063';
			$login = '<a href="profile?id='.$maker_login.'">'.$maker_login.'</a>';
			
			switch($type)
			{
				case Event::TYPE_PAGEVOITE: 
					$text = ($action>0) ? 'Кто-то выразил вам свое почтение.':
				                      	  'Кто-то выразил вам свое недовольство.';
					$color = ($action>0) ? $GOOD_COLOR : $BAD_COLOR; 
					$rating = ($action>0 ? '+':'-').Rating::INTERESTING_PAGE;
					$caption = ($action>0) ? 'Выражение признания' : 'Выражение недовольства';
					break;
				case Event::TYPE_HUBVOITE:
					$text = ($action>0) ? 
					'О вашем <a href="note?id='.$essence.'">хабе</a> было выражено положительное мнение.':
					'О вашем <a href="note?id='.$essence.'">хабе</a> было выражено отрицательное мнение.';
					$color = ($action>0) ? $GOOD_COLOR : $BAD_COLOR; 
					$rating = ($action>0 ? '+':'-').Rating::NOTE_VOITE;
					$caption = 'Мнение о хабе';
					 break;
				case Event::TYPE_INTERESTVOITE:
					$IBASE = new Interests();
					$text = "Пользователь ".$login." подтвердил ваш навык в области <b>\"".$IBASE->SelectName($essence)."\"</b>";
					$color = $GOOD_COLOR ;
					$caption = 'Подтверждение навыка';
					$rating = "+".Rating::INTEREST_CONFIRM;
				    break;
				case Event::TYPE_ADD_TO_INTERESTS:
					$text = "Вы интересны пользователю ".$login." !";
					$color = $GOOD_COLOR ;
					$caption = 'Актуальность';
					$rating = "+".Rating::INTERESTING_USER;
				    break;
				case Event::TYPE_DEL_FROM_INTERESTS:
					$text = "Вы больше не интересны пользователю ".$login." ...";
					$color = $BAD_COLOR ;
					$caption = 'Актуальность';
					$rating = "-".Rating::INTERESTING_USER;
				    break;
				case Event::TYPE_NOTE_COMMENT:
					$IDB->query("SELECT Text FROM qmex_comments WHERE ID=".$action);
					$comment = $IDB->one(0);
					$text = "Новое мнение к вашему <a href='note?id=".$essence."'>хабу</a> 
							 в Hubster :<br><br><div class='quote'>".Comment::ProcessText($comment)."</div>";
					$color = $GOOD_COLOR ;
					$caption = 'Мнение в Hubster';
				    break;
				case Event::TYPE_COMMENT_ANSWER:
					$IDB->query("SELECT Text FROM qmex_comments WHERE ID=".$essence);
					$MAIN = $IDB->one(0);
					$IDB->query("SELECT Text FROM qmex_comments WHERE ID=".$action);
					$ANSW = $IDB->one(0);
					$text = "На ваше мнение:<br><br><div class='quote'>".Comment::ProcessText($MAIN).
							"</div><br>Кто-то ответил:<br><br><div class='quote'>".Comment::ProcessText($ANSW)."</div>";
					$color = $GOOD_COLOR ;
					$caption = 'Ответ на мнение';
				    break;
				case Event::TYPE_SWOP_OCCURED:
					$text = "<div style='padding-bottom:5px; font-size:0.9em'>Интересный Swop:</div>".SWOP::CreateSwopById($essence,true);
					$color = $GOOD_COLOR ;
					$caption = 'Новая возможность / предложение!';
					break;
				case Event::TYPE_HUB_OCCURED:
					$hub_info = Note::getNoteInfo($essence);
					if(!$hub_info) continue;
					$creator = Human::getLoginById($hub_info['login']);
					$conceptions = Enum::Conceptions();
					$IBASE = new Interests();
					$IBASE->SelectIBASE();
					$text = "<a href='/profile?id=".$creator."' class='underlined'>".String::mb_ucfirst($creator)."</a>".
							" написал <b>'".$conceptions[$hub_info['type']]."'</b> о <b>'".$IBASE->SelectName($hub_info['theme'])."'</b>".
							"<br>в своём <a href='/note?id=".$essence."' class='underlined'>хабе ".htmlspecialchars($hub_info['substance'])."
							</a>";
					$color = $GOOD_COLOR ;
					$caption = 'Интересный материал';
					break;
				case Event::TYPE_INTEREST_ACHIEVEMENT:
					$achievement = Achievement::ShowProfileVariantAchievement($essence, true);
					$text = "Получено новое достижение:<br><br>".$achievement;
					$color = $GOOD_COLOR ;
					$caption = 'Новое достижение!';
					break;
				case Event::TYPE_ENV_CONF_NEWOPINION:				
					$IDB->query("SELECT conf_id, creator, content FROM qmex_conferences_opinions WHERE id=$essence");
					$data = $IDB->one();
					$conf_id = $data[0];
					$sender = Human::getLoginById( $data[1] );
					$content = mb_substr(strip_tags($data[2]), 0, 250).'...';
					$IDB->query("SELECT creator FROM qmex_conferences WHERE id=$conf_id");
					$env_of = Human::getLoginById( $IDB->one(0) );
					$caption = "Новое мнение в вашем окружении";
					$color = $GOOD_COLOR ;
					$text = 'В <b><a href="/talks?conf='.$conf_id.'">окружение</a></b> 
					пользователя <a href="/profile?id='.$env_of.'">'.$env_of.'</a> было отправлено новое сообщение<br>
					( от <a href="/profile?id='.$sender.'">'.$sender.'</a>) : <br><br> <i><div class="quote">'.$content.'</div></i>';
					break;
				case Event::TYPE_DIALOGMSG_THANKED:
					$IDB->query("SELECT dialog_id, Message FROM qmex_dialogs_msg WHERE id=$essence");
					$data = $IDB->one();
					$text = "Собеседник поблагодарил вас за ваше сообщение : <br><br>
							<div class='quote'>".(mb_substr( strip_tags($data[1]) ,0,250))."...</div><br>
							в <a href='/dialogs?dialog=".$data[0]."'>диалоге</a>.";
					$color = $GOOD_COLOR ;
					$caption = 'Благодарность в диалоге';
					break;
				default: { continue; };			
			}
			
			$block = '<div class="ui-event-item">
			<div class="ui-event-caption" style="color:'.$color.'">';
			if($rating!=0) $block.='<span class="ui-event-rating qHint" tag="Рейтинг" style="background:'.$color.'">'.$rating.'</span> ';
			$block.=$caption;
			$block.=' : </div>
			<div class="ui-event-text">'.$text.'</div>
			<div class="ui-event-footer">'.$datetime.'</div>
			</div>';
			
			$DATA[$id] = $block;
			
			}
			
			return $DATA;
			
		}
	
	
	};

?>