<?php

// It should be renamed to Hub class
class note{
	
	public static function showNote($id, $IS_FULL = false, $SHOW_AUTOR = true)
	{
		$id=(int)$id;
		$this_user = (isset($_SESSION["id"])) ? $_SESSION["id"] : -1;
		//$db_ = new db();
		$db = new db();
		
		$next_notes = array();
		$db->query("SELECT ID FROM qmex_user_notes WHERE Connection=$id");
		if($db->rowCount()>0) while($idN=$db->one(0)) array_push($next_notes,$idN);
		
		$db->query("SELECT Note, DateTime, Meaning, ID, Is_Private, Login, Theme, Type, Connection, Substance, updated_at 
					FROM qmex_user_notes WHERE ID=$id");
					
		if($db->rowCount()>0)
		{
		$data=$db->one();
		$creator = $data[5];

			if($data[4]==false || $creator==$this_user || in_array($this_user,Human::getInterestingPeople($creator)))
			{
			$sender_name = Human::getLoginById($creator);
			$node_id = $data[3];
			$db->query("SELECT COUNT(*) FROM qmex_voites WHERE type='note' AND essence=$node_id AND Voite<0");
			$dislikes = (int)$db->one(0);
			$db->query("SELECT COUNT(*) FROM qmex_voites WHERE type='note' AND essence=$node_id AND Voite>0");
			$likes =    (int)$db->one(0);
			//while($p_data = $db_->one()) if($p_data[0]==1) $likes++; else $dislikes++;
			$note_raiting = ($likes-$dislikes);
			$color = "";
			$rait_string = "$note_raiting";
			if($note_raiting>0) {$color="#070"; $rait_string="+$note_raiting";} else
			if($note_raiting<0) $color="#C00"; else
			if($note_raiting==0)$color="#777";
			
			//$color="#777";
			
			$note = $data[0];
			/*if(!$IS_FULL)
			{
				$MAX_L = (mb_strlen($note)>=2000) ? 2000 : mb_strlen($note);
				$substr_start = mb_strpos($note,"</",$MAX_L);
				if($substr_start>$MAX_L)
				{ 
				$substr_start = mb_strpos($note,">",$substr_start);
				$note = mb_substr($note,0,$substr_start+1).'...';
				} else $note = mb_substr($note,0,$MAX_L);
			}*/
			
			//$note = splitter($note,200,0,false); // ? check it
			
			$date = translate_date($data[1]);
			///$updated_at
			$updated_at = $data[10]==0 ? '' : '(изм.'.translate_date(date("Y-m-d H:i:s",$data[10])).')';
			$mean = htmlspecialchars($data[2]);
			$substance = htmlspecialchars($data[9]);
			
			$theme=$data[6];
			if($theme>0){
			$IBASE = new Interests();
			$theme = $IBASE->SelectName($theme);
			$theme = "<div class='ui_note_theme'>".$theme."</div>";
			} else $theme = '';
			
			$concept = $data[7];
			$conceptions = Enum::Conceptions();
			$conception = (in_array($concept,array_keys($conceptions))) ? 
			"<div class='ui_note_conception'>".$conceptions[$concept]."</div>" : '';
			
			$connection = $data[8];
			$connections = '';
			if($connection>0 || !empty($next_notes))
			{
			$connections ='<div class="ui_note_history">';
			if($connection>0) 
				$connections.='<u>Привязан к:</u> <b><a href="/note?id='.$connection.'">'.
				Note::GetBriefNote($connection,30).'</a></b><br>';
			if(!empty($next_notes))
			{
				$connections.='<u>Привязанные хабы:</u> ';
				foreach($next_notes as $next_note) 
				$connections.='<b><a href="/note?id='.$next_note.'">'.Note::GetBriefNote($next_note,30).'</a></b> , ';
			}
			$connections.="</div><br>";
			}
			
			
			$d = "note_".$node_id;
			
			echo '<div class="public-note" id="public-note-'.$id.'" tag="'.$id.'">';
			
			$CAPTION = "";
			
			if($this_user==$creator)
			{ 
			$path = 'qmex_img/UI/hubster/'; 
			$CAPTION = "<div class='ui_note_tools' id='tools_$d'>	
			<table class='full-width'>
			<tr><td>
			<table cellspacing=0>
			<tr> 			   
			<td><img src='".$path."edit.png' class='ui_note_tool edit_note' tag='Редактировать' num='$node_id' id='editor_$d'></td>
			<td><img src='".$path."remove.png' class='ui_note_tool rem_note' tag='Удалить' num='$node_id'></td><td>";	
			$CAPTION .= ($data[4]==false) ?
			"<img src='".$path."private.png' class='ui_note_tool private_note' tag='Скрыть хаб от посторонних' num='$node_id'>" :
			"<img src='".$path."common.png' class='ui_note_tool private_note_o' tag='Сделать доступным для всех' num='$node_id'>";	
			
			$CAPTION .=
			"<td><img src='".$path."connect-idea.png' class='ui_note_tool connect-idea' num='$node_id' tag='Написать продолжение..'
			num='$node_id'></td><td>";
			
			if($concept==Enum::$CONCEPTIONS['INTENT']) {
				$CAPTION .= ($_SESSION['target']==$node_id) ? 
				"<td><img src='".$path."idea-curr.png' class='ui_note_tool intent_curr' 
				tag='Это ваша основная цель. Нажмите, чтобы удалить ее.' num='$node_id'></td>" :
				"<td><img src='".$path."no-idea-curr.png' class='ui_note_tool make_intent' tag='Установить главной целью/текущим
				намерением' num='$node_id'></td>"; 
				
			}
			
			$CAPTION .= "
			<td align='center' class='qNoteRait' l='".$likes."' dl='".$dislikes."'>
			<div class='ui_note_rait_box'>
			R = <span style='font-weight:bold;color:$color;' id='note-rait-sign-$node_id'>".$rait_string."</span> 
			</div>
			</td>";
			 
			$CAPTION .= "</td><td></td></tr></table></td><td align='right'>".$theme.$conception."</td></tr></table></div>";
			} 
			elseif($this_user!=$creator){
			
			$auth = ($SHOW_AUTOR) ? 
			"<div class='ui_note_sender'>
			<a class='underlined' style='color:#06C' href='profile?id=".urlencode($sender_name)."'>".String::mb_ucfirst($sender_name)."</a>
			</div>" : '';
			
			$owner = '';
			if((in_array($concept,array_keys($conceptions))))
			{
				$db->query("SELECT Login FROM qmex_users WHERE Target=".$node_id);
				if($db->rowCount()>0)
				{
					$owner = $db->one(0);
					$owner = "Текущая цель для <a class='underlined' href='profile?id=".$owner."'>".$owner."</a>";
				}
			}
			$auth = (mb_strlen($owner)>0) ? $owner:$auth;
			
			$tt = (!Favorites::IsExists($this_user,'note',$node_id)) ? 
			"<img src='qmex_img/UI/favorite.png' num='$node_id' class='ui_note_tool add_to_favorites qHint' style='width:25px; height:25px' 
			tag='Добавить хаб в мои интересные материалы'/>" : 
			"<img src='qmex_img/UI/non-favorite.png' num='$node_id' class='ui_note_tool rem_from_favorites qHint' style='width:25px; 
			height:25px' tag='Удалить из интересных материалов'/>";
			
			$CAPTION .=  "
			<div class='ui_note_tools_no_owner'>
			<table class='full-width'>
			<tr><td>
			<table cellspacing=0>
			<tr>
			<td align='center' style='cursor:pointer' class='qNoteRait' l='".$likes."' dl='".$dislikes."' rowspan='2'>
			<div class='ui_note_rait_box'>
			<span style='font-weight:bold;color:$color' id='note-rait-sign-$node_id'>".$rait_string."</span>
			</div>
			</td>
			<td>
			<img src='qmex_img/UI/hubster/like.png' class='ui_note_tool note-like-item' style='border:none; padding:0' tag='+1' 
			num='$node_id'><br>
			<img src='qmex_img/UI/hubster/dislike.png' class='ui_note_tool note-dislike-item' style='border:none; padding:0' tag='–1'  
			num='$node_id'>
			</td>
			<td style='padding-left:15px;padding-right:15px'><div class='ui_note_auth_info'>".$auth."</div></td>
			<td>
			".$tt."
			<img src='qmex_img/UI/hubster/edit.png' num='".$node_id."' class='ui_note_tool connect_with_author qHint' 
			style='width:25px; height:25px' tag='Открыть диалог с автором по поводу этого хаба'/>
			</td>
			</tr>
			</table>
			</td><td align='right'>".$theme.$conception."</td></tr></table>			
			</div>";
			}
			
			
			echo "<div class='ui_single_note_data' id='data_$d' num='$node_id' sender='$creator'>";
			echo "<div class='ui_single_note_caption'><span class='ui_note_meaning'>
			<a href='note?id=".$node_id."' class='underlined' style='color:#00C'>".$mean."</a>
			</span><div class='ui_hub_date'><span style='color:#999'>".$updated_at."</span> ".$date."</div></div>";
			
			echo $CAPTION;
			echo "<div class='ui_single_note_content'>";
						
			echo $connections;
			
			if(trim($substance)!='') 
				echo "<div class='ui-note-substance qHint' tag='Основная мысль' id='substance_".$node_id."'>
				<table cellspacing=0>
				<tr>
				<td><img src='/qmex_img/UI/hubster/substance.png' width='30px' style='padding-right:10px'></td>
				<td>".$substance."</td>
				</tr>
				</table>
				</div><br>";			
			if($IS_FULL)
				echo "<iframe srcdoc='".addFrameStyles($note)."' class='public-frame' id='public-frame-".$id."' 
					  frameborder='0' height='0px' scroll='no' seamless></iframe>";		  
			if(!$IS_FULL) 
				echo "<div class='ui_note_full'>
					  <a href='note?id=".$node_id."'>
					  <div class='ui_note_go_inside_icon'></div>
					  ".( trim($mean)!='' ? "<div class='ui_single_note_content_caption'>".$mean."</div><br>" : 'изучить')."
					  </a></div>";
				
			echo '</div>';
					  
			echo "<div class='ui_single_note_comments'>
			<div class='comment-word-line' tag='".$node_id."'>
			<span class='comment-word qHint' tag='Мнения других людей..'>Мнения >></span>
			</div>
			<div class='note-comments' id='cb-".$node_id."'>
			<textarea class='ui_note_box' style='color:blue' id='note-new-comment-".$node_id."' placeholder='Что вы думаете по этому поводу?'></textarea><br>
			<span class='comment-send-word' tag='".$node_id."'>Отправить</span>
			<div class='note-comments-box' id='comments-box-$node_id'>";
			comment::echoCommensList($node_id,'ab_note',$this_user);
			echo "</div></div></div></div>";
			echo '</div>';
			
			}
			elseif($IS_FULL) echo '<div id="ErrMsg">Этот хаб был скрыт автором. <a href="/">Вернуться главную страницу</a></div>';
		}else echo '<div id="ErrMsg"><b>Ошибка.</b> Этот хаб был удален или никогда не существовал. <a href="/">Вернуться главную страницу</a></div>';
	}
	
	public static function getNoteInfo($id)
	{
		$db = new DB();
		$db->query("SELECT Login, Meaning, Substance, Theme, Type FROM qmex_user_notes WHERE ID=$id");
		if($db->rowCount()==0) return false;
		$data = $db->one();
		return array('login'=>$data[0],
					 'meaning'=>$data[1],
					 'substance'=>$data[2],
					 'theme'=>$data[3],
					 'type'=>$data[4]);
		}
	
	public static function getNoteName($id)
	{
		$db = new DB();
		$db->query("SELECT Meaning, Substance FROM qmex_user_notes WHERE ID=$id");
		$data = $db->one();	
		$meaning  = strip_tags($data[0]);
		$substance = strip_tags($data[1]);
		
		return mb_strlen(trim($meaning))==0 ? $substance : $meaning;
	}
	
	public static function GetBriefNote($id,$substr=20)
	{
		$db=new DB();
		$db->query("SELECT Note, Meaning FROM qmex_user_notes WHERE ID=".$id);
		$data = $db->one();
		$note = mb_substr(strip_tags($data[0]),0,$substr);
		$meaning = mb_substr(htmlspecialchars($data[1]),0,$substr);
		$intent = (mb_strlen($meaning)>0) ? $meaning : $note;
		if($intent==$meaning && mb_strlen($intent)<$data[1]) $intent.='...';
		if($intent==$note && mb_strlen($intent)<$data[0]) $intent.='...';
		
		return $intent;
		}
	
	public static function getOwner($id)
	{
		$db=new DB();
		$db->query("SELECT Login FROM qmex_user_notes WHERE ID=$id");
		return Human::getLoginById($db->one(0));
		}
	
	
	public static function getNoteRating($id)
	{
		$db=new DB();
		$db->query("SELECT SUM(Voite) FROM qmex_voites WHERE Essence=$id AND Type='note'");
		return (int)$db->one(0);
		}
		
	public static function getNoteSlogan($id)
	{
		$db=new DB();
		$db->query("SELECT Meaning FROM qmex_user_notes WHERE ID=$id");
		return $db->one(0);
		}
	
	public static function getRandomNoteId($type)
	{
		$db=new DB();
		$type = (int)$type;
		$q = $type>0 ? ' WHERE Type='.$type : '';
		$db->query("SELECT ID FROM qmex_user_notes ".$q." ORDER BY RAND() LIMIT 1");
		return $db->one(0);
		}
	
	public static function JSRoutine()
	{
		if(true): ?>
        <script src="/tools/class-routine/hubster.js" language="javascript" class="script-note"></script>
        <script> 
			HUBSTER.comm_cause_hub_connection = <?php echo Communication::CAUSE_HUB_CONNECTION ?>;
			HUBSTER.Main(); 
        </script>
		<? endif;
		}
}

?>