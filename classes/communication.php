<?php


class communication{
	
	const CAUSE_USEFUL_MAN = 1;
	const CAUSE_MSG_ANSWER = 2; //?
	const CAUSE_SWOP_CONNECTION = 3;
	const CAUSE_HUB_CONNECTION = 4;
	public static $CAUSES = array( Communication::CAUSE_USEFUL_MAN,
								   Communication::CAUSE_MSG_ANSWER, // ?
								   Communication::CAUSE_SWOP_CONNECTION,
								   Communication::CAUSE_HUB_CONNECTION
								  );
		
	public static function MakeConnection($to, $msg, $cause, $essence, $answer_on = -1)
	{
		$db = new db();
		if(!isset($_SESSION["id"])) return("<script>show_wnd('Вам необходимо войти в систему!',100,true,2.5,'red');</script>");
		
		if(mb_strlen(strip_tags($msg))>2048) return("<script>show_wnd('Сообщение слишком длинное!',100,true,2.5,'red');</script>");
		if(mb_strlen(strip_tags($msg))<5 && mb_strlen($msg)<50) 
		return("<script>WINDOW.ShowMessage('Сообщение слишком короткое!',WINDOW.C.ERROR);</script>");
		$msg = str_replace(chr(13),'<br>',$msg);
		$msg = mysql_real_escape_string(format($msg));
		
		$id = $_SESSION["id"];
		$to = (int)$to;
		$cause = (int)$cause;
		$essence = (int)$essence;
		
			/* dialog check */
			$DIALOG_ID = -1;
			$res = $db->query("SELECT id FROM qmex_dialogs WHERE Cause=$cause AND EssenceID=$essence AND
								   ( (Member1=$id AND Member2=$to) OR (Member1=$to AND Member2=$id) )");
			if($res && $db->rowCount()==0) 
			{
				$res = $db->query("INSERT INTO qmex_dialogs(Cause, EssenceID, Member1, Member2) VALUES($cause, $essence, $id, $to)");
				if($res) 
				$res = $db->query("SELECT id FROM qmex_dialogs WHERE Cause=$cause AND 
								   EssenceID=$essence AND Member1=$id AND Member2=$to");
			}
			if($res) $DIALOG_ID = $db->one(0);
				
			$updated = time();
			$db->query("UPDATE qmex_dialogs SET updated=$updated WHERE ID='$DIALOG_ID'");
				
			/* creation of message */
			if($res)
			$res = $db->query("INSERT INTO qmex_dialogs_msg(dialog_id, Creator, Message, IsRead, AnswerOf) 
							   VALUES($DIALOG_ID, $id, '$msg', 0, $answer_on)");
								   
			if($res) {
				$text = 'Сообщение отправлено.';
				return("<input type='hidden' tag='send-success'><script>show_wnd('".$text."',100,true,2.5);</script>"); 
			}
			return("<script>show_wnd('Произошла неизвестная ошибка. Сообщение отправить не удалось...',100,true,2.5,'red');</script>");
		
		}
	
	
	public static function ShowDialogSign($ID)
	{
		$ID = (int)$ID;
		
		$iam = $_SESSION['id'];
		
		$db = new db();
		$db->query("SELECT d.Cause, d.EssenceID, d.Member1, d.Member2, d.DateTime, COUNT(m.id), d.updated FROM qmex_dialogs AS d 
					INNER JOIN qmex_dialogs_msg AS m ON d.id=$ID AND m.dialog_id=d.id AND m.IsRead=0 AND NOT m.Creator=$iam ");
		if($db->rowCount()==0) return '';
		
		$data = $db->one();
		$cause = $data[0];
		$essence = $data[1];
		$member1 = $data[2];
		$member2 = $data[3];
		$datetime = translate_date($data[4]);
		$new_messages = $data[5];
		$new_messages_class = $new_messages>0 ? 'ui-dialog-new-messages-more' : 'ui-dialog-new-messages-zero';
		if($new_messages>0) $new_messages = '+'.$new_messages;
		$updated = translate_date(date('Y-m-d H:i:s',$data[6]));
		
		$another = $_SESSION['id']==$member1 ? $member2 : $member1;
		$user_info = Human::getUserInfo($another);
		
		$theme = '';
		switch($cause)
		{
			case Communication::CAUSE_USEFUL_MAN : 
				$theme = 'Общение по интересам';
				break;
			case Communication::CAUSE_SWOP_CONNECTION: 
				$theme = SWOP::CreateSwopById($essence, true);
				break;
				case Communication::CAUSE_HUB_CONNECTION: 
				$theme = 'Хаб :<br>
				<a href="/note?id='.$essence.'" class="href" style="font-size:1em; color:#039"><b>'.Note::getNoteName($essence).'</b></a>';
				break;
			}
		
		ob_start();
		if(true): ?>
        
        
        <div class='ui-dialog-box' id='dialog-box-<?php echo $ID ?>'>
        <div class='ui-dialog-box-caption'>
        <table class="full-width">
        <tr><td>
        <?php echo HTML::image_tag($user_info['pic'], array('height'=>'30px')) ?>
        </td><td>
        Диалог с пользователем 
        <?php echo HTML::link_to('/profile?id='.$user_info['login'], $user_info['login'], array('style'=>"color:#039")) ?>
        </td>
        <td>
        <div class='ui-dialog-time-creation'><?php echo $datetime ?></div>
        </td></tr>
        </table>
       
        </div>
        <div class='ui-dialog-box-theme'>
        <table class="full-width" border="0">
        <tr><td width="70%">
        <div class='ui-dialog-time-last-activity'>Последняя активность: <?php echo $updated ?></div>
        <div class='ui-dialog-box-theme-word'>Тема диалога:</div><br>
        <div class='ui-dialog-theme-content'><?php echo $theme; ?></div>
        </td><td width="30%" align="center">
        <div class="ui-dialog-new-messages <?php echo $new_messages_class ?>">
			<?php echo $new_messages; ?><br>
        	<div class='ui-dialog-messages-count-word'>новых сообщений</div>
        </div>
        <div class="ui-dialog-enter"><a href='/dialogs?dialog=<?php echo $ID ?>'>
        <button class='qButton qHint' tag='Войти в диалог'>Перейти к диалогу</button>
        </div>
        </td></tr>
        </table>
        </div>
        <div class='ui-dialog-footer'>
        <div class='ui-dialog-tool ui-dialog-close-dialog' tag='<?php echo $ID ?>'>
        <img src="qmex_img/UI/remove.png" class="ui-dialog-tool-img">Закрыть диалог
        </div>
        </div>
        </div>
        
        
        <?php
		endif;
		
		$DIALOG = ob_get_contents();
		ob_clean();
		return $DIALOG;
		
		}
	
	
	public static function ShowDialogMessage($ID)
	{
		
		$db = new DB();
		$db->query("SELECT Creator, Message, IsRead, DateTime, AnswerOf, Thanked FROM qmex_dialogs_msg WHERE id=$ID");
		$data = $db->one();
		
		$iam = @$_SESSION['id'];
		$creator = $data[0];
		//$message = String::ReplaceLinks(splitter($data[1],120,0,false), '#009');
		$message = splitter($data[1],120,0,false);
		$isread  = $data[2];
		$date    = translate_date($data[3]);
		$answer_of = $data[4];
		$thanked = $data[5];
		
		$answer = '';
		if($answer_of!=-1) {
			$db->query("SELECT Message FROM qmex_dialogs_msg WHERE id=$answer_of");
			$msg= $db->one(0);
			$answer = mb_substr($msg,0,500);
			if(mb_strlen($msg)>500) $answer.='...';
		}
		
		unset($db);
		
		$is_owner = $iam == $creator; 
		$user_info = Human::getUserInfo($creator);
		
		$style = $creator==$iam ? '0 20% 0 0' : '0 0 0 20%';
		
		ob_start();
		if(true): ?>
        
        <div class='ui-message-wrapper' style="margin:<?php echo $style ?>">
        <div class='ui-message' id='ui-message-<?php echo $ID?>'>
        <div class='ui-message-caption'>
        <?php echo HTML::link_to(
			'/profile?id='.$user_info['login'], 
			HTML::image_tag($user_info['pic'], array('height'=>'20px')).' '.$user_info['login'].( $iam==$creator ? '( Я )':'' )
			)?>
        <div class="ui-message-time"><?php echo $date; ?></div>
        </div>
        <?php if($answer_of>0): ?>
         <div class='ui-message-answer'>
          <div class="ui-message-answer-word">Ответ на <?php echo $creator==$iam ? '' : 'ваше' ?> сообщение:</div>
          <div class='ui-message-answer-body'>
		  	<iframe srcdoc='<?php echo addFrameStyles($answer, 'message') ?>' 
        	class='message-frame' frameborder='0' height='0px' scroll='no' seamless></iframe>
          </div>
         </div>
        <? endif ?>
        <div class='ui-message-content'>
        	<iframe srcdoc='<?php echo addFrameStyles($message, 'message') ?>' 
        	class='message-frame' frameborder='0' height='0px' scroll='no' seamless></iframe>
        </div>       
        <div class="ui-message-tools">   
        <table>
        <tr>
        
        <?php if(!$is_owner):?>
            <td style="padding-right:10px"> 
            <div class="ui_fast_sender ui-message-tool" id='ui-msg-answer-<?php echo $ID?>' tag="<?php echo $ID?>">
            <img src="qmex_img/UI/write.png" class="ui_act_msg"><span class="ui_f_txt">Ответить</span> 
            </div>
            </td>
        <? endif; ?>       
        <?php if(!$is_owner && $isread==0):?>
            <td style="padding-right:10px">
            <div class="ui_check_msg ui-message-tool" id="uiCheckMessage_<?php echo $ID?>" tag='<?php echo $ID?>'>
            <img src="qmex_img/UI/check.png" class="ui_act_msg"><span class="ui_f_txt">Отметить, как прочитанное</span>
            </div>
            </td>
        <? endif; ?> 
        <?php if(!$is_owner && !$thanked):?>
            <td style="padding-right:10px">
            <div class="ui_graduate_msg ui-message-tool" id="ui-msg-gratitude<?php echo $ID?>" tag='<?php echo $ID?>'>
            <img src="qmex_img/UI/qcoins-black.png" class="ui_act_msg"><span class="ui_f_txt">Поблагодарить <b>(1 qCoin)</b></span>
            </div>
            </td>
        <?php elseif($thanked):?>
        	<td style="padding-right:10px">
            <div class="ui-message-thanked">
            <img src="qmex_img/UI/qcoins-black.png" class="ui_act_msg">
            <span class="ui_f_txt"><?php echo $is_owner ? 'Собеседник сказал вам "спасибо" !' : 'Вы поблагодарили собеседника' ?></span>
            </div>
            </td>
        <? endif; ?>        
        <?php if($is_owner):?>
            <td>
            <div class="ui_removing_msg ui-message-tool" id="uiRemoveMessage_<?php echo $ID?>" tag='<?php echo $ID?>'>
            <img src="qmex_img/UI/remove.png" class="ui_act_msg"><span class="ui_f_txt">Удалить</span>
            </div>
            </td>
        <? endif; ?>
        
        </tr>
        </table>
        
        <div style="display:none" class="ui_fast_message_block" id="ui_sent_bar_<?php echo $ID?>">
		<table cellspacing="5px" class="full-width" border="0">
		<tr>
		<td><textarea cols="50" rows="3" id="uiFastAnswer_<?php echo $ID?>" class="uiUserMessageBox" placeholder="сообщение"></textarea></td>
		<td>
        <div class="ui_type_buttons">
        <button class="qButton ui_typed_btn ui_fast_msg_sender" tag="<?php echo $ID?>">Отправить</button>
		<button class="qButton ui_typed_btn ui_expanded_mode"
        onclick="location.href='/dialogs?message&answer_of=<?php echo $ID?>'">Расширенный режим</button>
        </div>
        </td>	
		</tr>
		</table>
		</div>
        
        </div>
        </div>
        </div>
        
        <?php
		endif;
		
		$MSG = ob_get_contents();
		ob_clean();
		return $MSG;
		
		}
	
	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	
	public static function getInfoForMessage($type, $essence)
	{
		// caption
		// additional info (body)
		// creator( owner ) of essence
		
		$info = array();
		
		if(!in_array($type,Communication::$CAUSES) || $essence<0) return $info;
		
		switch($type){		
			case Communication::CAUSE_USEFUL_MAN: 
				$info = Communication::InfoCollection1($essence);
				break;
			case Communication::CAUSE_MSG_ANSWER: 
				$info = Communication::InfoCollection2($essence);
				break;
			case Communication::CAUSE_SWOP_CONNECTION: 
				$info = Communication::InfoCollection3($essence);
				break;	
			case Communication::CAUSE_HUB_CONNECTION:
				$info = Communication::InfoCollection4($essence);
				break;		
			}
		
		return $info;					
		}
	
	private static function InfoCollection1($essence)
	{
		$info = array();
		$info['name-to'] = Human::getLoginById($essence);	
		$info['id-to']   = $essence;
		$info['caption']    = 'Связаться с пользователем '.HTML::link_to('/profile?id='.$info['name-to'], $info['name-to']).
							  ' по поводу интересов, занятий и увлечений.';
		$info['additional'] = '';
		return $info;
		}
		
		
	private static function InfoCollection2($essence)
	{
		$db = new DB();
		$db->query("SELECT Creator, Message FROM qmex_dialogs_msg WHERE id=$essence");
		$data = $db->one();
		$creator = $data[0];
		$msg = strip_tags($data[1],'img,span');
		unset($db,$data);
		
		$info = array();
		$info['name-to'] = Human::getLoginById($creator);	
		$info['id-to']   = $creator;
		$info['caption']    = 'Написать ответ пользователю '.HTML::link_to('/profile?id='.$info['name-to'], $info['name-to']).
							  ' на сообщение:';
		$info['additional'] = HTML::div_block($msg,array('style'=>'background-color:white; padding:10px'));
		return $info;
		}
		
	private static function InfoCollection3($essence)
	{
		$db = new DB();
		$db->query("SELECT Login,SwopType FROM qmex_swop WHERE ID=$essence");
		$data = $db->one();
		$user = $data[0];
		$type = $data[1];
		unset($db);
		
		$info = array();
		$info['name-to'] = Human::getLoginById($user);	
		$info['id-to']   = $user;
		$info['caption']    = 'Связаться с '.HTML::link_to('/profile?id='.$info['name-to'], $info['name-to']).
							  ' по поводу '.($type==SWOP::SWOP_USER_NEED ? 'потребности' : 'предложения').' :';
		$info['additional'] = SWOP::CreateSwopById($essence, true);
		return $info;
		}
		
	private static function InfoCollection4($essence)
	{
		$db = new DB();
		$db->query("SELECT Login FROM qmex_user_notes WHERE ID=$essence");
		$user    = $db->one(0);
		unset($db);
		
		$info = array();
		$info['name-to'] = Human::getLoginById($user);	
		$info['id-to']   = $user;
		$info['caption']    = 'Связаться с '.HTML::link_to('/profile?id='.$info['name-to'], $info['name-to']).
							  ' по поводу хаба:';

		$info['additional'] = '<div style="background-color:white; padding:10px; color:#000; font-size:9pt">
		<a href="/note?id='.$essence.'"><b>'.Note::getNoteName($essence).'</b></a>
		</div>';
		
		return $info;
		}
	
	}


?>