<?php

// This Class is provides access to DateBase differents variants //
class db
{
	public $last_query = NULL;
    private $identifier = NULL;
	private $is_connected = false;
	
	public function __construct()
	{
		$this->connect();
		}
		
	function connect()
	{
		try
		{
			$is_connect = @mysql_pconnect(SysInfo::DB_HOST,SysInfo::DB_LOGIN,SysInfo::DB_PASSWORD);
			if(!$is_connect) throw new Exception("Cannot connect to Server ".SysInfo::DB_HOST);
			$this->is_connected = true;
			mysql_select_db(SysInfo::DB_NAME);
			////////////////////////////////////////////////////////////
			mysql_query('SET NAMES "utf8"');   
			mysql_query("SET CHARACTER SET 'utf8'");
			mysql_query("SET SESSION collation_connection = 'utf8_general_ci'");
			mysql_query("SET time_zone='".date('P')."'");     
			////////////////////////////////////////////////////////////   
			}catch(Exception $e)
			{
				$info = Logs::getError($e->__toString());
				Logs::writeLogs(C::LOG_ERROR,$info);
			}
		}
			
	function query($sql)
	{
		
		if($this->is_connected == true)
		{
		$args = func_get_args();
		$arg = array_shift($args);
		$this->last_query = $arg;
		
		$this->identifier = mysql_query($arg);
		
		}
		return ($this->identifier==true) ? $this->identifier:false;
		}
		
	function one($index = -1)
	{
		if($this->identifier==NULL || $this->identifier==false) return NULL;
		$array = mysql_fetch_array($this->identifier);
		return ($index==-1) ? $array : $array[$index];
		}
		
	function rowCount($identify = NULL)
	{
		return ($identify==NULL) ? mysql_num_rows($this->identifier):mysql_num_rows($identify);	
		}
			
	function lastError()
	{
		return mysql_error();
		}
	
	function lastErrorCode()
	{
		return mysql_errno();
		}
	
	}

?>