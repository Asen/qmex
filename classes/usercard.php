<?php

class usercard
{
	private $data = array();
	private $curr_id = 0;
	
	function __construct($dt = NULL){
		if($dt!=NULL) $this->data = $dt;
		}
			
	function createSearchCard($dt = NULL)
	{
		if($dt!=NULL) $this->data = $dt;
		$login = $this->data['login'];
		$login_m = splitter($this->data['login_m'],30);
		$target_id = $this->data['target_id'];
		$photo = $this->data['photo'];
		$current_business = splitter($this->data['current_business'],30);
		$name = String::mb_ucfirst($this->data['name']); 
		$sename = String::mb_ucfirst($this->data['sename']);
		$city = String::mb_ucfirst($this->data['city']);
		$country = String::mb_ucfirst($this->data['country']);
		$caption = $this->data['caption'];
		$rating = (int)$this->data['raiting'];
		$qmex_position = (int)$this->data['qmex_position'];
		
		$rait_box_class = $rating>0 ? 'UI-good-rait-box' : 'UI-bad-rait-box';
		if($rating==0) $rait_box_class = 'UI-zero-rait-box';
		
		$interests = $this->data['interests'];
		$age = $this->data['age'];
		$prof = splitter($this->data['prof'],40);
		
		$target_display = '';
		$db = new DB();
		$db->query("SELECT Substance,Meaning FROM qmex_user_notes WHERE ID=".$target_id);
		$is_exists = $db->rowCount()>0;
		$data = $db->one();
		$substance = mb_substr(strip_tags($data[0]),0,15);
		$meaning = mb_substr(htmlspecialchars($data[1]),0,15);
		$intent = (mb_strlen($meaning)>0) ? $meaning : $substance;		
		$tag = ($is_exists && $target_id>0) ? '<span style=\'color:white\'>Основная цель/намерение:</span><hr>'.
		trim(mb_substr(strip_tags($data[0]),0,95)).'...' : 'Основная цель не определена.';		
		$target_display .= '<div class="qHint" tag="'.$tag.'" style="font-size:0.8em; font-family:Arial, Helvetica, sans-serif; background-color:#069; width:100%; box-sizing:border-box; -moz-box-sizing:border-box; text-align:left; padding:8px">';		
		$target_display .= ($target_id>=0) ? '<img src="qmex_img/UI/hubster/idea-curr.png" style="float:left; padding-right:5px" width="20px"><a href="note?id='.$target_id.'" style="color:white; font-weight:bold">'.$intent.'...</a>' : '<img src="qmex_img/UI/hubster/no-idea-curr.png" style="float:left; padding-right:5px" width="20px"><a href="#hub" style="color:white; font-weight:bold">Цель не установлена</a>';	
		$target_display .= '</div>';	
		
		
		$block_contents = "";
		$specializations = array();
		for($i=0;$i<=$interests->counter-1;$i++)
		{
			$i_none = false;
			$i_subval = $interests->spec_inter($i);
			$i_imain = $i_subval->interest;
			$i_rating = $interests->spec_inter($i)->rating;	
			$i_idi = $interests->spec_inter($i)->id;	
			$interest_id = $interests->spec_inter($i)->interest_id;
			array_push($specializations,$interest_id);

			$ikeys = array_filter(explode(" ",str_replace(","," ",$i_subval->key_words)));
			if(count($ikeys)==0) $i_none = true;
			$keys = 0;
			
			$keywords = "";
			if($i_none==false)
			{
				$j=0;
				foreach($ikeys as $key)
				{
				$keys+=3;
				$keywords .= "<span style='font-size:12px;'><a href='/search#keyinterest=".$interest_id."&keywords=".urlencode($key)."' style='color:#DDD'>".$key."</a>";
				if(++$j < count($ikeys)) $keywords .= " , ";
				$keywords .= "</span>";
				}
			}	
			
			$block = "
			<table class='UI-item' style='float:left; font-size:".(15)."px;' border=0 cellpadding='0' cellspacing='0'>
			<tr>
			<td class='UI-item-text'><a href='/search#keyinterest=".$interest_id."' style='color:white'>$i_imain</a></td>
			<td class='UI-item-rating qHint' tag=' ".$i_rating." подтверждений' align='center' r='".$i_rating."' idi='".$interest_id."'>".$i_rating."</td>
			</tr>
			<tr>
			<td colspan='2'><div class='UI-item-text-additional'>".$keywords."</div></td>
			</tr>
			</table>	
			";
			
			$block_contents.=$block;
			
		}
		
		// ... ... ... ... ... \\
		
		if(trim($prof)=='') $prof = Interests::DefineSpecialization($specializations);
		if(trim($block_contents)=='') $block_contents=' --- ';
		
		// ... ... ... ... ... \\
		
		$href = 'profile?id='.urlencode($login);
		
		$user_item = "
<table style='padding:10px' class='full-width' cellspacing='0'>
<tr class='search_single_user'>
<td width='25%' style='cursor:pointer;' valign='top'>".$target_display."
<a href='".$href."'><img class='u-face' src='$photo' width='100%'></a>
<div class='UI-rait-box ".$rait_box_class."'>$rating</div>
</td>		
<td valign='top' style='width:75%; cursor:pointer;' valign='top'>
<div style='padding:4px; margin-top:-10px; text-align:left; border-bottom:#069 solid 2px;'>
<table width='100%'>
<tr>
<td>
<span style='font-size:18px; font-weight:bold;'><a href='".$href."' title='$login' style='color:#069'>$login_m</a></span>
</td>
<td align=right valign=top width='25%'>
<div class='UI-user-qmex-position qHint' tag='Позиция актуальности пользователя среди всех'>
<span style='color:#A4CFFF'>qN:</span> ".$qmex_position."
</div>
</td>
</tr>
</table>
<div style='font-size:11px; color:#999; background-color:#EEE;'>
$caption
</div>
</div>

<div style='padding:4px; font-size:10pt; color:#BBB'>
<i>$current_business</i>
</div>

<div style='padding-left:20px'>
<div style='padding:4px; color:#008BCE; font-size:15px; border-bottom:1px solid #DDD'>
<b>$prof</b>
</div>
<div style='padding:4px; color:#008BCE; font-size:12px;'>
$country, $city
</div>
<div style='padding:4px; color:#008BCE; font-size:12px; border-bottom:1px solid #DDD'>
$name $sename <span style='color:#069'>&prime; <i>$age</i></span>
</div>
</div>

</td>
</tr>
<tr>
<td colspan='2' style='border:1px #069 dotted; padding:5px; font-size:15px' t>
<span style='font-weight:bold; font-size:12px; '>Интересы и занятия</span><br>
$block_contents
</td>
</tr>
</table>
";

return $user_item;
	}

	
}

?>