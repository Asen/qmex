<?php

class quer
{
	private $q_data;
	private $tool_bar;
	private $quer;
	private $db;
	
	public function __construct()
	{
		$this->db = new db();
		$this->q_data = array();
		}
	
	public function get($quer_id, $full_text = true)
	{
		$db= clone $this->db;
		$db_inner = clone $this->db;
		
		$queries = $db->query("SELECT ID, Owner, Caption, Text, Theme, Keywords, Country, City, Age_From, Age_To, DateTime_mk, Image, is_draft, ActualTo FROM qmex_quer WHERE ID=$quer_id ORDER BY ID DESC");
		if($db->rowCount()==0) { $this->quer=$this->error(); return; }
		$data = $db->one();
			
		$owner = (int)$data[1];
			
		$db_inner->query("SELECT Login FROM qmex_users WHERE ID=$owner");
		$login = $db_inner->one(); $login = trim(mb_strtolower($login[0]));

		$this->q_data = array('id'=>$data[0], 'owner'=>$owner, 'login'=>$login, 
			                  'caption'=>$data[2], 'text'=>$data[3], 'theme'=>$data[4],
							  'keywords'=>$data[5], 'city'=>$data[7], 'country'=>$data[6],
							  'age_from'=>$data[8], 'age_to'=>$data[9], 'date'=>$data[10],
							  'image'=>$data[11], 'is_draft'=>$data[12], 'actual_to'=>$data[13],
							  'is_split'=>(bool)$full_text);
								  
		$result = (int)$this->build();
		
		return $result;			 
		}
		
		
		public function build($note=NULL)
		{
			$data = ($note==NULL) ? $this->q_data:$note;
			if(empty($data)) return 0;
			
			
			$id = $data["id"];
			$owner = $data["owner"];
			$login = trim(mb_strtolower($data["login"]));	
			$caption = splitter(htmlspecialchars($data["caption"]),36);
			$text = $data["text"];
			$key = (int)$data["theme"];
			$IBASE = new Interests();
			$theme = $IBASE->SelectName($key);
			unset($IBASE);
			$keywords = htmlspecialchars($data["keywords"]);
			$country = htmlspecialchars($data["city"]);
			$city = htmlspecialchars($data["country"]);
			$age_from = (int)$data["age_from"];
			$age_to = (int)$data["age_to"];
			$dtmk = $data["date"];
			$image = htmlspecialchars($data["image"]);
			$is_draft = (int)$data["is_draft"];
			$actual_to = (int)$data["actual_to"];
			$full_text = (bool)$data["is_split"];
			
			if($is_draft==1 && $owner!=(int)@$_SESSION['id'])
			{
				 $this->quer = "<div style='padding:10px; color:#FFF; font-size:12px'>
				 Данный запрос недоступен. Автор переместил его в черновик.</div>";
				 
				 return 0;
			}
			
			if(trim($image)=="") $image = "/qmex_img/quer/needle.png";
			$keys=explode('|',$keywords);
			for($i=0;$i<count($keys);$i++) $keys[$i] = "<a href='quer?theme=".urlencode($keys[$i])."' style='color:#03F'>".$keys[$i]."</a>"; 
			$keys = implode(', ',$keys);
			
			// Extension of quer note //
			$lp = '';
			$more = '';
			if($full_text)
			{				
				if(mb_strlen($text)>300)
				{
					$lp='...';
					$more="<div style='float:right'><a href='quer?view=show&id=$id'><button class='qButton'>Читать дальше>></button></a></div><br><br>";
					}
				$start = 900;
				if(mb_strlen($text)>$start)
				{
					$start = mb_strpos($text,"</",900);
					$start = ($start==0) ? 900 : mb_strpos($text,">",$start);
				}
				$text = mb_substr($text,0,$start);
				$text.=$lp;
				$text=trim($text);
			}
			
			$text = addFrameStyles($text);
			
			///////////////////////////////////
			
			$top = 'Актуально до '.translate_date(date("Y-m-d H:i:s",$actual_to));
			if($country!='' && $city!='') $top.=" | Локация: ".$country.'/'.$city;else
			if($country!='' && $city=='') $top.=" | Локация: ".$country;else
			if($country=='' && $city!='') $top.=" | Локация: ".$city;
			
			if($age_from>0 or $age_to>0) $top.=" | Возраст: ";
			if($age_from==$age_to && $age_from>0) $top.=" $age_from ";else
			{
			if($age_from>0 && $age_to>0)  $top.=" от $age_from до $age_to";else
			if($age_from>0 && $age_to<=0) $top.="$age_from+";else 
			if($age_from<=0 && $age_to>0) $top.="до $age_to"; 
			}
			

			$this->quer = "<div class='ui-quer-query' id='quer_$id'>
			     %%s%%				 
				 <div class='ui-quer-caption'>
				 <div class='ui-quer-dt'>".$top."</div>
				 <table cellpadding='0px' cellspacing='5px' border='0'>
				 <tr>
				 <td rowspan='2' style='border-right:1px black dashed; padding-right:7px'><img src='".$image."'
				 onload='zoom(this,null,50);return false;'></td>
				 <td sty><div class='ui-quer-caption-top'><a href='quer?view=show&id=$id'>".$caption."</a></div></td>
				 </tr>
				 <tr>
				 <td><div class='ui-quer-caption-lower'><b>Создатель запроса : </b> <a href='/profile?id=".$login."' class='ui-quer-href'>$login</a></div></td>
				 </tr>
				 </table>
				 </div>
				 <div class='ui-quer-content'>
				 <iframe srcdoc='".$text."' class='quer-iframe' height='250px' width='100%' frameborder='0' scroll='no'></iframe>
				 <br><br>$more
				 <hr>
				 <div class='ui-quer-theme'><a href='quer?theme=".urlencode($key)."' t>$theme</a></div>
				 <div class='ui-quer-keys'>".$keys."</div>
				 </div>
				 </div>";
				 
				 
			$drafter = ($is_draft==1) ? 
			"<td class='ui-quer-from-draft qEdit' tag='Восстановить из черновика' point='$id'>
			<img src='qmex_img/UI/draft_from.png' width='15px' height='15px'>
			</td>"  :   
			"<td class='ui-quer-to-draft qEdit' tag='В черновик' point='$id'>
			<img src='qmex_img/UI/draft.png' width='15px' height='15px'>
			</td>";
				 
			$this->tool_bar = "";
			$user = (isset($_SESSION['id'])) ? $_SESSION['id']:-1; 
			if($user==$owner) 
			$this->tool_bar="
			<div class='ui-quer-tools'>
			<table cellpadding='0px' cellspacing='5px'>
			<tr>
			<td class='qEdit' tag='Редактировать'>
			<a href='quer?view=edit&query=$id'><img src='qmex_img/UI/write.png' width='15px' height='15px'></a>
			</td>
			<td class='ui-quer-remove qEdit' tag='Удалить' point='$id'>
			<img src='qmex_img/UI/remove.png' width='15px' height='15px'>
			</td>
			$drafter
			</tr>
			</table>
			</div>
			";
			
			return 1;
			} 
		
		private function error()
		{
			return '<br><div id="ErrMsg"><strong>Ошибка!</strong><br>Этот запрос был удален или никогда не существовал.</div>';
			}
		
		public function write()
		{
			echo str_replace("%%s%%",$this->tool_bar,$this->quer);
			}
			
		public function data()
		{
			return $this->q_data;
			}
	}

?>