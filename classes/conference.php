<?php

class conference{
	
	const NA_NOT_MEMBER = 1;
	const NA_NOT_STARTED = 2;
	const NA_WAS_CLOSED = 3;
	const OK_AVAILABLE = 4;
	
	public static function ShowConference($id)
	{
		$id = (int)$id;
		$DB = new DB();
		$DB->query("SELECT creator, datetime, description, is_private, plan_start, vector_interest, is_closed, img 
					FROM qmex_conferences AS conf WHERE ID=$id");
		$data = $DB->one();
		
		/*$DB->query("SELECT SUM(Voite) FROM qmex_voites 
					WHERE Type='conf_opinion' AND Essence IN (SELECT id FROM qmex_conferences_opinions WHERE conf_id=$id)");
		$raiting = $DB->one(0);*/
		
		$IBASE = new Interests();
		
		$creator = $data[0];
		$date    = $data[1];
		$descr   = str_replace("\n","<br>",$data[2]);
		$is_private = $data[3];
		$start_time = $data[4];
		$interest   = $data[5];
		$interest   = $IBASE->SelectName($interest);
		$now = qMexTime::getCurrent();
		$is_started = ($start_time==-1 || ($start_time - $now));
		$is_closed  = $data[6];
		$img = Conference::defineConferenceImage($data[7]);
			
		$type = ($is_private) ? 'Приватно' : 'Общедоступно';
		$class_prefix = ($is_private) ? 'private' : '';	
		if($is_closed){
			$class_prefix = 'closed';
			}
		
		if($start_time>0){ 
			$time_diff = ($now > $start_time) ? calculate_time_diff($now, $start_time) : 
												calculate_time_diff($start_time, $now) ;
			$time_diff = ($now > $start_time) ? 'Уже идет: '.$time_diff : 'Осталось: '.$time_diff;
			$time_color = ($now > $start_time) ? '#080':'#A00';
			$start_time = 'Начало: '.translate_timestamp($start_time).'<br><b>'.$time_diff.'</b>';
		} else $start_time = '';
		
		if(true): ?>
        
             
        <div class='ui_conference <?php echo $class_prefix; ?>'>
        <div class='ui_conference_caption'>
        <table width="100%">
        <tr>
        <td align="left">
       		Конференция <span class="ui_conference_number">№<?php echo $id ?></span>
        	<?php if($is_closed): ?><div class='ui_conference_closed'>(конференция закрыта)</div><?php endif ?>
        </td>
        <td align="center">
        	<div class='ui_conference_vector'><?php echo $interest; ?></div>
        </td>
        <td align="right">
        	<a href='talks?conf=<?php echo $id; ?>'>
            <button class='<?php echo $is_closed ? 'qButton_silver' : 'qButton'?>'>Вступить</button>
            </a>
        </td>
        </tr>
        </table>
        </div>
        <div class='info-caption'>
        <table>
        <tr>
        <td valign="top"><div class='ui_conference_sub'>-<?php echo $type ?>-</div></td>
        <td style="padding-left:10px; color:<?php echo $time_color; ?>"><div class='ui_conference_time'><?php echo $start_time ?></div></td>
        </tr>
        </table>
        </div>
        <table cellspacing="5px" border=0>
        <tr>
        <td valign="top">
        	<img src="<?php echo $img ?>" width="50px" height="50px" />
        </td>
        <td valign="top">
        	<div class='ui_conference_description'><b><u>О конференции:</u></b><br><?php echo $descr ?></div>
        </td>
        </tr>
        </table>
        </div>
           
        
        <? endif;
		
		}
		
	public static function defineConferenceImage($img)
	{
		return mb_strlen(trim($img))==0 ? 'qmex_img/UI/talks/undefined.png' : $img;
		}
		
	public static function getConferenceInfo($conf_id)
	{
		$conf_id = (int)$conf_id;
		$DB = new DB();
		$DB->query("SELECT description,creator,is_private,plan_start,vector_interest,img,words,linked_with_type,linked_with_id 
					FROM qmex_conferences WHERE ID=$conf_id");
		$data = $DB->one();
		return array('idea'=>$data[0], 
					 'creator'=>$data[1], 
					 'is_private'=>$data[2],
					 'plan_start'=>$data[3],
					 'vector'=>$data[4],
					 'img'=>Conference::defineConferenceImage($data[5]),
					 'words'=>$data[6],
					 'linked_with_type'=>$data[7],
					 'linked_with_id'=>$data[8],
					 'is_exists'=>$DB->rowCount()>0);
		}
		
	public static function getConferenceParticipantsCount($conf_id)
	{
		$conf_id = (int)$conf_id;
		$DB = new DB();
		$DB->query("SELECT id FROM qmex_conferences_opinions WHERE conf_id=$conf_id GROUP BY creator");
		return $data = $DB->rowCount();
		}
		
	public static function checkAvailability($conf_id, $user=-1)
	{
		if($user==-1 && isset($_SESSION['id'])) $user = $_SESSION['id']; else return false;
		$conf_id = (int)$conf_id;
		$user = (int)$user;
		$DB = new DB();
		$DB->query("SELECT plan_start,is_closed,linked_with_type FROM qmex_conferences WHERE id=$conf_id AND 
				   ( is_private=0 OR creator=$user OR (members LIKE '%;$user;%'))");
		$data = $DB->one();
		if($data[1]==1)        				   return Conference::NA_WAS_CLOSED;
		if($DB->rowCount()==0) 				   return Conference::NA_NOT_MEMBER;
		if($data[0]>qMexTime::getCurrent())    return Conference::NA_NOT_STARTED;
		return Conference::OK_AVAILABLE;
		}
		
	public static function isMembershipRequired($conf_id, $user=-1)
	{
		if($user==-1 && isset($_SESSION['id'])) $user = $_SESSION['id']; else return false;
		$conf_id = (int)$conf_id;
		$user = (int)$user;
		$DB = new DB();
		$DB->query("SELECT COUNT(*) FROM qmex_conferences WHERE ID=$conf_id AND (members_not_joined LIKE '%;$user;%')");
		return $DB->one(0)>0; 
		}
		
	public static function ShowOpinion($opinion_id)
	{
		$opinion_id = (int)$opinion_id;
		$DB = new DB();
		
		$DB->query("SELECT conf_id, creator, content, datetime FROM qmex_conferences_opinions WHERE id=$opinion_id");
		$data = $DB->one();
		$conf_id = $data[0];
		$creator = $data[1];
		$userinfo = Human::getUserInfo($creator);
		$conference_info = Conference::getConferenceInfo($conf_id);
		$login =  $userinfo['login'];
		$userpic = $userinfo['pic'];
		$is_owner = isset($_SESSION['id']) && $_SESSION['id']==$creator;
		$essence = $data[2];
		$datetime = translate_date($data[3]);
		
		$DB->query("SELECT COUNT(Voite) FROM qmex_voites WHERE essence=$opinion_id AND Type='conf_opinion' AND Voite=-1 ");
		$dislikes = $DB->one(0);
		$DB->query("SELECT COUNT(Voite) FROM qmex_voites WHERE essence=$opinion_id AND Type='conf_opinion' AND Voite=1 ");
		$likes = $DB->one(0);
		$rait = $likes-$dislikes;
		$color = $rait>0 ? "#339900" : "#933";
		$rait_sign = $rait>0 ? '+' : '';
		$class = $creator==$conference_info['creator'] ? 'creator-self' : '';
		
		if(true): ?>
                
        <div class='ui_conference_opinion <?php echo $class; ?>' tag='<?php echo $opinion_id?>'>
        <div class='ui_conference_header'>
        <table class="full-width" border=0><tr>
        <td width="50px"><img src='<?php echo $userpic; ?>' height="40px" width="40px"></td>
        <td>
        <div class='ui_conference_user'>
        <a href='/profile?id=<?php echo $login ?>'><?php echo String::mb_ucfirst($login); ?></a>
        </div>
        <div class='ui_conference_datetime'><?php echo $datetime; ?></div>
        </td><td align="right">
        <table>
        <tr>
			<?php if(!$is_owner):?>
            <td>
            <img src='qmex_img/UI/talks/like.png' class='ui_conference_opinion_tool opinion-like qHint' 
            tag='Одобрить' oid='<?php echo $opinion_id?>' by='<?php echo $creator?>'>
            </td>
            <?php endif; ?>
        <td>
        <div class='ui_conference_opinion_raiting qHint' style='color:<?php echo $color; ?>' id='conf-rait-<?php echo $opinion_id ;?>'
        	tag='<img src="qmex_img/UI/talks/like.png" width="10px"> <?php echo $likes?> 
             	| <img src="qmex_img/UI/talks/dislike.png" width="10px"> <?php echo $dislikes?>'>
		<?php echo $rait_sign.$rait; ?>
        </div>
        </td>
			<?php if(!$is_owner):?>
            <td>
            <img src='qmex_img/UI/talks/dislike.png' class='ui_conference_opinion_tool opinion-dislike qHint' 
            tag='Отвергнуть' oid='<?php echo $opinion_id?>' by='<?php echo $creator?>'>
            </td>
            <?php endif; ?>
        </tr>
        </table>
        
        </td>
        </tr></table>
        </div>
        <div class='ui_conference_content'>
        <iframe srcdoc='<?php echo addFrameStyles($essence, 'conference'); ?>' 
        class='conference-frame' frameborder='0' height='0px' style="max-height:450px" scroll='no' seamless></iframe>
        </div>
        </div>   
        
        <? endif;
		
		}
		
		
	public static function EnvRoutine($user, $added_user)
	{
		$db = new DB();
		
		$conf_id = -1;
		$db->query("SELECT id FROM qmex_conferences WHERE linked_with_type='env' AND linked_with_id=$user");
			if($db->rowCount() == 1) $conf_id = $db->one(0); 
			else{
				$db->query("INSERT INTO qmex_conferences(creator, description, img, is_private, vector_interest, words, 
							linked_with_type, linked_with_id) VALUES($user, 'Общение окружения', '', 1, -1, '', 'env', $user)");
				$db->query("SELECT id FROM qmex_conferences WHERE linked_with_type='env' AND linked_with_id=$user");
				$conf_id = $db->one(0);
			}
			$member = ';'.$added_user.';';
			$db->query("UPDATE qmex_conferences SET members=CONCAT(members,'$member') WHERE id=$conf_id");		
		}
	
		
	public static function printJSOpinionRoutine()
	{
		if(true): ?>
        <script src="/tools/class-routine/conference.js" language="javascript"></script>       
        <? endif;
		}
			
	}

?>