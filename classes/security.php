<?php


class security{
	
	const ERROR_PAGE = '/error';
	const DENIED_PAGE = '/denied';
	
	public static function isAuthorized()
	{
		return isset($_SESSION['entered']) && isset($_SESSION['id']);
		}
		
	public static function isCurrentAccountChecked()
	{
		if(!Security::isAuthorized()) return false;
		$user_id = (int)$_SESSION['id'];
		$db = new DB();
		$db->query("SELECT account_checked FROM qmex_users WHERE ID=$user_id");
		return (boolean)$db->one(0);
		}
	
	public static function checkAuthority($another_id)
	{
		return (Security::isAuthorized() && $_SESSION['id']==$another_id);
		}
		
	public static function checkAccess()
	{
		if(!Security::isAuthorized()) 
			die('<script>location.href="'.Security::DENIED_PAGE.'"</script>');
		}
	
	public static function SaveUserEntrance($user_id)
	{
		$user_id = (int)$user_id;
		$ip = $_SERVER['REMOTE_ADDR'];
		$db = new DB();
		$db->query("UPDATE qmex_users SET must_enter=1, last_ip_enter='$ip' WHERE ID=$user_id");
		echo '<script>document.cookie="__auto='.base64_encode($user_id).'; path=/; expires="+
			  (new Date( new Date().getTime() + 1000*3600*24*7 )).toUTCString()</script>';
		}
		
	public static function RemoveUserEntrance($user_id)
	{
		$user_id = (int)$user_id;
		$db = new DB();
		$db->query("UPDATE qmex_users SET must_enter=0, last_ip_enter='' WHERE ID=$user_id");
		foreach($_SESSION as $key=>$val) unset($_SESSION[$key]);
		echo '<script>document.cookie="__auto=0; path=/; expires="+(new Date(0)).toUTCString()</script>';
		}
	
	public static function GetEntranceInfo()
	{
		if(!isset($_COOKIE["__auto"])) 
			return array('login'=>'', 'password'=>'', 'checked'=>'');
			
		$user_id = base64_decode($_COOKIE["__auto"]); 
		$ip = $_SERVER['REMOTE_ADDR'];
		$db = new DB();
		$db->query("SELECT Login, Password, must_enter FROM qmex_users WHERE ID=$user_id AND must_enter=1 AND last_ip_enter='$ip'");
		if($db->rowCount()==0)
			return array('login'=>'', 'password'=>'', 'checked'=>'');
			
		$data = $db->one();
		return array('login'=>$data[0], 'password'=>$data[1], 'checked'=>'checked');	
	}

}

?>