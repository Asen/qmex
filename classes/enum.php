<?php

class enum{
	
	public static $SEXES = 
		array("М", "Ж");
	public static $MONTHES = 
		array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
	
	public static $CONCEPTIONS = array('INTENT'=>1,'INFO'=>2,'PROJECT'=>3,'SUCCESS'=>4,'NEWS'=>5);	
	public static function Conceptions($LANGID=SYSINFO::LANGID_RU)
	{
		$conceptions = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$conceptions[ Enum::$CONCEPTIONS['INTENT'] ] = 'Цель | Намерение';
			$conceptions[ Enum::$CONCEPTIONS['INFO'] ] = 'Полезная информация'; // "Рассуждения" ?
			$conceptions[ Enum::$CONCEPTIONS['PROJECT'] ] = 'о Проекте | об Идее';
			$conceptions[ Enum::$CONCEPTIONS['SUCCESS'] ] = 'Совет';            // "Истории"  ?
			$conceptions[ Enum::$CONCEPTIONS['NEWS'] ] = 'Новость';
			}
		return $conceptions;
		}
		
	public static function ConceptionsWords($LANGID=SYSINFO::LANGID_RU)
	{
		$conceptions = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$conceptions[ Enum::$CONCEPTIONS['INTENT'] ] = 'Цели && Намерения';
			$conceptions[ Enum::$CONCEPTIONS['INFO'] ] = 'Полезная информация'; 
			$conceptions[ Enum::$CONCEPTIONS['PROJECT'] ] = 'Проекты && Идеи';
			$conceptions[ Enum::$CONCEPTIONS['SUCCESS'] ] = 'Советы';
			$conceptions[ Enum::$CONCEPTIONS['NEWS'] ] = 'Новости';
			}
		return $conceptions;
		}
		
	public static function ConceptionsCharacters($LANGID=SYSINFO::LANGID_RU)
	{
		$conceptions = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$conceptions[ Enum::$CONCEPTIONS['INTENT'] ] = 'Целеустремленность';
			$conceptions[ Enum::$CONCEPTIONS['INFO'] ] = 'Информативность';
			$conceptions[ Enum::$CONCEPTIONS['PROJECT'] ] = 'Креативность';
			$conceptions[ Enum::$CONCEPTIONS['SUCCESS'] ] = 'Опыт';
			$conceptions[ Enum::$CONCEPTIONS['NEWS'] ] = 'Новостная активность';
			}
		return $conceptions;
		}
	
	/* ---------------------------------------------------------------------------------------- */
	
	public static $SWOP_SPECTRA = array('SERVICE'=>1, 'COMMUNICATION'=>2, 'COOPERATION'=>3, 
										'JOINACTIVITY'=>4, 'RESOURCES'=>5, 'PRODUCTION'=>6);
										
	public static function SwopSpectra($LANGID=SYSINFO::LANGID_RU)
	{
		$swops = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$swops[ Enum::$SWOP_SPECTRA['SERVICE'] ] = 'Услуга';
			$swops[ Enum::$SWOP_SPECTRA['COMMUNICATION'] ] = 'Общение';
			$swops[ Enum::$SWOP_SPECTRA['COOPERATION'] ] = 'Сотрудничество';
			$swops[ Enum::$SWOP_SPECTRA['JOINACTIVITY'] ] = 'Совместная деятельность';
			$swops[ Enum::$SWOP_SPECTRA['RESOURCES'] ] = 'Средства и ресурсы';
			$swops[ Enum::$SWOP_SPECTRA['PRODUCTION'] ] = 'Продукция';
			 
			}
		return $swops;
		}
		
	public static function SwopClientsSentencies($LANGID=SYSINFO::LANGID_RU)
	{
		$swops = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$swops[ Enum::$SWOP_SPECTRA['SERVICE'] ] = 'Кому были бы полезны услуги, связанные с ';
			$swops[ Enum::$SWOP_SPECTRA['COMMUNICATION'] ] = 'Кто ищет общения на тему ';
			$swops[ Enum::$SWOP_SPECTRA['COOPERATION'] ] = 'Кто ищет сотрудничества, касающегося специализации ';
			$swops[ Enum::$SWOP_SPECTRA['JOINACTIVITY'] ] = 'Кто ищет людей для совместной деятельности на тему ';
			$swops[ Enum::$SWOP_SPECTRA['RESOURCES'] ] = 'Кто ищет средства/ресурсы для реализации идей, связанных с ';
			$swops[ Enum::$SWOP_SPECTRA['PRODUCTION'] ] = 'Кому требуется продукция, связанная с ';
			 
			}
		return $swops;
		}
		
	public static function SwopHelpersSentencies($LANGID=SYSINFO::LANGID_RU)
	{
		$swops = array();
		if($LANGID==SYSINFO::LANGID_RU){ 
			$swops[ Enum::$SWOP_SPECTRA['SERVICE'] ] = 'Кто может оказать услуги, связанные с ';
			$swops[ Enum::$SWOP_SPECTRA['COMMUNICATION'] ] = 'Кому интересно общаться на тему ';
			$swops[ Enum::$SWOP_SPECTRA['COOPERATION'] ] = 'Кто предлагает сотрудичество, связанное с ';
			$swops[ Enum::$SWOP_SPECTRA['JOINACTIVITY'] ] = 'Кто заинтересован в совместной деятельности на тему ';
			$swops[ Enum::$SWOP_SPECTRA['RESOURCES'] ] = 'Кто мог бы предоставить средства/ресурсы для реализации идей, связанных с ';
			$swops[ Enum::$SWOP_SPECTRA['PRODUCTION'] ] = 'Кто предоставляет продукцию, связанную с ';
			 
			}
		return $swops;
		}
		
		
	/* ---------------------------------------------------------------------------------------- */
	
	public static $DEVELOPMENT_TYPES = array('MEETING'=>1, 'COMPETITION'=>2, 'OCCUPATION'=>3, 'TALKING'=>4, 
										'PRESENTATION'=>5, 'MASTER'=>6);
		
	public static function DevelopmentTypes($LANGID=SYSINFO::LANGID_RU)
	{
		$dev_types = array();
		if($LANGID==SYSINFO::LANGID_RU){ 			
			$dev_types[ Enum::$DEVELOPMENT_TYPES['MEETING'] ] = 'Встреча';
			$dev_types[ Enum::$DEVELOPMENT_TYPES['COMPETITION'] ] = 'Достижения';
			$dev_types[ Enum::$DEVELOPMENT_TYPES['OCCUPATION'] ] = 'Занятие';
			$dev_types[ Enum::$DEVELOPMENT_TYPES['TALKING'] ] = 'Разговор';
			$dev_types[ Enum::$DEVELOPMENT_TYPES['PRESENTATION'] ] = 'Презентация';
			$dev_types[ Enum::$DEVELOPMENT_TYPES['MASTER'] ] = 'Мастер-класс';			
			}
		return $dev_types;
		}
		
	}

?>