<?php

class swop{
	
	const SWOP_USER_NEED     = 1;
	const SWOP_USER_PROVIDE  = 2;
	
	public static $TYPES = array( SWOP::SWOP_USER_NEED, 
								  SWOP::SWOP_USER_PROVIDE );
	public static $RESTRICTIONS = array( SWOP::SWOP_USER_NEED => 7 ,  
										 SWOP::SWOP_USER_PROVIDE => 15);
										 
	public static function GetLastUserSwop($uid)
	{
		$uid = (int)$uid;
		$DBH = new DB();
		$DBH->query("SELECT ID FROM qmex_swop WHERE Login=$uid ORDER BY ID DESC LIMIT 1");
		return $DBH->one(0);	
		}				
									 
	public static function CreateSwopById($swopID, $is_return_swop = false)
	{
		$DBH = new DB();
		$DBH->query("SELECT Login, SwopType, ServiceType, Specialization, Definition, Date as LASTID, ID, SwopCore 
					 FROM qmex_swop WHERE ID=$swopID");
					 
		if($DBH->rowCount()==0) {
			$msg = 'Своп был удален.';
			if($is_return_swop) return $msg;
			else echo $msg;
			return;
			}
					 
		$SWOP = $DBH->one();		
		
		$iam = isset($_SESSION['id']) ? $_SESSION['id'] : -1;
		$owner = $SWOP[0];
		$is_owner = $owner==$iam;
		$swop_type = $SWOP[1];
		$service = Enum::SwopSpectra();
		$service = $service[$SWOP[2]];
		$IBA = new Interests();
		$spec = $IBA->SelectName($SWOP[3]);
		$definition = splitter(htmlspecialchars($SWOP[4]),35,0,true);
		$definition = str_replace("\n","<br>",$definition);
		$definition = String::ReplaceLinks($definition);
		$date = translate_date($SWOP[5]);
		$id = $SWOP[6];
		$swopcore = htmlspecialchars($SWOP[7]);				
		
		$DBH->query("SELECT id FROM qmex_conferences WHERE linked_with_type='swop' AND linked_with_id=".$id);
		$connected_talk = $DBH->one(0);
		$is_swop_has_talk = $connected_talk>1;
		
		$class_name = ($swop_type==SWOP::SWOP_USER_NEED) ? 'ui_swop-need' : 'ui_swop-provide';
		$from_date_label = ($swop_type==SWOP::SWOP_USER_NEED) ? 'с' : 'от';
		
		/* ---------------------------------------------------------------------------------- */
		/* ---------------------------------------------------------------------------------- */
		/* ---------------------------------------------------------------------------------- */
		ob_start();
		if(true): ?>
        
        
        <div class='ui_swop <?php echo $class_name ?>' id='swop-<?php echo $id ?>'>
        <div class='ui_swop_caption'>
        <table width="100%" cellspacing="0" cellpadding="0">
        <tr><td>
        <span class='ui-swop-service'><?php echo $service; ?></span> → 
        <span class='ui-swop-spec'><?php echo $spec; ?></span>
        </td><td align="right" valign="top">
        <span class='ui_swop-remove'>
		<?php if($is_owner): ?>
        	<img src="qmex_img/UI/swop/remove.png" class="ui_swop-tool ui_swop_remove qHint" 
        	sid="<?php echo $id; ?>" tag="Удалить" height="15px" />
        <?php endif; ?>
        </span>
        </td></tr>
        <?php if($swopcore!=''):?>
            <tr>
            <td colspan="2">
            <div class='ui_swop_core'> <span style="font-size:13px">→</span> [ <?php echo $swopcore ?> ]</div>
            </td>
            </tr>
        <? endif ?>
        </table>
        </div>
        <div class='ui_swop_definition'><?php echo $definition; ?></div>
        <div class="ui_swop_footer">
		<?php echo $from_date_label.' '.$date; ?>
        <span class='ui_swop-tools'>
        
        
        <?php if($is_swop_has_talk): ?>
        	<a href='/talks?conf=<?php echo $connected_talk; ?>'><img src="qmex_img/UI/swop/linked-conference.png" class="ui_swop-tool qHint" 
        	id="swop-conf-link-<?php echo $id; ?>" tag="Перейти к обсуждению" height="15px" /></a>
        <?php endif; ?>
          
          
        <?php if($is_owner): ?>
        <?php if(!$is_swop_has_talk):?>
        	<img src="qmex_img/UI/swop/connect_to_conf.png" class="ui_swop-tool ui_swop_link_conf qHint" 
        	sid="<?php echo $id; ?>" tag="Привязать обсуждение" height="15px" />
        <?php elseif($is_swop_has_talk): ?>
        	<img src="qmex_img/UI/swop/disconnect_conf.png" class="ui_swop-tool ui_swop_unlink_conf qHint" 
        	sid="<?php echo $id; ?>" tag="Отвязать привязанное обсуждение" height="15px" />
        <?php endif; ?>
        <?php endif; ?>
        
        
        <? if(!$is_owner && $swop_type==SWOP::SWOP_USER_PROVIDE): ?>
        <img src="qmex_img/UI/swop/connect.png" class="ui_swop-tool swop-connect-with-user qHint" 
        tag="Принять предложение" height="15px" sid="<?php echo $id; ?>" />
        <? elseif(!$is_owner && $swop_type==SWOP::SWOP_USER_NEED): ?>
        <img src="qmex_img/UI/swop/connect.png" class="ui_swop-tool swop-connect-with-user qHint" 
        tag="Сделать предложение" height="15px" sid="<?php echo $id; ?>" />
        <? endif ?>
        
        <?php if($swop_type==SWOP::SWOP_USER_NEED):?>
        <img src="qmex_img/UI/swop/need_who.png" class="ui_swop-tool swop-who-can-help qHint" 
        tag="У кого есть предложения на этот счет?" height="15px" sid="<?php echo $id; ?>" />
        <? elseif($swop_type==SWOP::SWOP_USER_PROVIDE): ?>
        <img src="qmex_img/UI/swop/provide_who.png" class="ui_swop-tool swop-who-is-needed qHint" 
        tag="Кто в этом заинтересован?" height="15px" sid="<?php echo $id; ?>" />
        <? endif ?>
        
        </span>
        </div>
        </div>
        
        <?php 
		endif;
		$SWOP = ob_get_contents();
		ob_clean();
		/* ---------------------------------------------------------------------------------- */
		/* ---------------------------------------------------------------------------------- */
		/* ---------------------------------------------------------------------------------- */
		
		if($is_return_swop) return $SWOP; else 
							echo   $SWOP;
		
		}
	
	
	public static function PrintJsRoutine()
	{
		if(true): ?>        
        <script src='/tools/class-routine/swop.js' language="javascript"></script>
        <script>
			SWOP.cause_swop_connection = <?php echo Communication::CAUSE_SWOP_CONNECTION ?>;
			SWOP.Main();
		</script>        
        <? endif;
		}

	
	}

?>