<?php

class development{
	
	public static function Show($dev_id)
	{
		$db = new DB();
		$dev_id = (int)$dev_id;
		
		$db->query("SELECT creator,caption,description,theme,keywords,country,city,picture,actual_time,event_kind,finish    
					FROM qmex_developments WHERE id=$dev_id");
		
		$data = $db->one();
		$creator = $data[0];
		$caption = $data[1];
		$description = $data[2];
		$theme = $data[3];
		$keywords = implode(', ', explode(' ',$data[4]));
		$country = $data[5];
		$city = $data[6];
		$picture = $data[7];
		$actual_time = $data[8];
		$kind = $data[9];
		$finish = $data[10];
		$color = $finish > qMexTime::getCurrent() ? '#063':'#933';
		
		$IBASE = new Interests();
		$IBASE->SelectIBASE();
		
		$EVENT_TYPES = Enum::DevelopmentTypes();
		
		
		if(true): ?>
        
        	<div class="full-width ui-developments-main-box">
            
            <img class='dev-cover' tag='<?php echo $dev_id; ?>' src='<?php echo $picture?>' style="min-height:100px" width="100%" />
            <div id='dev-caption-<?php echo $dev_id; ?>' class='dev-caption ui-development-caption'>
            	<?php if($kind>0): ?>
                	<div class='ui-developments-kind'>
                    	-<?php echo $kind>0 ? $EVENT_TYPES[$kind] : '' ?>-
                    </div>
                <?php endif; ?>	
				<?php echo $caption; ?>
            </div>
            <div class='ui-development-info'>
            	<div class="ui-developments-param" style="color:#035; font-weight:bold ">
                	<span style="font-size:12px; font-weight:normal"><?php echo $country ?></span>,
					<span style="font-size:17px;"><?php echo $city;  ?></span>
                </div>
				<div class="ui-developments-param" style='background-color:<?php echo $color?>;; opacity:0.5 ; padding:2px'>
                    <span style='color:#EEE'>
					<?php echo Development::getReadableTimeResidue($actual_time, $finish) ?>
                    </span>
                </div>   
                <div style="border-top:1px solid #069; margin:3px 0 3px 0"></div>   
                <div class='ui-developments-theme'><?php echo $IBASE->SelectName($theme) ?></div> 
                <div class='ui-developments-keys'><?php echo $keywords ?></div>    	
            </div>
            
            </div>
        
        <? endif;
		
		/*
		if(true): ?>
        
        <div class='developments-main-box'>
            <table cellspacing="0" border="0" class="full-width">
                <tr>
                	<td colspan="2">                   	
					<div class="developments-caption">
						<?php echo $caption; ?>
                        <?php if($kind>0): ?>
                        	<div class='developments-kind'>-<?php echo $kind>0 ? $EVENT_TYPES[$kind] : '' ?>-</div>
                        <?php endif; ?>							                                                   	
                    </div>	
                    </td>
                    <td rowspan="10" valign="middle" class='developments-comein' goto="<?php echo $dev_id ?>">
                        <div class='ui_development_go_inside_icon'></div>
                        <div style="color:#06F">интересно</div>
                    </td>
                </tr>
            	<tr>
                    <td width="25%" valign="top">
                   	 	<div style="background-color:#069">
                        <img src='<?php echo $picture?>' width="100%">
                        <div style="padding:10px">
                        <div class='developments-theme'><?php echo $IBASE->SelectName($theme) ?></div>
                        <div class='developments-keys'><?php echo $keywords ?></div> 
                        </div>
                        </div>
                    </td>
                    <td width="75%" valign="top" style="background-color:#EEE">                    
                    <div style="color:#038">
                    	<table cellspacing="10px">
                        	<tr>
                            	<td>
                                <div class="developments-param" style="font-size:1.2em;color:#036">
                                	<span style="font-size:14px; font-weight:normal"><?php echo $country ?></span> , 
									<?php echo $city  ?>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                	<div class="developments-param">
                                		<span style="font-size:15px"><?php echo translate_timestamp($actual_time) ?></span> —
                                        <?php echo translate_timestamp($finish) ?><br>
                                        <span style='color:<?php echo $color?>'>
											<?php echo Development::getReadableTimeResidue($actual_time, $finish) ?>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>                                      
                    </td>
                </tr>
            </table>
        </div>
        
		<? endif;
		*/
		}
	
	public static function generatePlaceMark($dev_id)
	{
		$db = new DB();
		$dev_id = (int)$dev_id;
		
		$db->query("SELECT caption,latitude,longitude,description,picture,theme,event_kind     
					FROM qmex_developments WHERE id=$dev_id");
		$data = $db->one();
		$caption = $data[0];
		$latitude = $data[1];
		$longitude = $data[2];
		$description = $data[3];
		$picture = $data[4];
		$theme = Interests::getName($data[5]);
		$event_kind = $data[6];
		$eventTypes = Enum::DevelopmentTypes();
		$point = array($latitude, $longitude);
		
		return array('coords' => $point,
					 'hint' => $caption."<br>(Нажмите для просмотра)",
					 'description' => '<div style="color:#069; font-size:11px">'.
					 				  '-'.$eventTypes[$event_kind].' | '.$theme.'-'.
									  '&nbsp;&nbsp;&nbsp;&nbsp;<a href="/developments?view=show&id='.$dev_id.'">Подробнее>></a>'.
									  '</div>'.
					 				  '<img src="'.$picture.'" width="100%" style=""/>'.
					 				  mb_substr(strip_tags($description),0,300).'...',
					 'footer' => '<a href="/developments?view=show&id='.$dev_id.'">Перейти>></a>',
					 'header' => $caption
					 );
					
		}
	
	public static function getReadableTimeResidue($stamp, $finish)
	{
		$now = qMexTime::getCurrent();
		if($now > $stamp)  
			return $now > $finish ? 
				'Уже прошло '.calculate_time_diff(qMexTime::getCurrent(), $finish).' назад' : 
				'До конца осталось '.calculate_time_diff(qMexTime::getCurrent(), $finish);
				
        	return (abs(($stamp - $now) / 3600.0)<=24 ? 'Начало совсем скоро!<br>Осталось: ' : 'Начало через: ').
			calculate_time_diff(qMexTime::getCurrent(), $stamp);
		}
	
	public static function getDevMembers($dev_id)
	{
		$db = new DB();
		$dev_id = (int)$dev_id;
		
		$db->query("SELECT user FROM qmex_developments WHERE event_id=$dev_id");
		$members = array();
		while($member = $db->one(0)) array_push($members, $member);
		
		return $members;
		
		}
		
	public static function wasThanked($dev,$user)
	{
		$db = new DB();
		$dev = (int)$dev;
		$user = (int)$user;
		
		$db->query("SELECT COUNT(*) FROM qmex_voites WHERE Login=$user AND Type='dev_thank' AND Essence=$dev");
		return $db->one(0)>0;
		
		}
	
	public static function printJSRoutine()
	{
		if(true): ?>        
        <script language="javascript" src="/tools/class-routine/development.js"></script>        
        <? endif;
		}
	
	
	}

?>