<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">-->
<!DOCTYPE html>
<html id="qmex">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="<?php echo Smarty::getDescription() ?>" />
<title><?php Smarty::getTitle() ?></title>
<noscript><meta http-equiv="refresh" content="0; URL=badbrowser.php"></noscript>
<link rel="stylesheet" href="/views/main/styles/animate.css">
<link rel="stylesheet" href="/views/main/styles/design.css">
<link rel="stylesheet" href="/views/main/styles/ui.css">
<link rel="stylesheet" href="/views/main/styles/definitions.css">
<link rel="stylesheet" href="/views/main/styles/common.css">
<link rel="stylesheet" href="/views/main/styles/uiElements.css">
<link rel="shortcut icon" href="/qmex_img/favicon.png">
<script src="/tools/jquery.js" type="text/javascript"></script>
<script src="/tools/alert.js" type="text/javascript"></script>
<script src="/tools/window.js" type="text/javascript"></script>
<script src="/tools/zoom.js" type="text/javascript"></script>
<script src="/tools/common.js" type="text/javascript"></script>
<script src="/tools/geo.js" type="text/javascript"></script>
<script src="/tools/notifies.js"></script>
<script src="/tools/ui.js"></script>
<script src="/tools/DOMjs.js"></script>
</head>
<body>

<?php Smarty::getServiceMessage() ?>
<table cellspacing="0" cellpadding="0" border="0" style="width:100%; height:100%">
<tr>

<td width="20%" id='qmex_head_menu' valign="top">

<div id="qmex_head">

<div id="qmex_head_hider" class="qHint" tag='Скрыть'>&lt;&lt;</div>

<div style="padding-left:20px; padding-right:20px; padding-top:20px">
<img src="/qmex_img/qmex.png" style="cursor:pointer;" onclick="location.href = '/'" id="qmex_logo" height="60pt">
</div>

<div style="font-family:Arial, Helvetica, sans-serif; font-style:italic; font-size:0.8em; color:#DDD; padding:5px; min-height:50px">
"<?php Smarty::getExpression() ?>"
</div>

<hr color="#0099FF" size="1">
<br>

<div style="padding-left:5px">
<?php Smarty::getSearchBar() ?>
</div>

<br>
<hr color="#0099FF" size="1">

<div style='width:20%; min-width:260px; padding-top:10px; padding-left:5pxs'>
<?php Smarty::getUserBar() ?>
</div>

<br>
<hr color="#0099FF" size="1">

<div style="padding-left:10px">
<?php Smarty::getMainMenu() ?>
</div>

<a href='/info?intro'>
<div id="qmex-intro">Введение в qMex</div>
</a>

</div>
</td>


<td width="80%" id='qmex_content_base' valign="top">
<table style="width:100%;" border="0" cellspacing="0">
<tr>
<td class="qCol" valign="top">
<div id="qLeftCol"></div>
<div id="ui_messages_notification" onclick="window.location='/dialogs'"></div>
</td>
<td valign="top">
<?php Smarty::getRegistrationCaption() ?>
<div id="qmex_content_block">
<div id="qcontent">

<?php Smarty::getMainContent() ?>
<?php Smarty::getAuthorizedSign() ?>

</div>
</div>
</td>
<td class="qCol" valign="top">
<div id="qRightCol"></div>
</td>
</tr>
</table>

<div id="Qinput"></div>
<div id="qmex_foot">
<table cellspacing="5px" border="0" id="qmex_foot_table">
<tr>
<td><a href="/">qMex</a> <span style="color:#BBB; font-weight:bold">&copy; <?php echo date("Y") ?></span></td>
<td><a href="info?page=about" class="footer">о сайте</a></td>
<td><a href="info?page=blog" class="footer">блог</a></td>
<td><a href="info?page=ads" class="footer">реклама</a></td>
<td><a href="info?page=support" class="footer">помощь</a></td>
<td><a href="/qmex-time" class="footer">qMex-Time</a></td>
</tr>
</table>
</div>

</td>

</table>

</body>
</html>

<!-- Main : JS.JS  -->
<script src="/tools/template/js.js"></script>