<div id="qUserPage" class="full-width" style="word-wrap: break-word;">

<?php Smarty::$DATA['userpage']->createCaption() ?>

<table style="width:95%; margin:0px auto;" cellspacing="10px" cellpadding="4px" border="0"  id='layout'>
<tr>
<td style="width:25%; max-width:200px;" valign="top" id="Minfo-box">

<?php Smarty::$DATA['userpage']->createTarget() ?>
<?php Smarty::$DATA['userpage']->createUserPicture() ?>
<?php Smarty::$DATA['userpage']->createLikeSystem() ?>
<?php Smarty::$DATA['userpage']->createConnectionSystem() ?>

</td>
<td valign="top" style="width:75%;">

<?php Smarty::$DATA['userpage']->createLoginLabel() ?>
<?php Smarty::$DATA['userpage']->createCurrentBusiness() ?>
<?php
$info = Smarty::$DATA['userpage']->getInterestsInfo();
echo $info['specialization'];
foreach($info['boxes'] as $box) 
	echo '<div style="float:left">'.$box.'</div>';
if(Smarty::$DATA['userpage']->isOwner()) : ?>
	<img src="qmex_img/edit.png" style="float:right" width="20px" class="qEdit" tag="Редактировать интересы" 
	onclick="location.href='editprofile?i'">
<?php endif ?>

</td>
</tr>
<tr>
<td colspan="2">

<table style="width:100%" cellpadding="0" cellspacing="0px" border=0>
<tr>
<td valign="top" width="50%" style="padding-right:5px">
<?php Smarty::$DATA['userpage']->createInfo() ?>
</td>
<td valign="top" width="50%">
<?php Smarty::$DATA['userpage']->createAbout() ?>
</td>
</tr>
</table>

</td>
</tr>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createResourcesData() ?>
</td>
</tr>

<tr>
<td colspan="2">
<div id='swop'>
<?php Smarty::$DATA['userpage']->createSwopTop() ?>
    <table cellspacing="15px" border=0 style="width:100%">
        <tr>
        <td valign="top" width="50%">
        	<div id='needs'>
            <?php Smarty::$DATA['userpage']->createSwopNeeds() ?>
            </div>
        </td>
        <td valign="top" width="50%">
        	<div id='provides'>
            <?php Smarty::$DATA['userpage']->createSwopProvides() ?>
            </div>
        </td>
        </tr>
    </table>
</div>
</td>
</tr>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createAchievements() ?>
</td>
</tr>

<tr>
<td colspan="2">
<div style="border-top:solid 1px #AAA; margin:10px 0 10px 0"></div>
</td>
</tr>
<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createUsefulPeopleList() ?>
</td>
</tr>
<tr>
<td colspan="2">
<div style="border-top:solid 1px #AAA; margin:10px 0 10px 0"></div>
</td>
</tr>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createHubster() ?>
</td>
</tr>

</table>
</div>

<script language="javascript">

$(setProfileImageSize);
$(window).on('resize', setProfileImageSize)

function setProfileImageSize()
{
	var layoutW = $("#layout").width()*0.25;
	$("#Minfo-box").width(layoutW);
	$("#QuserPhoto").width(layoutW);
	//$("#QuserPhoto").load( function(){ zoom( $("#QuserPhoto")[0], layoutW, null );} );
	}
	
</script>

