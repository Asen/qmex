<div style="padding:3px; border-bottom:1px solid #069; color:#069; font-size:8pt; font-weight:bold; margin-bottom:5px">
Мне нужно...</div>

<style>
.additional-need-keyword
{ float:left; padding:5px; margin:5px; background-color:#CEE7FF; font-weight:bold; color:#069; cursor:pointer; border:1px solid #069 }
.additional-need-keyword:hover
{ background-color:#069; color:#FFF }
</style>

<?php

$is_owner = Smarty::$DATA['userpage']->isOwner();
$UID = Smarty::$DATA['userpage']->data["id"];
$interests = Smarty::$DATA['userpage']->data['interests'];
$interests_data = array();
for($i=0;$i<$interests->counter;$i++)
{
	$interest = $interests->spec_inter($i);
	$keywords_data = array_filter(explode(",",$interest->key_words));
	$interest_id = $interest->interest_id;	
	$interests_data[$interest_id] = $keywords_data;
}

$DB = new DB();
$DB->query("SELECT ID FROM qmex_swop WHERE SwopType=".SWOP::SWOP_USER_NEED." AND Login=".$UID." ORDER BY ID DESC");
$COUNT = $DB->rowCount();

?>

<?php if($COUNT<SWOP::$RESTRICTIONS[SWOP::SWOP_USER_NEED] && $is_owner): ?>
<div id="add-need" class="swop-content-add">
<div style="font-size:8pt; padding-bottom:3px; border-bottom:1px dotted black">
Что вам необходимо в данный момент или будет нужно в недалеком будущем?
</div>
<table cellspacing="5px" width="100%" border="0">
<tr>
<td>
<select style="width:70%" id='spectrum-need'>
<option value="-1">Мне нужно ↓</option>
<?php 
$swops = Enum::SwopSpectra();
foreach($swops as $key=>$swop) 
echo '<option value="'.$key.'">'.$swop.'</option>';
?>
</select>
</td></tr>
<tr><td>
<select style="width:70%" id='spec-vector-need'>
<option value="-1">В сфере ↓</option>
<?php

$IBASE = new Interests();
$all_interests = array();

foreach($IBASE -> SelectIBASE() as $val) 
array_push($all_interests, $IBASE->SelectKey($val));

//$all_interests = array_intersect($all_interests, array_keys($_SESSION['interests']));

foreach($all_interests as $appropriate)
{
	$name = $IBASE->SelectName($appropriate);
	echo '<option value="'.$appropriate.'">'.$name.'</option>';
}

unset($IBASE);
?>
</select>
</td>
</tr>
<tr>
<td colspan="2">
<div style="font-size:0.7em">В чем заключается потребность:</div>
<textarea class='full-width common-swop-definitionBox' id='definition-need' rows='5'></textarea>
</td>
</tr>
<tr>
<td colspan="2" align="left">
<div id="swop-additional"></div>
<!--
<input type="text" class='full-width qHint' tag="Одно предложение, из которого была бы ясна суть потребности" 
id='core-need' style="padding:3px" maxlength="50" placeholder="Выразите кратко свою потребность" />
-->
</td>
</tr>
<tr>
<td colspan="2" align="right"><button class="qButton" onclick="createNeed()">Добавить</button></td>
</tr>
</table>
</div>
<?php elseif($is_owner): ?>

<div id="add-need" class="swop-content-add">
<div style="font-size:0.7em">
<b>У вас имеется слишком много потребностей.</b><br> Чтобы добавить новые, необходимо разобраться со старыми.
</div>
</div>

<? endif; ?>



<div id='needs-box' class="swop-content-box">
<?php
while($swopID = $DB->one(0))
{
	SWOP::CreateSwopById($swopID);
	}
?>
<?php if($COUNT==0):?>
<!--<div style="background-color:#CCC; color:#000; padding:10px; font-size:0.8em">Нет никаких потребностей от общества</div>-->
<div class='BlankProfileMsg'>Нет никаких потребностей от общества</div>
<?php endif ?>
</div>


<script>

$(function(){	
	$("#spec-vector-need").on('change', function(){ makeAdditionalNeeds( $(this).val() ) });	
	})

function makeAdditionalNeeds(interest)
{	
	$("#swop-additional").html("<div style='padding:3px; font-size:12px'>Какая именно область сферы?</div>");
	
	var interests = <?php echo json_encode($interests_data) ?>;
	var keywords = interests[interest+""]
	for(var k in keywords)
		$("#swop-additional").append("<div class='additional-need-keyword'>"+keywords[k]+"</div>");
		
	$("#swop-additional").slideUp(0);
	if(keywords.length>0){
		$("#swop-additional").slideDown(150);
		$(".additional-need-keyword").on('click', function(){ setAdditionalNeed(this) });
	}
	}
	
function setAdditionalNeed(set)
{
	$(".additional-need-keyword img").remove();
	$(".additional-need-keyword").css({'background-color':'#CEE7FF', 'color':'#069'})
	$(".additional-need-keyword").removeAttr("id");
	$(set).css({'background-color':'#069', 'color':'#FFF'})
	$(set).append("<img src='/qmex_img/UI/swop/check.png' width='15px' style='float:left; padding-right:5px' />")
	$(set).attr("id","core-need");
	}

function createNeed()
{
	var spectrum = parseInt($("#spectrum-need").val());
	var theme = parseInt($("#spec-vector-need").val());
	var definition = $("#definition-need").val();
	var swopcore = $("#core-need").text();
	
	if(spectrum==-1 || theme==-1 || definition.length<10){
		WINDOW.ShowMessage('Предоставьте более развернутое описание своей потребности!<br>'+
						   'Также убедитесь, что вы выбрали, что именно вам необходимо и в какой сфере ', WINDOW.C.ERROR);
		return 0;
		}
		
	var swopper = SendDynamicMsg(["create-swop",'spectrum','theme','text','core'],
								 [<?php echo SWOP::SWOP_USER_NEED ?>,spectrum,theme,definition,swopcore],
								 null,"handlers/swop.php","POST",'json');
								 
	if(swopper['result']=='*') 
	  WINDOW.ShowMessage('У вас уже есть похожая потребность.<br>'+
					   'Прежде чем добавить эту, удалите старую.', WINDOW.C.ERROR);
					   
	if(swopper['result']=='!') 
	WINDOW.ShowMessage('У вас уже достаточно потребностей!', WINDOW.C.ERROR);
					   
	if(swopper['result']=='+')
	{
		$("#needs-box").prepend('<div id="created-swop">'+swopper['swop']+'</div>');
		$("#created-swop").slideUp(0);
		$("#created-swop").slideDown(300);
		$("#created-swop").attr('id','');
	}
	
	}

</script>