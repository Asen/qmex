<div style="padding:3px; border-bottom:1px solid #069; color:#069; font-size:8pt; font-weight:bold; margin-bottom:5px">
Умения, навыки и предложения
</div>

<style>
.additional-provide-keyword
{ float:left; padding:5px; margin:5px; background-color:#CEE7FF; font-weight:bold; color:#069; cursor:pointer; border:1px solid #069 }
.additional-provide-keyword:hover
{ background-color:#069; color:#FFF }
</style>

<?php

$is_owner = Smarty::$DATA['userpage']->isOwner();
$UID = Smarty::$DATA['userpage']->data["id"];
$interests = Smarty::$DATA['userpage']->data['interests'];
$interests_data = array();
for($i=0;$i<$interests->counter;$i++)
{
	$interest = $interests->spec_inter($i);
	$keywords_data = array_filter(explode(",",$interest->key_words));
	$interest_id = $interest->interest_id;	
	$interests_data[$interest_id] = $keywords_data;
}

$DB = new DB();
$DB->query("SELECT ID FROM qmex_swop WHERE SwopType=".SWOP::SWOP_USER_PROVIDE." AND Login=".$UID." ORDER BY ID DESC");
$COUNT = $DB->rowCount();

?>

<?php if($COUNT<SWOP::$RESTRICTIONS[SWOP::SWOP_USER_PROVIDE] && $is_owner): ?>
<div id="add-provide" class="swop-content-add">
<div style="font-size:8pt; padding-bottom:3px; border-bottom:1px dotted black">
Расскажите о своих умениях и навыках, предоставьте людям возможность взаимодействовать с вами!
<!--Какие у вас есть предложения для других людей? Что могли бы предоставить и в чём помочь?-->
</div>
<table cellspacing="5px" width="100%" border="0">
<tr>
<td>
<select style="width:70%" id='spec-vector-provide'>
<option value="-1">Сфера ↓</option>
<?php

$IBASE = new Interests();
$all_interests = array();

foreach($IBASE -> SelectIBASE() as $val) 
array_push($all_interests, $IBASE->SelectKey($val));

$all_interests = array_intersect($all_interests, array_keys($_SESSION['interests']));

foreach($all_interests as $appropriate)
{
	$name = $IBASE->SelectName($appropriate);
	echo '<option value="'.$appropriate.'">'.$name.'</option>';
}

unset($IBASE);
?>
</select>
</td></tr>

<tr><td>
<select style="width:100%" id='spectrum-provide'>
<option value="-1">Что вы могли бы предложить ↓</option>
<?php 
$swops = Enum::SwopSpectra();
foreach($swops as $key=>$swop) 
echo '<option value="'.$key.'">'.$swop.'</option>';
?>
</select>
</td></tr>

<tr>
<td colspan="2">
<div style="font-size:0.7em">Суть предложения: </div>
<textarea class='full-width common-swop-definitionBox' id='definition-provide' rows='5'></textarea>
</td>
</tr>
<tr>
<td colspan="2" align="right">
<div id="swop-additional-provides" style="text-align:left"></div>
<!--
<input type="text" class='full-width qHint' tag="Несколько слов, выражающих суть предложения" 
id='core-provide' style="padding:3px" maxlength="50" placeholder="Кратко о предложении" />
-->
</td>
</tr>

<tr>
<td colspan="2" align="right"><button class="qButton" onclick="createProvide()">Предложить</button></td>
</tr>
</table>
</div>
<?php elseif($is_owner): ?>

<div id="add-need" class="swop-content-add">
<div style="font-size:0.7em">
<b>Вы уже предоставили очень много возможностей для других!</b>
</div>
</div>

<? endif; ?>


<div id='provides-box' class="swop-content-box">
<?php
while($swopID = $DB->one(0))
{
	SWOP::CreateSwopById($swopID);
	}
?>
<?php if($COUNT==0):?>
<!--<div style="background-color:#CCC; color:#000; padding:10px; font-size:0.8em">Предложений для других людей пока нет</div>-->
<div class='BlankProfileMsg'>Предложений для других людей пока нет</div>
<?php endif ?>
</div>


<script>

$(function(){	
	$("#spec-vector-provide").on('change', function(){ makeAdditionalProvides( $(this).val() ) });	
	})
	
	
function makeAdditionalProvides(interest)
{	
	$("#swop-additional-provides").html("<div style='padding:3px; font-size:12px'>В какой именно области?</div>");
	
	var interests = <?php echo json_encode($interests_data) ?>;
	var keywords = interests[interest+""]
	for(var k in keywords)
		$("#swop-additional-provides").append("<div class='additional-provide-keyword'>"+keywords[k]+"</div>");
		
	$("#swop-additional-provides").slideUp(0);
	$("#swop-additional-provides").slideDown(150);
	if(keywords.length>0)		
		$(".additional-provide-keyword").on('click', function(){ setAdditionalProvide(this) });
	else{
		var interest = $("#spec-vector-provide").val();
		    interest = $("#spec-vector-provide option[value='"+interest+"']").text();
		$("#swop-additional-provides").append("<div style='padding:10px; font-size:0.7em'> Добавьте ключевые слова к своему интересу '"+
		interest+"', чтобы уточнить область предложения.</div>");
	}
	
	}

function setAdditionalProvide(set)
{
	$(".additional-provide-keyword img").remove();
	$(".additional-provide-keyword").css({'background-color':'#CEE7FF', 'color':'#069'})
	$(".additional-provide-keyword").removeAttr("id");
	$(set).css({'background-color':'#069', 'color':'#FFF'})
	$(set).append("<img src='/qmex_img/UI/swop/check.png' width='15px' style='float:left; padding-right:5px' />")
	$(set).attr("id","core-provide");
	}


function createProvide()
{
	var spectrum = parseInt($("#spectrum-provide").val());
	var theme = parseInt($("#spec-vector-provide").val());
	var definition = $("#definition-provide").val();
	var swopcore = $("#core-provide").text();
	
	if(spectrum==-1 || theme==-1 || definition.length<10){
		WINDOW.ShowMessage('Вы выбрали сферу и то, что бы вы могли предложить?<br>'+
						   'Предоставьте полную информацию для других людей.', WINDOW.C.ERROR);
		return 0;
		}
		
	var swopper = SendDynamicMsg(["create-swop",'spectrum','theme','text','core'],
								 [<?php echo SWOP::SWOP_USER_PROVIDE ?>,spectrum,theme,definition,swopcore],
								 null,"handlers/swop.php","POST",'json');
	
	if(swopper['result']=='*') 
	WINDOW.ShowMessage('Вы уже создали похожую возможность ранее.<br>'+
					   'Прежде чем добавить новую, удалите старую.', WINDOW.C.ERROR);
			
	if(swopper['result']=='!') 
	WINDOW.ShowMessage('Вы уже предоставили достаточно возможностей!', WINDOW.C.ERROR);		   
	
	if(swopper['result']=='+')
	{
		$("#provides-box").prepend('<div id="created-swop">'+swopper['swop']+'</div>');
		$("#created-swop").slideUp(0);
		$("#created-swop").slideDown(300);	
		$("#created-swop").attr('id','');
	}
	
	}

</script>