<a name="hub"></a>

<?php

$id = Smarty::$DATA['userpage']->data["id"];
$is_owner = Smarty::$DATA['userpage']->isOwner();

?>

<style>
.UI-simple-menu-item{ font-size:8pt }
#ntype_description{ display:none; font-size:11px; font-family:Arial, Helvetica, sans-serif; color:#CCC; padding:3px;}
#ui_note_creation{ border-bottom:#09F double 3px; background-color:#036; padding:10px; }
</style>

<?php $next = (isset($_GET['ext'])) ? (int)$_GET['ext'] : 0; ?>
<?php include_once("tools/sided/veditor.php"); ?>
<?php if($is_owner):?>
<div class="ui_general_notes_box">

<div id="ui_note_creation">
<div style="color:#75AED0; font-family:Arial, Helvetica, sans-serif; font-size:0.80em; padding:3px">Появились ли новые мысли? Может, новые идеи, желания или намерения? А, может, произошли какие-то интересные события? Обо всем этом и подобном вы можете рассказать, 
написав новый хаб <b>:</b> </div>



<div style="padding-top:10px; padding-bottom:10px;">
<select id='ui_note_type' onchange="show_description(this)">
<option value="-1">-Что вы желаете написать?-</option>
<?php
foreach(ENUM::Conceptions() as $key=>$val)
echo '<option value="'.$key.'">'.$val.'</option>';
?>
</select>
<span style="color:#EEE">и</span>
<select id='ui_note_theme'>
<option value="-1">-О чем это будет?-</option>
<?php

$IBASE = new Interests();
$all_interests = array();

foreach($IBASE -> SelectIBASE() as $val) 
array_push($all_interests, $IBASE->SelectKey($val));

$all_interests = array_intersect($all_interests, array_keys($_SESSION['interests']));

foreach($all_interests as $appropriate)
{
	$name = $IBASE->SelectName($appropriate);
	echo '<option value="'.$appropriate.'">'.$name.'</option>';
}

unset($IBASE);
?>
</select>
<div id='ntype_description'></div>
</div>


<div style="font-size:0.7em; font-family:Arial, Helvetica, sans-serif; border-top:1px dotted #006">
<?php if($next>0) echo 'Продолжение к хабу о <b>"'.Note::GetBriefNote($next,50).'" : </b>';?>
</div>
<textarea class="ui_note_box" rows="3" id="ui_note_m_box" tag="none"></textarea>
<table cellpadding="0" cellspacing="5px" border="0" style="width:100%; padding-top:10px; color:#069">
<tr>
<td width="60%">
<input type="text" style="width:100%; box-sizing:border-box; color:#069; padding:5px;" maxlength="50" id="ui_note_meaning" placeholder="Название хаба">
<div style="border-top:2px solid #06F"></div>
<input type="text" style="width:100%; box-sizing:border-box; color:#069; padding:5px;" maxlength="150" id="ui_note_substance" placeholder="Основная суть хаба, кратко">
<div id='mark' style="color:#CCC; padding-top:3px">
Суть может быть выражена как одним предложением, так и группой ключевых слов, характеризующей хаб.
</div>
</td>
<td align="right" rowspan="2" width="40%" valign="top">
<input type="button" class="qButton" style="width:80%; box-sizing:border-box; height:30px; font-size:1em" value="В хаб!" onclick="lets_send_note()"><br>
<input type="button" class="qButton_silver" style="width:80%; box-sizing:border-box; font-size:0.7em;" value="Очистить" onclick="cls()">
</td>
</tr>
<tr>
<td colspan="2" style="height:25px"><div id="ui_note_center"></div></td>
</tr>
</table>
</div>

<? endif; ?>


<table cellspacing="3px" style="padding-top:20px; padding-bottom:20px; background-color:#036" class='UI-simple-menu-tab'>
<tr>
<?php
foreach(ENUM::ConceptionsWords() as $key=>$val)
echo '<td class="UI-simple-menu-item" onclick="location.href=\'/hubster?type='.$key.'&u='.$id.'\'">'.$val.'</td>';
?>
</tr>
</table>

<div style="border-bottom:3px solid #069; margin-top:25px; margin-bottom:25px"></div>

<div class="ui_notes_box">
<?php

$db = new DB();

$db->query("SELECT COUNT(*) FROM qmex_user_notes WHERE Login=$id");
$hubs_count = $db->one(0);

$db->query("SELECT ID FROM qmex_user_notes WHERE Login=$id ORDER BY updated_at DESC LIMIT 30");
$selected_hubs = $db->rowCount();
if($selected_hubs>0)
{
	while($id=$db->one(0)){
		Note::showNote($id,false,false);
		echo '<div style="color:black; text-align:center">***</div><br>';
	}
}
else echo "<div style='width:100%; text-align:center; font-size:14px;font-family:Arial, Helvetica, sans-serif; font-size:22px;' id='none_notes' t>В хабе ничего нет...</div>";

if($hubs_count>$selected_hubs) 
echo 
'<div style="text-align:center"> 
<a href="/hubster?u='.$id.'">
<button class="qButton" style="width:100%; padding:10px"> К другим хабам &gt;&gt; </button>
</a> 
</div>';

Note::JSRoutine();
Comment::showRoutine();

?>
</div>
</div>


<script>

function cls()
{
	WINDOW.Confirm("Удалить содержимое записи?", function(){
		GetEditor('ui_note_m_box').setContent("");	
			
		});
	}

function show_description(o)
{
	var ind = o.selectedIndex;
	var descr = "";
	switch(ind)
	{
		
		case 1: 
		descr="Опишите свою цель или намерение так, чтобы людям было это интересно! "+
			  "И вам удастся быстро привлечь к себе интересующихся людей.<br>"+
			  "<u>Хаб этого типа можно установить как свою цель или текущее намерение: </u>"+
			  "<div style='padding:5px'><input type='checkbox' id='set_as_current_aim'>"+
			  "Установить этот хаб текущей целью или намерением.</div>"; 
		break;
		case 2: 
		descr="Поделитесь полезной информацией, касающейся ваших интересов, специализаций и т.д."; 
		break;
		case 3: 
		descr="Расскажите о своем проекте или о своей идее людям!"; 
		break;
		case 4: 
		descr="В хабе такого типа вы можете написать полезный совет, основанный на вашем жизненном опыте, "+
			  "а также рассказать о каких-либо историях из прошлого, касающихся интересов, идей, проектов и намерений";
		break;
		case 5: 
		descr="Поделитесь своими новостями с другими";
		break;
		default: descr=""; break;
		}
	$("#ntype_description").css({'display':( (descr=='') ? 'none':'block' )}); 
	$("#ntype_description").html(descr);
	}

function lets_send_note()
{
	var ext = <?php echo $next;?>;
	var note = GetEditor('ui_note_m_box').getContent();
	var mean = $("#ui_note_meaning").val();
	var substance = $("#ui_note_substance").val();
	var theme = parseInt($("#ui_note_theme").val());
	var type = parseInt($("#ui_note_type").val());
	var additional="";
	if(type==<?php echo Enum::$CONCEPTIONS['INTENT'] ?>) 
		additional = $("#set_as_current_aim:checked").length;
	if(note!='')
	{
		var res = SendDynamicMsg(['note_case',"mean_case","substance","theme","type","ext","additional"],
							 [note,mean,substance,theme,type,ext,additional],"ui_note_center","handlers/uiHandler.php");
		if(type==<?php echo Enum::$CONCEPTIONS['INTENT'] ?> && parseInt(additional)==1 && $("#ui_note_center").html().indexOf("SucMsg")>0) 
			{
				location.reload();
			}
		if($("#ui_note_center").html().indexOf("SucMsg")>0)
		{          		
		note = SendDynamicMsg(["last_note"],['1'],null,"handlers/uiHandler.php","GET");	
		$("#none_notes").remove();	            
		$(".ui_notes_box").prepend(note);
		Ready();
		$(".public-note:first").slideUp(0);
		$(".public-note:first").slideDown(300);
		window.setTimeout(function(){
			var identifier = $("#note-identifier").val();
			$("#note-identifier").remove();
			COMMON.IFrameTest("#public-frame-"+identifier); 
			},320);
		GetEditor('ui_note_m_box').setContent("");
		window.scrollBy(0, 500); 
		}
	window.setTimeout(function(){$("#ui_note_center").html("");},3000);
	} else WINDOW.ShowMessage("Пустая запись не может быть создана!",WINDOW.C.ERROR);
} 
	
$(function(){
	InitEditor("#ui_note_m_box","250px");
	});
	
</script>