<div style="margin-top:-3px;">
<?php

$likes =  Smarty::$DATA['userpage']->data["likes"];
$dislikes =  Smarty::$DATA['userpage']->data["dislikes"];
$id = Smarty::$DATA['userpage']->data["id"];
$this_user = Smarty::$DATA['userpage']->data["authorized_user"];
$raiting = Smarty::$DATA['userpage']->data["common_raiting"];
$is_owner = Smarty::$DATA['userpage']->isOwner();

$all = $likes+$dislikes;
if($all==0) $all=1;
$per_likes = ($likes*100)/($all);

////////////////////////////////////////

$like_flag = "";

if(!$is_owner && Security::isAuthorized())
{
	Smarty::$DATA['db']->query("SELECT Voite FROM qmex_voites WHERE (Login=$id AND Who=$this_user AND Type='page')");
	if(Smarty::$DATA['db']->rowCount()>0)
	{
		$voite = Smarty::$DATA['db']->one();
		$voite = $voite[0];
		$like_flag = ($voite==1) ? "<div id='SucMsg'>Вам нравится этот пользователь.</div>":
		                           "<div id='ErrMsg'>Вам не нравится этот пользователь.</div>";
		}
}

$arrows = array();
$arrows[0] = !$is_owner ? '<td><div class="qEdit" tag="Выразить почтение" onclick="voite(true)">
<img src="qmex_img/UI/like.png" width="21px" height="21px"></div></td>':'';
$arrows[1] = !$is_owner ? '
<td><div type="button" class="qEdit" tag="Выразить недовольство" onclick="voite(false)">
<img src="qmex_img/UI/dislike.png" width="21px" height="21px"></td>
</div>':'';

?>

<style>
#common-rating{ font-size:12px; padding:10px; font-weight:bold; text-align:left; background-color:black; color:white; cursor:pointer; }
#common-rating:hover{ background-color:#069;  }
</style>

<a href='rating?id=<?php echo $id;?>' class='non-underlined'>
<div id='common-rating' class='qHint' tag='Нажмите, чтобы получить более подробную информацию.'>
<?php 
$color = (float)$raiting > 0 ? '#C1FFC1' : '#FBB';
if($raiting==0) $color = '#CCC';  
?>
Значимость &bull; 
<span style="color:<?php echo $color ?>; font-family:Georgia, 'Times New Roman', Times, serif">
<?php echo $raiting;?>
</span> <span id='mark' style="color:#AAA">(?)</span>
</div>
</a>

<div style="border:1px #000 dotted; padding:2px">
<div style="font-size:12px; padding-bottom:3px; text-align:left; border-bottom:1px dotted #000" t>
Актуальность : <b><?php echo $likes-$dislikes;?></b>
</div>
<table id="ui_rait_table" style="width:100%; text-align:center;" border="0" cellspacing="2px">
<tr>
<?php echo $arrows[0];?>
<td width="<?php echo ($id!=$this_user) ? '80' : '100'; ?>%">
<div id="rating" style="width:100%; height:5px; border:1px #DDD solid;" >
<div id="likes" style="float:left;width:<?php echo $per_likes?>%; height:5px; background-color:#096"></div>
<div id="dislikes" style="float:left;width:<?php echo 100-$per_likes?>%; height:5px; background-color:#C66"></div>
</div>
</td>
<?php echo $arrows[1];?>
</tr>
<tr>
<td colspan="3" style="text-align:left">
<div style="font-size:12px; padding:3px" t>
| <img src="qmex_img/l_view.png" width="10px" height="10px" style="padding-right:5px"><span id="likepad"><?php echo $likes?><span> |  
<img src="qmex_img/disl_view.png" width="10px" height="10px" style="padding-right:5px;"><span id="dislikepad"><?php echo $dislikes?></span> |
</div>
</td>
</tr>
</table>
<div id="voite_msg"><?php echo $like_flag;?></div>
</div>

<script language="javascript">
function voite(voite_type)
{
	var user = $("#uid").val();
	var voite = (voite_type==true) ? 1:-1;
	res = SendDynamicMsg(['mvoite','user','voite','type'],[1,user,voite,'page']);
	if(res.indexOf('voite-error')==-1) $("#voite_msg").html(res); else
	                                   $("#voite_msg").append(res);
	}	
</script>