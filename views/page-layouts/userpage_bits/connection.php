<?php

$id = Smarty::$DATA['userpage']->data["id"];
$this_user = Smarty::$DATA['userpage']->data["authorized_user"];
$is_owner = Smarty::$DATA['userpage']->isOwner();

?>


<?php if(Security::isAuthorized() && $is_owner): ?>
    <button class="qButton qHint" tag="Ваш графический идентификатор в qMex." style="margin-top:4px; width:100%;" id="ChangePicture">
    Изменить изображение
    </button>
<?php endif; ?>
<?php if(!$is_owner): ?>
	<div class="qButton qHint" tag="Вам интересны занятия и увлечения этого человека?<br>Вы можете связаться с ним!" 
    style="margin-top:4px;" id="uiConnection">
    <img src="qmex_img/write.png" width="16px" height="16px" style="padding-right:5px;padding-bottom:3px;float:left">
    Связь по интересам
    </div>
<?php endif; ?>


<?php
if(Security::isAuthorized() && !$is_owner)
{
	$db = new DB();
	$id_u = $_SESSION['id'];
	$db->query("SELECT ID FROM qmex_user_bookmarks WHERE ID1=$id_u AND ID2=$id");
	$iam_already_added = $db->rowCount();
	$db->query("SELECT ID FROM qmex_user_bookmarks WHERE ID1=$id AND ID2=$id_u");
	$iam_was_added_too = $db->rowCount();
	
	if(!$iam_already_added)
		echo '<button class="qButton" style="margin-top:4px; width:100%" id="ui_qmex_person_bookmark">Добавить в свое окружение</button>';
	if($iam_already_added && !$iam_was_added_too)
		echo '<div id="SucMsg" class="IState">Еще не подтвердил, что желает состоять в вашем окружении. 
		<a onclick="return false;" id="remove_user_from_i">Отменить?</a></div>';		 
	if($iam_was_added_too){		
		$msg = "";
		$rem = '<a onclick="return false;" id="remove_user_from_i">Удалить?</a>';
		if($iam_already_added && !$iam_was_added_too) 
			$msg = "Войдет в ваше окружение сразу после того, как добавит вас в свое";
		if($iam_already_added && $iam_was_added_too)
			$msg = "Состоит в вашем окружении.";
		if(!$iam_already_added && $iam_was_added_too){
			$msg = "Хочет добавить вас в свое окружение. Для подтверждения добавьте и вы, если желаете.";
			$rem = '';
		}
			
		echo '<div id="SucMsg" class="IState">'.$msg.' '.$rem.'</div>';
	}
}

?>

<div id="proposal"></div>

<script language="javascript">

$(function()
{
	$("#uiConnection").bind('click',function(){
		location.href='dialogs?message&cause='+<?php echo Communication::CAUSE_USEFUL_MAN?>+'&id='+$("#uid").val();
		});
	$("#ui_qmex_person_bookmark").bind('click',function(){proposal(this);});
	$("#remove_user_from_i").click(function(e) {RemoveFromI()});

});

function ChangeGraphic(e){ UiUpdator('graphic',e); }

function proposal(obj)
{
	var uid = parseInt($("#uid").val());
	var user = $("#username").val();
	var res=SendDynamicMsg(['proposal','uid'],[1,uid],null,"handlers/uiHandler.php");
	var msg = '<?php 
	echo (Security::isAuthorized() && !$is_owner) ? 
	( $iam_was_added_too ? 'добавлен в ваше окружение!' : 'будет добавлен в ваше окружение, если он в ответ добавит вас в свое.') : '' ?>';
	if(res=="1") 
	{
		$("#proposal").html("<div id='SucMsg' class='ui_must_hide_block' style='font-size:13px;'><b>"+user+"</b> "+msg+"</div>");
		$(obj).slideUp(500);
		return;
	}
	if(res=="-"){
		$("#proposal").html("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px;'>"+
		"В вашем окружении уже слишком много людей</div>");
		return;
		}
	if(res=="*"){
		$("#proposal").html("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px;'>"+
		"Для создания своего окружения необходимо сперва подтвердить свою действительность.</div>");
		return;
		}
		
	$("#proposal").html("<div id='ErrMsg' class='ui_must_hide_block' style='font-size:13px;'>Возникла ошибка.</div>");
		
	}
	
	
function RemoveFromI()
{
	var uid = parseInt($("#uid").val());
	var user = $("#username").val();
	WINDOW.Confirm("Вы точно уверены, что <b>"+user+"</b> больше не актуален для вашего окружения?",function(){
		var res=SendDynamicMsg(['proposalRemove','uid'],[1,uid],null,"handlers/uiHandler.php");
		if(parseInt(res)==1) {
			$(".IState").slideUp(100);
			$("#proposal").html("<div id='SucMsg' class='ui_must_hide_block' style='font-size:13px;'><b>"+user+
										"</b> удален из вашего окружения.</div>");
										}
		});
	}
	
</script>