<?php

$current_business = Smarty::$DATA['userpage']->data["current_business"];
$caption = Smarty::$DATA['userpage']->data["caption"];
$pagescheme = (int)Smarty::$DATA['userpage']->data["SCHEME"];
$user_id = (int)Smarty::$DATA['userpage']->data["id"];
$login = Smarty::$DATA['userpage']->data["login"];

include("views/page-layouts/page-".$pagescheme.".php");
unset(Smarty::$DATA['userpage']);

?>



<input type="hidden" value="<?php echo $login ?>" id="username">
<input type="hidden" value="<?php echo $user_id ?>" id="uid">
<input type="hidden" value="<?php echo $_SESSION['id'] ?>" id="user">



<script language="javascript">


UI.completeSettings(<?php echo $user_id ?>);


$(function()
{
	document.title='qMex • '+$("#username").val();	
	if( $("#uid").val() != $("#user").val() ) RaitInterests();
	$("#ipeople-box-roll").click(function(){ $("#people-list-roll").click(); });
	$("#people-list").slideUp(0);
	$("#people-list-roll").toggle(
		function(){ 
			if($("#people-list:animated").length>0) return; 
			$("#people-list").slideDown(1000); 
			var o = $("#people-list-arrow").first();
			Rotate(-90,o); 
			$(o).attr("tag","Скрыть"); 
		},  
		function(){ 
			if($("#people-list:animated").length>0) return;  
			$("#people-list").slideUp(1000); 
			var o = $("#people-list-arrow").first();
			Rotate(0,o); 
			$(o).attr("tag","Показать"); 
		}
		);
		
	$("#ChangePosition").on('click', function(){ 
		WINDOW.Prompt("Несколько слов о своих текущих делах", 
					  "Напишите о том, чем вы в данный момент занимаетесь или о своих планах:", ChangeStatus, 
					  {'value':"<?php echo $current_business ?>"}); 
	});	
	
	$("#ChangeCaption").on('click', function(){ 
		WINDOW.Prompt("Кто вы?", 
					  "Небольшое описание того, кем вы являетесь. Ваше главное занятие или главный интерес, например."+
					  "<br><b>( Ключевая информация, по которой вас можно было бы<br> легко найти ) :</b>", ChangeCaption, 
					  {'value':"<?php echo $caption ?>", "height":180}); 
	});
	
	$("#ChangePicture").on('click', function(){ 
		WINDOW.Prompt("Изменить свой графический идентификатор", 
					  "Скопируйте и вставьте ссылку изображения в поле ниже:"+
					  "<div style='font-size:0.8em'>( Не используйте изображения крупного формата )</div>", ChangeGraphic, 
					  {'value':""}); 
	});
		
	});

function ChangeStatus(e){  UiUpdator('status',e); }
function ChangeGraphic(e){ UiUpdator('graphic',e); }
function ChangeCaption(e){ UiUpdator('caption',e); }
	
function UiUpdator(type, value)
{
	var ret_to = type=='graphic' ? "sub_text_capt" : null; 
	var res = SendDynamicMsg(["UiChanger",'value'],[type,value],ret_to,"handlers/ask.php","POST",'json');
	WINDOW.ShowMessage(res['msg'], res['code']=='+' ? WINDOW.C.SUCCESS : WINDOW.C.ERROR);
	if(res['code']=='+') window.setTimeout(function(){ location.href=location.href }, 1000);
	}

function RaitInterests()
{	
	$(".UI-item-rating").mouseover(function(){
		w = $(this).width();
		$(this).html("+");
		$(this).removeClass('qHint');
		$(this).attr("title","Подтвердить");
		$(this).width(w);
		});
	$(".UI-item-rating").mouseout(function(){
		r = $(this).attr("r");
		$(this).html(r);
		$(this).addClass('qHint');
		$(this).removeAttr("title");
		});
	$(".UI-item-rating").click(function(){
		idi = $(this).attr("idi");
		uid = $("#uid").val();
		res = SendDynamicMsg(['confirm_interest','idi','uid'],[1,idi,uid],null,'handlers/uiHandler.php','POST');
		if(res=='-') WINDOW.ShowMessage('Для оценивания необходимо подтвердить свою действительность.', WINDOW.C.ERROR);
		if(res==-1) show_wnd("Вы уже подтвердили этот интерес.",100,true,2,"#069",false);
		if(res==1) { r = parseInt($(this).attr("r"))+1; $(this).attr("r",r), $(this).text(r); }
		});
	}

</script>