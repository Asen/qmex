
<?php Smarty::$DATA['userpage']->createCaption() ?>
<?php Smarty::$DATA['userpage']->createLoginLabel() ?>

<table cellpadding="0" cellspacing="10px" border="0" class="full-width" id='layout'>
<tr>

<td width="60%" valign="top" height="50px">

<table class="full-width" cellspacing="0px">
<tr><td>

<div style="padding:0px 0 10px 0">
<?php Smarty::$DATA['userpage']->createCurrentBusiness() ?>
</div>

<?php
$info = Smarty::$DATA['userpage']->getInterestsInfo();
echo $info['specialization'];
foreach($info['boxes'] as $box) 
	echo '<div style="float:left">'.$box.'</div>';
if(Smarty::$DATA['userpage']->isOwner()) : ?>
	<img src="qmex_img/edit.png" style="float:right" width="20px" class="qEdit" tag="Редактировать интересы" 
	onclick="location.href='editprofile?i'">
<?php endif ?>
</td></tr>

<tr><td>
<br>
<?php Smarty::$DATA['userpage']->createResourcesData() ?>
<br>
</td></tr>

<tr><td>
<br>
<?php Smarty::$DATA['userpage']->createUsefulPeopleList() ?>
<br>
</td></tr>

<tr><td>
<br>
<?php Smarty::$DATA['userpage']->createHubster() ?>
<br>
</td></tr>

</table>
</td>

<td width="40%" valign="top" rowspan="3">
	
    <table class="full-width">
    <tr><td>
	<?php Smarty::$DATA['userpage']->createTarget() ?>
    <?php Smarty::$DATA['userpage']->createUserPicture() ?>
    <?php Smarty::$DATA['userpage']->createLikeSystem() ?>
    <?php Smarty::$DATA['userpage']->createConnectionSystem() ?>
    <br><br>
    </td></tr>
    <tr><td>
    <?php Smarty::$DATA['userpage']->createInfo() ?>
    <br />
    <?php Smarty::$DATA['userpage']->createAbout() ?>
    <br>
    </td></tr>
    <tr><td>
	<?php Smarty::$DATA['userpage']->createAchievements() ?>
	<br>
    </td></tr>
    <tr><td>
    <div style="padding-top:20px">	
    <?php Smarty::$DATA['userpage']->createSwopTop() ?>
	<div id='swop'>
    <div id='needs'>
		<?php Smarty::$DATA['userpage']->createSwopNeeds() ?>
    </div><br><br>
    <div id='provides'>
		<?php Smarty::$DATA['userpage']->createSwopProvides() ?>
    </div>
	</div>  
    </div>
    </td></tr>
    </table>
</td>

</tr>

<tr>
<td colspan="2">

</td>
</tr>

</table>



<script language="javascript">

$(setProfileImageSize);
$(window).on('resize', setProfileImageSize)

function setProfileImageSize()
{
	$("#QuserPhoto").width($("#qUserPageHighCaption").width()*0.4);
	$("#Minfo-box").width($("#qUserPageHighCaption").width()*0.4);
	//$("#QuserPhoto").load( function(){ zoom( $("#QuserPhoto")[0], $("#layout").width()*0.25, null );} );
	}
	
</script>