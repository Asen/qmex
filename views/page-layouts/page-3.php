<table cellpadding="0" cellspacing="0" border="0" class="full-width" id='layout'>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createCaption() ?>
<?php Smarty::$DATA['userpage']->createLoginLabel() ?>
<?php Smarty::$DATA['userpage']->createCurrentBusiness() ?>
</td>
</tr>

<tr><td valign="top">
<table cellpadding="0" cellspacing="10px" border="0" style="width:100%">
<tr>
<td width="25%" rowspan="10" valign="top" align="left" id="Minfo-box">

	<?php Smarty::$DATA['userpage']->createTarget() ?>
    <?php Smarty::$DATA['userpage']->createUserPicture() ?>
    <?php Smarty::$DATA['userpage']->createLikeSystem() ?>
    <?php Smarty::$DATA['userpage']->createConnectionSystem() ?>
    
<div style="padding:15px"></div>

<?php
$info = Smarty::$DATA['userpage']->getInterestsInfo();
echo $info['specialization'];
foreach($info['boxes'] as $box) echo $box;
if(Smarty::$DATA['userpage']->isOwner()) : ?>
	<div style="text-align:center">
	<img src="qmex_img/edit.png" width="30px" class="qEdit" tag="Редактировать интересы" onclick="location.href='editprofile?i'">
    </div>
<?php endif ?>
    
</td>
<td width="75%" valign="top">
	<?php Smarty::$DATA['userpage']->createInfo() ?>
</td></tr>
<tr><td>
	<?php Smarty::$DATA['userpage']->createAbout() ?>
</td></tr>
<tr><td>
	<?php Smarty::$DATA['userpage']->createResourcesData() ?>
</td>
</tr>


<tr>
<td colspan="2">
<div id='swop'>
<?php Smarty::$DATA['userpage']->createSwopTop() ?>
    <table cellspacing="5px" border=0 style="width:100%">
        <tr>
        <td valign="top" width="50%">
        	<div id='needs'>
            <?php Smarty::$DATA['userpage']->createSwopNeeds() ?>
            </div>
        </td>
        <td valign="top" width="50%">
        	<div id='provides'>
            <?php Smarty::$DATA['userpage']->createSwopProvides() ?>
            </div>
        </td>
        </tr>
    </table>
</div>
</td>
</tr>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createAchievements() ?>
</td>
</tr>

<tr>
<td colspan="2">
<div style="border-top:solid 1px #AAA; margin:10px 0 10px 0"></div>
</td>
</tr>
<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createUsefulPeopleList() ?>
</td>
</tr>
</tr>
<tr>
<td colspan="2">
<div style="border-top:solid 1px #AAA; margin:10px 0 10px 0"></div>
</td>
</tr>

<tr><td>
<?php Smarty::$DATA['userpage']->createHubster() ?>
</td></tr>

</table>

</td>
</tr>
</table>


<script language="javascript">

$(setProfileImageSize);
$(window).on('resize', setProfileImageSize)

function setProfileImageSize()
{
	$("#QuserPhoto").width($("#layout").width()*0.25);
	$("#Minfo-box").width($("#layout").width()*0.25);
	//$("#QuserPhoto").load( function(){ zoom( $("#QuserPhoto")[0], $("#layout").width()*0.25, null );} );
	}
	
</script>

