<table cellpadding="0" cellspacing="10px" border="0" style="padding:0px" class="full-width" id='layout'>

<tr>
<td colspan="2">
<?php Smarty::$DATA['userpage']->createCaption() ?>
<?php Smarty::$DATA['userpage']->createLoginLabel() ?>
<?php Smarty::$DATA['userpage']->createCurrentBusiness() ?>
</td>
</tr>

<tr>
<td colspan="2">
<?php
$info = Smarty::$DATA['userpage']->getInterestsInfo();
echo $info['specialization'];
foreach($info['boxes'] as $box) 
	echo '<div style="float:left">'.$box.'</div>';
if(Smarty::$DATA['userpage']->isOwner()) : ?>
	<img src="qmex_img/edit.png" style="float:right" width="20px" class="qEdit" tag="Редактировать интересы" 
	onclick="location.href='editprofile?i'">
<?php endif ?>
</td>
</tr>

<tr>
<td width="30%" valign="top" id="Minfo-box">
	<?php Smarty::$DATA['userpage']->createTarget() ?>
    <?php Smarty::$DATA['userpage']->createUserPicture() ?>
    <?php Smarty::$DATA['userpage']->createLikeSystem() ?>
    <?php Smarty::$DATA['userpage']->createConnectionSystem() ?>
</td>
<td width="70%" valign="top">
<?php Smarty::$DATA['userpage']->createInfo() ?>
<br>
<?php Smarty::$DATA['userpage']->createAbout() ?>
</td>
</tr>

<tr>
<td colspan="2">
<div style="padding-right:20%">
<?php Smarty::$DATA['userpage']->createResourcesData() ?>
</div>
</td>
</tr>

<tr>
<td colspan="2">
<div id='swop' style="padding-left:20%">
<?php Smarty::$DATA['userpage']->createSwopTop() ?>
    <table cellspacing="5px" border=0 style="width:100%">
        <tr>
        <td valign="top" width="50%">
        	<div id='needs'>
            <?php Smarty::$DATA['userpage']->createSwopNeeds() ?>
            </div>
        </td>
        <td valign="top" width="50%">
        	<div id='provides'>
            <?php Smarty::$DATA['userpage']->createSwopProvides() ?>
            </div>
        </td>
        </tr>
    </table>
</div>
</td>
</tr>

<tr>
<td colspan="2">
<div style="padding-right:30%">
<?php Smarty::$DATA['userpage']->createAchievements() ?>
</div>
</td>
</tr>


<tr>
<td colspan="2">
<div style="padding: 5% 0% 0% 20%">
<?php Smarty::$DATA['userpage']->createUsefulPeopleList() ?>
</div>
</td>
</tr>


<tr>
<td colspan="2">
<div style="padding:5% 15% 0 0%">
<?php Smarty::$DATA['userpage']->createHubster() ?>
</div>
</td>
</tr>

</td>
</tr>
</table>

<script language="javascript">

$(setProfileImageSize);
$(window).on('resize', setProfileImageSize)

function setProfileImageSize()
{
	$("#Minfo-box").width($("#layout").width()*0.3);
	$("#QuserPhoto").width($("#layout").width()*0.3);
	//$("#QuserPhoto").load( function(){ zoom( $("#QuserPhoto")[0], $("#layout").width()*0.3, null );} );
	}
	
</script>

