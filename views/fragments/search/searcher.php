<?php

$val = isset($_SESSION['q']) ? urldecode($_SESSION["q"]) : "";

?>

<table cellpadding="0" cellspacing="0" style="width:100%; min-width:200px">
<tr>
<td><input type="text" class="QsearchBar UI-input search_bit qHint" id="qSearch" autocomplete="off" name="q" value="<?php echo $val;?>" 
tag="Вводите интересы, навыки, специализации, названия проектов и находите людей, связанных с ними!" placeholder='Поиск нужных людей...' style="border-color:white">
</td>
<td>
<div class="qButton" style="height:21px; width:21px; border-radius:2px; border-color:white" onclick="startSearch()">
<img src="qmex_img/search.png" width="100%" height="21px">
</div>
</td>
</tr>
</table>

<script src="/tools/views/searcher.js" language="javascript"></script>

<?php
if(isset($_SESSION["q"])) unset($_SESSION["q"]);
?>