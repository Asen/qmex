<?php

if(isset($_SESSION["entered"]) && !isset($_SESSION["DisplayGreeting"]))
{
	$_SESSION["DisplayGreeting"] = true;
	$show_greeting = true;
	}

if(isset($_POST["Exit"])){
	Security::RemoveUserEntrance($_SESSION['id']);
	die('<script>location.href="/"</script>');
}

$login_p = $_SESSION["login"];
$login_m = mb_substr($_SESSION["login"],0,15);
if(mb_strlen($login_m)<mb_strlen($_SESSION["login"])) $login_m.='...';
$login_m = get_not_escaped($login_m); 
?>

<div style="margin-top:-15px; padding-top:5px; position:fixed; float:right" id="userdata_reserve"></div>
<div id="userdata">
<table cellpadding="0" cellspacing="0" style="width:100%">
<tr><td valign="bottom">
<a href="/#hub">
<?php 
$num = Human::getUserTarget($_SESSION['id']);

Smarty::$DATA['db']->query("SELECT Substance, Meaning FROM qmex_user_notes WHERE ID=".$num);
$data = Smarty::$DATA['db']->one();
$substance = mb_substr(strip_tags($data[0]),0,15);
$meaning = htmlspecialchars(mb_substr($data[1],0,15));
$intent_complex = (mb_strlen($meaning)>0) ? $meaning : $substance;
$intent = htmlspecialchars(trim(mb_substr($intent_complex,0,50)));
if( mb_strlen($intent_complex)>mb_strlen($intent) ) $intent.='...';

if($num>0) {
	$tag = '<span style=\'color:white\'>Текущая цель/намерение:</span><hr><div style=\'padding-top:10px; padding-bottom:10px\'>'.trim(mb_substr(strip_tags($data[0]),0,95)).'...</div><hr><span style=\'color:white; font-size:8pt\'>Ее можно изменить или удалить</span>';
	}
else $tag = 'Вы можете установить свою основную цель или текущее намерение, создав новый хаб.<div style=\'color:white; font-weight:100\'>Нажмите, чтобы продолжить.</div>';

echo '<div class="user-box-item qHint" tag="'.$tag.'" style="font-size:0.8em; font-family:Arial, Helvetica, sans-serif">';

if($num>=0) echo '<img src="qmex_img/UI/hubster/idea-curr.png" style="float:left; padding-right:5px" width="15px"><a href="note?id='.$num.'" style="color:white; font-weight:bold">'.$intent.'</a>'; else echo '<img src="qmex_img/UI/hubster/no-idea-curr.png" style="float:left; padding-right:5px" width="15px"><a href="#hub" style="color:white; font-weight:bold">Имеется цель?</a>';

echo '</div>';
?> 
</a>
</td>

<td align="right">
<form method="post" id="LoginMain" name="LoginMain">
<button name="Exit" class="qButton" onclick="tryExit();return false;">Выйти</button>
<span id="h_a_s">
<span style="padding-right:5px; width:25px; float:right" id="has">
<button class="qButton" id="hide_and_seek" style="width:20px;">-</button>
</span>
</span>
</form>
</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:100%">
<tr><td width="1%">
<div style="text-align:center; padding-top:5px; width:15px; height:30px; border:white solid 1px;" class="user-box-item qHint" tag='qМеню' id="userMenu"><img src="qmex_img/arrow_down.png" height="10px"></div>
</td><td align="left" valign="middle">
<div align="left" class="user-box-item qHint" tag="Это вы" style="font-family:Verdana, Geneva, sans-serif; font-size:1em;" onclick="location.href='profile?id=<?php echo urlencode($login_p)?>'" title="<?php echo $login_p?>">
<img src="<?php echo Human::getUserPhoto($_SESSION['id'])?>" width="30px" height="30px" style="float:left; padding-right:5px"> <?php echo $login_m?></div>
</td></tr>
</table>

</div>

<script language="javascript">

function tryExit()
{
	WINDOW.Confirm("Вы уверены, что желаете выйти из qMex?",function(){ 
		SendDynamicMsg(['Exit'],[1],null,'/','POST');
		location.href='/';
	 });
	}


$(function(){

if(<?php echo isset($show_greeting) ? 'true' : 'false' ?>)
	WINDOW.ShowMessage('Добро пожаловать в qMex!', WINDOW.C.STANDARD, 2);

$("#userMenu").each(function(index, element) {ContextMenu(element);});
$("#userMenu").toggle(function(){ContextMenu(this)},function(){rContextMenu(this)});
$("#hide_and_seek").toggle(
	function(){
		$("#userdata").slideUp('slow'); 
		 window.setTimeout(
			function(){
				$("#has").appendTo("#userdata_reserve")},600);
				$("#userdata_reserve").width($("#userdata").width());
				 $(this).text('+')
				 }, 
			function(){
				$("#userdata").slideDown('slow');
				$("#has").prependTo("#h_a_s"); $(this).text('-');
				}
			);
					
_resize();

});

var _resize = function(){$("#userdata").width($("#tablog").width()+"px"); }
_resize();
				
$(window).resize(function(e) {
	_resize();
});

</script>
