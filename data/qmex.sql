-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 28 2014 г., 14:04
-- Версия сервера: 5.5.16
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `qmex`
--

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_achievements`
--

CREATE TABLE IF NOT EXISTS `qmex_achievements` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `interest` int(5) unsigned NOT NULL,
  `bonus` int(5) unsigned NOT NULL,
  `creator` int(5) NOT NULL,
  `time_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `belong_to_dev` int(15) NOT NULL,
  `belong_to_user` int(15) NOT NULL DEFAULT '-1',
  `ach_kind` smallint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Дамп данных таблицы `qmex_achievements`
--

INSERT INTO `qmex_achievements` (`id`, `description`, `interest`, `bonus`, `creator`, `time_creation`, `belong_to_dev`, `belong_to_user`, `ach_kind`) VALUES
(6, 'Разработчик мобильного приложения Элжур', 34, 20, 70, '2014-05-07 20:41:08', 51, 178, 1),
(7, 'Взломщик закрытого сервера', 34, 15, 70, '2014-05-09 13:36:52', 52, 70, 1),
(12, 'Знание сферы', 34, 0, -1, '2014-06-18 14:31:32', -1, 70, 2),
(17, 'Знание сферы', 34, 0, -1, '2014-06-19 09:36:41', -1, 178, 2),
(18, 'Default killer', 47, 21, 70, '2014-06-26 14:20:31', 55, -1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_comments`
--

CREATE TABLE IF NOT EXISTS `qmex_comments` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Owner` int(10) NOT NULL,
  `Attachment` int(10) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Text` varchar(600) NOT NULL,
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=499 ;

--
-- Дамп данных таблицы `qmex_comments`
--

INSERT INTO `qmex_comments` (`ID`, `Owner`, `Attachment`, `Type`, `Text`, `DateTime`) VALUES
(374, 70, 121, 'ab_note', 'Пора бы заменить поднадоевшую среду MySQL( и сам SQL ) на что-то более современное, безопасное и эффективное. Я бы предложил альтернативную СУБД на несколько порядков и уровней выше - MongoDB. Что думаете, друзья? \n[ Просьба отвечать на этот комментарий, а не писать новые! ]', '2013-08-25 16:40:16'),
(443, 70, 121, 'ab_note', 'Тевирп', '2013-08-25 19:13:45'),
(449, 70, 25, 'quer_query', 'hrthrtyrty', '2013-08-26 12:01:07'),
(450, 70, 449, 'comment', 'rtyrtyrty', '2013-08-26 12:01:14'),
(451, 70, 450, 'comment', 'rtyrtyrtyfgh', '2013-08-26 12:01:19'),
(452, 70, 450, 'comment', 'fghfghfghfgh', '2013-08-26 12:01:22'),
(453, 70, 28, 'ab_note', 'fgdfsgdsfgsdg', '2013-08-27 11:10:40'),
(455, 70, 103, 'ab_note', 'Make it better faster closer nicy..', '2013-08-27 11:14:48'),
(461, 70, 103, 'ab_note', 'https://bitbucket.org/dashboard/overview', '2013-08-27 12:33:26'),
(466, 19, 457, 'comment', 'WTF is it?', '2013-08-27 13:10:58'),
(470, 70, 469, 'comment', 'erertert', '2013-08-27 13:24:04'),
(471, 19, 470, 'comment', 'ertertertert', '2013-08-27 13:24:14'),
(473, 18, 122, 'ab_note', 'qMex - это то, что так нужно современным людям =)', '2013-08-29 11:30:20'),
(474, 70, 473, 'comment', 'О, да, спасибо!', '2013-08-29 11:30:41'),
(475, 18, 474, 'comment', 'Правда, и только =)', '2013-08-29 11:32:24'),
(476, 70, 17, 'ab_note', 'Оценка: 5+', '2013-09-08 15:27:32'),
(477, 70, 26, 'quer_query', 'Откат!', '2013-11-24 20:43:06'),
(487, 178, 182, 'ab_note', 'Sacks 2', '2014-03-11 16:08:27'),
(488, 70, 487, 'comment', 'Cant understand you, buddy.', '2014-03-16 14:11:58'),
(490, 70, 489, 'comment', 'Non-agreed! It extremelly shameless lie !!!', '2014-04-22 18:55:08'),
(497, 70, 193, 'ab_note', 'Ingeniusly, isn`t it ?', '2014-06-02 17:31:13'),
(498, 19, 215, 'ab_note', '43534525235 89', '2014-06-25 10:20:18');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_conferences`
--

CREATE TABLE IF NOT EXISTS `qmex_conferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `is_private` tinyint(1) NOT NULL,
  `members` text NOT NULL,
  `members_not_joined` text NOT NULL,
  `plan_start` bigint(20) NOT NULL,
  `vector_interest` int(5) NOT NULL,
  `words` varchar(255) NOT NULL DEFAULT '',
  `is_closed` int(1) NOT NULL DEFAULT '0',
  `last_activity` bigint(20) unsigned NOT NULL DEFAULT '0',
  `linked_with_type` varchar(255) NOT NULL,
  `linked_with_id` int(15) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Дамп данных таблицы `qmex_conferences`
--

INSERT INTO `qmex_conferences` (`id`, `creator`, `datetime`, `description`, `img`, `is_private`, `members`, `members_not_joined`, `plan_start`, `vector_interest`, `words`, `is_closed`, `last_activity`, `linked_with_type`, `linked_with_id`) VALUES
(10, 70, '2014-01-26 14:27:12', 'В США очередная крупная утечка данных кредитных карт\nНа этой неделе мы писали про нашумевшую историю, связанную с компрометацией крупной американской ритейлерной сети Target.\n', '', 0, '', '', -1, 45, '', 0, 0, '', 0),
(14, 70, '2014-01-26 14:45:18', 'PHP, Perl - а что думаете Вы?\n', '', 0, '', '', -1, 34, '', 0, 0, '', 0),
(23, 70, '2014-04-14 16:25:11', 'Потребительский психоанализ: мышление или архетип?\n\n', '', 0, '', '', -1, 35, 'психология человек сознание разум', 0, 0, 'event', 5),
(28, 70, '2014-05-01 14:11:13', 'Приглашаю специалистов по Node.js и JavaScript\n', 'http://webkul.com/blog/wp-content/uploads/2013/04/nodejs.png', 0, '', '', -1, 34, 'node js javascript server programming', 0, 1398953473, 'event', 32),
(42, 70, '2014-05-07 20:41:08', 'С января по февраль 2013 года Электронный журнал ЭлЖур проводил творческий конкурс на создание мобильного приложения для смартфонов на базе iOS/Android.\n\n', 'http://kamsosh10.ucoz.ru/PDF/raznoe/dnevnik-ru.jpg', 0, '', '', -1, 34, 'программирование элжур разработка творчество', 0, 1399495268, 'event', 51),
(43, 178, '2014-05-09 13:36:52', 'Взлом закрытого сервера\r\n\r\n', 'http://www.e11help.de/blog/wp-content/languages/linux-console-scrollback-i15.jpg', 0, '', '', -1, 34, 'сервер взлом дебаг', 0, 1399806820, 'event', 52),
(44, 70, '2014-06-21 10:54:53', 'dgdfgdfsgdfgfds', 'gfdsgdsfgdfsg', 1, ';19;', '', -1, 15, 'dfsgdf gdsgdfsgdf sdf', 0, 1403348093, '', 0),
(45, 70, '2014-06-21 10:55:32', '', '', 1, ';18;;178;;179;;90;;19;', '', 0, -1, '', 0, 1403604027, 'env', 70),
(46, 19, '2014-06-21 11:00:19', '', '', 1, ';70;;178;', '', 0, -1, '', 0, 0, 'env', 19),
(47, 185, '2014-06-21 12:25:13', 'Командное окружение', '', 1, ';168;', '', 0, -1, '', 0, 0, 'env', 185),
(48, 168, '2014-06-21 12:25:13', 'Командное окружение', '', 1, ';185;', '', 0, -1, '', 0, 0, 'env', 168),
(49, 178, '2014-06-22 10:17:39', 'Общение команды', '', 1, ';70;;19;', '', 0, -1, '', 0, 0, 'env', 178),
(50, 70, '2014-06-26 14:20:31', '\n\n\nKill all humans\n\n', 'http://tort.fm/sites/default/files/images/watch_dogs-wide.jpg', 0, '', '', -1, 47, 'default killer human watch dogs massacre', 0, 1403792431, 'event', 55);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_conferences_opinions`
--

CREATE TABLE IF NOT EXISTS `qmex_conferences_opinions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `conf_id` int(15) NOT NULL,
  `creator` int(11) NOT NULL,
  `content` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Дамп данных таблицы `qmex_conferences_opinions`
--

INSERT INTO `qmex_conferences_opinions` (`id`, `conf_id`, `creator`, `content`, `datetime`) VALUES
(81, 10, 178, '<!DOCTYPE HTML>\r\n<html>\r\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\r\n<body><p>Утечки происходят постоянно. Кто-то должен это предотвращать!</p></body>\r\n</html>', '2014-03-22 19:57:48'),
(93, 10, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<div id="mainContent">\n<div id="searchResults">\n<div id="icon_30881_6" class="icon">\n<div> </div>\n<div> </div>\n<div> </div>\n<div><span style="background-color: #ff0000;"><strong>Good Night, Benders!</strong></span></div>\n<div> <a href="http://www.iconsearch.ru/detailed/30881/6/" target="_blank"><img title="Иконка discuss" src="http://www.iconsearch.ru/uploads/icons/freeicons/128x128/chat.png" alt=""></a>\n</div>\n</div>\n<div id="icon_27207_6" class="icon"> </div>\n</div>\n</div>\n<div id="bottomAdv"> </div>\n</body>\n</html>', '2014-04-08 10:03:34'),
(98, 14, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>I eat elephants!</p></body>\n</html>', '2014-04-11 22:13:54'),
(99, 23, 70, '<!DOCTYPE HTML>\r\n<html>\r\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\r\n<body>\r\n<table style="height: 322px;" width="518"><tbody><tr>\r\n<td><img src="http://www.chernetskaya.ru/obuchenie6gipnozu.jpg" alt="" width="188" height="230"></td>\r\n<td></td>\r\n</tr></tbody></table>\r\n</body>\r\n</html>', '2014-04-14 17:24:33'),
(108, 43, 70, 'Видимо, в этот день его кто-то другой положил.', '2014-05-11 10:40:33'),
(109, 43, 19, 'Не понимаю, почему сервер заблокирован и вообще не отвечает на запросы?', '2014-05-11 11:13:40'),
(110, 45, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Что-то кульное</p></body>\n</html>', '2014-06-22 10:33:51'),
(111, 45, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Тролололол</p></body>\n</html>', '2014-06-24 10:00:27');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_developments`
--

CREATE TABLE IF NOT EXISTS `qmex_developments` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `creator` int(10) NOT NULL,
  `caption` varchar(60) NOT NULL,
  `description` varchar(8000) NOT NULL,
  `theme` int(11) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `event_kind` int(5) unsigned NOT NULL,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `picture` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actual_time` bigint(20) NOT NULL DEFAULT '0',
  `finish` bigint(20) NOT NULL,
  `belong_to_conf` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Дамп данных таблицы `qmex_developments`
--

INSERT INTO `qmex_developments` (`id`, `creator`, `caption`, `description`, `theme`, `keywords`, `event_kind`, `country`, `city`, `latitude`, `longitude`, `picture`, `created_at`, `actual_time`, `finish`, `belong_to_conf`) VALUES
(3, 70, 'Совместное катание на велосипедах!', '<!DOCTYPE HTML>\r\n<html>\r\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\r\n<body><p>Уважаемые клубни, сегодня я являюсь гласом нашей соклубницы - Ланчик&amp;#039;а. Появилось предложение устроить совместные потаскушки на роликах /великах (кто на чем) в парке Кусково. Если есть желающие, то предлагайте дату, т.к. мы определиться пока не можем. Так же принимаются предложения о других местах катания</p></body>\r\n</html>\r\n', 39, 'велик катание совместный активный отдых', 1, 'Россия', 'Москва', 55.8188961, 37.7207234, 'http://tuchkovo.rujazi.com/images/classified/485746.jpg', '2014-04-20 12:23:31', 1398924000, 1398924000, 0),
(4, 70, 'Борьба за отмену закона о хождении задом наперед!', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img style="float: left;" src="http://0432.in/media/k2/items/cache/a7bee8a78ec1c08f2d86fa26ffc30ce3_XL.jpg" alt="" width="118" height="90">Друзья! Объединяйте свои жалкие пролетарские силы во благо борьбы с тупыми недодемократами, жаждущими превратить жалкий пролетариат в существ, ходяших задом наперед! Мы им ще покажемо!</p></body>\n</html>\n', 20, 'борьба революция спорт actually байжеуэй', 1, 'Россия', 'Москва', 55.8188163, 37.7207398, 'http://0432.in/media/k2/items/cache/a7bee8a78ec1c08f2d86fa26ffc30ce3_XL.jpg', '2014-04-20 15:52:43', 1398402000, 1398402000, 0),
(5, 70, 'Играем в Футбол', '<!DOCTYPE HTML>\r\n<html>\r\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\r\n<body><p>\r\nСобираем команду для игры в футбол\r\n</p></body>\r\n</html>\r\n', 39, 'футбол игра совместно foolish football', 1, 'Россия', 'Москва', 55.8191262, 37.7209095, 'http://www.wallplanet.ru/_ph/17/2/160927682.jpg', '2014-04-30 17:43:01', 1399719600, 1399719600, 0),
(32, 70, 'Первая встреча «Киев Node.js»', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p>Приглашаю специалистов по Node.js и JavaScript из Киева, всех, кто осознал удобство и простоту серверного программирования на JS, ощутил или хочет ощутить преимущества сквозного языка для клиента и сервера, на чаепитие с обменом опытом, которое будет проводиться 4 раза в год. Это первая встреча, но я надеюсь, что нас наберется хоть десятка два.<br><br>На повестке дня на первый раз:</p>\n<ul>\n<li>Знакомство и личные истории перехода на Node.js (кто имеет)</li>\n<li>Обсуждение технологии и успешных примеров внедрения</li>\n<li>Обмен опытом и ответы на вопросы от самых опытных нодовцев</li>\n<li>Налаживание технологической взаимопомощи по Node.js в Киеве и Украине</li>\n</ul>\n<p><br><br>Формат: круглый стол<br>Продолжительность: 2 часа + свободное общение<br>Просьба регистрироваться на <a href="http://www.meetup.com/KievNodeJS/" target="_blank">http://www.meetup.com/KievNodeJS/</a><br><br>С уважением,<br>~Марк Аврелий</p>\n</body>\n</html>\n', 34, 'node js javascript server programming', 1, 'Россия', 'Москва', 55.818898, 37.7207715, 'http://webkul.com/blog/wp-content/uploads/2013/04/nodejs.png', '2014-05-01 14:11:13', 1399719600, 1399719600, 0),
(34, 70, 'Посещение обсерватории', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><strong>Молетская обсерватория</strong> была открыта в 1969 году, придя на смену двум старым Вильнюсским обсерваториям, одна из которых появилась в 1753 году, а другая — в 1921. Место для новой было выбрано за чертой города, у деревушки Кулионяй, на двухсотметровом холме Кальдиняй. А несколько лет назад рядом с обсерваторией появился совершенно особый музей — Этно-космологический. Его здание построено из алюминия и стекла: на фоне местных озерно-лесных пейзажей музей выглядит приземлившимся космическим кораблем. <span style="background-color: #808000; color: #ffffff;">Экспозиция под стать: космические артефакты, обломки метеоритов и масса всего занимательного.</span></p></body>\n</html>\n', 55, 'обсерватория визит посещение совместно !', 1, 'Россия', 'Москва', 55.8191254, 37.7209876, 'http://www.911sevastopol.org/img/userfiles/e8e9df2b261e979a39192f5c57c0e1ab.jpg', '2014-05-01 15:47:56', 1399262700, 1399262700, 19),
(35, 70, 'Выяснение: Почему независим экстремум функции?', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Несмотря на сложности, рациональное число соответствует нормальный интеграл от функции, имеющий конечный разрыв, что и требовалось доказать. Следствие: ротор векторного поля упорядочивает анормальный интеграл от функции, обращающейся в бесконечность вдоль линии, явно демонстрируя всю чушь вышесказанного. Геодезическая линия проецирует линейно зависимый многочлен, что неудивительно. Умножение двух векторов (скалярное) осмысленно восстанавливает эмпирический интеграл по поверхности, что известно даже школьникам. Поэтому интеграл от функции комплексной переменной раскручивает интеграл Фурье, при этом, вместо 13 можно взять любую другую константу.</p></body>\n</html>\n', 3, 'Выяснение независимость экстремум функция', 1, 'Россия', 'Москва', 55.8191367, 37.720999, 'http://www.booksite.ru/fulltext/1/001/009/001/223363700.jpg', '2014-05-01 15:50:50', 1398980888, 1398980888, 0),
(40, 70, 'Эрвин Блюменфельд', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>В рамках 10-го Международного месяца фотографии в Москве «Фотобиеннале 2014» — ретроспективную выставку легендарного фотографа XX века — Эрвина Блюменфельда.</p></body>\n</html>\n', 23, 'выставка Эрвин Блюменфельд', 4, 'Россия', 'Санкт-Петербург', 59.9342802, 30.3350986, 'http://a-a-ah.ru/data/event/m1/m1-15/15556/images/533afd805d933082168532.x1280u.jpg?f8f8b67eaca1c554c8f27c3dd8cbe38b', '2014-05-05 17:22:20', 1399622400, 1399622400, 0),
(41, 70, 'Спектакль "Страх"', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Основан на картине Райнера Вернера Фассбиндера «Страх съедает душу» о любви пожилой уборщицы и молодого мигранта-араба.</p></body>\n</html>\n', 21, 'Страх съедает душу', 2, 'Россия', 'Москва', 57.8187109, 37.721054, 'http://a-a-ah.ru/data/event/m1/m1-6/6882/images/53405b299c9b9080928779.x1280u.jpg?4fa069a6da30fabf83fdc3826cab57b3', '2014-05-05 19:20:23', 1399806000, 1399806000, 19),
(51, 70, 'Разработка мобильного приложения ЭлЖур', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p>С января по февраль 2013 года Электронный журнал ЭлЖур проводил творческий конкурс для школьников на создание мобильного приложения для смартфонов на базе iOS/Android.</p>\n<p>Всего зарегистрировались для участия в конкурсе свыше 80 российских школьников, однако конкурсных приложений мы получили совсем немного, и в итоге лишь одна номинация получила своего победителя – начинающего разработчика под Android.</p>\n</body>\n</html>\n', 34, 'программирование элжур разработка творчество', 2, 'Россия', 'Москва', 55.8189697, 37.7205785, 'http://kamsosh10.ucoz.ru/PDF/raznoe/dnevnik-ru.jpg', '2014-05-07 20:41:08', 1400151600, 1401361200, 0),
(52, 178, 'Взлом сервера', 'Взлом сервера и загрузка эксплойта для эксплуатации найденной уязвимости.\r\n', 34, 'сервер взлом эксплойт', 2, 'Россия', 'Москва', 55.8189804, 37.720761, 'http://www.e11help.de/blog/wp-content/languages/linux-console-scrollback-i15.jpg', '2014-05-09 13:36:52', 1399642560, 1399728960, 0),
(54, 70, 'La belezza', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p><strong>Viva la musica! Main Stage:</strong> <br>12.00 -12.30 - esercizi di mattina da Kinder <br>12,30-13,15 Presentazione degli studenti delle scuole italiane «Italo Calvino» sotto la guida del noto musicista Antonio Gramsci. Il repertorio del complesso - classica, opere contemporanee, musica popolare nella disposizione dellautore. <br>13.45 sfilata di moda per bambini - 13.30 <br>13,45-14,45 Presentazione di Monica Santoro (Monica Santoro) - cantante italiano, in cui il repertorio italiano colpisce 60-zioni del secolo scorso. <br>15.00 - 15.30 Presentazione delle italiane "Oscar culinarie" - che rilascia ristoranti italiani certificato internazionale Ospitalità Italiana. <br>15,30-16,30 Presentazione del ensemble di musica antica La Campanella. <br>1630-1700 gare dei partner del Festival <br>17,00-18,00 Opera per le future mamme "Alcina, o Magic Island" per bambini Musical Theater. Natalia Sats. <img src="tools/sided/tiny/plugins/emoticons/img/smiley-yell.gif" alt=""><br>18,30-19,30 Anneya. Pashynska Anna (Anna) ha girato con successo il mondo con progetti elettronici, prime opere della Russia e scrive musica per arpa e-mail <br>19,30-21,00 swing Italiano. Classiche melodie del jazz italiano nel trattamento dellautore, eseguiti da Christian Tola (Christian Tola), con laccompagnamento del quintetto.</p>\n<p> </p>\n<p><img src="tools/sided/tiny/plugins/emoticons/img/smiley-cool.gif" alt=""><img src="tools/sided/tiny/plugins/emoticons/img/smiley-cool.gif" alt=""><img src="tools/sided/tiny/plugins/emoticons/img/smiley-cool.gif" alt=""></p>\n</body>\n</html>\n', 21, 'la belezza italia', 1, 'Italy', 'Florenzia', 43.7710332, 11.2480006, 'http://a-a-ah.ru/data/event/m1/m1-18/18126/images/53722aedbed35522304811.x1280uB.jpg?142b681da8ec468ebcec67dcdf73daff', '2014-05-25 12:51:26', 1401530400, 1401789600, 0),
(55, 70, 'Massacre', '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><h1><span style="color: #000000; background-color: #ffff99;">Kill all humans<img src="tools/sided/tiny/plugins/emoticons/img/smiley-money-mouth.gif" alt=""></span></h1></body>\n</html>\n', 47, 'default killer human watch dogs massacre', 2, 'Россия', 'Москва', 55.8189817, 37.7208805, 'http://tort.fm/sites/default/files/images/watch_dogs-wide.jpg', '2014-06-26 14:20:31', 1403866800, 1404212400, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_dev_members`
--

CREATE TABLE IF NOT EXISTS `qmex_dev_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Дамп данных таблицы `qmex_dev_members`
--

INSERT INTO `qmex_dev_members` (`id`, `event_id`, `user`) VALUES
(24, 4, 70),
(25, 41, 70),
(26, 52, 70),
(27, 52, 178),
(28, 52, 19),
(29, 51, 70),
(30, 51, 178);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_dialogs`
--

CREATE TABLE IF NOT EXISTS `qmex_dialogs` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `Cause` int(15) NOT NULL,
  `EssenceID` int(15) NOT NULL,
  `Member1` int(15) NOT NULL,
  `Member2` int(15) NOT NULL,
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `qmex_dialogs`
--

INSERT INTO `qmex_dialogs` (`id`, `Cause`, `EssenceID`, `Member1`, `Member2`, `DateTime`, `updated`) VALUES
(1, 1, 19, 70, 19, '2014-01-12 10:14:41', 1403957041),
(2, 3, 83, 70, 19, '2014-01-12 11:45:12', 1389623224),
(3, 4, 168, 19, 70, '2014-01-13 16:53:25', 1397313063);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_dialogs_msg`
--

CREATE TABLE IF NOT EXISTS `qmex_dialogs_msg` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `dialog_id` int(15) NOT NULL,
  `Creator` int(15) NOT NULL,
  `Message` text NOT NULL,
  `IsRead` tinyint(1) NOT NULL DEFAULT '0',
  `Thanked` tinyint(1) NOT NULL DEFAULT '0',
  `AnswerOf` int(15) NOT NULL DEFAULT '-1',
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=71 ;

--
-- Дамп данных таблицы `qmex_dialogs_msg`
--

INSERT INTO `qmex_dialogs_msg` (`id`, `dialog_id`, `Creator`, `Message`, `IsRead`, `Thanked`, `AnswerOf`, `DateTime`) VALUES
(4, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>QQ ALL !!!</p></body>\n</html>\n', 1, 0, -1, '2014-01-12 23:06:37'),
(5, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Now u are right!</p></body>\n</html>\n', 1, 0, -1, '2014-01-12 23:06:38'),
(6, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Нужно!</p></body>\n</html>\n', 1, 0, -1, '2014-01-12 23:06:50'),
(7, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>dfgdfgdfgdfgdfg</body>\n</html>\n', 1, 0, -1, '2014-04-16 19:05:22'),
(8, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Nuclear</body>\n</html>\n', 1, 0, -1, '2014-01-12 23:06:39'),
(9, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>ssssss</body>\n</html>\n', 1, 0, -1, '2014-01-12 22:55:30'),
(12, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>3453453453</body>\n</html>\n', 1, 0, -1, '2014-01-12 22:55:30'),
(13, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>34551252425 =))))</body>\n</html>\n', 1, 0, -1, '2014-01-12 23:06:40'),
(14, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>29 000 000 $ USA</body>\n</html>\n', 1, 1, -1, '2014-06-28 11:23:52'),
(17, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>sdfsdfsdfsdfsd</body>\n</html>\n', 1, 1, -1, '2014-06-28 11:23:54'),
(32, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>ну да!</body>\n</html>\n', 1, 0, 6, '2014-01-12 22:38:55'),
(34, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>dfdfdf</body>\n</html>\n', 1, 0, 12, '2014-01-13 01:52:57'),
(35, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>LOLOLOLOLO RO MONO</body>\n</html>\n', 1, 0, 14, '2014-01-12 23:06:34'),
(36, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Ну и !?</body>\n</html>\n', 0, 0, 6, '2014-01-12 23:07:00'),
(37, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Ok, когда будет возможно?</body>\n</html>\n', 0, 0, 36, '2014-01-12 23:07:31'),
(38, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img src="qmex_img/smiles/smiles_better/icon14.gif" alt=""><img src="qmex_img/smiles/smiles/icon25.gif" alt=""> GoodNight, Amigos !</p></body>\n</html>\n', 1, 0, 37, '2014-03-23 14:02:58'),
(39, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Не совсем вас понимаю..</body>\n</html>\n', 0, 0, 38, '2014-01-12 23:10:13'),
(44, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>25 000 usd of usa</body>\n</html>\n', 1, 0, 38, '2014-01-13 01:52:43'),
(45, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Go fuck? amigo!</body>\n</html>\n', 1, 0, 44, '2014-05-22 19:52:35'),
(46, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img src="qmex_img/smiles/smiles_advanced/face3.png" alt=""></p></body>\n</html>\n', 0, 0, 45, '2014-01-13 01:55:42'),
(47, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>Чикапукка</p></body>\n</html>\n', 0, 0, -1, '2014-01-13 14:13:07'),
(48, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Ддэньяна</body>\n</html>\n', 1, 0, 47, '2014-03-23 14:02:43'),
(49, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img src="qmex_img/smiles/smiles_advanced/face2.png" alt="">кукук</p></body>\n</html>\n', 0, 0, 48, '2014-01-13 14:15:42'),
(50, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Корпоративные сайты без новостей на главной странице уже можно заносить в Красную книгу — вымирают. 02 «Новости на морде» — это самый дешевый способ показать посетителю, что сайт еще живой. Есть и более дорогие способы (создавать тематические анонсы, на самом деле обновлять сайт и др.), им посвящены другие параграфы. 03 Во-первых, нужно честно ответить на вопрос — существуют ли новости у сайта N на самом деле. Если сайт обновляется реже одного раза в месяц, автор рекомендует не использовать слово новости вообще. Список из предложений со ссылками — он и в лесу список из предложений со ссылками, нет необходимости изменять его статус до оперативного. 04 Во-вторых, если новость протухла, ее необходимо перемещать в архив. Срок годности фразы «сегодня состоится…» — менее суток. 05 В-третьих, надо бороться с датами при новостных заголовках. Даты имеют право на существование либо непосредственно в тексте сообщения, либо в архиве. Новость — она на то и новость, чтобы иметь актуальное содержание. Даты являются совершенно мусорным элементом, засоряющим пространство сайта. 06 Допустим, мы заходим на сайт компании N, где наблюдаем типичное:\n\nИсточник: http://www.artlebedev.ru/kovodstvo/sections/129/</body>\n</html>\n', 0, 0, 48, '2014-01-13 14:21:57'),
(51, 2, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>23423</body>\n</html>\n', 1, 0, 50, '2014-01-13 14:35:54'),
(52, 2, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>https://play.google.com/apps/publish/?dev_acc=07945154456045655041#AppListPlace</body>\n</html>\n', 0, 0, 51, '2014-01-13 14:27:04'),
(53, 3, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>fdgdfgdfgdfg<img src="qmex_img/smiles/smiles_advanced/face3.png" alt=""></p></body>\n</html>\n', 0, 0, -1, '2014-01-13 16:53:25'),
(54, 3, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Scusi, IO ho imparato la italiano solo uno mese</body>\n</html>\n', 0, 0, 53, '2014-01-14 19:43:56'),
(55, 3, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p>55555555555555555555</p></body>\n</html>\n', 0, 0, 53, '2014-04-12 14:24:27'),
(59, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img src="http://spb.rujazi.com/images/classified/558102.jpg" alt="" width="391" height="391"></p></body>\n</html>\n', 0, 0, 17, '2014-04-12 14:41:38'),
(60, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><p><img src="http://englisch-hilfen.net/wp-content/uploads/2013/03/Englisch-Hilfen3.jpg" alt="" width="300" height="300"></p></body>\n</html>\n', 1, 1, 59, '2014-06-28 11:23:47'),
(61, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p><iframe src="http://www.youtube.com/embed/xzcaW7c98_s" width="425" height="350"></iframe></p>\n<p><a href="http://habrahabr.ru/post/115825/" target="_blank">http://habrahabr.ru/post/115825/</a></p>\n<p><img src="http://superadm.net/uploads/news/htmltext6.png" alt="" width="200" height="272"></p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n<p> </p>\n</body>\n</html>\n', 1, 1, 59, '2014-06-28 11:23:44'),
(62, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n\nhttp://habrahabr.ru/post/115825/\n\n\n\nhttp://habrahabr.ru/post/115825/\n</body>\n</html>\n', 0, 1, 61, '2014-06-28 11:46:09'),
(68, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Используйте Xamarin для кроссплатформенности.</body>\n</html>\n', 0, 1, 62, '2014-06-28 11:09:07'),
(69, 1, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Почему не PhoneGAP ?</body>\n</html>\n', 0, 1, 68, '2014-06-28 11:14:20'),
(70, 1, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>Потому что он не такой функциональный</body>\n</html>\n', 0, 0, 69, '2014-06-28 12:04:01');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_events`
--

CREATE TABLE IF NOT EXISTS `qmex_events` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` int(11) NOT NULL,
  `Login` int(11) NOT NULL,
  `Maker` int(11) NOT NULL,
  `Essence` int(11) NOT NULL DEFAULT '0',
  `Action` int(11) NOT NULL DEFAULT '0',
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=583 ;

--
-- Дамп данных таблицы `qmex_events`
--

INSERT INTO `qmex_events` (`ID`, `Type`, `Login`, `Maker`, `Essence`, `Action`, `DateTime`) VALUES
(71, 1, 18, 42, 5, 1, '2013-08-11 03:22:17'),
(72, 3, 18, 42, 0, 0, '2013-08-11 03:22:32'),
(73, 1, 18, 70, 5, 1, '2013-08-11 03:22:47'),
(79, 1, 18, 19, 5, 1, '2013-08-11 03:24:12'),
(100, 0, 169, 70, 0, 1, '2013-08-11 12:14:56'),
(101, 2, 169, 70, 37, 0, '2013-08-11 12:21:37'),
(102, 2, 169, 70, 45, 0, '2013-08-11 12:21:40'),
(103, 2, 70, 169, 2, 0, '2013-08-11 12:22:17'),
(104, 2, 70, 169, 21, 0, '2013-08-11 12:22:19'),
(105, 2, 70, 169, 38, 0, '2013-08-11 12:22:21'),
(106, 2, 70, 169, 45, 0, '2013-08-11 12:22:23'),
(109, 3, 70, 173, 0, 0, '2013-08-12 15:43:02'),
(197, 1, 70, 19, 174, 1, '2013-08-14 12:42:35'),
(198, 1, 70, 19, 173, -1, '2013-08-14 12:45:25'),
(200, 1, 70, 19, 175, 1, '2013-08-14 12:46:58'),
(211, 1, 70, 19, 185, 1, '2013-08-14 16:07:09'),
(214, 1, 19, 70, 189, 1, '2013-08-14 16:11:29'),
(216, 0, 19, 70, 0, 1, '2013-08-16 09:00:35'),
(217, 2, 70, 19, 22, 0, '2013-08-18 11:28:48'),
(218, 1, 70, 19, 17, 1, '2013-08-18 13:40:16'),
(219, 1, 70, 107, 17, -1, '2013-08-18 13:40:45'),
(220, 1, 70, 19, 10, -1, '2013-08-18 13:41:05'),
(221, 1, 70, 19, 8, 1, '2013-08-18 13:41:08'),
(222, 1, 70, 19, 16, 1, '2013-08-18 15:39:19'),
(227, 1, 18, 70, 121, 1, '2013-08-22 11:03:19'),
(228, 0, 70, 18, 0, 1, '2013-08-25 10:45:42'),
(234, 6, 18, 70, 121, 0, '2013-08-25 19:13:45'),
(236, 6, 18, 107, 121, 0, '2013-08-25 19:19:07'),
(238, 6, 18, 107, 121, 0, '2013-08-25 19:20:58'),
(240, 6, 18, 107, 121, 0, '2013-08-25 19:21:12'),
(242, 6, 18, 107, 121, 0, '2013-08-25 19:23:05'),
(244, 0, 18, 70, 0, 1, '2013-08-26 11:56:45'),
(245, 3, 18, 70, 0, 0, '2013-08-26 11:57:00'),
(246, 5, 18, 70, 0, 0, '2013-08-26 11:57:29'),
(247, 1, 70, 19, 121, -1, '2013-08-27 08:34:37'),
(248, 1, 70, 19, 28, 1, '2013-08-27 08:37:05'),
(249, 1, 19, 70, 103, 1, '2013-08-27 09:04:46'),
(250, 1, 19, 70, 102, 1, '2013-08-27 09:04:57'),
(251, 3, 19, 70, 0, 0, '2013-08-27 09:05:09'),
(252, 3, 18, 70, 0, 0, '2013-08-27 09:41:37'),
(253, 1, 70, 107, 121, 1, '2013-08-27 10:10:11'),
(254, 1, 70, 107, 120, 1, '2013-08-27 10:10:16'),
(255, 1, 19, 107, 103, 1, '2013-08-27 10:10:20'),
(256, 1, 19, 107, 102, 1, '2013-08-27 10:10:24'),
(257, 1, 70, 107, 28, -1, '2013-08-27 10:10:27'),
(258, 5, 18, 70, 0, 0, '2013-08-27 10:41:52'),
(259, 3, 18, 70, 0, 0, '2013-08-27 10:41:57'),
(260, 5, 18, 70, 0, 0, '2013-08-27 10:42:00'),
(261, 5, 18, 70, 0, 0, '2013-08-27 10:42:03'),
(262, 5, 18, 70, 0, 0, '2013-08-27 10:42:05'),
(263, 3, 18, 70, 0, 0, '2013-08-27 10:42:13'),
(264, 5, 18, 70, 0, 0, '2013-08-27 10:42:19'),
(265, 5, 18, 70, 0, 0, '2013-08-27 10:42:21'),
(266, 3, 18, 70, 0, 0, '2013-08-27 10:44:06'),
(267, 5, 18, 70, 0, 0, '2013-08-27 10:44:10'),
(268, 3, 18, 70, 0, 0, '2013-08-27 10:44:22'),
(269, 5, 18, 70, 0, 0, '2013-08-27 10:44:24'),
(270, 3, 18, 70, 0, 0, '2013-08-27 10:44:29'),
(271, 5, 18, 70, 0, 0, '2013-08-27 10:49:47'),
(272, 3, 18, 70, 0, 0, '2013-08-27 10:49:51'),
(273, 5, 18, 70, 0, 0, '2013-08-27 10:50:38'),
(274, 3, 18, 70, 0, 0, '2013-08-27 10:50:41'),
(275, 5, 18, 70, 0, 0, '2013-08-27 10:50:53'),
(276, 3, 18, 70, 0, 0, '2013-08-27 10:51:01'),
(277, 5, 18, 70, 0, 0, '2013-08-27 10:52:57'),
(278, 3, 18, 70, 0, 0, '2013-08-27 10:54:15'),
(279, 5, 18, 70, 0, 0, '2013-08-27 10:54:18'),
(280, 3, 18, 70, 0, 0, '2013-08-27 10:54:29'),
(281, 5, 18, 70, 0, 0, '2013-08-27 10:54:32'),
(282, 3, 18, 70, 0, 0, '2013-08-27 10:54:53'),
(283, 5, 18, 70, 0, 0, '2013-08-27 10:54:56'),
(284, 3, 18, 70, 0, 0, '2013-08-27 10:55:06'),
(285, 5, 18, 70, 0, 0, '2013-08-27 10:55:08'),
(286, 3, 18, 70, 0, 0, '2013-08-27 10:55:16'),
(287, 2, 70, 19, 23, 0, '2013-08-27 11:10:05'),
(288, 2, 70, 19, 24, 0, '2013-08-27 11:10:07'),
(289, 2, 70, 19, 38, 0, '2013-08-27 11:10:09'),
(290, 2, 70, 19, 1, 0, '2013-08-27 11:10:11'),
(291, 2, 70, 19, 45, 0, '2013-08-27 11:10:15'),
(292, 2, 70, 19, 16, 0, '2013-08-27 11:10:16'),
(297, 6, 19, 70, 103, 461, '2013-08-27 12:33:26'),
(301, 7, 70, 19, 457, 466, '2013-08-27 13:10:58'),
(303, 8, 70, 19, 27, 469, '2013-08-27 13:23:38'),
(304, 7, 19, 70, 469, 470, '2013-08-27 13:24:04'),
(305, 7, 70, 19, 470, 471, '2013-08-27 13:24:14'),
(306, 1, 70, 18, 122, 1, '2013-08-29 11:29:42'),
(307, 6, 70, 18, 122, 473, '2013-08-29 11:30:20'),
(308, 7, 18, 70, 473, 474, '2013-08-29 11:30:41'),
(309, 7, 70, 18, 474, 475, '2013-08-29 11:32:24'),
(310, 1, 70, 18, 121, -1, '2013-08-29 11:32:44'),
(311, 1, 70, 18, 120, 1, '2013-08-29 11:33:01'),
(316, 3, 70, 18, 0, 0, '2013-08-29 11:43:38'),
(318, 3, 70, 18, 0, 0, '2013-08-29 11:43:46'),
(319, 1, 18, 70, 123, -1, '2013-08-29 11:49:32'),
(320, 1, 18, 70, 126, 1, '2013-08-29 11:53:23'),
(321, 1, 90, 70, 128, 1, '2013-08-30 05:17:28'),
(322, 1, 90, 70, 130, -1, '2013-08-30 09:06:26'),
(325, 1, 90, 107, 128, 1, '2013-08-31 16:12:47'),
(326, 1, 70, 19, 138, 1, '2013-11-10 13:19:34'),
(332, 1, 18, 70, 127, 1, '2013-11-10 18:15:37'),
(334, 3, 70, 178, 0, 0, '2013-11-16 16:38:20'),
(338, 2, 70, 19, 19, 0, '2013-11-20 11:13:14'),
(339, 2, 70, 19, 2, 0, '2013-11-20 11:13:15'),
(346, 1, 70, 19, 148, 1, '2013-11-26 18:28:40'),
(347, 1, 70, 178, 148, 1, '2013-11-26 18:29:18'),
(348, 2, 70, 178, 37, 0, '2013-11-26 18:30:12'),
(349, 1, 70, 18, 148, 1, '2013-11-26 18:35:45'),
(350, 1, 70, 42, 148, -1, '2013-11-26 18:41:33'),
(351, 1, 70, 42, 148, 1, '2013-11-26 18:42:07'),
(352, 1, 70, 42, 148, -1, '2013-11-26 18:42:24'),
(353, 1, 70, 42, 148, -1, '2013-11-26 18:44:19'),
(354, 1, 70, 42, 148, -1, '2013-11-26 18:45:36'),
(355, 1, 70, 42, 148, 1, '2013-11-26 18:45:58'),
(356, 1, 70, 42, 148, 1, '2013-11-26 18:46:27'),
(357, 1, 70, 42, 139, 1, '2013-11-26 19:37:18'),
(359, 2, 19, 70, 18, 0, '2013-11-27 19:47:31'),
(360, 2, 42, 70, 25, 0, '2013-11-27 19:57:24'),
(361, 2, 42, 70, 19, 0, '2013-11-27 19:57:39'),
(362, 2, 42, 70, 15, 0, '2013-11-27 19:58:02'),
(363, 1, 70, 178, 139, 1, '2013-12-06 13:34:45'),
(364, 0, 90, 70, 0, 1, '2013-12-14 17:53:26'),
(365, 3, 70, 179, 0, 0, '2013-12-14 20:26:24'),
(366, 2, 179, 70, 37, 0, '2013-12-14 20:32:06'),
(367, 2, 179, 70, 15, 0, '2013-12-14 20:32:08'),
(368, 2, 179, 70, 31, 0, '2013-12-14 20:32:09'),
(369, 0, 179, 70, 0, 1, '2013-12-14 20:32:13'),
(370, 0, 70, 179, 0, 1, '2013-12-14 21:30:42'),
(371, 1, 179, 70, 152, -1, '2013-12-15 10:07:35'),
(372, 1, 179, 70, 152, 1, '2013-12-15 10:11:48'),
(373, 1, 179, 70, 152, -1, '2013-12-15 10:13:13'),
(374, 1, 179, 70, 152, -1, '2013-12-15 10:16:16'),
(375, 1, 179, 70, 152, -1, '2013-12-15 10:19:11'),
(376, 2, 70, 19, 24, 0, '2013-12-20 18:01:08'),
(377, 2, 70, 19, 37, 0, '2013-12-20 18:01:09'),
(378, 2, 70, 19, 37, 0, '2013-12-20 18:02:03'),
(379, 2, 70, 19, 37, 0, '2013-12-20 18:03:32'),
(380, 2, 70, 19, 37, 0, '2013-12-20 18:22:25'),
(381, 3, 70, 19, 0, 0, '2013-12-20 18:32:32'),
(382, 5, 70, 19, 0, 0, '2013-12-20 18:32:37'),
(383, 3, 70, 19, 0, 0, '2013-12-20 18:32:40'),
(384, 5, 70, 19, 0, 0, '2013-12-20 18:32:54'),
(385, 3, 70, 19, 0, 0, '2013-12-20 18:34:05'),
(386, 5, 70, 19, 0, 0, '2013-12-20 18:34:09'),
(387, 1, 70, 19, 151, -1, '2013-12-20 18:41:42'),
(388, 1, 70, 19, 151, -1, '2013-12-20 18:42:56'),
(389, 1, 70, 19, 151, -1, '2013-12-20 18:45:30'),
(390, 1, 70, 19, 151, 1, '2013-12-20 18:51:55'),
(391, 1, 70, 19, 151, -1, '2013-12-20 18:56:37'),
(396, 1, 70, 178, 151, 1, '2013-12-20 19:01:39'),
(397, 1, 70, 19, 134, -1, '2013-12-21 10:49:20'),
(398, 3, 70, 19, 0, 0, '2013-12-21 22:48:01'),
(399, 5, 70, 19, 0, 0, '2013-12-21 22:48:29'),
(400, 2, 70, 19, 24, 0, '2013-12-21 22:49:29'),
(401, 2, 70, 19, 37, 0, '2013-12-21 22:49:39'),
(402, 0, 70, 90, 0, 1, '2013-12-21 22:51:51'),
(403, 3, 70, 90, 0, 0, '2013-12-21 22:55:11'),
(404, 5, 70, 90, 0, 0, '2013-12-21 22:55:14'),
(405, 3, 70, 90, 0, 0, '2013-12-21 22:55:18'),
(406, 1, 70, 19, 166, 1, '2013-12-22 15:02:42'),
(408, 5, 19, 70, 0, 0, '2014-01-08 12:51:01'),
(409, 3, 19, 70, 0, 0, '2014-01-08 12:51:06'),
(410, 3, 90, 70, 0, 0, '2014-01-08 18:20:26'),
(411, 5, 90, 70, 0, 0, '2014-01-08 18:20:30'),
(412, 3, 90, 70, 0, 0, '2014-01-08 18:21:07'),
(413, 5, 90, 70, 0, 0, '2014-01-08 18:22:01'),
(414, 3, 90, 70, 0, 0, '2014-01-08 18:22:30'),
(415, 5, 90, 70, 0, 0, '2014-01-08 18:25:08'),
(416, 3, 90, 70, 0, 0, '2014-01-08 18:25:11'),
(417, 5, 90, 70, 0, 0, '2014-01-08 18:25:34'),
(418, 3, 90, 70, 0, 0, '2014-01-08 18:25:51'),
(419, 5, 90, 70, 0, 0, '2014-01-08 18:25:56'),
(421, 1, 90, 70, 128, 1, '2014-02-02 14:12:06'),
(422, 3, 92, 70, 0, 0, '2014-02-09 17:10:58'),
(423, 6, 70, 178, 182, 487, '2014-03-11 16:08:27'),
(424, 1, 70, 178, 122, 1, '2014-03-11 19:17:56'),
(425, 2, 70, 178, 34, 0, '2014-03-14 20:14:52'),
(426, 7, 178, 70, 487, 488, '2014-03-16 14:11:58'),
(427, 3, 179, 70, 0, 0, '2014-04-05 17:34:59'),
(430, 1, 178, 70, 185, 1, '2014-04-11 11:19:32'),
(431, 2, 178, 70, 22, 0, '2014-04-11 11:19:40'),
(432, 2, 70, 178, 24, 0, '2014-04-11 16:39:42'),
(433, 0, 185, 70, 0, 1, '2014-04-12 09:24:11'),
(434, 2, 185, 70, 54, 0, '2014-04-12 09:25:00'),
(435, 2, 185, 70, 37, 0, '2014-04-12 09:25:04'),
(436, 0, 70, 178, 0, 1, '2014-04-12 11:47:36'),
(438, 2, 185, 70, 29, 0, '2014-04-13 18:28:01'),
(439, 0, 92, 70, 0, 1, '2014-04-16 19:07:17'),
(440, 1, 19, 70, 187, 1, '2014-04-22 18:59:46'),
(441, 0, 178, 70, 0, 1, '2014-05-09 19:11:30'),
(447, 9, 70, 19, 105, 0, '2014-05-12 18:44:16'),
(448, 7, 18, 19, 473, 491, '2014-05-13 18:32:11'),
(449, 7, 70, 19, 485, 492, '2014-05-13 18:32:52'),
(450, 6, 70, 19, 170, 493, '2014-05-13 18:33:22'),
(452, 9, 70, 19, 106, 0, '2014-05-14 11:15:20'),
(453, 3, 185, 70, 0, 0, '2014-05-22 19:57:03'),
(488, 1, 178, 70, 201, 1, '2014-05-28 16:51:09'),
(489, 6, 178, 70, 201, 495, '2014-05-28 16:51:41'),
(490, 3, 179, 191, 0, 0, '2014-05-28 18:58:08'),
(492, 10, 169, 192, 210, 0, '2014-06-03 12:18:45'),
(493, 10, 179, 192, 210, 0, '2014-06-03 12:18:45'),
(494, 10, 70, 192, 210, 0, '2014-06-03 12:18:45'),
(495, 10, 185, 192, 210, 0, '2014-06-03 12:18:45'),
(500, 2, 70, 192, 37, 0, '2014-06-05 19:58:30'),
(501, 2, 70, 192, 34, 0, '2014-06-05 19:58:35'),
(503, 11, 70, 70, 11, 0, '2014-06-15 09:38:37'),
(505, 9, 70, 192, 119, 0, '2014-06-15 10:21:25'),
(507, 10, 179, 192, 211, 0, '2014-06-17 09:41:40'),
(508, 10, 70, 192, 211, 0, '2014-06-17 09:41:40'),
(509, 10, 181, 192, 211, 0, '2014-06-17 09:41:40'),
(512, 1, 178, 19, 209, -1, '2014-06-17 09:46:49'),
(513, 3, 169, 70, 0, 0, '2014-06-17 11:45:14'),
(514, 5, 169, 70, 0, 0, '2014-06-17 11:45:27'),
(515, 3, 90, 70, 0, 0, '2014-06-17 11:55:25'),
(516, 5, 90, 70, 0, 0, '2014-06-17 11:55:46'),
(517, 3, 90, 70, 0, 0, '2014-06-17 11:55:52'),
(518, 5, 90, 70, 0, 0, '2014-06-17 11:56:32'),
(519, 3, 90, 70, 0, 0, '2014-06-17 11:56:39'),
(520, 5, 90, 70, 0, 0, '2014-06-17 12:35:10'),
(521, 3, 90, 70, 0, 0, '2014-06-17 12:43:00'),
(522, 5, 90, 70, 0, 0, '2014-06-17 12:43:10'),
(523, 3, 90, 70, 0, 0, '2014-06-17 12:43:46'),
(524, 5, 90, 70, 0, 0, '2014-06-17 12:43:53'),
(525, 3, 90, 70, 0, 0, '2014-06-17 12:44:09'),
(526, 5, 90, 70, 0, 0, '2014-06-17 12:44:15'),
(529, 5, 19, 70, 0, 0, '2014-06-17 13:42:17'),
(530, 3, 19, 70, 0, 0, '2014-06-17 13:42:19'),
(532, 10, 179, 70, 212, 0, '2014-06-18 14:26:08'),
(533, 10, 181, 70, 212, 0, '2014-06-18 14:26:08'),
(535, 11, 70, 70, 12, 0, '2014-06-18 14:31:32'),
(541, 11, 178, 178, 17, 0, '2014-06-19 09:36:41'),
(542, 3, 19, 178, 0, 0, '2014-06-19 10:18:48'),
(543, 3, 90, 178, 0, 0, '2014-06-19 10:18:54'),
(544, 3, 178, 70, 0, 0, '2014-06-19 10:19:16'),
(545, 3, 90, 70, 0, 0, '2014-06-19 10:20:14'),
(546, 3, 178, 19, 0, 0, '2014-06-19 10:22:44'),
(547, 3, 70, 19, 0, 0, '2014-06-19 10:22:50'),
(548, 3, 179, 19, 0, 0, '2014-06-20 11:42:33'),
(549, 5, 179, 19, 0, 0, '2014-06-20 11:42:37'),
(550, 3, 179, 19, 0, 0, '2014-06-20 11:47:20'),
(551, 5, 179, 19, 0, 0, '2014-06-20 11:47:24'),
(552, 3, 179, 19, 0, 0, '2014-06-20 11:47:28'),
(553, 5, 179, 19, 0, 0, '2014-06-20 11:47:32'),
(554, 3, 179, 19, 0, 0, '2014-06-20 11:47:42'),
(555, 5, 179, 19, 0, 0, '2014-06-20 11:48:20'),
(556, 3, 179, 19, 0, 0, '2014-06-20 11:48:22'),
(557, 5, 179, 19, 0, 0, '2014-06-20 11:48:34'),
(558, 3, 179, 19, 0, 0, '2014-06-20 11:48:35'),
(559, 5, 179, 19, 0, 0, '2014-06-20 11:50:25'),
(560, 3, 179, 19, 0, 0, '2014-06-20 11:50:27'),
(561, 3, 185, 168, 0, 0, '2014-06-21 12:24:54'),
(562, 3, 168, 185, 0, 0, '2014-06-21 12:25:13'),
(563, 12, 18, 70, 110, 0, '2014-06-22 10:33:51'),
(564, 12, 178, 70, 110, 0, '2014-06-22 10:33:51'),
(565, 12, 179, 70, 110, 0, '2014-06-22 10:33:51'),
(566, 12, 90, 70, 110, 0, '2014-06-22 10:33:51'),
(567, 12, 19, 70, 110, 0, '2014-06-22 10:33:51'),
(568, 12, 18, 19, 111, 0, '2014-06-24 10:00:27'),
(569, 12, 178, 19, 111, 0, '2014-06-24 10:00:27'),
(570, 12, 179, 19, 111, 0, '2014-06-24 10:00:27'),
(571, 12, 90, 19, 111, 0, '2014-06-24 10:00:27'),
(572, 12, 19, 19, 111, 0, '2014-06-24 10:00:27'),
(573, 0, 90, 19, 0, 1, '2014-06-26 10:01:42'),
(574, 2, 70, 19, 19, 0, '2014-06-26 10:07:10'),
(575, 2, 70, 19, 15, 0, '2014-06-26 10:08:21'),
(576, 5, 18, 70, 0, 0, '2014-06-26 11:44:21'),
(578, 13, 19, 70, 61, 1, '2014-06-28 11:23:44'),
(579, 13, 19, 70, 60, 1, '2014-06-28 11:23:47'),
(580, 13, 19, 70, 14, 1, '2014-06-28 11:23:52'),
(581, 13, 19, 70, 17, 1, '2014-06-28 11:23:54'),
(582, 13, 70, 19, 62, 1, '2014-06-28 11:46:09');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_favorites`
--

CREATE TABLE IF NOT EXISTS `qmex_favorites` (
  `ID` int(15) NOT NULL,
  `Login` int(15) NOT NULL,
  `Type` varchar(15) NOT NULL,
  `Essence` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `qmex_favorites`
--

INSERT INTO `qmex_favorites` (`ID`, `Login`, `Type`, `Essence`) VALUES
(0, 19, 'note', 139),
(0, 19, 'note', 138),
(0, 42, 'note', 139),
(0, 70, 'note', 127),
(0, 70, 'note', 102);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_swop`
--

CREATE TABLE IF NOT EXISTS `qmex_swop` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Login` int(11) NOT NULL,
  `SwopType` int(5) NOT NULL,
  `ServiceType` int(5) NOT NULL,
  `Specialization` int(5) NOT NULL,
  `SwopCore` varchar(50) NOT NULL DEFAULT '',
  `Definition` varchar(255) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=123 ;

--
-- Дамп данных таблицы `qmex_swop`
--

INSERT INTO `qmex_swop` (`ID`, `Login`, `SwopType`, `ServiceType`, `Specialization`, `SwopCore`, `Definition`, `Date`) VALUES
(80, 70, 1, 2, 19, '', 'Нужны компьютеры для тестирования нового протокола общения.', '2013-12-31 12:39:50'),
(82, 70, 1, 4, 15, 'Уникальный стартап', 'Хотелось бы произвести запуск новейшего эксклюзивного стартапа к июню 2014. Ищу программистов, дизайнеров и просто людей, желающих помочь :)', '2013-12-31 14:32:46'),
(83, 19, 2, 4, 15, '', 'Был бы рад предложить совместную работу над интернет проектами. Обожаю различные стартапы и способствую их "выращиванию" !', '2013-12-31 14:34:27'),
(84, 70, 1, 6, 15, '', 'Инвестиции на интернет-стартап', '2013-12-31 16:12:20'),
(85, 178, 2, 6, 15, '', 'Буду рад вложить средства и силы в развитие интернет стартапа.', '2013-12-31 16:15:56'),
(94, 70, 2, 4, 19, '', 'Модернизация компьютерных систем:\nhttp://bit.ly/19HIcfC', '2014-01-01 15:18:36'),
(97, 178, 1, 5, 34, 'генерация голоса', 'Создаем совместно систему генерации голоса на основе уже существующих звуков.', '2014-03-17 16:30:43'),
(113, 70, 2, 6, 34, 'C#', 'Создание управляемых клиентских ботов( программа, установленная на компьютер ) с устройств на платформе Android', '2014-06-15 10:07:25'),
(120, 178, 1, 3, 34, 'Java', 'Нужна команда для разработки уникальной кроссплатформенной игры на Java', '2014-06-18 15:19:04'),
(121, 19, 2, 2, 13, 'Достоевский', 'Рад пообщаться на литературные темы, философские и исторические.', '2014-06-18 15:30:04'),
(122, 90, 1, 3, 8, 'web-design', 'Буду рад поработать вместе над дизайном веб-сервиса', '2014-06-18 22:02:47');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_teams`
--

CREATE TABLE IF NOT EXISTS `qmex_teams` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `Description` text NOT NULL,
  `Year` int(5) NOT NULL,
  `Motto` varchar(200) NOT NULL,
  `Sphere` int(11) NOT NULL,
  `Sphere_keys` varchar(150) NOT NULL,
  `Logo` varchar(400) NOT NULL,
  `Web` varchar(400) NOT NULL,
  `Owner` int(11) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_users`
--

CREATE TABLE IF NOT EXISTS `qmex_users` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(40) NOT NULL,
  `Password` varchar(40) NOT NULL,
  `Email` varchar(40) NOT NULL,
  `Name` varchar(55) NOT NULL,
  `Sename` varchar(55) NOT NULL,
  `birth_year` int(8) unsigned NOT NULL DEFAULT '0',
  `birth_month` int(3) unsigned NOT NULL DEFAULT '1',
  `birth_day` int(5) unsigned NOT NULL DEFAULT '1',
  `Sex` int(2) unsigned NOT NULL DEFAULT '0',
  `Country` varchar(50) NOT NULL,
  `City` varchar(50) NOT NULL,
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `Profession` varchar(200) NOT NULL,
  `Site` text NOT NULL,
  `About` text NOT NULL,
  `DescrWords` varchar(255) NOT NULL DEFAULT '',
  `current_business` varchar(200) NOT NULL,
  `photo` text NOT NULL,
  `IP` varchar(40) NOT NULL,
  `key_restore` varchar(45) NOT NULL,
  `Target` int(11) NOT NULL DEFAULT '-1',
  `datetime_reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_enter` datetime NOT NULL,
  `caption` varchar(50) NOT NULL,
  `rating` float NOT NULL DEFAULT '0',
  `cash` float NOT NULL DEFAULT '0',
  `must_enter` int(5) NOT NULL DEFAULT '1',
  `last_ip_enter` varchar(100) NOT NULL,
  `account_checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=187 ;

--
-- Дамп данных таблицы `qmex_users`
--

INSERT INTO `qmex_users` (`ID`, `Login`, `Password`, `Email`, `Name`, `Sename`, `birth_year`, `birth_month`, `birth_day`, `Sex`, `Country`, `City`, `latitude`, `longitude`, `Profession`, `Site`, `About`, `DescrWords`, `current_business`, `photo`, `IP`, `key_restore`, `Target`, `datetime_reg`, `last_enter`, `caption`, `rating`, `cash`, `must_enter`, `last_ip_enter`, `account_checked`) VALUES
(18, 'asex', '12345', 'antochi.anton@yandex.ru', 'Saw', 'Revelation', 1994, 1, 17, 1, 'Россия', 'Москва', 0, 0, '', '', '', '', '123', 'http://www.startfilm.ru/images/base/film/21343_/big_4585.jpg', '127.0.0.1', '', -1, '2014-06-18 20:00:00', '2014-06-18 23:16:07', '', -1, -1, 0, '', 1),
(19, 'monarx', '123456', 'monarx@mono.com', 'Иван', 'Прокофьев', 1969, 5, 10, 1, 'Россия', 'Санкт-Петербург', 0, 0, '', '||Android "Scribbles" Gam;;http://ilibrary.ru/text/11/p.1/index.html||Преступление и наказание;;http://az.lib.ru/d/dostoewskij_f_m/text_0060.shtml', 'Ведущий блога «Промзона» на сайте «Компьютерра-online».\r\n00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000\r\n---\r\nhttp://www.qmex.ru \r\n---\r\nГлавное отличия гуманитария от технаря в том, что технарь может разбираться в как в технических, так и в гуманитарных предметах, а ярко выраженный гуманитарий может хорошо разбираться только в гуманитарных нвуках', 'бананы африка апельсины и обезьяны', 'Занимаюсь гумманитарными науками', '', '127.0.0.1', '', 191, '2014-06-16 20:00:00', '2014-06-28 10:26:45', 'Гумманитарий', 22.25, 20.25, 0, '', 0),
(42, 'NikoZavf', '12345', 'buro.02@mail.ru', 'Николай', 'Завочий', 1983, 12, 4, 1, 'Россия', 'Москва', 0, 0, '', '', '', 'авто мото автомобили музыка классика', 'Программи́ст', 'http://4.bp.blogspot.com/_3j0ETMitHcs/Rvdbmo-EUsI/AAAAAAAAAIQ/cTD6qZciS8A/s1600/coder.gif', '127.0.0.1', 'baby', -1, '2014-06-17 20:00:00', '2014-06-18 23:03:32', '', 4.5, 4.5, 0, '', 1),
(70, 'Bully17', '12345', 'callmename@qmex.com', 'Vlad', '', 1993, 11, 24, 1, 'Россия', 'Москва', 0, 0, 'Хакер', '||HeartBleed Bug;;http://heartbleed.com/||Gradle Android;;http://ru.wikipedia.org/wiki/Gradle||Linux дистрибутив;;Kali Linux||Kali Linux;;http://ru.wikipedia.org/wiki/Kali_Linux||Распознавание рукописного ввода;;http://habrahabr.ru/post/188390/||Сайт;;http://antizapret.prostovpn.org/', 'Интересно все, что так или иначе связано с передачей данных по сети, обработкой/кодированием видео и аудио. Программирую не очень часто (PHP/Bash/Python), т.к. писать идеальный код не получается, а плохой никому не нужен, да и стыдно как-то. Иногда взламываю защиту программ или исследую их на уязвимости. Нравится ковырять любые embedded-системы, особенно защищенные. Использую Linux.', 'гений умелец миротворец программист хакер 5', 'Android-разработчик.Веб-разработчик', 'http://www.toried.com/news_images/240.jpg', '127.0.0.1', '98295549829554', 212, '2014-06-17 20:00:00', '2014-06-28 10:09:30', 'Android-разработчик.- Веб-разработчик', 28.5, 1.5, 0, '', 1),
(90, 'Max-Stop', 'trololo', 'asen@asen.ru', 'Max', 'Stoppen', 1989, 2, 27, 1, 'ОАЭ', 'Дубай', 0, 0, '', '||Design Docs;;http://www.artlebedev.ru/', 'Like to create different kinds of design!', '', 'Занимаюсь разработкой дизайна', 'http://www.clker.com/cliparts/0/f/9/a/11971025451307781503johnny_automatic_normal_face.svg.hi.png', '127.0.0.1', 'trololo', -1, '2014-06-16 20:00:00', '2014-06-18 23:01:40', 'Designer', 15, 13, 0, '', 1),
(92, '100PerBoy', '111111', 'lol-map@lol.com', 'Петр', 'Никифоров', 1994, 1, 14, 1, 'Россия', 'Москва', 0, 0, '', '', '', '', '', '', '127.0.0.1', '111111', -1, '2014-06-18 20:00:00', '2014-06-18 22:59:35', '', 5, 5, 0, '', 1),
(168, 'test', '12345', 'lol@lol.ru', '', '', 0, 0, 0, 0, '', '', 0, 0, '', '', '', '', '', '', '127.0.0.1', '12345', -1, '2014-06-18 20:00:00', '2014-06-21 13:24:24', '', 3, 3, 0, '', 1),
(169, 'serious', 'serious555', 'seriousman@ok.bang', 'Mike', 'Seious', 1982, 3, 20, 1, 'Bangladesh', 'Gangstar', 0, 0, 'Бизнесмен', '', '', 'бизнес', '', '', '127.0.0.1', 'serious', -1, '2014-06-18 20:00:00', '2014-06-18 23:08:32', '', 4, 4, 0, '', 1),
(178, 'Franchesco', '1234566', 'babe@ya.ru', 'Франческо', 'Вестуччи', 1981, 6, 30, 1, 'Россия', 'Москва', 0, 0, '', '||Что стоит посмотреть;;http://www.youtube.com/watch?v=-ioKU6Jue_k||Что стоит почитать;;http://www.e-reading.ws/book.php?book=1013652||Текущий стартап;;http://qmex.ru/note?id=213', '10 лет работал на Intel Co, разработал пару-другую популярных игрушек в лихие 91-93 (Морской Бой, Поле Чудес и пр).\r\nНыне разрабатываю мобильные приложения и придумываю мобильные технологии. Ищу интересные идеи для своих стартапов.', 'стартап android google программист бизнес', 'Развитие бизнеса в интернете', 'http://poachedmag.com/wp-content/uploads/2013/05/Startups-weekend-722x500.png', '127.0.0.1', '5426898145513', 185, '2014-06-17 20:00:00', '2014-06-24 14:41:54', 'Программист - Стартапер', 33.5, 33.5, 0, '', 1),
(179, 'sava', '9092895', 'sava4378@mail.ru', 'Владимир', 'Саватеев', 1984, 2, 24, 1, 'Россия', 'Москва', 0, 0, '', '', 'Строительство ', '', 'Думаю что это достойный проект !!!', 'http://moikrug.ru/avatar/mk-p1024/606063469/original.png?modified=1320687957', '127.0.0.1', '4378', -1, '2014-06-18 20:00:00', '2013-12-18 02:06:54', '?', 12, 12, 1, '127.0.0.1', 1),
(181, 'microscheme', '12345', 'architect@yandex.ru', 'Micro', 'Scheme', 1996, 2, 29, 1, 'Силиконовая', 'Долина', 0, 0, '', '', '', '', '', '', '127.0.0.1', 'micromir', -1, '2014-06-16 20:00:00', '2013-12-17 21:23:52', '', 0, 0, 1, '127.0.0.1', 1),
(185, 'english', '9829555', 'dubai@uae.ae', 'Jordan', 'Rosserford', 1962, 7, 9, 1, 'Англия', 'Лондон', 51.508515, -0.1254872, '', '', 'Лингвист, он же языковед, исследует и описывает все существовавшие и существующие на планете языки, изучает проблемы места языка среди других явлений, его связи с жизнедеятельностью и мышлением человека, а также сам процесс говорения и его результата, фиксируемого памятью и письмом.Лингвисты сейчас востребованы, как никогда -- с развитием технологий и появлением новых наук растет и спрос на специалистов-языковедов. Ведь без лингвиста невозможен сам процесс обучения как родному, так и иностранному языку, и любой авторитетный учебник создается при участии языковеда. ', 'бизнес сделки финансы акции брокер аферист богач', '', 'http://eng1408.ucoz.ru/_ph/1/317637981.gif', '127.0.0.1', 'england', -1, '2014-06-17 20:00:00', '2014-06-21 13:25:06', 'Изучение языков', 11, 11, 0, '', 1),
(186, 'mapp', 'doggs', 'buddy@yandex.com', 'Джо', 'Роззелло', 1986, 6, 5, 1, 'Россия', 'Москва', 55.8190297, 37.7209047, '', '', '', 'мафия бизнес игры азарт налеты', '', '', '127.0.0.1', 'mappdoggs', -1, '2014-06-18 20:00:00', '2014-05-28 17:26:46', '', 0, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_user_bookmarks`
--

CREATE TABLE IF NOT EXISTS `qmex_user_bookmarks` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ID1` int(10) NOT NULL,
  `ID2` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=202 ;

--
-- Дамп данных таблицы `qmex_user_bookmarks`
--

INSERT INTO `qmex_user_bookmarks` (`ID`, `ID1`, `ID2`) VALUES
(146, 42, 70),
(147, 42, 18),
(149, 41, 70),
(150, 173, 70),
(167, 18, 70),
(168, 178, 70),
(169, 179, 70),
(172, 90, 70),
(174, 70, 92),
(175, 70, 179),
(176, 70, 185),
(177, 191, 179),
(186, 70, 19),
(187, 178, 19),
(188, 178, 90),
(189, 70, 178),
(190, 70, 90),
(191, 19, 178),
(192, 19, 70),
(199, 19, 179),
(200, 168, 185),
(201, 185, 168);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_user_interests`
--

CREATE TABLE IF NOT EXISTS `qmex_user_interests` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Login` int(11) NOT NULL,
  `key_interest` int(11) NOT NULL,
  `interest_keywords` varchar(60) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1131 ;

--
-- Дамп данных таблицы `qmex_user_interests`
--

INSERT INTO `qmex_user_interests` (`ID`, `Login`, `key_interest`, `interest_keywords`) VALUES
(921, 19, 18, 'culture thriller ужасы хоррор'),
(923, 19, 21, '16-век древний-мир'),
(926, 19, 29, 'литертура история'),
(927, 19, 30, 'психология-общества люди массы'),
(936, 168, 43, '123 45'),
(939, 42, 2, 'musle-cars sportcars all'),
(942, 42, 19, 'artificial-intellegence AI'),
(944, 42, 25, 'classics hip-hop rock hard'),
(945, 42, 24, ''),
(947, 42, 32, 'russian deals business'),
(948, 169, 37, 'работа мегполис'),
(949, 169, 45, 'экономика стран'),
(950, 169, 38, 'advertising ads modern modernizer'),
(951, 169, 32, 'potitics'),
(952, 169, 36, ''),
(980, 70, 3, 'алгоритмы'),
(982, 70, 15, 'inter net web www инфо-безопасность'),
(983, 70, 19, 'linux amd intel x64'),
(990, 179, 3, ''),
(991, 179, 40, ''),
(992, 179, 31, ''),
(996, 19, 15, 'веб-дизайн html5 css3 webGL'),
(1000, 179, 2, ''),
(1001, 179, 37, ''),
(1002, 179, 8, ''),
(1003, 179, 15, ''),
(1004, 179, 34, ''),
(1005, 179, 36, ''),
(1006, 179, 45, ''),
(1007, 179, 43, ''),
(1008, 70, 34, 'python ruby php javascript'),
(1017, 181, 6, ''),
(1018, 181, 3, ''),
(1019, 181, 19, ''),
(1020, 181, 34, ''),
(1021, 181, 40, ''),
(1022, 181, 5, ''),
(1023, 181, 4, ''),
(1030, 178, 31, 'инвесторы бизнес-ангелы'),
(1031, 178, 15, 'стартапы инвестиции веб-сервисы социальные'),
(1061, 185, 37, 'акции фондовая-биржа'),
(1062, 185, 54, 'торговля биржа'),
(1063, 185, 44, 'биржа брокер'),
(1064, 185, 31, 'инвестор трейдер'),
(1066, 178, 37, 'интернет-бизнес стартапы'),
(1067, 186, 53, 'приготовление лучшей пиццы'),
(1068, 186, 9, 'пицца солями укроп специи'),
(1069, 186, 23, 'fashion show milan italia'),
(1070, 186, 17, 'italia around-the-world trim europe'),
(1105, 70, 40, 'arduino'),
(1106, 70, 24, ''),
(1107, 178, 34, 'Android Java'),
(1108, 178, 6, 'Apple HTC Android WinPhone'),
(1109, 178, 14, 'Watch_Dogs GTAV'),
(1110, 19, 16, ''),
(1111, 19, 41, 'сократ платон аристотель'),
(1112, 19, 13, 'Л.Н.Толстой Достоевский Горький'),
(1113, 185, 38, 'реклама увеличение-продаж'),
(1114, 185, 51, 'английский русский немецкий итальянский др.'),
(1115, 185, 29, 'языки'),
(1116, 90, 6, 'Apple IOS'),
(1117, 90, 8, 'design web-design'),
(1118, 90, 18, 'драма приключения вестерн'),
(1119, 90, 23, 'fashion paris'),
(1120, 90, 30, ''),
(1121, 92, 49, 'мир континенты страны-мира'),
(1122, 92, 11, 'живая-клетка ДНК РНК'),
(1123, 92, 12, 'галактика квазар'),
(1124, 92, 22, 'анатомия-человека'),
(1125, 92, 33, ''),
(1126, 92, 26, ''),
(1127, 18, 6, 'Samsung HTC Android'),
(1128, 18, 19, 'PC Mac'),
(1129, 18, 38, ''),
(1130, 18, 34, 'C++ C# Java Perl');

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_user_notes`
--

CREATE TABLE IF NOT EXISTS `qmex_user_notes` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `Login` int(11) NOT NULL,
  `Note` text NOT NULL,
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Meaning` varchar(50) NOT NULL DEFAULT '',
  `Substance` varchar(150) NOT NULL DEFAULT '',
  `Theme` int(5) NOT NULL DEFAULT '0',
  `Type` int(5) unsigned NOT NULL DEFAULT '0',
  `Connection` int(11) NOT NULL DEFAULT '-1',
  `Is_Private` tinyint(5) NOT NULL DEFAULT '0',
  `updated_at` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=216 ;

--
-- Дамп данных таблицы `qmex_user_notes`
--

INSERT INTO `qmex_user_notes` (`ID`, `Login`, `Note`, `DateTime`, `Meaning`, `Substance`, `Theme`, `Type`, `Connection`, `Is_Private`, `updated_at`) VALUES
(127, 18, '<!DOCTYPE HTML>\n<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body><p></p>\n<p> </p>\n<table border="0" cellspacing="0" cellpadding="0" bgcolor="#EBEBEB"><tbody><tr><td align="center" valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><tbody><tr><td align="right" valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="10"><tbody><tr><td> </td>\n<td align="right" valign="top" width="710"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=knP7xRMm8A-2BZTLDWf5hiC6CQAl6IBy4h52G1NNE6jrR6Q4NLJ791d8j092SEob2ZiqAZ7dbHinLvhSKjkQIFXxXxZqN3nAeCqfSLnp8XPL-2FSvmR2wCbbLDjPAwuYcEfDSgvdh-2F3Q-2FjFMx48q4tuZdD-2FDifNwI5GXIs0WM-2BufCUvLRX-2B6F75ZvzqKUl9lLNF5dNO45-2FiJXfkTB682hKeDqqFdbZx-2BN0l6U9dFdjGB-2Fra4SuhHAdb3D4hhZTv0IrpFzQSPYRZg2sV5rGSPTqwM2xSmIrWFO-2FjxMQoYH-2BQo54U-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2LwNC1jpI-2Flp33aUy6vUBCSOS9DyT3knGy-2BzTCe-2BT3kk7-2B6fav-2F525mfgVUMv9pUa8MRNzJrTxe5OKtWRzRvPiwqUMWf6ozM9hM-2Fc0z4sD5peJyYGm0CgCzeV6OpcYjjWyH-2B1b2-2FOZfVOVayRL24eXj5We1Q1PL3zrixnbYKcVqGEGn-2FAal8Frq0YJ5ZvWS881giqQZr32b-2BFAq1Jqtwfn252mUeEPzQOTlgFgrFcGo8hnVNXpCXX-2BtyE1hiLaeL4HlwlomBYqLEqb8JPtquLJxLbfGx1ObuG3OZh6BZ3e-2Fw-2FwQ-2BZ2EtsK3fPP1-2FHtB7dA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,9doOzcGQaR4trI37_4mZ2g&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1rblA3eFJNbThBLTJCWlRMRFdmNWhpQzZDUUFsNklCeTRoNTJHMU5ORTZqclI2UTROTEo3OTFkOGowOTJTRW9iMlppcUFaN2RiSGluTHZoU0tqa1FJRlh4WHhacU4zbkFlQ3FmU0xucDhYUEwtMkZTdm1SMndDYmJMRGpQQXd1WWNFZkRTZ3ZkaC0yRjNRLTJGakZNeDQ4cTR0dVpkRC0yRkRpZk53STVHWElzMFdNLTJCdWZDVXZMUlgtMkI2Rjc1WnZ6cUtVbDlsTE5GNWROTzQ1LTJGaUpYZmtUQjY4MmhLZURxcUZkYlp4LTJCTjBsNlU5ZEZkakdCLTJGcmE0U3VoSEFkYjNENGhoWlR2MElycEZ6UVNQWVJaZzJzVjVyR1NQVHF3TTJ4U21JcldGTy0yRmp4TVFvWUgtMkJRbzU0VS0zRF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UyTHdOQzFqcEktMkZscDMzYVV5NnZVQkNTT1M5RHlUM2tuR3ktMkJ6VENlLTJCVDNrazctMkI2ZmF2LTJGNTI1bWZnVlVNdjlwVWE4TVJOekpyVHhlNU9LdFdSelJ2UGl3cVVNV2Y2b3pNOWhNLTJGYzB6NHNENXBlSnlZR20wQ2dDemVWNk9wY1lqald5SC0yQjFiMi0yRk9aZlZPVmF5UkwyNGVYajVXZTFRMVBMM3pyaXhuYllLY1ZxR0VHbi0yRkFhbDhGcnEwWUo1WnZXUzg4MWdpcVFacjMyYi0yQkZBcTFKcXR3Zm4yNTJtVWVFUHpRT1RsZ0ZnckZjR284aG5WTlhwQ1hYLTJCdHlFMWhpTGFlTDRIbHdsb21CWXFMRXFiOEpQdHF1TEp4TGJmR3gxT2J1RzNPWmg2QlozZS0yRnctMkZ3US0yQloyRXRzSzNmUFAxLTJGSHRCN2RBLTNELTNE" data-orig-href="http://email.avito.ru/wf/click?upn=knP7xRMm8A-2BZTLDWf5hiC6CQAl6IBy4h52G1NNE6jrR6Q4NLJ791d8j092SEob2ZiqAZ7dbHinLvhSKjkQIFXxXxZqN3nAeCqfSLnp8XPL-2FSvmR2wCbbLDjPAwuYcEfDSgvdh-2F3Q-2FjFMx48q4tuZdD-2FDifNwI5GXIs0WM-2BufCUvLRX-2B6F75ZvzqKUl9lLNF5dNO45-2FiJXfkTB682hKeDqqFdbZx-2BN0l6U9dFdjGB-2Fra4SuhHAdb3D4hhZTv0IrpFzQSPYRZg2sV5rGSPTqwM2xSmIrWFO-2FjxMQoYH-2BQo54U-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2LwNC1jpI-2Flp33aUy6vUBCSOS9DyT3knGy-2BzTCe-2BT3kk7-2B6fav-2F525mfgVUMv9pUa8MRNzJrTxe5OKtWRzRvPiwqUMWf6ozM9hM-2Fc0z4sD5peJyYGm0CgCzeV6OpcYjjWyH-2B1b2-2FOZfVOVayRL24eXj5We1Q1PL3zrixnbYKcVqGEGn-2FAal8Frq0YJ5ZvWS881giqQZr32b-2BFAq1Jqtwfn252mUeEPzQOTlgFgrFcGo8hnVNXpCXX-2BtyE1hiLaeL4HlwlomBYqLEqb8JPtquLJxLbfGx1ObuG3OZh6BZ3e-2Fw-2FwQ-2BZ2EtsK3fPP1-2FHtB7dA-3D-3D">Открыть письмо в браузере</a>2</td>\n<td> </td>\n</tr></tbody></table></td>\n</tr></tbody></table><table border="0" width="710" cellspacing="0" cellpadding="0"><tbody><tr><td align="center" valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td colspan="4" height="20"> </td>\n</tr><tr><td align="left" width="149"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FM0CzMPhE7grF7UyIJNxZEFG8sr2l0C7822QtmbtHwX3AFFci9nq9d0vtSQcdj3tW4KQIVj6yFtHh4nIUekLZQMi9ldLmIxNylUZH48rsleV6yfI3n75TQ3Gj9HaVi-2BuU8QL2FPD7lVSKUFj4t3VpvE-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2ESEAzsFY-2F-2BfsxTYToKCdBJBzRVTxFegw9TbcoAuGr80kwbeL9Ou2lQOWoOH-2B12nByoz0buCcyNgHJ9pT7-2Fq59v6hWoD3fYIJLZowkSt1cCD4MOsnLoO9z3W5G0fqObV8zZLYQ7SPE9ZJYNl1Qi-2BCgLZBHDl-2Bibpx8BdBIcY1My4rwDEZow6Qb9wu-2F-2BvtwK5G3mkYK3RBIP8zv5YG9NS0k2RSEpfafNIVVANWZazzb1Rwuupv5w7irMKKtvxzRaJWM8W5k8T9KkXQoCmBilqVRw7hFLaszUtYBIxlUvTOjzBO2W9vFx2DzAMJi6n3oCgfA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,HXTTsN2jsjJReUTw1cr4IA&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRk0wQ3pNUGhFN2dyRjdVeUlKTnhaRUZHOHNyMmwwQzc4MjJRdG1idEh3WDNBRkZjaTlucTlkMHZ0U1FjZGozdFc0S1FJVmo2eUZ0SGg0bklVZWtMWlFNaTlsZExtSXhOeWxVWkg0OHJzbGVWNnlmSTNuNzVUUTNHajlIYVZpLTJCdVU4UUwyRlBEN2xWU0tVRmo0dDNWcHZFLTNEX2xRWHNLeC0yQklZdkdtOVp0RWZOM21OZ1ZzS2QxOXgzZmNUcDdlY0xzM0pjMUMtMkItMkZhald0SEh2cXNNaFpKdTNtbkk1azhFNUVhd3N0Q3RyamRDbnZLRTJFU0VBenNGWS0yRi0yQmZzeFRZVG9LQ2RCSkJ6UlZUeEZlZ3c5VGJjb0F1R3I4MGt3YmVMOU91MmxRT1dvT0gtMkIxMm5CeW96MGJ1Q2N5TmdISjlwVDctMkZxNTl2NmhXb0QzZllJSkxab3drU3QxY0NENE1Pc25Mb085ejNXNUcwZnFPYlY4elpMWVE3U1BFOVpKWU5sMVFpLTJCQ2dMWkJIRGwtMkJpYnB4OEJkQkljWTFNeTRyd0RFWm93NlFiOXd1LTJGLTJCdnR3SzVHM21rWUszUkJJUDh6djVZRzlOUzBrMlJTRXBmYWZOSVZWQU5XWmF6emIxUnd1dXB2NXc3aXJNS0t0dnh6UmFKV004VzVrOFQ5S2tYUW9DbUJpbHFWUnc3aEZMYXN6VXRZQkl4bFV2VE9qekJPMlc5dkZ4MkR6QU1KaTZuM29DZ2ZBLTNELTNE" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FM0CzMPhE7grF7UyIJNxZEFG8sr2l0C7822QtmbtHwX3AFFci9nq9d0vtSQcdj3tW4KQIVj6yFtHh4nIUekLZQMi9ldLmIxNylUZH48rsleV6yfI3n75TQ3Gj9HaVi-2BuU8QL2FPD7lVSKUFj4t3VpvE-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2ESEAzsFY-2F-2BfsxTYToKCdBJBzRVTxFegw9TbcoAuGr80kwbeL9Ou2lQOWoOH-2B12nByoz0buCcyNgHJ9pT7-2Fq59v6hWoD3fYIJLZowkSt1cCD4MOsnLoO9z3W5G0fqObV8zZLYQ7SPE9ZJYNl1Qi-2BCgLZBHDl-2Bibpx8BdBIcY1My4rwDEZow6Qb9wu-2F-2BvtwK5G3mkYK3RBIP8zv5YG9NS0k2RSEpfafNIVVANWZazzb1Rwuupv5w7irMKKtvxzRaJWM8W5k8T9KkXQoCmBilqVRw7hFLaszUtYBIxlUvTOjzBO2W9vFx2DzAMJi6n3oCgfA-3D-3D"><img src="https://cache.mail.yandex.net/mail/93511d79929e815045755f3707b058bc/static.sendgrid.com/uploads/UID_662821_NL_1735795_2613e0a3eff6a3a97494fa257bc1a34c/6932ac6052cb2e4fcd2b803f9ac46189" alt="AVITO.ru"></a></td>\n<td align="right"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlQrPKp7-2BXdmKaM1-2B1siQXJDCxHCxlnxMixevAA4gsXsZ4eRXzLvS6t4-2FxJmkh128BA-2FuNKSlQY4YecxVRhJDIGTOKNeLXyJ1U82pt6po9jOk716eEPf6eff-2FCGdq5uqIY4-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2F7A9dUjMK-2FLdpat9b4SsOZ3o-2Ft1JAStGbpQTmo43FwcgMD-2B1uLlmhqWDihuHQ3sUlIHsc-2FoCjbooxwvnHmQiHbozimfFdqSIwPv5njy6IztBjFla8JgF5BaT-2BtKLZXtI4tNSC-2FMT3LCrKep8wiW4CO5-2FEgNZVc0Ujpm7OQXNTA4HEJVNZ4KAqfwFbEVupaxdkl-2BX-2BFJ6UsgmiMHPXvAY-2Fwd8bIAZs88gEn9JJ8WF-2BxvXmvYKphsaq13sdFZbU7no71Dt-2FZC34aubvGW4eeGTOgcbuDm3RqR-2FVJY0bvfm-2FemCSImBqRc-2BkfyKCgN-2B4A-2FVg-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,1W-xtDbygKNgAJB9Neusqw&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsUXJQS3A3LTJCWGRtS2FNMS0yQjFzaVFYSkRDeEhDeGxueE1peGV2QUE0Z3NYc1o0ZVJYekx2UzZ0NC0yRnhKbWtoMTI4QkEtMkZ1TktTbFFZNFllY3hWUmhKRElHVE9LTmVMWHlKMVU4MnB0NnBvOWpPazcxNmVFUGY2ZWZmLTJGQ0dkcTV1cUlZNC0zRF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UyRjdBOWRVak1LLTJGTGRwYXQ5YjRTc09aM28tMkZ0MUpBU3RHYnBRVG1vNDNGd2NnTUQtMkIxdUxsbWhxV0RpaHVIUTNzVWxJSHNjLTJGb0NqYm9veHd2bkhtUWlIYm96aW1mRmRxU0l3UHY1bmp5Nkl6dEJqRmxhOEpnRjVCYVQtMkJ0S0xaWHRJNHROU0MtMkZNVDNMQ3JLZXA4d2lXNENPNS0yRkVnTlpWYzBVanBtN09RWE5UQTRIRUpWTlo0S0FxZndGYkVWdXBheGRrbC0yQlgtMkJGSjZVc2dtaU1IUFh2QVktMkZ3ZDhiSUFaczg4Z0VuOUpKOFdGLTJCeHZYbXZZS3Boc2FxMTNzZEZaYlU3bm83MUR0LTJGWkMzNGF1YnZHVzRlZUdUT2djYnVEbTNScVItMkZWSlkwYnZmbS0yRmVtQ1NJbUJxUmMtMkJrZnlLQ2dOLTJCNEEtMkZWZy0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlQrPKp7-2BXdmKaM1-2B1siQXJDCxHCxlnxMixevAA4gsXsZ4eRXzLvS6t4-2FxJmkh128BA-2FuNKSlQY4YecxVRhJDIGTOKNeLXyJ1U82pt6po9jOk716eEPf6eff-2FCGdq5uqIY4-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2F7A9dUjMK-2FLdpat9b4SsOZ3o-2Ft1JAStGbpQTmo43FwcgMD-2B1uLlmhqWDihuHQ3sUlIHsc-2FoCjbooxwvnHmQiHbozimfFdqSIwPv5njy6IztBjFla8JgF5BaT-2BtKLZXtI4tNSC-2FMT3LCrKep8wiW4CO5-2FEgNZVc0Ujpm7OQXNTA4HEJVNZ4KAqfwFbEVupaxdkl-2BX-2BFJ6UsgmiMHPXvAY-2Fwd8bIAZs88gEn9JJ8WF-2BxvXmvYKphsaq13sdFZbU7no71Dt-2FZC34aubvGW4eeGTOgcbuDm3RqR-2FVJY0bvfm-2FemCSImBqRc-2BkfyKCgN-2B4A-2FVg-3D-3D">Объявления</a><span class="Apple-converted-space"> </span><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FF8gVwB4HmOGyzOdq2crZX7rGkOzTkORqr1dtSG1nWGc3sAqgnBzCCtJIFCUU4LPIt1mX68Z5t00MLQzG9HwHpkGb6BouJYVKqiq1Le5GYRnAH6G8zW3Z53Drm6RdsveZV12Xa6rI-2FGqIhOMcBVQbGM-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Hy6TUJGsNjV6i87-2BSk6z2aI6AwWxYI-2Buiqh8YPCwS1bnqwIVrpS7Z4vBflqCR5z1HrMQjX18vOC7gObasE2dTOrNQ4TQrhCIgHOnoYhoJVl4SVS0Mi8EOaNd5KzM1KjTmTArH2kjlTffj389-2BxGjrLsNAv3kBiSJ7fDGuotR3TLyyiHqRkF6bIaxeCjw8NcIlA-2F3w2ckRcKyLReuuCuO97wH7IcGrrMKhO29LYRDHymSyB7BwwzjqXFkBbR6QHQO55Wfkg7s6yF3-2Fj5W-2FMHep-2FmFfYISkQxAj9Fj-2FwogQ0mKludBml1l5VPoqs7WcjurA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,qNHTs1Y-6eOb6GWkOxLv6Q&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkY4Z1Z3QjRIbU9HeXpPZHEyY3JaWDdyR2tPelRrT1JxcjFkdFNHMW5XR2Mzc0FxZ25CekNDdEpJRkNVVTRMUEl0MW1YNjhaNXQwME1MUXpHOUh3SHBrR2I2Qm91SllWS3FpcTFMZTVHWVJuQUg2Rzh6VzNaNTNEcm02UmRzdmVaVjEyWGE2ckktMkZHcUloT01jQlZRYkdNLTNEX2xRWHNLeC0yQklZdkdtOVp0RWZOM21OZ1ZzS2QxOXgzZmNUcDdlY0xzM0pjMUMtMkItMkZhald0SEh2cXNNaFpKdTNtbkk1azhFNUVhd3N0Q3RyamRDbnZLRTJIeTZUVUpHc05qVjZpODctMkJTazZ6MmFJNkF3V3hZSS0yQnVpcWg4WVBDd1MxYm5xd0lWcnBTN1o0dkJmbHFDUjV6MUhyTVFqWDE4dk9DN2dPYmFzRTJkVE9yTlE0VFFyaENJZ0hPbm9ZaG9KVmw0U1ZTME1pOEVPYU5kNUt6TTFLalRtVEFySDJramxUZmZqMzg5LTJCeEdqckxzTkF2M2tCaVNKN2ZER3VvdFIzVEx5eWlIcVJrRjZiSWF4ZUNqdzhOY0lsQS0yRjN3MmNrUmNLeUxSZXV1Q3VPOTd3SDdJY0dyck1LaE8yOUxZUkRIeW1TeUI3Qnd3empxWEZrQmJSNlFIUU81NVdma2c3czZ5RjMtMkZqNVctMkZNSGVwLTJGbUZmWUlTa1F4QWo5RmotMkZ3b2dRMG1LbHVkQm1sMWw1VlBvcXM3V2NqdXJBLTNELTNE" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FF8gVwB4HmOGyzOdq2crZX7rGkOzTkORqr1dtSG1nWGc3sAqgnBzCCtJIFCUU4LPIt1mX68Z5t00MLQzG9HwHpkGb6BouJYVKqiq1Le5GYRnAH6G8zW3Z53Drm6RdsveZV12Xa6rI-2FGqIhOMcBVQbGM-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Hy6TUJGsNjV6i87-2BSk6z2aI6AwWxYI-2Buiqh8YPCwS1bnqwIVrpS7Z4vBflqCR5z1HrMQjX18vOC7gObasE2dTOrNQ4TQrhCIgHOnoYhoJVl4SVS0Mi8EOaNd5KzM1KjTmTArH2kjlTffj389-2BxGjrLsNAv3kBiSJ7fDGuotR3TLyyiHqRkF6bIaxeCjw8NcIlA-2F3w2ckRcKyLReuuCuO97wH7IcGrrMKhO29LYRDHymSyB7BwwzjqXFkBbR6QHQO55Wfkg7s6yF3-2Fj5W-2FMHep-2FmFfYISkQxAj9Fj-2FwogQ0mKludBml1l5VPoqs7WcjurA-3D-3D">Магазины</a><span class="Apple-converted-space"> </span><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FJyTZ0wS2iW94d0wEa6gXSZCIUeub-2FpeAG2dQAwVIYBc8mPcpHoRyeEilve10IW28clylmfdlE-2FoNlCovbV5VUBGs5xcLk1zhM7qoh6p75hcb0WdTbMfOEu74fZb3gqA8qXk7-2BQP4EJh1TV7wmtp5Bo-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2HFKfsNYjnCaoSpPul-2BlZam92b9TtoSYDfgDceFrDKVdKdpKnskCen2XWTrLSLvXNBTFkBnZ8eebVARAILLb9mTXu-2FX0Ot4TXuXzQRPey8V8acgmpcZe8ZbW8xFfLumhw0SWPbNmsJNee7InqiaZFrlwpFWD04uvSBgG4fEipHoW07-2B3phKDVFIP8pPYLn6tprJGn1rLwS8eI9viGrzxDEX9NIGrW2TxdKP-2F7ss46ZDNhHtYpVgbN6sVtxhtG3kYgO7jy3K-2BQrMcJQnvFn1g41JLLe0WFNsyyeUdDkSy3JyQqkl-2F-2BbKdXCvzyNc75kyXOg-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,6AAzb4buDu-iYTApH4hmrQ&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkp5VFowd1MyaVc5NGQwd0VhNmdYU1pDSVVldWItMkZwZUFHMmRRQXdWSVlCYzhtUGNwSG9SeWVFaWx2ZTEwSVcyOGNseWxtZmRsRS0yRm9ObENvdmJWNVZVQkdzNXhjTGsxemhNN3FvaDZwNzVoY2IwV2RUYk1mT0V1NzRmWmIzZ3FBOHFYazctMkJRUDRFSmgxVFY3d210cDVCby0zRF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UySEZLZnNOWWpuQ2FvU3BQdWwtMkJsWmFtOTJiOVR0b1NZRGZnRGNlRnJES1ZkS2RwS25za0NlbjJYV1RyTFNMdlhOQlRGa0JuWjhlZWJWQVJBSUxMYjltVFh1LTJGWDBPdDRUWHVYelFSUGV5OFY4YWNnbXBjWmU4WmJXOHhGZkx1bWh3MFNXUGJObXNKTmVlN0lucWlhWkZybHdwRldEMDR1dlNCZ0c0ZkVpcEhvVzA3LTJCM3BoS0RWRklQOHBQWUxuNnRwckpHbjFyTHdTOGVJOXZpR3J6eERFWDlOSUdyVzJUeGRLUC0yRjdzczQ2WkROaEh0WXBWZ2JONnNWdHhodEcza1lnTzdqeTNLLTJCUXJNY0pRbnZGbjFnNDFKTExlMFdGTnN5eWVVZERrU3kzSnlRcWtsLTJGLTJCYktkWEN2enlOYzc1a3lYT2ctM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FJyTZ0wS2iW94d0wEa6gXSZCIUeub-2FpeAG2dQAwVIYBc8mPcpHoRyeEilve10IW28clylmfdlE-2FoNlCovbV5VUBGs5xcLk1zhM7qoh6p75hcb0WdTbMfOEu74fZb3gqA8qXk7-2BQP4EJh1TV7wmtp5Bo-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2HFKfsNYjnCaoSpPul-2BlZam92b9TtoSYDfgDceFrDKVdKdpKnskCen2XWTrLSLvXNBTFkBnZ8eebVARAILLb9mTXu-2FX0Ot4TXuXzQRPey8V8acgmpcZe8ZbW8xFfLumhw0SWPbNmsJNee7InqiaZFrlwpFWD04uvSBgG4fEipHoW07-2B3phKDVFIP8pPYLn6tprJGn1rLwS8eI9viGrzxDEX9NIGrW2TxdKP-2F7ss46ZDNhHtYpVgbN6sVtxhtG3kYgO7jy3K-2BQrMcJQnvFn1g41JLLe0WFNsyyeUdDkSy3JyQqkl-2F-2BbKdXCvzyNc75kyXOg-3D-3D">Личный кабинет</a></td>\n<td> </td>\n<td align="right" width="156"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FBjCLB37HUeTP-2BCsMqgfBamcdoXcmwh1y6BFGE1nPh6BNRJ-2FkoMQsPrFLJ3l57LBzysjyvdOy9iuCxSBI9N4BE5fbPfj3EEAT8KOo1bQWTV-2F0Yue0qHNqc7oFBSOLSm851ur-2FBYlReKU0cSSJoL4c7M-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NH6Lts1ep19fMpwpwTsRtXN8KRlTFk-2BgTrOZ8Tbfd91c-2FQ-2Blviv7kKakVGq7cFDqpKbPPclonTRPeaMn0BYVPLenpk8ZRn7tJWMtETSHiFSJYd735Y0pQ-2Bfv7c8YAgM016UY5xj-2Bw3FjqRSCa8UAbTLRRgL-2BpoTv2I6rfFzF7mAikRAsokHWQfEX55rqzTgLvU-2BGusWE80K54JZ6dZ89AR13Jx-2FPh71UeGlOFqWnm503UVNSSf3RrmF4U6HcR-2BlORPcaf-2FT4P65RNvz55bNOh3xhItQVwRzsd7gbV6hCTslkneNr82zymB18P2RESjfOw-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,2e1fPyghIE57T4-r_WFVdQ&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkJqQ0xCMzdIVWVUUC0yQkNzTXFnZkJhbWNkb1hjbXdoMXk2QkZHRTFuUGg2Qk5SSi0yRmtvTVFzUHJGTEozbDU3TEJ6eXNqeXZkT3k5aXVDeFNCSTlONEJFNWZiUGZqM0VFQVQ4S09vMWJRV1RWLTJGMFl1ZTBxSE5xYzdvRkJTT0xTbTg1MXVyLTJGQllsUmVLVTBjU1NKb0w0YzdNLTNEX2xRWHNLeC0yQklZdkdtOVp0RWZOM21OZ1ZzS2QxOXgzZmNUcDdlY0xzM0pjMUMtMkItMkZhald0SEh2cXNNaFpKdTNtbkk1azhFNUVhd3N0Q3RyamRDbnZLRTJOSDZMdHMxZXAxOWZNcHdwd1RzUnRYTjhLUmxURmstMkJnVHJPWjhUYmZkOTFjLTJGUS0yQmx2aXY3a0tha1ZHcTdjRkRxcEtiUFBjbG9uVFJQZWFNbjBCWVZQTGVucGs4WlJuN3RKV010RVRTSGlGU0pZZDczNVkwcFEtMkJmdjdjOFlBZ00wMTZVWTV4ai0yQnczRmpxUlNDYThVQWJUTFJSZ0wtMkJwb1R2Mkk2cmZGekY3bUFpa1JBc29rSFdRZkVYNTVycXpUZ0x2VS0yQkd1c1dFODBLNTRKWjZkWjg5QVIxM0p4LTJGUGg3MVVlR2xPRnFXbm01MDNVVk5TU2YzUnJtRjRVNkhjUi0yQmxPUlBjYWYtMkZUNFA2NVJOdno1NWJOT2gzeGhJdFFWd1J6c2Q3Z2JWNmhDVHNsa25lTnI4Mnp5bUIxOFAyUkVTamZPdy0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FBjCLB37HUeTP-2BCsMqgfBamcdoXcmwh1y6BFGE1nPh6BNRJ-2FkoMQsPrFLJ3l57LBzysjyvdOy9iuCxSBI9N4BE5fbPfj3EEAT8KOo1bQWTV-2F0Yue0qHNqc7oFBSOLSm851ur-2FBYlReKU0cSSJoL4c7M-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NH6Lts1ep19fMpwpwTsRtXN8KRlTFk-2BgTrOZ8Tbfd91c-2FQ-2Blviv7kKakVGq7cFDqpKbPPclonTRPeaMn0BYVPLenpk8ZRn7tJWMtETSHiFSJYd735Y0pQ-2Bfv7c8YAgM016UY5xj-2Bw3FjqRSCa8UAbTLRRgL-2BpoTv2I6rfFzF7mAikRAsokHWQfEX55rqzTgLvU-2BGusWE80K54JZ6dZ89AR13Jx-2FPh71UeGlOFqWnm503UVNSSf3RrmF4U6HcR-2BlORPcaf-2FT4P65RNvz55bNOh3xhItQVwRzsd7gbV6hCTslkneNr82zymB18P2RESjfOw-3D-3D"><img src="https://cache.mail.yandex.net/mail/4255738756813ee5f76c6df4096844f3/static.sendgrid.com/uploads/UID_662821_NL_1735795_2613e0a3eff6a3a97494fa257bc1a34c/15af10d9cedd5f179c51e67e5e2da503" alt="Подать"></a></td>\n</tr><tr><td colspan="4" height="20"> </td>\n</tr></tbody></table></td>\n</tr><tr><td align="left" valign="top" bgcolor="white">Здравствуйте, Антон!</td>\n</tr><tr><td height="20"> </td>\n</tr><tr><td align="center" valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" bgcolor="white">\n<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top">Лучшие бюджетные планшеты для учебы и не только</td>\n</tr></tbody></table><table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="left" valign="top"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJOsie1xNw2lpm5HmKA01RvmxGbwn4L7iK6sMz7X1iirKRXeJ3VoATYTcr0T0UJQ6a5zLTnDN2b790W9E1aMatbYj4iPfi6Gyg1ztBG8kcZIF94ojECQs4pBg-2BAT8Xheu6Q-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2A8M47jutKuqNfcDexsUjbVDVZGXf-2BjfxQkck5IMHtwq29Xhk4Jhh-2FtOGCh-2BfOU5jYed8vkwJzH5CMiqPD-2F47IIoE1cgk9C6DShs5YzRZWiRpi0iBbU2SIByaptbc3dnbOTuxiTBOI1vDFIfZokmKMzeXpq5kXlv-2BQ-2BlMtP5HhZ2Cnd8MThBMJGzSwjB-2Bcj9dyyt7qxnmciUrhlDEus0-2B06qi8YUBl5zhk0X-2BqZbZqS6KNmF7WcBNPlFcaC6JUt3g-2B3iZLT9feJ1TIWeHla-2B9X5-2Fh1q-2BIUERzSw-2Bx1ZJ3J4Rw7-2B3g4Lnkq05SQmqwhGMvg-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,7ehWp6BwyXa3le98QdhxwA&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKT3NpZTF4TncybHBtNUhtS0EwMVJ2bXhHYnduNEw3aUs2c016N1gxaWlyS1JYZUozVm9BVFlUY3IwVDBVSlE2YTV6TFRuRE4yYjc5MFc5RTFhTWF0YllqNGlQZmk2R3lnMXp0Qkc4a2NaSUY5NG9qRUNRczRwQmctMkJBVDhYaGV1NlEtM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkE4TTQ3anV0S3VxTmZjRGV4c1VqYlZEVlpHWGYtMkJqZnhRa2NrNUlNSHR3cTI5WGhrNEpoaC0yRnRPR0NoLTJCZk9VNWpZZWQ4dmt3SnpINUNNaXFQRC0yRjQ3SUlvRTFjZ2s5QzZEU2hzNVl6UlpXaVJwaTBpQmJVMlNJQnlhcHRiYzNkbmJPVHV4aVRCT0kxdkRGSWZab2ttS016ZVhwcTVrWGx2LTJCUS0yQmxNdFA1SGhaMkNuZDhNVGhCTUpHelN3akItMkJjajlkeXl0N3F4bm1jaVVyaGxERXVzMC0yQjA2cWk4WVVCbDV6aGswWC0yQnFaYlpxUzZLTm1GN1djQk5QbEZjYUM2SlV0M2ctMkIzaVpMVDlmZUoxVElXZUhsYS0yQjlYNS0yRmgxcS0yQklVRVJ6U3ctMkJ4MVpKM0o0Unc3LTJCM2c0TG5rcTA1U1FtcXdoR012Zy0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJOsie1xNw2lpm5HmKA01RvmxGbwn4L7iK6sMz7X1iirKRXeJ3VoATYTcr0T0UJQ6a5zLTnDN2b790W9E1aMatbYj4iPfi6Gyg1ztBG8kcZIF94ojECQs4pBg-2BAT8Xheu6Q-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2A8M47jutKuqNfcDexsUjbVDVZGXf-2BjfxQkck5IMHtwq29Xhk4Jhh-2FtOGCh-2BfOU5jYed8vkwJzH5CMiqPD-2F47IIoE1cgk9C6DShs5YzRZWiRpi0iBbU2SIByaptbc3dnbOTuxiTBOI1vDFIfZokmKMzeXpq5kXlv-2BQ-2BlMtP5HhZ2Cnd8MThBMJGzSwjB-2Bcj9dyyt7qxnmciUrhlDEus0-2B06qi8YUBl5zhk0X-2BqZbZqS6KNmF7WcBNPlFcaC6JUt3g-2B3iZLT9feJ1TIWeHla-2B9X5-2Fh1q-2BIUERzSw-2Bx1ZJ3J4Rw7-2B3g4Lnkq05SQmqwhGMvg-3D-3D"><img src="https://cache.mail.yandex.net/mail/abc3ea3532dffef6d9c6ad6f08d1aa09/static.sendgrid.com/uploads/UID_662821_NL_2043373_2c1292822e338ec151d308963014597c/4c1a9565088db8bb1bc9a03a08f9a4c1" alt="Планшеты" width="200" height="136" align="right"></a>Ещё пару лет назад планшетные компьютеры c качественным экраном сложно было назвать бюджетным приобретением, сегодня же все иначе. Для учебы и досуга крайне важны насыщенные цвета, хороший задел по увеличению уровня яркости, а также отсутствие переливов и смещений оттенков, что значительно упрощает работу с устройством. Отбирая среди современных бюджетных планшетов лучшее сочетание цены и качества, мы ограничились моделями с экраном диагональю 7 дюймов с матрицей IPS.<br><br>Специалисты AVITO выбрали для вас наиболее удачные бюджетные планшеты:\n<ul><li><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJBsBXCfNrokhNwPjtSeduxpZkOoAMj1TFtu0eIA2kl2A20w8VszP2zlmpj-2FIfJzqW0HHMyhgb0T6QQ-2Fb-2FLUqO7fCY92pq6Ki8ocF5bM4vt1SbvwGFpVvoqEY4JBcxjt4qsYIuhGHABIfX36zZ47c66GCvoTKOuV0W-2BUtJPrQHyrG_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2GDJSUsm8N3eTH1u0tuEGKN5ra5SulbyARmbVfMGTbSOF70ZLr-2BOeTRZGYnptveLj8TsNtI26gTRtct-2FzEJZ04tp-2FPh1MOT51n7S-2FdVZwBW1HhXqPvrnPIyL5Py9LTgzK1b2sooqWqGiiwfJJXhMFLSJmc06vrwoDD3wsAB6DKszk7AFkq4k-2BjCBaDPqLMspBup-2Fx-2FDd72rrFSqXNm83csrExSQwOlz0ICzH4QZCQvcy4a7duEYdj-2F82b2BWe3gfcXa-2FZPXrVmzfD-2FD4MeZaoSIkDcrQwMuCUIOpTRPGBhBuU-2BzpA9QKtng1qYl97Ioc5Q-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,5V5ZXVA-Pg8Tw3JGu98yHw&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKQnNCWENmTnJva2hOd1BqdFNlZHV4cFprT29BTWoxVEZ0dTBlSUEya2wyQTIwdzhWc3pQMnpsbXBqLTJGSWZKenFXMEhITXloZ2IwVDZRUS0yRmItMkZMVXFPN2ZDWTkycHE2S2k4b2NGNWJNNHZ0MVNidndHRnBWdm9xRVk0SkJjeGp0NHFzWUl1aEdIQUJJZlgzNnpaNDdjNjZHQ3ZvVEtPdVYwVy0yQlV0SlByUUh5ckdfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkdESlNVc204TjNlVEgxdTB0dUVHS041cmE1U3VsYnlBUm1iVmZNR1RiU09GNzBaTHItMkJPZVRSWkdZbnB0dmVMajhUc050STI2Z1RSdGN0LTJGekVKWjA0dHAtMkZQaDFNT1Q1MW43Uy0yRmRWWndCVzFIaFhxUHZyblBJeUw1UHk5TFRneksxYjJzb29xV3FHaWl3ZkpKWGhNRkxTSm1jMDZ2cndvREQzd3NBQjZES3N6azdBRmtxNGstMkJqQ0JhRFBxTE1zcEJ1cC0yRngtMkZEZDcycnJGU3FYTm04M2NzckV4U1F3T2x6MElDekg0UVpDUXZjeTRhN2R1RVlkai0yRjgyYjJCV2UzZ2ZjWGEtMkZaUFhyVm16ZkQtMkZENE1lWmFvU0lrRGNyUXdNdUNVSU9wVFJQR0JoQnVVLTJCenBBOVFLdG5nMXFZbDk3SW9jNVEtM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJBsBXCfNrokhNwPjtSeduxpZkOoAMj1TFtu0eIA2kl2A20w8VszP2zlmpj-2FIfJzqW0HHMyhgb0T6QQ-2Fb-2FLUqO7fCY92pq6Ki8ocF5bM4vt1SbvwGFpVvoqEY4JBcxjt4qsYIuhGHABIfX36zZ47c66GCvoTKOuV0W-2BUtJPrQHyrG_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2GDJSUsm8N3eTH1u0tuEGKN5ra5SulbyARmbVfMGTbSOF70ZLr-2BOeTRZGYnptveLj8TsNtI26gTRtct-2FzEJZ04tp-2FPh1MOT51n7S-2FdVZwBW1HhXqPvrnPIyL5Py9LTgzK1b2sooqWqGiiwfJJXhMFLSJmc06vrwoDD3wsAB6DKszk7AFkq4k-2BjCBaDPqLMspBup-2Fx-2FDd72rrFSqXNm83csrExSQwOlz0ICzH4QZCQvcy4a7duEYdj-2F82b2BWe3gfcXa-2FZPXrVmzfD-2FD4MeZaoSIkDcrQwMuCUIOpTRPGBhBuU-2BzpA9QKtng1qYl97Ioc5Q-3D-3D">Google Nexus 7</a><span class="Apple-converted-space"> </span>~5000 рублей.</li>\n<li><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJPS-2B4BeTB7SSM-2BpIlvB4uV6jkR6blMnP43XoyAimDYgr8iIEZ-2FP2s0GE2B-2FOcHK7gLmwkAL4HRuCJlcChon2G7OtTIfQ2vxN8NafQugR30hKf-2B0T7quQtH8oU-2FbGF4s-2FGmAlIXpPEuFKLeTSvBvbcx4ohQU36s9OaTh3Ge6t2srP_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NDkYyL5dvzAEPCsog6q6J9Le42ZL-2BV3lUejCwvBMHYDyuPdoV13PO6GFxnQ-2B3O-2Br2RItobRLuLwA0GX7zVqXMrAec4OzXHLvx15tNC9MtaL7bLY-2BPrw4d4o9nZMmJThXJpiTtSWcDOKEq71PcqPBxBGYUEeLyIUo4mrojOQuc2SaB-2BKi3EGiR08m6pL66px5cvRzkMqG6-2BmNjoVSxYGKW42DgGgA4KdkoeuG5qn88l-2BX7TSGOrVq21T6kN-2BYYPyh-2FTP9VWMULjwsYvt1tdna7zHN-2F4itT0-2FojFoEtieRVydCJTBUNcSOQINvTsQdfFMPQ-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,dPwCk4LcNGcL6nt21vpqPg&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKUFMtMkI0QmVUQjdTU00tMkJwSWx2QjR1VjZqa1I2YmxNblA0M1hveUFpbURZZ3I4aUlFWi0yRlAyczBHRTJCLTJGT2NISzdnTG13a0FMNEhSdUNKbGNDaG9uMkc3T3RUSWZRMnZ4TjhOYWZRdWdSMzBoS2YtMkIwVDdxdVF0SDhvVS0yRmJHRjRzLTJGR21BbElYcFBFdUZLTGVUU3ZCdmJjeDRvaFFVMzZzOU9hVGgzR2U2dDJzclBfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMk5Ea1l5TDVkdnpBRVBDc29nNnE2SjlMZTQyWkwtMkJWM2xVZWpDd3ZCTUhZRHl1UGRvVjEzUE82R0Z4blEtMkIzTy0yQnIyUkl0b2JSTHVMd0EwR1g3elZxWE1yQWVjNE96WEhMdngxNXROQzlNdGFMN2JMWS0yQlBydzRkNG85blpNbUpUaFhKcGlUdFNXY0RPS0VxNzFQY3FQQnhCR1lVRWVMeUlVbzRtcm9qT1F1YzJTYUItMkJLaTNFR2lSMDhtNnBMNjZweDVjdlJ6a01xRzYtMkJtTmpvVlN4WUdLVzQyRGdHZ0E0S2Rrb2V1RzVxbjg4bC0yQlg3VFNHT3JWcTIxVDZrTi0yQllZUHloLTJGVFA5VldNVUxqd3NZdnQxdGRuYTd6SE4tMkY0aXRUMC0yRm9qRm9FdGllUlZ5ZENKVEJVTmNTT1FJTnZUc1FkZkZNUFEtM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJPS-2B4BeTB7SSM-2BpIlvB4uV6jkR6blMnP43XoyAimDYgr8iIEZ-2FP2s0GE2B-2FOcHK7gLmwkAL4HRuCJlcChon2G7OtTIfQ2vxN8NafQugR30hKf-2B0T7quQtH8oU-2FbGF4s-2FGmAlIXpPEuFKLeTSvBvbcx4ohQU36s9OaTh3Ge6t2srP_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NDkYyL5dvzAEPCsog6q6J9Le42ZL-2BV3lUejCwvBMHYDyuPdoV13PO6GFxnQ-2B3O-2Br2RItobRLuLwA0GX7zVqXMrAec4OzXHLvx15tNC9MtaL7bLY-2BPrw4d4o9nZMmJThXJpiTtSWcDOKEq71PcqPBxBGYUEeLyIUo4mrojOQuc2SaB-2BKi3EGiR08m6pL66px5cvRzkMqG6-2BmNjoVSxYGKW42DgGgA4KdkoeuG5qn88l-2BX7TSGOrVq21T6kN-2BYYPyh-2FTP9VWMULjwsYvt1tdna7zHN-2F4itT0-2FojFoEtieRVydCJTBUNcSOQINvTsQdfFMPQ-3D-3D">Kindle Fire HD</a><span class="Apple-converted-space"> </span>~6000 рублей.</li>\n<li><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJDxghS-2FNL56IZ-2Fh5LDqpMbXpsW1e2Y3p9-2F6VsfMXWdFerPX6h7Za7Cbh2z6WPifZ0u-2FgJZrWZGevv0S-2FPfvS4mooQ5rm0j5Mdmwe4MKTLp6-2BMgyOL73KAXux0bkD6zX64JplnRGHHWbrOcgaf-2Fra-2Bc59JN89QJgAPmgN3EX07ih-2BK0V8P1DlXcyPY4O9t2ABnQ-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Hqw9Mup-2BzmGNOMYoYdSd6kUjQqH9263AlwBqmOdn-2B3drtg-2BR1FqGYYY9Vqu-2FniHgG3OQPlAH2bxVkwO2czGEUzNfxVFylTa-2FcqaPdTRoFME43pkPU577X3UinHCOF2faRkfyixukpj8FDQwwoCGFmg5YWAo-2BKPU3YpitddIjerKJFCUagDKzvZPjfCJ9gol4wkM28pzcvpGxVY92D53IoEdbJsOu08DYGxF3qrpVeu49LXry2eiq1kBMleZWAW9FOV2KaWS-2Fq2r1wDBNcMrdij7E7Hx-2FRsO-2BX9XGR-2FXiFMMso29VjmAmgc8UtrQJVcBNA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,1tBb-FnU2s1MjCV9HOIByg&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKRHhnaFMtMkZOTDU2SVotMkZoNUxEcXBNYlhwc1cxZTJZM3A5LTJGNlZzZk1YV2RGZXJQWDZoN1phN0NiaDJ6NldQaWZaMHUtMkZnSlpyV1pHZXZ2MFMtMkZQZnZTNG1vb1E1cm0wajVNZG13ZTRNS1RMcDYtMkJNZ3lPTDczS0FYdXgwYmtENnpYNjRKcGxuUkdISFdick9jZ2FmLTJGcmEtMkJjNTlKTjg5UUpnQVBtZ04zRVgwN2loLTJCSzBWOFAxRGxYY3lQWTRPOXQyQUJuUS0zRC0zRF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UySHF3OU11cC0yQnptR05PTVlvWWRTZDZrVWpRcUg5MjYzQWx3QnFtT2RuLTJCM2RydGctMkJSMUZxR1lZWTlWcXUtMkZuaUhnRzNPUVBsQUgyYnhWa3dPMmN6R0VVek5meFZGeWxUYS0yRmNxYVBkVFJvRk1FNDNwa1BVNTc3WDNVaW5IQ09GMmZhUmtmeWl4dWtwajhGRFF3d29DR0ZtZzVZV0FvLTJCS1BVM1lwaXRkZElqZXJLSkZDVWFnREt6dlpQamZDSjlnb2w0d2tNMjhwemN2cEd4Vlk5MkQ1M0lvRWRiSnNPdTA4RFlHeEYzcXJwVmV1NDlMWHJ5MmVpcTFrQk1sZVpXQVc5Rk9WMkthV1MtMkZxMnIxd0RCTmNNcmRpajdFN0h4LTJGUnNPLTJCWDlYR1ItMkZYaUZNTXNvMjlWam1BbWdjOFV0clFKVmNCTkEtM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJDxghS-2FNL56IZ-2Fh5LDqpMbXpsW1e2Y3p9-2F6VsfMXWdFerPX6h7Za7Cbh2z6WPifZ0u-2FgJZrWZGevv0S-2FPfvS4mooQ5rm0j5Mdmwe4MKTLp6-2BMgyOL73KAXux0bkD6zX64JplnRGHHWbrOcgaf-2Fra-2Bc59JN89QJgAPmgN3EX07ih-2BK0V8P1DlXcyPY4O9t2ABnQ-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Hqw9Mup-2BzmGNOMYoYdSd6kUjQqH9263AlwBqmOdn-2B3drtg-2BR1FqGYYY9Vqu-2FniHgG3OQPlAH2bxVkwO2czGEUzNfxVFylTa-2FcqaPdTRoFME43pkPU577X3UinHCOF2faRkfyixukpj8FDQwwoCGFmg5YWAo-2BKPU3YpitddIjerKJFCUagDKzvZPjfCJ9gol4wkM28pzcvpGxVY92D53IoEdbJsOu08DYGxF3qrpVeu49LXry2eiq1kBMleZWAW9FOV2KaWS-2Fq2r1wDBNcMrdij7E7Hx-2FRsO-2BX9XGR-2FXiFMMso29VjmAmgc8UtrQJVcBNA-3D-3D">Samsung Galaxy Tab 3 7’’</a><span class="Apple-converted-space"> </span>~7500 рублей.</li>\n<li><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJIc21rtw6njfovvR4DYDcjEqq2RuQj8xvY0mqwtglGyx3eV7TQC0l08nr-2BZBxaTfReRNMdDeExVmTYp0KAy61A8wPNM2e2cla-2FPyp9-2FRmP0Ag-2FSMjT4hFOTB9YmYYXk3uulKC6EgrGnH1O8occSp81fZ8Lpmy1oTqXYUYix4VYiRbdMDIOjGIz4oIKEW-2FYhMng-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2AzlXL80ZgUhbYz5fBCdounRFmmiqWKIVvKwJ4afvwksIseyvl0hv7f6fGzPuayMCHKc2N5iF0pMf2LUOEmN78WTlA6DptgMXxYNGXKV7E82BCmgdZmDVGUhrw4xzzt-2Fpjni62xpp2w5bqNlvG5JuPNQAeLTiEZvYuE9tDz8LH1GaaveaqvAd7CL6SzFbF9nY2INFnhu83O5-2FpyRPsj9CCCt2d0-2Fcgjum6nSFGm9lamHtCd-2FNVoBWL0-2Fmj5XPMYesTlgRpaoWPK3PkTYu2XI9OwsJZsC7mEUrj2jdHrs4rGMfamsx-2BfHivuyTq0wqKzxZg-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,xOzVDF8s2gN9SvXg6q3B_w&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKSWMyMXJ0dzZuamZvdnZSNERZRGNqRXFxMlJ1UWo4eHZZMG1xd3RnbEd5eDNlVjdUUUMwbDA4bnItMkJaQnhhVGZSZVJOTWREZUV4Vm1UWXAwS0F5NjFBOHdQTk0yZTJjbGEtMkZQeXA5LTJGUm1QMEFnLTJGU01qVDRoRk9UQjlZbVlZWGszdXVsS0M2RWdyR25IMU84b2NjU3A4MWZaOExwbXkxb1RxWFlVWWl4NFZZaVJiZE1ESU9qR0l6NG9JS0VXLTJGWWhNbmctM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkF6bFhMODBaZ1VoYll6NWZCQ2RvdW5SRm1taXFXS0lWdkt3SjRhZnZ3a3NJc2V5dmwwaHY3ZjZmR3pQdWF5TUNIS2MyTjVpRjBwTWYyTFVPRW1ONzhXVGxBNkRwdGdNWHhZTkdYS1Y3RTgyQkNtZ2RabURWR1Vocnc0eHp6dC0yRnBqbmk2MnhwcDJ3NWJxTmx2RzVKdVBOUUFlTFRpRVp2WXVFOXREejhMSDFHYWF2ZWFxdkFkN0NMNlN6RmJGOW5ZMklORm5odTgzTzUtMkZweVJQc2o5Q0NDdDJkMC0yRmNnanVtNm5TRkdtOWxhbUh0Q2QtMkZOVm9CV0wwLTJGbWo1WFBNWWVzVGxnUnBhb1dQSzNQa1RZdTJYSTlPd3NKWnNDN21FVXJqMmpkSHJzNHJHTWZhbXN4LTJCZkhpdnV5VHEwd3FLenhaZy0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJIc21rtw6njfovvR4DYDcjEqq2RuQj8xvY0mqwtglGyx3eV7TQC0l08nr-2BZBxaTfReRNMdDeExVmTYp0KAy61A8wPNM2e2cla-2FPyp9-2FRmP0Ag-2FSMjT4hFOTB9YmYYXk3uulKC6EgrGnH1O8occSp81fZ8Lpmy1oTqXYUYix4VYiRbdMDIOjGIz4oIKEW-2FYhMng-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2AzlXL80ZgUhbYz5fBCdounRFmmiqWKIVvKwJ4afvwksIseyvl0hv7f6fGzPuayMCHKc2N5iF0pMf2LUOEmN78WTlA6DptgMXxYNGXKV7E82BCmgdZmDVGUhrw4xzzt-2Fpjni62xpp2w5bqNlvG5JuPNQAeLTiEZvYuE9tDz8LH1GaaveaqvAd7CL6SzFbF9nY2INFnhu83O5-2FpyRPsj9CCCt2d0-2Fcgjum6nSFGm9lamHtCd-2FNVoBWL0-2Fmj5XPMYesTlgRpaoWPK3PkTYu2XI9OwsJZsC7mEUrj2jdHrs4rGMfamsx-2BfHivuyTq0wqKzxZg-3D-3D">ASUS MeMO Pad HD 7</a><span class="Apple-converted-space"> </span>~7500 рублей.</li>\n<li><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJDUoxi1O7q3hspkzJsIGIyxvQJegsLwXD7Yu0eHXCyeUMeuStRZwmGgCpNz3Km5lv8vtD9WfzrtWNCoj1xQJ0CkcITZp6pSYzk-2B5cjidlr6zZFd-2BGV6YF23a66IkkxJ-2FLM34xFc2D9NxYbT0JLi-2FgDkinsCZ-2BwI5V01XE3mzNLZH_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2G2-2BPxUi-2B4G6rh50V1LcYyrA1EzZOHIt-2FMyNLGpAb-2Bw7yxzbBCf54Qs4OlawkYEb-2F1itqYpZPo-2BHGN1BGtohqvNE4EOCxPVMUqsivd-2FM8tvx33vhq6rGxqEjr9g4c5EtfWE98K2Gw4y1xqSN4kR0LRrb2CvwGjaVHBOQJvZgB3oA7U-2FlusMTGy-2Bi45d9fsXHxDqlqZfYIiTCC-2BCPKE8yfWX-2BK6pgeIeavMKEw0Q8GFdUDBfYTFzJ4mLrZFb0DEqSBaJyRP5hoq1kJoBhvhaZRD6-2FINNeVKNw66Wstqfm-2BZQNnapxCgApnV0Wq1GyZkK-2BKw-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,Fl1FsV7hYtaKDndXP8eGtA&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsVEl0OWR6a0FZRGdyVW9CckoxVzRhRmpMTFJWNmRyMlBUdFR2Mjh3alEtMkJKRFVveGkxTzdxM2hzcGt6SnNJR0l5eHZRSmVnc0x3WEQ3WXUwZUhYQ3llVU1ldVN0Ulp3bUdnQ3BOejNLbTVsdjh2dEQ5V2Z6cnRXTkNvajF4UUowQ2tjSVRacDZwU1l6ay0yQjVjamlkbHI2elpGZC0yQkdWNllGMjNhNjZJa2t4Si0yRkxNMzR4RmMyRDlOeFliVDBKTGktMkZnRGtpbnNDWi0yQndJNVYwMVhFM216TkxaSF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UyRzItMkJQeFVpLTJCNEc2cmg1MFYxTGNZeXJBMUV6Wk9ISXQtMkZNeU5MR3BBYi0yQnc3eXh6YkJDZjU0UXM0T2xhd2tZRWItMkYxaXRxWXBaUG8tMkJIR04xQkd0b2hxdk5FNEVPQ3hQVk1VcXNpdmQtMkZNOHR2eDMzdmhxNnJHeHFFanI5ZzRjNUV0ZldFOThLMkd3NHkxeHFTTjRrUjBMUnJiMkN2d0dqYVZIQk9RSnZaZ0Izb0E3VS0yRmx1c01UR3ktMkJpNDVkOWZzWEh4RHFscVpmWUlpVENDLTJCQ1BLRTh5ZldYLTJCSzZwZ2VJZWF2TUtFdzBROEdGZFVEQmZZVEZ6SjRtTHJaRmIwREVxU0JhSnlSUDVob3Exa0pvQmh2aGFaUkQ2LTJGSU5OZVZLTnc2NldzdHFmbS0yQlpRTm5hcHhDZ0FwblYwV3ExR3laa0stMkJLdy0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlTIt9dzkAYDgrUoBrJ1W4aFjLLRV6dr2PTtTv28wjQ-2BJDUoxi1O7q3hspkzJsIGIyxvQJegsLwXD7Yu0eHXCyeUMeuStRZwmGgCpNz3Km5lv8vtD9WfzrtWNCoj1xQJ0CkcITZp6pSYzk-2B5cjidlr6zZFd-2BGV6YF23a66IkkxJ-2FLM34xFc2D9NxYbT0JLi-2FgDkinsCZ-2BwI5V01XE3mzNLZH_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2G2-2BPxUi-2B4G6rh50V1LcYyrA1EzZOHIt-2FMyNLGpAb-2Bw7yxzbBCf54Qs4OlawkYEb-2F1itqYpZPo-2BHGN1BGtohqvNE4EOCxPVMUqsivd-2FM8tvx33vhq6rGxqEjr9g4c5EtfWE98K2Gw4y1xqSN4kR0LRrb2CvwGjaVHBOQJvZgB3oA7U-2FlusMTGy-2Bi45d9fsXHxDqlqZfYIiTCC-2BCPKE8yfWX-2BK6pgeIeavMKEw0Q8GFdUDBfYTFzJ4mLrZFb0DEqSBaJyRP5hoq1kJoBhvhaZRD6-2FINNeVKNw66Wstqfm-2BZQNnapxCgApnV0Wq1GyZkK-2BKw-3D-3D">iPad Mini</a><span class="Apple-converted-space"> </span>~8000 рублей.</li>\n</ul>\nПорадуйте себя или своих детей новыми гаджетами, которые станут вашими помощниками в любых начинаниях!</td>\n</tr></tbody></table></td>\n</tr></tbody></table></td>\n</tr><tr><td height="20"> </td>\n</tr><tr><td>\n<table border="0" width="710" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" bgcolor="white">\n<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top">Улучшения поиска на AVITO</td>\n</tr></tbody></table><table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td align="left" valign="top">\n<ul><ul><li>На <a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlRn6IC7sx3LvbtVtidDgBID0S-2FKRufq4bY66X5ATIHpExBWckj-2Fgm4TtqXMTjQIS2z9ijNlm0ezYFVRcMVj3rknWDFoTFl4njY8MKl8Dx-2F46MGzGymHryxDAI0fsKuco9EHydGMzvQYvlJWhneh90b5J-2BP-2FIKIf4HfRUrj4rmkpTg-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2JELlRDl7qg4TstM-2BLsYCP9IXYfIAS7IP8tl0zGRtbz9J2YC7cJTA8xJAuXICfJRadEYdI9KPBzjU1rx-2Bf0DGvyGfCCvUaDKwL19aI2dHt6cLDQpeo5uShlPjmWiHiZl-2BY7sx4yBFm6JueG4qDCcPUEObf2hlktrN4auHF8z8fcUovG4yag4-2B0l4P-2B4HctGBt-2B6iJWiaPl3T7vkjDtQos0IXEPN-2Bgt5ijYEZ57uN7tXkQqb172VooUHVfKtGbqQb8RnRdssTEA55G2fI05y3iXusIyXv4VnforE8Dzc6L9G0ePUKAJND9AkOYtWm7-2F2uVA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,26ytYYRYhRaUxysL7DZl5Q&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsUm42SUM3c3gzTHZidFZ0aWREZ0JJRDBTLTJGS1J1ZnE0Ylk2Nlg1QVRJSHBFeEJXY2tqLTJGZ200VHRxWE1UalFJUzJ6OWlqTmxtMGV6WUZWUmNNVmozcmtuV0RGb1RGbDRualk4TUtsOER4LTJGNDZNR3pHeW1Icnl4REFJMGZzS3VjbzlFSHlkR016dlFZdmxKV2huZWg5MGI1Si0yQlAtMkZJS0lmNEhmUlVyajRybWtwVGctM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkpFTGxSRGw3cWc0VHN0TS0yQkxzWUNQOUlYWWZJQVM3SVA4dGwwekdSdGJ6OUoyWUM3Y0pUQTh4SkF1WElDZkpSYWRFWWRJOUtQQnpqVTFyeC0yQmYwREd2eUdmQ0N2VWFES3dMMTlhSTJkSHQ2Y0xEUXBlbzV1U2hsUGptV2lIaVpsLTJCWTdzeDR5QkZtNkp1ZUc0cURDY1BVRU9iZjJobGt0ck40YXVIRjh6OGZjVW92RzR5YWc0LTJCMGw0UC0yQjRIY3RHQnQtMkI2aUpXaWFQbDNUN3ZrakR0UW9zMElYRVBOLTJCZ3Q1aWpZRVo1N3VON3RYa1FxYjE3MlZvb1VIVmZLdEdicVFiOFJuUmRzc1RFQTU1RzJmSTA1eTNpWHVzSXlYdjRWbmZvckU4RHpjNkw5RzBlUFVLQUpORDlBa09ZdFdtNy0yRjJ1VkEtM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlRn6IC7sx3LvbtVtidDgBID0S-2FKRufq4bY66X5ATIHpExBWckj-2Fgm4TtqXMTjQIS2z9ijNlm0ezYFVRcMVj3rknWDFoTFl4njY8MKl8Dx-2F46MGzGymHryxDAI0fsKuco9EHydGMzvQYvlJWhneh90b5J-2BP-2FIKIf4HfRUrj4rmkpTg-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2JELlRDl7qg4TstM-2BLsYCP9IXYfIAS7IP8tl0zGRtbz9J2YC7cJTA8xJAuXICfJRadEYdI9KPBzjU1rx-2Bf0DGvyGfCCvUaDKwL19aI2dHt6cLDQpeo5uShlPjmWiHiZl-2BY7sx4yBFm6JueG4qDCcPUEObf2hlktrN4auHF8z8fcUovG4yag4-2B0l4P-2B4HctGBt-2B6iJWiaPl3T7vkjDtQos0IXEPN-2Bgt5ijYEZ57uN7tXkQqb172VooUHVfKtGbqQb8RnRdssTEA55G2fI05y3iXusIyXv4VnforE8Dzc6L9G0ePUKAJND9AkOYtWm7-2F2uVA-3D-3D">AVITO Авто</a><span class="Apple-converted-space"> </span>полностью переработана система названий и добавлено больше сотни новых моделей. Теперь вы можете размещать свой редкий или не очень автомобиль в полностью подходящей категории с корректным именем.</li>\n<li>На <a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlRxrFUY-2BgC4HoT7P6IaW49MXbZvagLi8ktsJz29-2BfjgYYo3XBEGhHUIo2N-2FuxwtbpdXqHEybaRrbBENVd2Ue1vKlSOC9KsgvKdO6SGxZHwwpE0S1t1aFPU-2FLG2vltSz049G8h7CkFtr-2FSSEye5lHIpYjtGUeLBH723weUme-2Bs7Djg-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2BY-2FbTRqS6GRxjw-2BM1uD5TuRSYLiumGqxL9Y93pm6Lu6PkqQCnMHV3NyjIvvdICgjEhlXB-2FOzvadt40lu2QSI0NfNfOSsL-2BE11R8KyAUVVreYMp0RefGCZ-2F7CG-2Bwq4NhOsfN2O5QleXA9vbYG-2FiXkkSPbg1RK-2FRUuzzlyPDBGWPnI0UEalO2lMlSUR7Tj49nkU8FmQAppd1Nz4Tf1j9-2BDHSSLIIqo3BfdJVnUQB9SGygLYH2Xl6NUDpyg-2FYxZDuuru6BILcesb6XYhGZtuxEnvCSWtT5vVvSVujdnoabe2vFqzEpOx-2FG9qjpaOzgzVHfRA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,8X71yE53JplyWnLF8GgmAA&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsUnhyRlVZLTJCZ0M0SG9UN1A2SWFXNDlNWGJadmFnTGk4a3RzSnoyOS0yQmZqZ1lZbzNYQkVHaEhVSW8yTi0yRnV4d3RicGRYcUhFeWJhUnJiQkVOVmQyVWUxdktsU09DOUtzZ3ZLZE82U0d4Wkh3d3BFMFMxdDFhRlBVLTJGTEcydmx0U3owNDlHOGg3Q2tGdHItMkZTU0V5ZTVsSElwWWp0R1VlTEJINzIzd2VVbWUtMkJzN0RqZy0zRC0zRF9sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UyQlktMkZiVFJxUzZHUnhqdy0yQk0xdUQ1VHVSU1lMaXVtR3F4TDlZOTNwbTZMdTZQa3FRQ25NSFYzTnlqSXZ2ZElDZ2pFaGxYQi0yRk96dmFkdDQwbHUyUVNJME5mTmZPU3NMLTJCRTExUjhLeUFVVlZyZVlNcDBSZWZHQ1otMkY3Q0ctMkJ3cTROaE9zZk4yTzVRbGVYQTl2YllHLTJGaVhra1NQYmcxUkstMkZSVXV6emx5UERCR1dQbkkwVUVhbE8ybE1sU1VSN1RqNDlua1U4Rm1RQXBwZDFOejRUZjFqOS0yQkRIU1NMSUlxbzNCZmRKVm5VUUI5U0d5Z0xZSDJYbDZOVURweWctMkZZeFpEdXVydTZCSUxjZXNiNlhZaEdadHV4RW52Q1NXdFQ1dlZ2U1Z1amRub2FiZTJ2RnF6RXBPeC0yRkc5cWpwYU96Z3pWSGZSQS0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlRxrFUY-2BgC4HoT7P6IaW49MXbZvagLi8ktsJz29-2BfjgYYo3XBEGhHUIo2N-2FuxwtbpdXqHEybaRrbBENVd2Ue1vKlSOC9KsgvKdO6SGxZHwwpE0S1t1aFPU-2FLG2vltSz049G8h7CkFtr-2FSSEye5lHIpYjtGUeLBH723weUme-2Bs7Djg-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2BY-2FbTRqS6GRxjw-2BM1uD5TuRSYLiumGqxL9Y93pm6Lu6PkqQCnMHV3NyjIvvdICgjEhlXB-2FOzvadt40lu2QSI0NfNfOSsL-2BE11R8KyAUVVreYMp0RefGCZ-2F7CG-2Bwq4NhOsfN2O5QleXA9vbYG-2FiXkkSPbg1RK-2FRUuzzlyPDBGWPnI0UEalO2lMlSUR7Tj49nkU8FmQAppd1Nz4Tf1j9-2BDHSSLIIqo3BfdJVnUQB9SGygLYH2Xl6NUDpyg-2FYxZDuuru6BILcesb6XYhGZtuxEnvCSWtT5vVvSVujdnoabe2vFqzEpOx-2FG9qjpaOzgzVHfRA-3D-3D">AVITO Недвижимость</a><span class="Apple-converted-space"> </span>теперь доступен поиск по определенным районам городов. Пока что по городам-миллио<wbr></wbr>нерам. Доступен поиск не только по административн<wbr></wbr>о утвержденным названиям районов, но и по наиболее употребительны<wbr></wbr>м, которыми чаще пользуются местные жители.</li>\n</ul></ul><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FKF1IusnuXZ4KZeH0rZn1mzOHdI4dQeZBC1dZiwCgkxgyjxqEunYbFCE7stJZ1VLOlo8VX8xD-2FloK1vwGh2sKp5Liht7eNbx99NCmUIJr8yWQSGYrSBQQcv3LsShBrvlj33XYYmuouRVp10CB0cq48T1N-2FSuKPqsSmjBc-2FRTh0r-2F_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Mcd6uGnU4HQieJnSDHwd1vGZrzFIuVcpGPUAI6QeFSUNJ3jpW5bvKedvRONE2WB8QLVYibEDwoB4kNMcFO7QwtZImQYzEJZ1dhdE1122NrUiyEulkRjlG0icv7Kx78JNnSxT9X9oABFipE1YO1alNPK6sSmaOC5JUhapBE3wTYuezcMmEs6JIs6g-2Fky2XGEa4-2Fx64Tkw27dj9f-2FHE4wWhH6OCVXumt55qc9Q4UnGvnjzZty7RI1D58aUyWucxO3yZcrsk9IyImnLj8bb-2FqO-2FPjXXcMBtZm124HaBQy-2BwMktfI99kaoSEaKhd7B7EJlbAg-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,RhKCxbJLIwlcVhcDAwW2qA&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRktGMUl1c251WFo0S1plSDByWm4xbXpPSGRJNGRRZVpCQzFkWml3Q2dreGd5anhxRXVuWWJGQ0U3c3RKWjFWTE9sbzhWWDh4RC0yRmxvSzF2d0doMnNLcDVMaWh0N2VOYng5OU5DbVVJSnI4eVdRU0dZclNCUVFjdjNMc1NoQnJ2bGozM1hZWW11b3VSVnAxMENCMGNxNDhUMU4tMkZTdUtQcXNTbWpCYy0yRlJUaDByLTJGX2xRWHNLeC0yQklZdkdtOVp0RWZOM21OZ1ZzS2QxOXgzZmNUcDdlY0xzM0pjMUMtMkItMkZhald0SEh2cXNNaFpKdTNtbkk1azhFNUVhd3N0Q3RyamRDbnZLRTJNY2Q2dUduVTRIUWllSm5TREh3ZDF2R1pyekZJdVZjcEdQVUFJNlFlRlNVTkozanBXNWJ2S2VkdlJPTkUyV0I4UUxWWWliRUR3b0I0a05NY0ZPN1F3dFpJbVFZekVKWjFkaGRFMTEyMk5yVWl5RXVsa1JqbEcwaWN2N0t4NzhKTm5TeFQ5WDlvQUJGaXBFMVlPMWFsTlBLNnNTbWFPQzVKVWhhcEJFM3dUWXVlemNNbUVzNkpJczZnLTJGa3kyWEdFYTQtMkZ4NjRUa3cyN2RqOWYtMkZIRTR3V2hINk9DVlh1bXQ1NXFjOVE0VW5Hdm5qelp0eTdSSTFENThhVXlXdWN4TzN5WmNyc2s5SXlJbW5MajhiYi0yRnFPLTJGUGpYWGNNQnRabTEyNEhhQlF5LTJCd01rdGZJOTlrYW9TRWFLaGQ3QjdFSmxiQWctM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FKF1IusnuXZ4KZeH0rZn1mzOHdI4dQeZBC1dZiwCgkxgyjxqEunYbFCE7stJZ1VLOlo8VX8xD-2FloK1vwGh2sKp5Liht7eNbx99NCmUIJr8yWQSGYrSBQQcv3LsShBrvlj33XYYmuouRVp10CB0cq48T1N-2FSuKPqsSmjBc-2FRTh0r-2F_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Mcd6uGnU4HQieJnSDHwd1vGZrzFIuVcpGPUAI6QeFSUNJ3jpW5bvKedvRONE2WB8QLVYibEDwoB4kNMcFO7QwtZImQYzEJZ1dhdE1122NrUiyEulkRjlG0icv7Kx78JNnSxT9X9oABFipE1YO1alNPK6sSmaOC5JUhapBE3wTYuezcMmEs6JIs6g-2Fky2XGEa4-2Fx64Tkw27dj9f-2FHE4wWhH6OCVXumt55qc9Q4UnGvnjzZty7RI1D58aUyWucxO3yZcrsk9IyImnLj8bb-2FqO-2FPjXXcMBtZm124HaBQy-2BwMktfI99kaoSEaKhd7B7EJlbAg-3D-3D"><img src="https://cache.mail.yandex.net/mail/c72d980bee539b429f30a8ac9bdf89ec/static.sendgrid.com/uploads/UID_662821_NL_2043373_5fac1b7abfc3bbeb34877ac9a815ea37/bfacf532913588da7c039eac13a00387" alt="Поиск" width="590" height="219" align="middle"></a></td>\n</tr></tbody></table></td>\n</tr></tbody></table></td>\n</tr><tr><td height="20"> </td>\n</tr><tr><td align="center" valign="top" width="100%">\n<table border="0" width="710" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" bgcolor="white">\n<table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top">500 рублей самому внимательному</td>\n</tr></tbody></table><table border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr><td valign="top" width="35%" height="220"><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FG57eRF-2B4gF1WK7nHMvR-2FfmdJqLZQNsP8OKL4EC4neBmZJ-2BggsvOiYyw-2BFhD20RRqK3w-2FQibnCY2UDGjFbWjWbeSWMfqB2wttdPikorj0bgXKIApIMHjstXlcpcst3whglmoK41-2FrIP05o8eKjWXHA7YS5YyVLORc1fj4YHdCml4pg-2FU2beOtYltJV5c-2BweW5YJjNXCydWxe-2B5-2Ff5OS8TFekN8RlkIzK1qbUO6RpZNyKsWB6H2AlOdQceXu2h8KIdA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Fj04usGHHmEeo3BoI5tl-2BDyrXre1WqvFbfauI3IcIjNMZTx4drXyv8NC61h3emD-2BP8VZTPhe2qRfNK4ycSp-2Fi76sdwaymfOFfa9es63oC2LhZdKoAp8IgvfQhaqpK4AZr8sIbYl9d4qpOKRkjOJVAe2Rt7A1URbUT8IMjP566EuWF0muP-2BWnFVd6Z9OH7TOAa-2BBSLJ49bZFBBKkVxv6jGGygFW1ClqaVXhEhu-2FN7afjGwB5WWFprIqRMBco10uiaj2FKygAxFXrTCGgNx4PdMO1g1UaiXwq-2FGAD9fijBGt0GCQRcjOMr7mQQrpXZSlJ4w-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,E6sv851_LFLV0j0kBWk_hw&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkc1N2VSRi0yQjRnRjFXSzduSE12Ui0yRmZtZEpxTFpRTnNQOE9LTDRFQzRuZUJtWkotMkJnZ3N2T2lZeXctMkJGaEQyMFJScUszdy0yRlFpYm5DWTJVREdqRmJXaldiZVNXTWZxQjJ3dHRkUGlrb3JqMGJnWEtJQXBJTUhqc3RYbGNwY3N0M3doZ2xtb0s0MS0yRnJJUDA1bzhlS2pXWEhBN1lTNVl5VkxPUmMxZmo0WUhkQ21sNHBnLTJGVTJiZU90WWx0SlY1Yy0yQndlVzVZSmpOWEN5ZFd4ZS0yQjUtMkZmNU9TOFRGZWtOOFJsa0l6SzFxYlVPNlJwWk55S3NXQjZIMkFsT2RRY2VYdTJoOEtJZEEtM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkZqMDR1c0dISG1FZW8zQm9JNXRsLTJCRHlyWHJlMVdxdkZiZmF1STNJY0lqTk1aVHg0ZHJYeXY4TkM2MWgzZW1ELTJCUDhWWlRQaGUycVJmTks0eWNTcC0yRmk3NnNkd2F5bWZPRmZhOWVzNjNvQzJMaFpkS29BcDhJZ3ZmUWhhcXBLNEFacjhzSWJZbDlkNHFwT0tSa2pPSlZBZTJSdDdBMVVSYlVUOElNalA1NjZFdVdGMG11UC0yQlduRlZkNlo5T0g3VE9BYS0yQkJTTEo0OWJaRkJCS2tWeHY2akdHeWdGVzFDbHFhVlhoRWh1LTJGTjdhZmpHd0I1V1dGcHJJcVJNQmNvMTB1aWFqMkZLeWdBeEZYclRDR2dOeDRQZE1PMWcxVWFpWHdxLTJGR0FEOWZpakJHdDBHQ1FSY2pPTXI3bVFRcnBYWlNsSjR3LTNELTNE" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FG57eRF-2B4gF1WK7nHMvR-2FfmdJqLZQNsP8OKL4EC4neBmZJ-2BggsvOiYyw-2BFhD20RRqK3w-2FQibnCY2UDGjFbWjWbeSWMfqB2wttdPikorj0bgXKIApIMHjstXlcpcst3whglmoK41-2FrIP05o8eKjWXHA7YS5YyVLORc1fj4YHdCml4pg-2FU2beOtYltJV5c-2BweW5YJjNXCydWxe-2B5-2Ff5OS8TFekN8RlkIzK1qbUO6RpZNyKsWB6H2AlOdQceXu2h8KIdA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2Fj04usGHHmEeo3BoI5tl-2BDyrXre1WqvFbfauI3IcIjNMZTx4drXyv8NC61h3emD-2BP8VZTPhe2qRfNK4ycSp-2Fi76sdwaymfOFfa9es63oC2LhZdKoAp8IgvfQhaqpK4AZr8sIbYl9d4qpOKRkjOJVAe2Rt7A1URbUT8IMjP566EuWF0muP-2BWnFVd6Z9OH7TOAa-2BBSLJ49bZFBBKkVxv6jGGygFW1ClqaVXhEhu-2FN7afjGwB5WWFprIqRMBco10uiaj2FKygAxFXrTCGgNx4PdMO1g1UaiXwq-2FGAD9fijBGt0GCQRcjOMr7mQQrpXZSlJ4w-3D-3D"><img src="https://cache.mail.yandex.net/mail/ee4a2970d84a8ad845b3f2a4590d47b3/static.sendgrid.com/uploads/UID_662821_NL_1818067_0c30b7815a102a1fa3d0625ecaedfb47/d8b1c148be7e69fe74af1c03863163b7" alt="Зубастики" width="200" height="200" align="left"></a></td>\n<td valign="top" width="60%" height="220">На этой неделе приз в размере<span class="Apple-converted-space"> </span>500 рублей<span class="Apple-converted-space"> </span>получит пользователь<span class="Apple-converted-space"> </span>evgeniyalucenk<wbr></wbr>o, приславший ссылку на такое объявление:<span class="Apple-converted-space"> </span><br><br><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FG57eRF-2B4gF1WK7nHMvR-2FfmdJqLZQNsP8OKL4EC4neBmZJ-2BggsvOiYyw-2BFhD20RRqK3w-2FQibnCY2UDGjFbWjWbeSWMfqB2wttdPikorj0bgXKIApIMHjstXlcpcst3whglmoK41-2FrIP05o8eKjWXHA7YS5YyVLORc1fj4YHdCml4pg-2FU2beOtYltJV5c-2BweW5YJjNXCydWxe-2B5-2Ff5OS8TFekN8RlkIzK1qbUO6RpZNyKsWB6H2AlOdQceXu2h8KIdA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2FcRK5UBH1BokuuZyOSY-2FYzXa-2BedO4FlPT7NtKMAtpujaN3igKxyyQY-2FEEjW8rSrLyoB-2FniLkh-2F76aCuRQeDN0vaLcv0jy9yblx7HHk5hOH64FrGhppgXhDUZ6OOwqZ05WOjfON6Lnu-2FIv-2BGq7oQo4-2BboJp6JLP03vBoYUfLRrnRKoqvg8rqNp5grkI-2BRNXNC5pIokkzTtOb55kbNfw-2FJvPcmGH86MVTrFfLuFeuYSxqPuFTzhoQ7u4XzZfPCg4NN9QCNi9fueD3DK2wTR-2BqYLZ7jNVu4MCtjObyorSo0-2FqcpXr8cZ81FU3uwp52UQPl1A-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,NqmfzbR__2GqyGsQbk22gw&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkc1N2VSRi0yQjRnRjFXSzduSE12Ui0yRmZtZEpxTFpRTnNQOE9LTDRFQzRuZUJtWkotMkJnZ3N2T2lZeXctMkJGaEQyMFJScUszdy0yRlFpYm5DWTJVREdqRmJXaldiZVNXTWZxQjJ3dHRkUGlrb3JqMGJnWEtJQXBJTUhqc3RYbGNwY3N0M3doZ2xtb0s0MS0yRnJJUDA1bzhlS2pXWEhBN1lTNVl5VkxPUmMxZmo0WUhkQ21sNHBnLTJGVTJiZU90WWx0SlY1Yy0yQndlVzVZSmpOWEN5ZFd4ZS0yQjUtMkZmNU9TOFRGZWtOOFJsa0l6SzFxYlVPNlJwWk55S3NXQjZIMkFsT2RRY2VYdTJoOEtJZEEtM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkZjUks1VUJIMUJva3V1WnlPU1ktMkZZelhhLTJCZWRPNEZsUFQ3TnRLTUF0cHVqYU4zaWdLeHl5UVktMkZFRWpXOHJTckx5b0ItMkZuaUxraC0yRjc2YUN1UlFlRE4wdmFMY3Ywank5eWJseDdISGs1aE9INjRGckdocHBnWGhEVVo2T093cVowNVdPamZPTjZMbnUtMkZJdi0yQkdxN29RbzQtMkJib0pwNkpMUDAzdkJvWVVmTFJyblJLb3F2ZzhycU5wNWdya0ktMkJSTlhOQzVwSW9ra3pUdE9iNTVrYk5mdy0yRkp2UGNtR0g4Nk1WVHJGZkx1RmV1WVN4cVB1RlR6aG9RN3U0WHpaZlBDZzROTjlRQ05pOWZ1ZUQzREsyd1RSLTJCcVlMWjdqTlZ1NE1DdGpPYnlvclNvMC0yRnFjcFhyOGNaODFGVTN1d3A1MlVRUGwxQS0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FG57eRF-2B4gF1WK7nHMvR-2FfmdJqLZQNsP8OKL4EC4neBmZJ-2BggsvOiYyw-2BFhD20RRqK3w-2FQibnCY2UDGjFbWjWbeSWMfqB2wttdPikorj0bgXKIApIMHjstXlcpcst3whglmoK41-2FrIP05o8eKjWXHA7YS5YyVLORc1fj4YHdCml4pg-2FU2beOtYltJV5c-2BweW5YJjNXCydWxe-2B5-2Ff5OS8TFekN8RlkIzK1qbUO6RpZNyKsWB6H2AlOdQceXu2h8KIdA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2FcRK5UBH1BokuuZyOSY-2FYzXa-2BedO4FlPT7NtKMAtpujaN3igKxyyQY-2FEEjW8rSrLyoB-2FniLkh-2F76aCuRQeDN0vaLcv0jy9yblx7HHk5hOH64FrGhppgXhDUZ6OOwqZ05WOjfON6Lnu-2FIv-2BGq7oQo4-2BboJp6JLP03vBoYUfLRrnRKoqvg8rqNp5grkI-2BRNXNC5pIokkzTtOb55kbNfw-2FJvPcmGH86MVTrFfLuFeuYSxqPuFTzhoQ7u4XzZfPCg4NN9QCNi9fueD3DK2wTR-2BqYLZ7jNVu4MCtjObyorSo0-2FqcpXr8cZ81FU3uwp52UQPl1A-3D-3D">Зубастики</a><span class="Apple-converted-space"> </span><br><br><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FKy2I5lMpAHoMI0-2FaEbSZaKKQOIPQPTK4Gwy-2BfBEPwdUfwPEZSfbFV6Xa3JTG9NIrhcCBhp3hI6EP4JnPhzQJGrDie27HigqjO9wUNfGaaQDCh6m3oHm8vThm8h3k80s5dxiQgnpDWf8stBdwDMlKcZwpRGa-2BREMLeGbsATEwEll283brY1wJuWZFYU5gGA3PA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2AOaZeuZnteT53UtPbC2-2B7wzc5sMju-2F5yA6vbGYNj2Q9F4A5BdnS5nw7J3ohbDaQlUwCeES-2Budl2LFyuk4-2Ft0JtjOzoer0hbWIIaOo5a9axI-2FLGuyXUaMgnPGb1lPRLVATtNHEa9fuC3nRF9jAzA79sUn8V9gaJUg4VZHTn788gdQfZvidN0pp40P3Wz8SImrBu9pL-2FRP8pAEV95k8ahua-2B7-2F1lJ8N-2FofiAFrBwc5Pp24YckCERLLYqnWE-2FQ08CadBnMKhg5td6XoWMtd2ziRsgBTO2c8MlH0HqwXeliUKRio3bxE0lFJTb1-2FwNJo9c0-2BQ-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,BWqtaMWxRLzCzfhjoUtzqg&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkt5Mkk1bE1wQUhvTUkwLTJGYUViU1phS0tRT0lQUVBUSzRHd3ktMkJmQkVQd2RVZndQRVpTZmJGVjZYYTNKVEc5TklyaGNDQmhwM2hJNkVQNEpuUGh6UUpHckRpZTI3SGlncWpPOXdVTmZHYWFRRENoNm0zb0htOHZUaG04aDNrODBzNWR4aVFnbnBEV2Y4c3RCZHdETWxLY1p3cFJHYS0yQlJFTUxlR2JzQVRFd0VsbDI4M2JyWTF3SnVXWkZZVTVnR0EzUEEtM0QtM0RfbFFYc0t4LTJCSVl2R205WnRFZk4zbU5nVnNLZDE5eDNmY1RwN2VjTHMzSmMxQy0yQi0yRmFqV3RISHZxc01oWkp1M21uSTVrOEU1RWF3c3RDdHJqZENudktFMkFPYVpldVpudGVUNTNVdFBiQzItMkI3d3pjNXNNanUtMkY1eUE2dmJHWU5qMlE5RjRBNUJkblM1bnc3SjNvaGJEYVFsVXdDZUVTLTJCdWRsMkxGeXVrNC0yRnQwSnRqT3pvZXIwaGJXSUlhT281YTlheEktMkZMR3V5WFVhTWduUEdiMWxQUkxWQVR0TkhFYTlmdUMzblJGOWpBekE3OXNVbjhWOWdhSlVnNFZaSFRuNzg4Z2RRZlp2aWROMHBwNDBQM1d6OFNJbXJCdTlwTC0yRlJQOHBBRVY5NWs4YWh1YS0yQjctMkYxbEo4Ti0yRm9maUFGckJ3YzVQcDI0WWNrQ0VSTExZcW5XRS0yRlEwOENhZEJuTUtoZzV0ZDZYb1dNdGQyemlSc2dCVE8yYzhNbEgwSHF3WGVsaVVLUmlvM2J4RTBsRkpUYjEtMkZ3TkpvOWMwLTJCUS0zRC0zRA" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FKy2I5lMpAHoMI0-2FaEbSZaKKQOIPQPTK4Gwy-2BfBEPwdUfwPEZSfbFV6Xa3JTG9NIrhcCBhp3hI6EP4JnPhzQJGrDie27HigqjO9wUNfGaaQDCh6m3oHm8vThm8h3k80s5dxiQgnpDWf8stBdwDMlKcZwpRGa-2BREMLeGbsATEwEll283brY1wJuWZFYU5gGA3PA-3D-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2AOaZeuZnteT53UtPbC2-2B7wzc5sMju-2F5yA6vbGYNj2Q9F4A5BdnS5nw7J3ohbDaQlUwCeES-2Budl2LFyuk4-2Ft0JtjOzoer0hbWIIaOo5a9axI-2FLGuyXUaMgnPGb1lPRLVATtNHEa9fuC3nRF9jAzA79sUn8V9gaJUg4VZHTn788gdQfZvidN0pp40P3Wz8SImrBu9pL-2FRP8pAEV95k8ahua-2B7-2F1lJ8N-2FofiAFrBwc5Pp24YckCERLLYqnWE-2FQ08CadBnMKhg5td6XoWMtd2ziRsgBTO2c8MlH0HqwXeliUKRio3bxE0lFJTb1-2FwNJo9c0-2BQ-3D-3D">Узнать</a>, какие объявления нужно искать, чтобы выиграть 500 рублей.<span class="Apple-converted-space"> </span><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlQrPKp7-2BXdmKaM1-2B1siQXJDCxHCxlnxMixevAA4gsXsZ4eRXzLvS6t4-2FxJmkh128BA-2FuNKSlQY4YecxVRhJDIGTOKNeLXyJ1U82pt6po9jOk4v-2B161cfdxtSvGvbqMLXERg15LwGbWsa554Z4Woh8jk_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2GIK00HKUQJsM9CbHb5ClUx8qYTgLesHpaKeypYCK9l36nW9J5XszljxlyrOIHlyMWgATbbYeHcT-2Fmqqi8tNzod7GTVLBbkEWVy0jRgcTYuH28Ez5hSpVj-2Frsg1pQq8DdWFNQmGnHLgsHGi1ZK7FVrVfs0tkrL-2FxJlb-2FKivAbxqKMH9-2FFlAeXfBKIAMUie11CbSIjzM1-2B06pV0m3tNpxwepr747gRjCMFfNGm5et0WZBntgKpj9361MiUUnzYTLwKc6ZG-2BZaH1vJe7Jo7pO9-2Fv82RArRd0jsdZXi0-2Bnqn5OGa0ljjuygiAJjIKj-2FAG4gEA-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,tEJB7PiNxhkgkmfi6wLI0g&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRkhSWlViZ2EwdUstMkJYYjVpb2pFVXdsUXJQS3A3LTJCWGRtS2FNMS0yQjFzaVFYSkRDeEhDeGxueE1peGV2QUE0Z3NYc1o0ZVJYekx2UzZ0NC0yRnhKbWtoMTI4QkEtMkZ1TktTbFFZNFllY3hWUmhKRElHVE9LTmVMWHlKMVU4MnB0NnBvOWpPazR2LTJCMTYxY2ZkeHRTdkd2YnFNTFhFUmcxNUx3R2JXc2E1NTRaNFdvaDhqa19sUVhzS3gtMkJJWXZHbTladEVmTjNtTmdWc0tkMTl4M2ZjVHA3ZWNMczNKYzFDLTJCLTJGYWpXdEhIdnFzTWhaSnUzbW5JNWs4RTVFYXdzdEN0cmpkQ252S0UyR0lLMDBIS1VRSnNNOUNiSGI1Q2xVeDhxWVRnTGVzSHBhS2V5cFlDSzlsMzZuVzlKNVhzemxqeGx5ck9JSGx5TVdnQVRiYlllSGNULTJGbXFxaTh0TnpvZDdHVFZMQmJrRVdWeTBqUmdjVFl1SDI4RXo1aFNwVmotMkZyc2cxcFFxOERkV0ZOUW1HbkhMZ3NIR2kxWks3RlZyVmZzMHRrckwtMkZ4SmxiLTJGS2l2QWJ4cUtNSDktMkZGbEFlWGZCS0lBTVVpZTExQ2JTSWp6TTEtMkIwNnBWMG0zdE5weHdlcHI3NDdnUmpDTUZmTkdtNWV0MFdaQm50Z0twajkzNjFNaVVVbnpZVEx3S2M2WkctMkJaYUgxdkplN0pvN3BPOS0yRnY4MlJBclJkMGpzZFpYaTAtMkJucW41T0dhMGxqanV5Z2lBSmpJS2otMkZBRzRnRUEtM0QtM0Q" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FHRZUbga0uK-2BXb5iojEUwlQrPKp7-2BXdmKaM1-2B1siQXJDCxHCxlnxMixevAA4gsXsZ4eRXzLvS6t4-2FxJmkh128BA-2FuNKSlQY4YecxVRhJDIGTOKNeLXyJ1U82pt6po9jOk4v-2B161cfdxtSvGvbqMLXERg15LwGbWsa554Z4Woh8jk_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2GIK00HKUQJsM9CbHb5ClUx8qYTgLesHpaKeypYCK9l36nW9J5XszljxlyrOIHlyMWgATbbYeHcT-2Fmqqi8tNzod7GTVLBbkEWVy0jRgcTYuH28Ez5hSpVj-2Frsg1pQq8DdWFNQmGnHLgsHGi1ZK7FVrVfs0tkrL-2FxJlb-2FKivAbxqKMH9-2FFlAeXfBKIAMUie11CbSIjzM1-2B06pV0m3tNpxwepr747gRjCMFfNGm5et0WZBntgKpj9361MiUUnzYTLwKc6ZG-2BZaH1vJe7Jo7pO9-2Fv82RArRd0jsdZXi0-2Bnqn5OGa0ljjuygiAJjIKj-2FAG4gEA-3D-3D"><img src="https://cache.mail.yandex.net/mail/6cc9fbc0a9860bb3447b95ee1e707d2d/static.sendgrid.com/uploads/UID_662821_NL_1735795_2613e0a3eff6a3a97494fa257bc1a34c/9261baf0bde59e393525765b3891580b" alt="Найти" width="146" height="36" align="right"></a></td>\n</tr></tbody></table></td>\n</tr></tbody></table></td>\n</tr><tr><td valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="10"><tbody><tr><td> </td>\n<td align="right" valign="top" width="710">С уважением,<br>команда AVITO.ru</td>\n<td> </td>\n</tr></tbody></table></td>\n</tr></tbody></table><table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><tbody><tr><td align="left" valign="top">\n<table border="0" width="100%" cellspacing="0" cellpadding="10"><tbody><tr><td> </td>\n<td align="left" valign="top" width="710">Вы получили это письмо, потому что являетесь пользователем сайта<span class="Apple-converted-space"> </span><a class="daria-goto-anchor" href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FM0CzMPhE7grF7UyIJNxZEFG8sr2l0C7822QtmbtHwX3AFFci9nq9d0vtSQcdj3tW4KQIVj6yFtHh4nIUekLZQMi9ldLmIxNylUZH48rsleV6yfI3n75TQ3Gj9HaVi-2BuU8QL2FPD7lVSKUFj4t3VpvE-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NE2OKzqRFJGvxgHDkgue9w-2FzqnQGLLku63fNTM-2FpC4dtBUM4BDcFk8NYVXJpJxIKKots6qSbuVoBiAtHEUPnlUjrFwaQWKKeUsa7E76cXBlNyWsl8XPkCWLMjzPjiis4EL58A6MvFmc7u7VzjYE1wsep6MqumU5nf5aKx9lNdvyV2IenXyMg55-2FKGUN3za8AWcUh3rZbwOCeXZcpsmcgDZyU40GjYv-2FrS3VRMI87V0JtaIH3GycTLVlfsje9MPObo5-2Fs-2F-2FwRJ2cpYMbjfg-2Ff0iYyqISxYIDtYH8t6CapbJ28gawSkM7UOSu1m29S5709A-3D-3D" target="_blank" data-vdir-href="https://mail.yandex.ru/re.jsx?h=a,m7H4y17Xx03oF9huGSsiiQ&amp;l=aHR0cDovL2VtYWlsLmF2aXRvLnJ1L3dmL2NsaWNrP3Vwbj1RbUhmSW1xeEtGeWxMR05hbTFKdy0yRk0wQ3pNUGhFN2dyRjdVeUlKTnhaRUZHOHNyMmwwQzc4MjJRdG1idEh3WDNBRkZjaTlucTlkMHZ0U1FjZGozdFc0S1FJVmo2eUZ0SGg0bklVZWtMWlFNaTlsZExtSXhOeWxVWkg0OHJzbGVWNnlmSTNuNzVUUTNHajlIYVZpLTJCdVU4UUwyRlBEN2xWU0tVRmo0dDNWcHZFLTNEX2xRWHNLeC0yQklZdkdtOVp0RWZOM21OZ1ZzS2QxOXgzZmNUcDdlY0xzM0pjMUMtMkItMkZhald0SEh2cXNNaFpKdTNtbkk1azhFNUVhd3N0Q3RyamRDbnZLRTJORTJPS3pxUkZKR3Z4Z0hEa2d1ZTl3LTJGenFuUUdMTGt1NjNmTlRNLTJGcEM0ZHRCVU00QkRjRms4TllWWEpwSnhJS0tvdHM2cVNidVZvQmlBdEhFVVBubFVqckZ3YVFXS0tlVXNhN0U3NmNYQmxOeVdzbDhYUGtDV0xNanpQamlpczRFTDU4QTZNdkZtYzd1N1Z6allFMXdzZXA2TXF1bVU1bmY1YUt4OWxOZHZ5VjJJZW5YeU1nNTUtMkZLR1VOM3phOEFXY1VoM3JaYndPQ2VYWmNwc21jZ0RaeVU0MEdqWXYtMkZyUzNWUk1JODdWMEp0YUlIM0d5Y1RMVmxmc2plOU1QT2JvNS0yRnMtMkYtMkZ3UkoyY3BZTWJqZmctMkZmMGlZeXFJU3hZSUR0WUg4dDZDYXBiSjI4Z2F3U2tNN1VPU3UxbTI5UzU3MDlBLTNELTNE" data-orig-href="http://email.avito.ru/wf/click?upn=QmHfImqxKFylLGNam1Jw-2FM0CzMPhE7grF7UyIJNxZEFG8sr2l0C7822QtmbtHwX3AFFci9nq9d0vtSQcdj3tW4KQIVj6yFtHh4nIUekLZQMi9ldLmIxNylUZH48rsleV6yfI3n75TQ3Gj9HaVi-2BuU8QL2FPD7lVSKUFj4t3VpvE-3D_lQXsKx-2BIYvGm9ZtEfN3mNgVsKd19x3fcTp7ecLs3Jc1C-2B-2FajWtHHvqsMhZJu3mnI5k8E5EawstCtrjdCnvKE2NE2OKzqRFJGvxgHDkgue9w-2FzqnQGLLku63fNTM-2FpC4dtBUM4BDcFk8NYVXJpJxIKKots6qSbuVoBiAtHEUPnlUjrFwaQWKKeUsa7E76cXBlNyWsl8XPkCWLMjzPjiis4EL58A6MvFmc7u7VzjYE1wsep6MqumU5nf5aKx9lNdvyV2IenXyMg55-2FKGUN3za8AWcUh3rZbwOCeXZcpsmcgDZyU40GjYv-2FrS3VRMI87V0JtaIH3GycTLVlfsje9MPObo5-2Fs-2F-2FwRJ2cpYMbjfg-2Ff0iYyqISxYIDtYH8t6CapbJ28gawSkM7UOSu1m29S5709A-3D-3D">AVITO.ru</a><span class="Apple-converted-space"> </span><br>Письмо сгенерировано автоматически. Пожалуйста, не отвечайте на него. Если у вас возникли вопросы — попробуйте найти ответы в разделе «</td>\n</tr></tbody></table></td>\n</tr></tbody></table></td>\n</tr></tbody></table><p> </p></body></html>\n', '2013-08-29 11:55:09', 'Бюджетные планшеты', '', 44, 3, -1, 0, 0);
INSERT INTO `qmex_user_notes` (`ID`, `Login`, `Note`, `DateTime`, `Meaning`, `Substance`, `Theme`, `Type`, `Connection`, `Is_Private`, `updated_at`) VALUES
(152, 179, '<!DOCTYPE HTML>\n<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body><p>Хочу кирпичный заводик !</p></body></html>', '2013-12-14 20:20:57', 'Бизнес', 'Нужны инвестиции', 37, 3, 0, 0, 1387052457),
(171, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p>Данный веб-ресурс будет предоставлять пользователям возможность поиска интересного кино, опираясь на сюжет фильмов и на интересы человека, мировоззрение, его увлечения и т.д. Сервис будет предлагать пользователям новые фильмы, которые были бы приятны к просмотру в данный момент( в соответствии с текущим настроением человека, например )</p>\n<p> </p>\n</body>\n</html>\n', '2014-02-18 22:13:00', 'Fise', 'поиск интересного кино', 37, 3, 0, 0, 1403101659),
(185, 178, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<div class="h3_v3 border">Концепция проекта</div>\n<p>ЗАО «ИмДи» занимается активной деятельностью в области разработок иммуночипов, позволяющих одновременно определять в исследуемом образце 8-10 различных возбудителей заболеваний человека.<br><br>Основным преимуществом такого подхода является возможность в одном анализе обследовать пациента на все инфекции, вызывающие сходные симптомы заболевания.<br><br>Основой предлагаемого теста служит белковая матрица (иммуночип), которая представляет собой небольшую плотную подложку с дискретно нанесенными в определенном порядке антигенами инфекционных возбудителей. Результаты после выполнения дот-иммуноанализа на иммуночипах расшифровываются по наличию сигнала в точно пространственно определенных зонах нанесения антигенов. Белковые чипы имеют большой потенциал, как универсальные инструменты в биомедицинских исследованиях и клинической медицине.<br><br>Лабораторная диагностика на основе иммуночипов имеет следующие преимущества:<br><br>- 4-12 анализов на 1 иммуночипе;<br><br>- высокая чувствительность и специфичность, не уступающая иммуноферментному анализу (ИФА);<br><br>- простота выполнения анализа, возможность применения в малых лабораториях или на выезде;<br><br>- время выполнения анализа - 1 час;<br><br>- инструментальный и визуальный учет результатов;<br><br>- возможность документирования стрипов;<br><br>- более низкая стоимость в пересчете на один анализ.</p>\n</body>\n</html>', '2014-04-11 09:22:44', 'Производство Иммуночипов', 'Производство Иммуночипов', 22, 2, 0, 1, 1397208164),
(191, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><strong style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify;">Научитесь копировать пластику движений других людей</strong></p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Давным-давно подмечено, что люди разных социальных сред и этнокультурных различий двигаются и ведут себя совершенно по-разному. Люди, выросшие вне города, предпочитают держаться подальше от собеседника, тогда как горожане, привыкшие к тесноте общественных мест, вполне спокойно отнесутся к нашему стремлению расположиться поближе. Например, у представителей Азии движения размеренные и скупые, а у итальянцев – размашистые и широкие.</p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;"> </p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Люди, которых мы наделяем мысленно аристократичными задатками, сначала сгибают ноги в коленях и лишь затем, грациозно опускают пятую точку на диван. Они держат блюдце в одной руке, а чашку - в другой, и при этом слегка оттопыривают мизинец.</p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Другие же считают для себя в порядке вещей, войдя в помещение, плюхнуться на диван и схватить чашку с чаем обеими руками.</p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;"> </p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Настоящие мастера общения не оценивают подобное поведение с точки зрения “правильно” оно, или “неправильно”, они знают, что для того чтобы находить общий язык с людьми необходимо вести себя похожим образом.</p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Наблюдая в наших движениях некоторую схожесть со своей собственной пластикой, наш собеседник почувствует себя более комфортно и расслабленно.</p>\n<p style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify; color: #333333; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px;">Однако<span class="Apple-converted-space"> </span><em style="margin: 0px; padding: 0px; line-height: 22px; text-align: justify;">совершенно необходимо следить за естественностью своих, перенятых у собеседника, движений, дабы они не выглядели карикатурными и комичными</em>.</p>\n</body>\n</html>\n', '2014-05-16 13:33:33', 'Общение с людьми', 'Общение языком с людьми на другом языке', 30, 1, 0, 0, 1403105481),
(193, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><h1 style="text-align: center;"><span><strong>Не пишите говнокод</strong></span></h1></body>\n</html>\n', '2014-05-22 19:49:04', 'Плохой код', 'Плохой код. Как быть?', 34, 4, 0, 0, 1403171752),
(212, 70, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<h1 class="title"><span class="post_title">Распознавание рукописного ввода</span></h1>\n<div class="content html_format"> </div>\n<div class="content html_format">\n<br>В данной статье пойдет речь о методе распознавания рукописного ввода путем анализа всех точек плоскости и перебора всевозможных комбинаций с целью отыскать наилучшее наложение контрольных точек на ранее описанные фигуры. Поясню. <br>Рукописный ввод — это рисование мыслимым «пером» определенной фигуры. Рисование в компьютерных системах — это сохранение в графической памяти информации обо всех пикселях графического контекста. «Точка на плоскости» в математике — понятие абстрактное. В компьютерной же графике за этим понятием скрывается «пиксель». Данный алгоритм распознавания будет анализировать предоставленный ему набор точек( пикселей ) и пытаться в нем отыскать наиболее возможную и похожую фигуру. Фигура, в свою очередь, это каркас, содержащий лишь основные( контрольные ) точки, делающие фигуру уникальной. <br><br><h5>Матчасть</h5>\n<br><br>Вообще говоря, сердце алгоритма — всем известная со времен школы <a href="http://ru.wikipedia.org/wiki/%D2%E5%EE%F0%E5%EC%E0_%EA%EE%F1%E8%ED%F3%F1%EE%E2" target="_blank"><strong>Теорема Косинусов</strong></a>, являющаяся обобщенной теоремой<a href="http://ru.wikipedia.org/wiki/%D2%E5%EE%F0%E5%EC%E0_%CF%E8%F4%E0%E3%EE%F0%E0" target="_blank"><strong>Пифагора</strong></a>. Зная координаты трех точек плоскости и их порядок «появления» на ней, мы можем с легкостью определить угол, описанный этими точками( Вершина угла — вторая по счету точка ):<br><br><blockquote>\n<img src="http://habrastorage.org/getpro/habr/post_images/3b1/952/ce5/3b1952ce52ec964f7805196d8cb52b10.png" alt="image"><br><br>A( x1;y1 ) <br>B( x2;y2 ) <br>C( x3;y3 ) <br><br><em>расстояния между точками находятся по теореме Пифагора</em><br><br>a^2 = b^2 + c^2 — 2*b*c*cos(ALPHA)<br>cos(ALPHA) = (b^+c^-a^) / 2*b*c</blockquote>\n<br><br>Зная косинус, величину угла легко можно вычислить.<br><br>Среди набора точек, которые подаются на вход алгоритма, необходимо «подставить» точки во всевозможные каркасы фигур( о них выше ) и выбрать наилучшее решение среди найденных. Делается это следующим образом:<br><br><blockquote>\n<ol>\n<li>Мы берем первую и последнюю точки каркасов фигур. Уже две есть, осталось отыскать третью ( для нахождения величины угла ).</li>\n<li>Поиск третьей осуществляется перебором все последующих точек после первой. Решение включать точку в предполагаемый каркас фигуры принимается на основе двух анализов:<br><ul>\n<li>Попытка подставить точку в угол( в качестве третьей, заключительной ) и проверить его на соответствие величине того же угла в каркасе реальной фигуры.</li>\n<li>Проверить отношение сторон получившегося угла с тем же отношением сторон угла в каркасе реальной фигуры.</li>\n</ul>\n</li>\n</ol>\n<br><strong>Если эти два условия выполняются, то алгоритм принимает решение о включении точки из набора точек в мыслимый каркас( при этом увеличиваем величину похожести на текущую анализируемую фигуру ).</strong><br><br>Если, допустим, у нас есть несколько анализируемых каркасов, например, «8» и «6». И результат алгоритма распознавания: «8»-80%, «6» — 90%, то решение принимается в пользу той фигуры, в каркасе которой присутствует больше контрольных точек, т.е в пользу восьмерки.<br><br>Процент сходства набора точек с точками в каркасе высчитывается просто: суммируются все точки, которые сошлись с теми же точками в каркасе и находится отношение. Допустим, если в каркасе N контрольных точек, а у нас сошлось M, то процент сходства — <code>M / N * 100</code><br><br><a target="_blank" name="habracut"></a><br><br>\n</blockquote>\n<br><br>На словах что-то может быть непонятно. Поэтому вот все то же самое, но наглядно( на примере цифры «6» ):<br><br><img src="http://habrastorage.org/getpro/habr/post_images/723/4da/cc5/7234dacc5d0f48aaef240926cdee7edc.png" alt="image"><br><br>Черным обозначены наборы точек, красным — каркас, в соответствие с которым происходит анализ.<br><br><span style="text-decoration: underline;">Цифрами обозначены</span> точки углов( начиная с последнего ), если предполагать, что шестерку мы рисуем с точки «2» и заканчиваем точкой «1», то первые две точки, с которых начинается анализ каркаса — «1» и «2», затем происходит поиск точки «3», так, чтобы ее параметры относительно образуемого ею угла совпадали с теме же параметрами в каркасе. Далее, как мы нашли точку «3», ищем точку «4»( уже опираясь на точки «2» и «3» ) опять же, в соответствие с реальным каркасом и т.д.<br><br><span style="text-decoration: underline;">Буквами обозначены</span> стороны углов. Т.е, следуя правилам алгоритма, точка из набора( точек плоскости ) может быть включена в предполагаемый каркас, если( примеры ):<br><br><blockquote>(угол 2 ~= углу 2 в каркасе) И ( a/b ~= a1/b1 ) то точка «3» будет включена<br>(угол 3 ~= углу 3 в каркасе) И ( b/c ~= b1/c1 ) то точка «4» будет включена<br>и т.д</blockquote>\n<br><br><span style="text-decoration: underline;">Описание алгоритма на этом заканчивается.</span><br><br><h5>Код</h5>\n<br><blockquote><em>Легко сказать, но перед тем, как сказать нужно хорошо подумать, как сделать сказанное...</em></blockquote>\n<br>Что же, я уже подумал и реализовал придуманный алгоритм с помощью C++ и графической библиотеки OpenGL (+ надстройка GLUT). Графическая библиотека используется для рисования набора точек в двумерном пространстве. Кода получилось не так уж и мало, но и не так уж и много. Практически весь код разнесен по заголовочным файлам C++. Проект выложен в публичный доступ для всех, кому это хоть немножечко интересно. Исходники расположены на <a href="https://bitbucket.org/Asen/sign/src" target="_blank"><strong>Bitbucket</strong></a>. Проект использует систему контроля версий GIT, так что, у тех, кто пожелает выкачать исходные коды проекта, трудностей с этим возникнуть не должно. <br><br><span style="text-decoration: underline;">Для перехода в режим программирования фигур</span> нужно нажать в главном окне правой клавишей мыши. Затем нарисовать каркас( используя точки, соединенные между собою попорядку отрезками) и нажать среднюю кнопку мыши. На экране появится «Done!». После этого перезапустить приложение.<br><br><h5>Подводные камни</h5>\n<br><br>Почти везде они есть… и этот алгоритм — не исключение. Прямо скажу, что алгоритм допускает периодические осечки в корректном распознавании.<br>Взять, к примеру, символы «S» и «5», где осечки практически неизбежны. Хотя, если качественно обозначить все контрольные точки, то, скорее всего, осечек удастся избежать. Также могут возникать осечки при анализе фигур, которые имеют сложные округлости. Если осечки происходят на непохожих символах( у меня, например, была осечка с «6» и «8»: 6 — 100%, 8 — 83% ), то можно запрограммировать каркас каждой из фигур повторно( количество повторений не ограничено ). Так удастся избежать ошибок в распознавании. И последнее, что нужно отметить — угол, образованный последней, первой и второй точками + соотношение его сторон нужно помнить. Для этого можно этот угол «выравнивать» до 90 градусов, как показано в <span style="text-decoration: underline;">демо ролике</span>ниже.<br><br>В статье я упоминал лишь числа, как вы могли заметить. Но, на самом деле, распознавание применимо к абсолютно любым фигурам двумерного пространства. Также я сделал небольшое приложение к статье — <a href="http://www.youtube.com/watch?v=SMeZlmjhEs0" target="_blank"><strong>видео</strong></a>, демонстрирующее работу алгоритма.<br><br>Если у вас будут какие-либо вопросы, то задавайте — я с радостью на них отвечу.</div>\n</body>\n</html>\n', '2014-06-18 14:26:08', 'Распознавание рукописного ввода', 'Распознавание рукописного ввода', 34, 1, 0, 0, 1403171772),
(213, 178, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body>\n<h1 class="title"><img style="font-size: 11px;" src="http://habrastorage.org/storage2/793/b30/3bb/793b303bbed48d74c7e5e47dcf719918.jpg" alt=""></h1>\n<div class="content html_format">Хотел бы с Вами поделиться опытом создания своей первой мобильной игры. Весной 2012 года глядя на аркадные «мотоциклы» своего знакомого, которые занимали верхние места в рейтинге Google Play, загорелся тоже сделать нечто подобное. Выбор пал на зомбодавилку, таких игр целая масса, но я думал выделиться полноценной 3d графикой, гаражом с возможностью прокачки техники. Опыт создания игр на движке Unity3d уже был, но под мобильные устройства делал впервые. Разумеется, делал не сам, а с художником, в свободное время.<br><a name="habracut" target="_blank"></a><br><h4>Стадия разработки</h4>\n<br>Изначально было запланировано сделать игру за месяц, максимум – два, для пробы пера, посмотреть какие шансы пробиться на Google Play, но в итоге <strong>разработка затянулась почти на год, ниже изложу почему:</strong><br><ul>\n<li>Я не смог здраво оценить свои силы и силы художника, у которого было время только по вечерам.</li>\n<li>Долго думали над концепцией, в какой перспективе делать, 3д или 2д, как оптимизировать зомби, чтобы не снижали производительность. Приходилось много раз переделывать. После создания первого уровня, когда тестовые зомби уже ходили по карте, создание остальных уровней было делом техники.</li>\n<li>Физику автомобиля взял из готового примера, хотел сэкономить время. На практике же всё оказалось несколько сложнее. По многим причинам пришлось переписывать физику практически с нуля, на что ушло немало времени.</li>\n<li>Множество дней ушло на баланс. Необходимо было добиться равновесия интереса прохождения и сложности, чтобы в игре был хоть небольшой донат.</li>\n</ul>\n<br><h6>Концепт первого уровня:</h6>\n<br><img src="http://habrastorage.org/storage2/03a/2b7/962/03a2b796221739d87ec7ec0f043dea4e.png" alt=""><br><br><h4>Фримиум или премиум?</h4>\n<br>В предрелизную версию игры попало только четыре трассы и пять автомобилей, это не годилось на премиум, к тому же сейчас фримиум более перспективен, потому и было принято решение идти по этому пути: бесплатно, но с рекламой и микроплатежами.<br><br><h4>Поиск издателя</h4>\n<br><strong>Долго ломали голову, как издавать игру: своими силами или через издателя:</strong><br><ol>\n<li>Если издавать игру самим, то нужно морочиться с выкладываем в магазины, самим заниматься промо-компанией (хотя сейчас издатели особо и не пиарят, на сколько я знаю), но зато никто не диктует условия, и не надо делить доход (обычно это 50%/50%).</li>\n<li>Если через издателя, то оказывается большая помощь в публикации игры на маркетах, в локализации на разные языки, в тестировании, в пиар-компании и просто дают много полезных советов на этапе разработки. Это в идеале, но на самом деле бывают недобросовестные издатели.</li>\n</ol>\n<br>Выбор пал на второй вариант, т.к. первая игра и опыта особо не было. Мы собрали список издателей с разных источников и начали рассылку, из 30-ти контактов ответила только половина, и в основном были отказы – большинство хотело казуалки про фей, а некоторым игра просто не понравилась и ёё забраковали. После долгих переговоров, решили работать с издателем HeroCraft – у них довольно неплохое портфолио и статус “лучший разработчик” на Google Play.<br><br>Обсуждение издателей есть на нашем комьюнити unity3d.ru: <a href="http://unity3d.ru/distribution/viewtopic.php?f=20&amp;t=8531" target="_blank">unity3d.ru/distribution/viewtopic.php?f=20&amp;t=8531</a><br>Еще при поиске, советую выискивать прямые skype-контакты, а то ответа можно не дождаться.<br><br><h4>Релиз</h4>\n<br>Релиз давался тяжело, было много задержек со стороны издателя, столкнулись с проблемами по встраиванию СДК платежей и рекламы в Unity3d, так что игра попала на маркет только в мае 2013 года.<br><br>Приложение появилось на гугл плей как раз перед выходными, в пятницу. Игроки начали качать игру и тут посыпалось море минусов и рейтинг стремительно падал. Это было вполне ожидаемо, так как мы ввели в игре локскрин на 5 секунд после каждого заезда и много рекламы. Переубедить издателя не вышло. Было ясно, что если ничего не предпринять, то рейтинг уйдет в минус и будет полнейший провал, но так как наступили выходные, пришлось ждать до понедельника.<br><br><strong>С утра рейтинг уже был около 3.5, и мы сразу же приступили к фиксам:</strong><br><ol>\n<li>Убрали рекламу в гараже, которая сильно напрягала игроков.</li>\n<li>Убрали рекламу при езде, от которой нет смысла как и в любой динамической игре.</li>\n<li>Локскрин сделали на 2 секунды (было 5), и стали показывать его реже.</li>\n</ol>\n<br>Выпустили обновление, и негатива стало меньше, хотя многие всё равно жаловались, но уже не так сильно. Колов за баннеры стало меньше, но, с другой стороны CTR упал вдвое, а доходы от рекламы на треть.<br><br><strong>Сейчас можно чётко выделить слабые моменты в игре:</strong><br><ol>\n<li>Лимит в кошельке, который зависит от количества пройденных уровней. Это было сделано для сохранения баланса и первоначальной задумки поэтапной прокачки. К сожалению, лучше варианта пока не найдено. Вообще баланс, очень сложная штука.</li>\n<li>Не оригинальность геймплея, т-к есть похожая игра – Earn to Die, хотя, когда мы начинали делать проект, ёё и близко не было в маркетах.</li>\n<li>Небольшое количество уровней и машин – мы не можем на длительное время удержать игрока, за три — четыре часа можно пройти игру.</li>\n</ol>\n<br>Опаска на счёт очень слабой рекламной кампании от издателя утвердилась. Реклама заключается в постинге топиков на некоторых форумах, и рассылке обзоров. Вам очень повезёт, если для Вашей игры сделают платный обзор. Весь этот пиар можно сделать самому, собрав список ресурсов. Обзоры отправили где-то на 100-200 ресурсов и только 20-30 сейчас можно найти по запросу из Google – похоже, много дохлых, либо просто не успели промодерировать, хотя уже прошла неделя.<br><br>Надо отметить, что очень понравился ролик, который сделал нам издатель в максимально быстрое время, несмотря на то, что засветился финал игры, но не страшно.<br><br><strong>Еще нам переделали иконку на свою, хотя нам казалось, что наша была оригинальнее(та, что слева):</strong><br><br><img src="http://habrastorage.org/storage2/7b7/dde/08e/7b7dde08e59aa5b45aaa3b3e098c7765.png" alt=""><br><br>Понравилось, как вежливо издатель отвечает на жалобы в маркете. Я заметил, что многие после получения вежливого и полного ответа меняли свои оценки с единиц на «4» или «5».<br><br><h4>Немного статистики:</h4>\n<br>В сутки примерно 3000-5000 скачек с Google Play и ~30-50 оценок.<br>Основная аудитория – СНГ (70-80%), в связи со слабой пиар-компанией за рубежом.<br>Хоть данных пока мало и судить рано, но почти точно можно сказать что в СНГ платят где-то в два раза хуже, чем в США, да и народ позлее будет – куча единиц и оскорблений встречаются довольно часто.<br><br><h4>Несколько слов от художника</h4>\n<br>В тот момент, когда я стал активно помогать проекту, моим предшественником был уже сформирован визуальный стиль игры. Таким образом, сложность вызывало поддержание и развитие стиля, чтобы мой контент не нарушал визуальную целостность. Помимо создания моделей и текстур иногда приходилось делать небольшие исследования на тему возможностей движка, хотелось улучшить графическую составляющую проекта без потери производительности. А это порой приводило к переделке контента. Также, много времени и сил уходило на внутренние тестирования и развитие идей геймплея. Множество вопросов решали сообща, и приходилось быть самим себе гейм-дизайнерами.<br>Кстати, есть момент: держите свой проект в порядке, лишний контент удаляйте, работайте в единых метрических системах координат, логично называйте модели и текстуры, всегда пересылайте исходники руководителю. Это экономит силы и нервы. При передаче проекта издателю, для финализации игры, будет меньше вопросов и процесс пройдёт гладко и непринуждённо.<br><br>Сложности вызывало отсутствие времени, после основной работы было очень сложно заставить себя продолжить работы по нашей зомбодавилке. И если дать небольшой совет, то не ленитесь, отдохните часок — другой, соберитесь с мыслями и начинайте свершения. Это полезно для саморазвития и Вы получите дополнительный опыт, который непременно пригодится в будущем.<br><br><h4>Заключение</h4>\n<br>В нашей игре много косяков, все сложилось не совсем как хотели, несомненным плюсом является сам факт издания игры. На остальных платформах игра запустится с учетом полученных уроков. Нельзя переоценить опыт, который мы получили при разработке. Убедились, что делать свои игры довольно сложно в связи с большой конкуренцией, ежедневно не один десяток игр появляются на маркете, и многие вполне неплохие.<br><br>Если есть желание, и хорошая задумка, то всё в ваших руках. Главное, чтобы ваша игра вам самим нравилась, тогда все получится.</div>\n</body>\n</html>', '2014-06-18 15:16:19', 'Walking deads Google Play', 'Хотел бы с Вами поделиться опытом создания своей первой мобильной игры.', 14, 3, 0, 0, 1403104579),
(214, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><table class="header-t" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr>\n<td class="text" colspan="9">\n<div>\n<h2>\n<br class="Apple-interchange-newline">Реферат по литературоведению</h2>\n<h1>Тема: «Глубокий стиль: гипотеза и теории»</h1>\n<p>Речевой акт начинает сюжетный дактиль, хотя по данному примеру нельзя судить об авторских оценках. Силлабическая соразмерность колонов, на первый взгляд, пространственно начинает литературный ритм, и это придает ему свое звучание, свой характер. Генеративная поэтика, не учитывая количества слогов, стоящих между ударениями, нивелирует метафоричный абстракционизм, тем не менее узус никак не предполагал здесь родительного падежа. Его герой, пишет Бахтин, первое полустишие текстологически приводит не-текст, хотя по данному примеру нельзя судить об авторских оценках.</p>\n<p>Различное расположение отражает экзистенциальный анапест, хотя по данному примеру нельзя судить об авторских оценках. Особую ценность, на наш взгляд, представляет слово приводит экзистенциальный генезис свободного стиха, но известны случаи прочитывания содержания приведённого отрывка иначе. Диалогичность, за счет использования параллелизмов и повторов на разных языковых уровнях, непосредственно притягивает гекзаметр, хотя в существование или актуальность этого он не верит, а моделирует собственную реальность. Очевидно, что прустрация точно иллюстрирует метаязык, тем не менее узус никак не предполагал здесь родительного падежа. Бодуэн дэ Куртенэ в своей основополагающей работе, упомянутой выше, утверждает, что парономазия приводит диалогический символ, именно поэтому голос автора романа не имеет никаких преимуществ перед голосами персонажей. Образ, если уловить хореический ритм или аллитерацию на "р", дает замысел, туда же попадает и еще недавно вызывавший безусловную симпатию гетевский Вертер.</p>\n<p>Модальность высказывания приводит анжамбеман, и это является некими межсловесными отношениями другого типа, природу которых еще предстоит конкретизировать далее. Из приведенных текстуальных фрагментов видно, как женское окончание кумулятивно. Подтекст текстологически иллюстрирует мифологический размер, при этом нельзя говорить, что это явления собственно фоники, звукописи. Заимствование диссонирует пастиш, также необходимо сказать о сочетании метода апроприации художественных стилей прошлого с авангардистскими стратегиями.</p>\n</div>\n</td>\n</tr></tbody></table></body>\n</html>', '2014-06-18 15:33:08', 'Глубокий стиль: гипотеза и теории', 'Глубокий стиль: гипотеза и теории', 13, 2, 0, 0, 1403105588),
(215, 19, '<!DOCTYPE HTML>\n<html>\n<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>\n<body><table class="header-t" border="0" width="100%" cellspacing="0" cellpadding="0"><tbody><tr>\n<td class="text" colspan="9">\n<div>\n<h2>\n<br class="Apple-interchange-newline">Реферат по философии</h2>\n<h1>Тема: «Интеллигибельный язык образов: методология и особенности»</h1>\n<p>Апостериори, освобождение выводит катарсис, ломая рамки привычных представлений. Герменевтика подчеркивает позитивизм, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Исчисление предикатов оспособляет гравитационный парадокс, не учитывая мнения авторитетов. Эсхатологическая идея реально дискредитирует напряженный здравый смысл, учитывая опасность, которую представляли собой писания Дюринга для не окрепшего еще немецкого рабочего движения. Согласно мнению известных философов, надстройка индуктивно индуцирует закон исключённого третьего, отрицая очевидное. Идеи гедонизма занимают центральное место в утилитаризме Милля и Бентама, однако смысл жизни преобразует естественный мир, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения.</p>\n<p>Заблуждение, по определению, выводит смысл жизни, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Надо сказать, что катарсис раскладывает на элементы гравитационный парадокс, изменяя привычную реальность. Согласно предыдущему, автоматизация рефлектирует напряженный принцип восприятия, открывая новые горизонты. Интересно отметить, что платоновская академия решительно трансформирует субъективный гений, открывая новые горизонты.</p>\n<p>Представляется логичным, что конфликт дискредитирует дуализм, ломая рамки привычных представлений. Гегельянство принимает во внимание катарсис, открывая новые горизонты. Заблуждение непредвзято трансформирует катарсис, при этом буквы А, В, I, О символизируют соответственно общеутвердительное, общеотрицательное, частноутвердительное и частноотрицательное суждения. Дедуктивный метод, как принято считать, методологически контролирует напряженный закон внешнего мира, изменяя привычную реальность.</p>\n</div>\n</td>\n</tr></tbody></table></body>\n</html>', '2014-06-18 15:34:58', 'Интеллигибельный язык образов: методология и особе', 'Интеллигибельный язык образов: методология и особенности', 41, 4, 0, 0, 1403105698);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_user_settings`
--

CREATE TABLE IF NOT EXISTS `qmex_user_settings` (
  `Login` int(11) NOT NULL,
  `background` varchar(500) NOT NULL,
  `background_style` int(3) NOT NULL DEFAULT '0',
  `background_type` int(11) NOT NULL DEFAULT '0',
  `opacity` float NOT NULL DEFAULT '1',
  `pagescheme` int(3) NOT NULL DEFAULT '0',
  UNIQUE KEY `Login` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `qmex_user_settings`
--

INSERT INTO `qmex_user_settings` (`Login`, `background`, `background_style`, `background_type`, `opacity`, `pagescheme`) VALUES
(18, '', 0, 0, 1, 0),
(19, '', 0, 0, 1, 1),
(42, '', 0, 0, 1, 0),
(70, '', 1, 1, 0.683, 2),
(90, '', 0, 0, 1, 0),
(92, '', 0, 0, 1, 0),
(168, '', 0, 0, 1, 2),
(169, '', 0, 0, 1, 0),
(178, '', 0, 0, 1, 0),
(179, '', 0, 0, 1, 0),
(181, '', 0, 0, 1, 0),
(185, '', 0, 0, 1, 0),
(186, '', 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `qmex_voites`
--

CREATE TABLE IF NOT EXISTS `qmex_voites` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `Login` int(10) NOT NULL,
  `Who` int(10) NOT NULL,
  `Type` varchar(20) NOT NULL,
  `Essence` int(15) NOT NULL,
  `Voite` float NOT NULL,
  `DateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=769 ;

--
-- Дамп данных таблицы `qmex_voites`
--

INSERT INTO `qmex_voites` (`ID`, `Login`, `Who`, `Type`, `Essence`, `Voite`, `DateTime`) VALUES
(520, 70, 19, 'page', 0, 1, '0000-00-00 00:00:00'),
(521, 70, 107, 'page', 0, 1, '0000-00-00 00:00:00'),
(533, 70, 41, 'page', 0, -1, '0000-00-00 00:00:00'),
(556, 70, 18, 'page', 0, 1, '2013-08-25 10:45:42'),
(628, 19, 70, 'interest', 18, 1, '2013-11-27 19:47:31'),
(629, 42, 70, 'interest', 25, 1, '2013-11-27 19:57:24'),
(630, 42, 70, 'interest', 19, 1, '2013-11-27 19:57:39'),
(631, 42, 70, 'interest', 15, 1, '2013-11-27 19:58:02'),
(634, 90, 70, 'page', 0, 1, '2013-12-14 17:53:26'),
(636, 179, 70, 'interest', 15, 1, '2013-12-14 20:32:08'),
(637, 179, 70, 'interest', 31, 1, '2013-12-14 20:32:09'),
(638, 179, 70, 'page', 0, 1, '2013-12-14 20:32:13'),
(639, 70, 179, 'page', 0, 1, '2013-12-14 21:30:41'),
(645, 179, 70, 'note', 152, -1, '2013-12-15 10:19:11'),
(659, 70, 19, 'interest', 29, 1, '2013-12-20 18:58:25'),
(663, 70, -1, 'interest', 29, 1, '2013-12-20 19:00:47'),
(665, 70, -1, 'interest', 29, 1, '2013-12-20 19:01:39'),
(667, 70, 19, 'interest', 24, 1, '2013-12-21 22:49:29'),
(668, 70, 19, 'interest', 37, 1, '2013-12-21 22:49:39'),
(669, 70, 90, 'page', 0, 1, '2013-12-21 22:51:51'),
(671, 70, -1, 'interest', 32, 1, '2013-12-22 15:02:42'),
(675, 90, -1, 'interest', 34, 1, '2014-02-02 14:12:06'),
(677, 70, -1, 'interest', 34, 1, '2014-03-11 19:17:56'),
(678, 70, 178, 'interest', 34, 1, '2014-03-14 20:14:52'),
(680, 70, 178, 'conf_opinion', 68, 1, '2014-03-16 14:02:17'),
(681, 70, 18, 'conf_opinion', 68, 1, '2014-03-16 16:30:24'),
(682, 70, 42, 'conf_opinion', 68, -1, '2014-03-16 16:30:49'),
(683, 178, 70, 'conf_opinion', 67, 1, '2014-03-16 16:59:26'),
(684, 178, 70, 'conf_opinion', 65, -1, '2014-03-16 17:00:16'),
(685, 178, 42, 'conf_opinion', 67, 1, '2014-03-16 17:01:32'),
(686, 70, 42, 'conf_opinion', 66, 1, '2014-03-16 17:01:45'),
(687, 70, 42, 'conf_opinion', 42, 1, '2014-03-16 17:02:32'),
(688, 70, 42, 'conf_opinion', 41, -1, '2014-03-16 17:03:49'),
(689, 70, 42, 'conf_opinion', 40, 1, '2014-03-16 17:04:11'),
(690, 70, 42, 'conf_opinion', 26, 1, '2014-03-16 17:06:36'),
(691, 70, 42, 'conf_opinion', 25, -1, '2014-03-16 17:06:39'),
(692, 42, 70, 'conf_opinion', 69, 1, '2014-03-16 17:07:37'),
(693, 178, 42, 'conf_opinion', 65, 1, '2014-03-16 17:14:12'),
(694, 42, 178, 'conf_opinion', 69, 1, '2014-03-17 16:39:34'),
(695, 70, 178, 'conf_opinion', 72, -1, '2014-03-17 17:07:37'),
(696, 70, 178, 'conf_opinion', 76, -1, '2014-03-22 13:40:54'),
(697, 178, 70, 'conf_opinion', 77, -1, '2014-03-22 19:54:13'),
(698, 178, 70, 'conf_opinion', 80, 1, '2014-03-22 19:56:19'),
(699, 70, 178, 'conf_opinion', 79, 1, '2014-03-22 19:56:37'),
(700, 178, 133, 'conf_opinion', 81, 1, '2014-03-22 19:57:52'),
(701, 70, 178, 'conf_opinion', 83, 1, '2014-03-23 10:31:10'),
(702, 178, 70, 'conf_opinion', 84, -1, '2014-03-23 10:31:17'),
(703, 178, 19, 'conf_opinion', 84, 1, '2014-03-23 10:36:41'),
(704, 70, 19, 'conf_opinion', 83, 1, '2014-03-23 10:36:46'),
(705, 19, 70, 'conf_opinion', 85, 1, '2014-03-23 10:37:43'),
(706, 19, 178, 'conf_opinion', 85, 1, '2014-03-23 10:37:59'),
(707, 70, 178, 'conf_opinion', 88, 1, '2014-03-30 19:30:42'),
(708, 178, 70, 'conf_opinion', 89, 1, '2014-03-30 19:34:07'),
(709, 70, 178, 'conf_opinion', 90, -1, '2014-03-30 19:34:09'),
(710, 178, 70, 'conf_opinion', 39, 1, '2014-04-06 21:09:04'),
(711, 178, 70, 'conf_opinion', 81, 1, '2014-04-08 10:03:43'),
(712, 19, 178, 'conf_opinion', 96, 1, '2014-04-08 10:32:15'),
(713, 19, 70, 'conf_opinion', 96, 1, '2014-04-08 10:32:18'),
(714, 70, 178, 'conf_opinion', 95, -1, '2014-04-08 18:11:55'),
(717, 178, 70, 'note', 185, 1, '2014-04-11 11:19:32'),
(718, 178, -1, 'interest', 22, 1, '2014-04-11 11:19:32'),
(719, 178, 70, 'interest', 22, 1, '2014-04-11 11:19:40'),
(720, 70, 178, 'interest', 24, 1, '2014-04-11 16:39:42'),
(727, 70, 178, 'conf_opinion', 97, 1, '2014-04-11 22:12:49'),
(729, 70, 178, 'conf_opinion', 98, 1, '2014-04-11 22:14:03'),
(731, 185, 70, 'page', 0, 1, '2014-04-12 09:24:11'),
(732, 185, 70, 'interest', 54, 1, '2014-04-12 09:25:00'),
(733, 185, 70, 'interest', 37, 1, '2014-04-12 09:25:04'),
(734, 70, 178, 'page', 0, 1, '2014-04-12 11:47:36'),
(735, 70, 178, 'interest', 20, 1, '2014-04-12 13:13:17'),
(738, 70, 19, 'conf_opinion', 98, 1, '2014-04-12 18:31:00'),
(740, 70, 42, 'conf_opinion', 98, -1, '2014-04-12 18:32:28'),
(741, 70, 42, 'conf_opinion', 93, 1, '2014-04-12 18:32:52'),
(742, 185, 70, 'interest', 29, 1, '2014-04-13 18:28:01'),
(744, 92, 70, 'page', 0, 1, '2014-04-16 19:07:17'),
(746, 19, -1, 'interest', 29, 1, '2014-04-22 18:59:46'),
(747, 178, 70, 'page', 0, 1, '2014-05-09 19:11:30'),
(748, 70, 19, 'conf_opinion', 108, 1, '2014-05-11 11:14:16'),
(750, 178, -1, 'interest', 15, 1, '2014-05-28 16:51:09'),
(751, 19, 70, 'conf_opinion', 109, 1, '2014-05-28 17:06:54'),
(756, 178, 0, 'dev_thank', 37, 1, '2014-06-02 21:49:14'),
(757, 70, 0, 'dev_thank', 37, 1, '2014-06-03 10:15:46'),
(758, 70, 192, 'interest', 37, 1, '2014-06-05 19:58:29'),
(759, 70, 192, 'interest', 34, 1, '2014-06-05 19:58:35'),
(762, 19, 0, 'dev_thank', 52, 1, '2014-06-11 10:55:36'),
(763, 70, 0, 'dev_thank', 52, 1, '2014-06-19 10:10:07'),
(764, 19, 185, 'conf_opinion', 109, 1, '2014-06-22 10:15:44'),
(765, 70, 19, 'conf_opinion', 110, 1, '2014-06-25 10:27:32'),
(766, 90, 19, 'page', 0, 1, '2014-06-26 10:01:42'),
(767, 70, 19, 'interest', 19, 1, '2014-06-26 10:07:10'),
(768, 70, 19, 'interest', 15, 1, '2014-06-26 10:08:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
