<?php include '../classes/sysinfo.php'; ?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>qMex Account Confirmation</title>
</head>

<body>

<div id="letter" style="padding:0; margin:0; background-color:#FFF; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
<div id="header" style="background-color:#069; padding:15px 0px 15px 5%">
<table style="width:90%"><tr>
<td><img src="<?php echo SYSINFO::SITE ?>/qmex_img/qmex.png" height="50px" style="float:left"></td>
<td><div id="confirmation-text" style="color:#FFF; font-size:1.3em; font-weight:bold">qID Account Confirmation</div></td>
</tr></table>
</div>
<div id="content" style="padding:20px; font-size:1.2em">
Для подтверждения действительности своего qID перейдите, пожалуйста, по ссылке :<br>
<a href="[HREF]" style="color:#039; text-decoration:underline">[HREF]</a>
</div>
</div>

</body>
</html>
